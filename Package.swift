// swift-tools-version:5.3
import PackageDescription

let package = Package(
    name: "cpplib",
    platforms: [.iOS("9.0")],//minimum support platform version
    products: [
        // Products define the executables and libraries produced by a package, and make them visible to other packages.
        .library(
            name: "cpplib",
            targets: ["cpplib"]),
    ],
    dependencies: [], 
    targets: [
        .target(
            name: "cpplib",
            dependencies: [],
            exclude: [// relative path from target's root(cpplib/Sources/cpplib)
                "json/smoketest",
                "json/doc",
                "readstream/test",
                "pugixml/docs",
                "pugixml/smoketest",
                "pugixml/tests",
                "dash/test",
                "dnld/test",
                "aa",
                "product/nuvyyo/tuner/test"
            ],
            cxxSettings: [
                //.headerSearchPath("openssl/include", .when(platforms: [.iOS])), // relative path from target's root(cpplib/Sources/cpplib)
                .headerSearchPath("acba"),
                .headerSearchPath("pugixml/src"),
                .headerSearchPath("dash"),
                .headerSearchPath("boost"),
                .headerSearchPath("dnld"),
                .headerSearchPath("atsc3/web"),
                .headerSearchPath("atsc3/model"),
                .headerSearchPath("atsc3/task"),
                .headerSearchPath("atsc3/platform"),
                .headerSearchPath("atsc3/unifiedtv"),
                .headerSearchPath("readstream"),
                .headerSearchPath("json/include"),
                .headerSearchPath("json/acba"),
                .headerSearchPath("unifiedtv"),
                .headerSearchPath("atsc1/model"),
                .headerSearchPath("atsc1/task"),
                .headerSearchPath("atsc1/platform"),
                .headerSearchPath("atsc1/unifiedtv"),
                .headerSearchPath("product/nuvyyo/tuner"),
                .headerSearchPath("tuner"),

            ]/*,
            linkerSettings: [
                .unsafeFlags(["-l/Users/turboj/Desktop/workspace/other_repositories/cpplib/openssl/lib/libcrypto.a"])
            ]*/
        )
    ],
    cxxLanguageStandard: .cxx11
)
