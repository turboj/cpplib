#include <sys/stat.h>
#include "atsc1_model.h"
#include "atsc1_task.h"
#include "atsc1_platform.h"
#include "acba.h"

using namespace acba;

static Log l("init1");

bool atsc1::init (int &argc, char **&argv, atsc1_Platform *p)
{
    l.info("Wiring components...");
    mkdir(model.storageRoot.c_str(), 0755);
    task.platform = p;
    task.tuneManager.init();
    model.currentServiceUpdate.addListener(&task.serviceManager.selector);

    l.info("Setting web server...");
    task.httpServer.start(8081, &task.eventDriver);
    task.httpServer.bind("/ts/", &task.tsStreamer);
/*
    model.knownStreamLocationsUpdate.addListener(&task.tuneManager.discovery);
    model.currentServiceUpdate.addListener(&task.applicationManager.selection);
    model.currentServiceUpdate.addListener(&task.serviceManager.selector);
    model.currentServiceUpdate.addListener(&task.serviceManager.alertFlusher);
    model.currentServiceUpdate.addListener(
            &task.ratingHandler.serviceChangeListener);
    model.esgFragmentUpdate.addListener(
            &task.ratingHandler.esgUpdateListener);
    model.config.update.addListener(&model.preference);
    model.preference.update.addListener(
            &task.ratingHandler.preferenceChangeListener);
    model.backgroundServicesUpdate.addListener(&task.serviceManager.selector);
    model.serviceUpdate.addListener(&task.serviceManager.selector);
    task.rmp.rmpPeriodChanged.addListener(&task.ratingHandler.rmpPeriodChangeListener);
    model.preference.update.addListener(&task.rmp.ccPrefUpdateListener);

    task.eventDriver.setTimeout(0, &task.serviceManager.collector);
    task.tuneManager.join(0, ip2u("224.0.23.60"), 4937,
            &task.serviceManager.llsReader);

    l.info("Setting web server...");
    web.httpServer.start(8080, &task.eventDriver);
    web.rpcManager.init();
    web.httpServer.bind("/atscCmd", &web.rpcManager);
#if ALLOW_WEBSOCKET_WITHOUT_ATSCMD
    web.httpServer.bind("", &web.rpcManager);
#endif
    web.httpServer.bind("/file", &web.fileServer);
    web.httpServer.bind("/dash", &web.dashStreamer);
    web.httpServer.bind("/mpu", &web.mpuStreamer); // fix path
    web.httpServer.bind("/app", &web.appServer);

    web.httpServer.bind(CD_WEB_CONTEXT_ROOT, &web.cdServer);
    web.httpServer.bind(ATSC_CD, &web.cdServer);
    web.httpServer.bind(CD_REMOTE_WS_ENDPOINT, &web.cdServer);
    web.httpServer.bind(CD_LOCAL_WS_ENDPOINT, &web.cdServer);

    l.info("ApplicationManager.init()");
    task.applicationManager.init();
    task.downloadManager.start();

    CdManager::getInstance()->start();
*/
    l.info("done");
    return true;
}
