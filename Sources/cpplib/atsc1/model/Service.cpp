#include "atsc1_model.h"
#include "acba.h"

using namespace acba;
using namespace atsc1;

static Log l("svc1");

void Service::dump ()
{
    l.info("Service name[%s] freq:%d progNum:%d major:%d minor:%d",
            name.c_str(), freq, progNum, majorNum, minorNum);
// TODO : Dump PMT Info
}
