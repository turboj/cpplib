#include "atsc1_model.h"
#include "acba.h"

using namespace atsc1;
using namespace acba;

atsc1::Model atsc1::model;

static Log l("model1");

static const char *bar = "================================================================================\n";

void Model::dump ()
{
    l.info("");
    l.info("%s", bar);
    l.info("== DUMP KNOWN STREAMS");
    l.info("%s", bar);
    for (auto loc: knownStreamLocations) {
        l.info("Stream %s", loc.c_str());
    }

    l.info("");
    l.info("%s", bar);
    l.info("== DUMP TRANSPORT STREAMS");
    l.info("%s", bar);
    for (auto ts: transportStreamByLocation) {
        ts.second->dump();
    }

    l.info("");
    l.info("%s", bar);
    l.info("== ETc.");
    l.info("%s", bar);
    if (currentService) {
        l.info("Currently Selected Service:%d/%d(%d.%d) %s",
                currentService->freq, currentService->progNum,
                currentService->majorNum, currentService->minorNum,
                currentService->name.c_str());
    } else {
        l.info("Currently no selected service");
    }
}

bool Model::selectService (Service *srv)
{
    if (model.currentService.get() == srv) return true;

    if (srv) {
        // TODO: check validity of the given service
        l.info("Select service %s (%d/%d)", srv->name.c_str(),
                srv->freq, srv->progNum);
    } else {
        l.info("Unselect service %s (%d/%d)",
                model.currentService->name.c_str(),
                model.currentService->freq,
                model.currentService->progNum);
    }
    model.currentService = srv;
    model.currentServiceUpdate.notify();
    return true;
}
