#ifndef ATSC1_MODEL_H
#define ATSC1_MODEL_H

#include <vector>
#include <map>
#include <set>
#include <sstream>
#include <algorithm>

#include "acba.h"
#include "atsc1_si_type.h"

namespace atsc1 {

struct ESInfo: acba::RefCounter<ESInfo> {
    ESInfo (uint8_t t, uint16_t p): streamType(t), pid(p) {}

    uint8_t streamType;
    uint16_t pid;
};

struct ServiceMediaInfo: acba::RefCounter<ServiceMediaInfo> {
    ServiceMediaInfo (uint16_t pmt, uint16_t pcr): pmtPid(pmt), pcrPid(pcr),
            rawPmt(nullptr) {}
    ~ServiceMediaInfo () {
        if (rawPmt) {
            free(rawPmt);
            rawPmt = nullptr;
        }
    }

    uint16_t pmtPid;
    uint16_t pcrPid;
    std::vector<acba::Ref<ESInfo>> eleStreams;
    uint8_t version;
    int crc32;
    byte_t *rawPmt;
};

struct VCTInfo: acba::RefCounter<VCTInfo> {
    VCTInfo (uint8_t v, int t): version(v), tsid(t) {}

    uint8_t version;
    uint16_t tsid;
    acba::Buffer rawVct;
};

struct RatingDimension: acba::RefCounter<RatingDimension> {

    std::string dimName;
    std::map<uint8_t, std::string> abbrRatingValues;
    std::map<uint8_t, std::string> ratingValues;
};

struct RatingInfo: acba::RefCounter<RatingInfo> {
    RatingInfo (uint8_t v, uint8_t r): version(v), region(r) {}

    uint8_t version;
    uint8_t region;
    std::string regionName;
    std::map<uint8_t, acba::Ref<RatingDimension>> dimensions;
};

struct Service: acba::RefCounter<Service> {

    Service (std::string loc, uint16_t p, uint16_t s): tsLoc(loc),
            progNum(p), sourceId(s) {
        std::stringstream ss;
        ss << tsLoc << "/" << p;
        srvLoc = ss.str();
        std::replace(srvLoc.begin(), srvLoc.end(), ':', '_');
    }

    void dump ();

    std::string tsLoc;
    int freq;
    uint16_t progNum;
    uint16_t sourceId;
    uint16_t tsid;
    int majorNum;
    int minorNum;
    std::string name;
    std::string srvLoc;
    acba::Ref<ServiceMediaInfo> mediaInfo;

    acba::Update<const acba::Ref<Service>&> mediaInfoUpdate;
};

struct TransportStream: acba::RefCounter<TransportStream> {

    TransportStream (std::string loc): location(loc) {}

    void dump ();

    int freq;
    uint16_t tsid;
    std::string location;

    std::map<int, acba::Ref<Service> > services; // progNum -> Service
    acba::Ref<VCTInfo> vctInfo;
    std::map<uint8_t, acba::Ref<RatingInfo>> ratingInfos;
    std::map<uint16_t, std::map<uint16_t, acba::Ref<SITable>>> eits; // idx -> (sourceId -> SITable(eit))
    std::map<uint16_t, std::map<uint32_t, acba::Ref<SITable>>> etts; // idx -> (ETM_id -> SITable(ett))
};

struct Model {

    Model (): storageRoot("./atsc1") {}

    ////////////////////////////////////////////////////////////////////////////
    // Network Management

    void addKnownStreamLocation (const std::string &s) {
        knownStreamLocations.push_back(s);
        knownStreamLocationsUpdate.notify();
    }

    std::vector<std::string> knownStreamLocations;
    acba::Update<> knownStreamLocationsUpdate;

    ////////////////////////////////////////////////////////////////////////////
    // Service Management

    std::map<int, acba::Ref<TransportStream> > transportStreams;
    std::map<const std::string, acba::Ref<TransportStream> >
            transportStreamByLocation;
    acba::Update<const acba::Ref<TransportStream>&> transportStreamUpdate;

    bool selectService (Service *srv);
    acba::Ref<Service> currentService;
    acba::Update<> currentServiceUpdate;

    // EIT(EPG program event informaton) update
    acba::Update<const acba::Ref<TransportStream>&> eitUpdate;

    // Etc.

    std::string storageRoot;
    //Config config; // Do not access here for accessing preference
    //Preference preference;

    void dump ();
};

extern Model model;

}
#endif
