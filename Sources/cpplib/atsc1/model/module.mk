$(call begin-target, atsc1_model, staticlib)
    LOCAL_SRCS := $(wildcard $(LOCAL_PATH)/*.cpp)
    LOCAL_IMPORTS := readstream-staticlib pugixml-staticlib

    EXPORT_CXXFLAGS := -I$(LOCAL_PATH)
    EXPORT_IMPORTS := $(LOCAL_IMPORTS)
$(end-target)
