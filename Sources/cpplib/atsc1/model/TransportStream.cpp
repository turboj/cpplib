#include <sstream>
#include "atsc1_model.h"
#include "acba.h"

using namespace atsc1;
using namespace acba;

static Log l("ts1");

void TransportStream::dump ()
{
    l.info("Transport Stream freq:%d tsid:%d num_svcs:%d", freq, tsid,
            services.size());
    for (auto svc: services) svc.second->dump();
}
