#ifndef ATSC1_SI_TYPE_H
#define ATSC1_SI_TYPE_H

#include <map>

#include "acba.h"

namespace atsc1 {

enum SI_TAB_TYPE {
    SI_TAB_PAT,
    SI_TAB_PMT,
    SI_TAB_MGT,
    SI_TAB_VCT,
    SI_TAB_RRT,
    SI_TAB_EIT,
    SI_TAB_ETT
};

enum SI_TAB_TID {
    SI_PAT_TID = 0x00,
    SI_PMT_TID = 0x02,
    SI_MGT_TID = 0xC7,
    SI_VCT_TID = 0xC8,
    SI_RRT_TID = 0xCA,
    SI_EIT_TID = 0xCB,
    SI_ETT_TID = 0xCC,
};

struct SISection: acba::RefCounter<SISection> {
    SISection (): data(nullptr) {}
    ~SISection () {
        if (data) {
            free(data);
            data = nullptr;
        }
    }
    uint8_t secNum;
    int crc32;
    int dataLen;
    byte_t *data;
};

struct SITable: acba::RefCounter<SITable> {

    SITable (SI_TAB_TYPE _type, uint16_t _pid): type(_type), pid(_pid),
            tabExt(0) {}

    SI_TAB_TYPE type;
    uint16_t pid;
    uint16_t tabExt;
    uint8_t version;
    uint8_t lsn;
    std::map<uint8_t, acba::Ref<SISection>> sections;
};

}
#endif
