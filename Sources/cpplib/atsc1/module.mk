# Pseudo target including all atsc1 modules
$(call begin-target, atsc1, staticlib)
    LOCAL_SRCS := $(wildcard $(LOCAL_PATH)/*.cpp)
    LOCAL_IMPORTS := atsc1_model-staticlib atsc1_task-staticlib
    LOCAL_IMPORTS += atsc1_platform_interface
    EXPORT_IMPORTS := $(LOCAL_IMPORTS)
$(end-target)
