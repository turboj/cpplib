#ifndef ATSC1_PLATFORM_H
#define ATSC1_PLATFORM_H

#ifdef __cplusplus
extern "C" {
#endif

typedef struct atsc1_Platform atsc1_Platform;
typedef struct atsc1_Tuner atsc1_Tuner;
typedef struct atsc1_TunerStatus atsc1_TunerStatus;

enum atsc1_CALLBACK_TYPE {
    atsc1_CALLBACK_END, // notify that callback will be no longer called.
    atsc1_CALLBACK_TUNER_LOCKED,
    atsc1_CALLBACK_TUNER_UNLOCKED,
    atsc1_CALLBACK_TS,
};

enum atsc1_MediaMode_Mask {
    atsc1_MMODE_VIDEO = 0x01,
    atsc1_MMODE_AUDIO = 0x10,
};

struct atsc1_TuneParams {
    int frequency;
    void (*callback) (atsc1_CALLBACK_TYPE type, const byte_t *data, int sz,
            void *ctx);
    void *context;
};

struct atsc1_Platform {

    /**
     * Each tuner has its dedicated thread which calls callbacks.
     * Multiple tuner may share a single thread.
     * but single tuner cannot span to multiple threads.
     * tuners: [out] Array of atsc1_Tuner to store
     * nTuners: [in/out] Give size of @arg tuners and then get number of tuners
     *          set.
     */
    void (*getTuners) (atsc1_Tuner **tuners, int *nTuners);
    void (*getTunerStatus) (atsc1_Tuner *tuner, atsc1_TunerStatus *st);
    bool (*startTune) (atsc1_Tuner *tuner, atsc1_TuneParams *tuneParams);
    void (*stopTune) (atsc1_Tuner *tuner);

    /**
     * Media Control
     */
    void (*rmpStart) (const char *url);
    void (*rmpStop) ();
    void (*playStream) (int mode, uint16_t pcrPID,
            uint16_t videoPID, uint8_t vType,
            uint16_t audioPID, uint8_t aType);
    void (*stopStream) (int mode);

    /**
     * MPEG Section or Table Handling
     */

};

#ifdef __cplusplus
}
#endif
#endif
