LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)
LOCAL_MODULE := atsc1
LOCAL_SRC_FILES += \
        $(wildcard $(LOCAL_PATH)/*.cpp) \
        $(wildcard $(LOCAL_PATH)/model/*.cpp) \
        $(wildcard $(LOCAL_PATH)/task/*.cpp) \

LOCAL_C_INCLUDES += \
        $(LOCAL_PATH)/model \
        $(LOCAL_PATH)/task \
        $(LOCAL_PATH)/platform \
        $(LOCAL_PATH)/../json/single_include \
        $(LOCAL_PATH)/../json/acba \
        $(LOCAL_PATH)/../boost
LOCAL_EXPORT_C_INCLUDES := $(LOCAL_C_INCLUDES)
LOCAL_STATIC_LIBRARIES := acba readstream
LOCAL_EXPORT_LDLIBS := -lz

ifeq ($(USE_TUNER_SERVER),true)
    LOCAL_STATIC_LIBRARIES += tuner_server
endif
ifeq ($(USE_TUNER_CLIENT),true)
    LOCAL_STATIC_LIBRARIES += tuner_client
endif

include $(BUILD_STATIC_LIBRARY)
