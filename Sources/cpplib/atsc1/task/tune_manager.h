#ifndef TUNE_MANAGER_1_H
#define TUNE_MANAGER_1_H

#include "stream_reader.h"
#include "si_manager.h"
#include "atsc1_model.h"
#include "atsc1_platform.h"
#include "acba.h"
#include "EventDriver.h"
#include "ts_reader.h"

namespace atsc1 {

struct Tune: acba::RefCounter<Tune> {

    Tune (const std::string &loc): location(loc),locked(false),
            mutex(PTHREAD_MUTEX_INITIALIZER) {}

    std::string location;

    enum EVENT {
        EVENT_TUNER_LOCKED,
        EVENT_TUNER_UNLOCKED,
        EVENT_TS_LIST_CHANGED
    };

    virtual bool start () = 0;
    virtual void stop () = 0;
    virtual void startSF (uint16_t pid) = 0;
    virtual void cancelSF (uint16_t pid) = 0;
    virtual void startStreaming (uint16_t pid, const StreamReaderRef &sr) = 0;
    virtual void stopStreaming (uint16_t pid, const StreamReaderRef &sr) = 0;

    bool isLocked () {
        acba::ScopedLock sl(mutex);
        return locked;
    }

    void addListener (acba::Listener<EVENT> *l);
    void removeListener (acba::Listener<EVENT> *l);

protected:
    pthread_mutex_t mutex;
    acba::Update<EVENT> update;
    bool locked;
    void notify (EVENT e);
};

struct TuneProvider: acba::RefCounter<TuneProvider> {

    TuneProvider (const std::string &t): type(t) {}

    std::string type;

    virtual Tune *create (const std::string &loc, const acba::Ref<SICollector> &collector) = 0;
    virtual void destroy(const acba::Ref<Tune> &t) = 0;
};

struct TuneContext: ChainNode<TuneContext> {
    Tune *tune;
};

struct TuneManager {

    TuneManager () {}

    std::vector<acba::Ref<TuneProvider> > tuneProviders;

    struct TuneHolder {
        TuneHolder (): useCount(0) {}
        acba::Ref<Tune> tune;
        acba::Ref<TuneProvider> provider;
        acba::Ref<SICollector> collector;
        int useCount;
    };

    std::map<const std::string, TuneHolder> tunes;

    void init ();
    void registerTuneProvider (const acba::Ref<TuneProvider> &prov);

    Tune *requestTune (const std::string &loc);
    void releaseTune (const acba::Ref<Tune> &t);
};

}
#endif
