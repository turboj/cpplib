$(call begin-target, atsc1_task, staticlib)
    LOCAL_SRCS := $(wildcard $(LOCAL_PATH)/*.cpp)
    LOCAL_IMPORTS := atsc1_model-staticlib atsc1_platform_interface
    ifeq ($(USE_TUNER_SERVER),true)
        LOCAL_IMPORTS += tuner_server-staticlib
    endif
    ifeq ($(USE_TUNER_CLIENT),true)
        LOCAL_IMPORTS += tuner_client-staticlib
    endif

    EXPORT_CXXFLAGS := -I$(LOCAL_PATH)
    EXPORT_IMPORTS := $(LOCAL_IMPORTS)
    EXPORT_LDFLAGS := -lz
$(end-target)
