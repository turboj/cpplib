#if USE_TUNER_CLIENT

#include <string>

#include "ts_reader.h"
#include "section_reader.h"
#include "tune_manager.h"
#include "atsc1_task.h"
#include "EventDriver.h"
#include "acba.h"

#include "tuner.h"
#include "tuner_client.h"

using namespace std;
using namespace acba;
using namespace atsc1;
using namespace tuner;

static Log l("vatsc1tune");

struct VirtualAtsc1Tune: Tune {

    TuneContext tuneContext;
    TsReader tsReader;
    StreamReaderRef sectReader;
    acba::Ref<SICollector> collector;

    Ref<TuneProxy> proxy;

    Log l;

    VirtualAtsc1Tune (const string &loc, const Ref<SICollector> &col):
            Tune(loc), l("atsc1tune", loc+" "), collector(col) {
        sectReader = new SectionReader(col.get());
        tuneContext.tune = this;
    }

    bool start () {
        using namespace placeholders;
        proxy = client().tune(location, bind(&VirtualAtsc1Tune::onTunerCallback,
                this, _1, _2, _3));
        if (!proxy) return false;

        ref(); // corresponding deref() occurrs on CALLBACK_END
        collector->start(this);
        return true;
    }

    void stop () {
        proxy->dispose();
        proxy = nullptr;
    }

    void startSF (uint16_t pid)
    {
        tsReader.addPidStreamReader(pid, sectReader);
    }

    void cancelSF (uint16_t pid)
    {
        tsReader.removePidStreamReader(pid, sectReader);
    }

    void startStreaming (uint16_t pid, const StreamReaderRef &sr)
    {
        l.info("startStreaming pid=%d", pid);
        tsReader.addPidStreamReader(pid, sr);
    }

    void stopStreaming (uint16_t pid, const StreamReaderRef &sr)
    {
        l.info("stopStreaming pid=%d", pid);
        tsReader.removePidStreamReader(pid, sr);
    }

    void onTunerCallback (CALLBACK_TYPE type, const byte_t *data, int sz) {
        l.trace("onTunerCallback %d", type);

        switch (type) {
        case CALLBACK_TUNER_LOCKED:
            {
                ScopedLock sl(mutex);
                if (locked) break;
                locked = true;
                update.notify(EVENT_TUNER_LOCKED);
            }
            break;

        case CALLBACK_TUNER_UNLOCKED:
            {
                ScopedLock sl(mutex);
                if (!locked) break;
                locked = false;
                update.notify(EVENT_TUNER_UNLOCKED);
            }
            break;

        case CALLBACK_TS:
            while (sz > 0) {
                int rsz = tsReader.read(data, sz, &tuneContext);
                if (rsz == 0) break;
                data += rsz;
                sz -= rsz;
            }
            break;

        case CALLBACK_END:
            l.warn("callback end.");
            deref();
            break;

        default:
            l.warn("Unsupported tuner callback %d", type);
            break;
        }
    }
};

struct VirtualAtsc1TuneProvider: TuneProvider {

    VirtualAtsc1TuneProvider (): TuneProvider("atsc1") {}

    Tune *create (const string &loc, const Ref<SICollector> &col)
            override {
        auto vat = new VirtualAtsc1Tune(loc, col);
        if (!vat->start()) {
            delete vat;
            return nullptr;
        }

        return vat;
    }

    void destroy (const acba::Ref<Tune> &t) override {
        auto at = static_cast<VirtualAtsc1Tune*>(t.get());
        at->stop();
    }
};

void VirtualAtsc1TuneProvider_init ()
{
    task.tuneManager.registerTuneProvider(new VirtualAtsc1TuneProvider());
}

#endif
