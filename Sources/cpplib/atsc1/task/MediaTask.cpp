#include "service_manager.h"
#include "atsc1_task.h"
#include "acba.h"

using namespace std;
using namespace atsc1;
using namespace acba;

static Log l("media1");

static bool isVideo (uint8_t type)
{
   switch (type) {
   case 0x01: // ISO/IEC 11172-2 Video
   case 0x02: // ITU-T Rec. H.262 | ISO/IEC 13818-2 Video
              // or ISO/IEC 11172-2 constrained parameter video stream
   case 0x80: // for SCTE
   case 0x1B: // AVC video stream as defined in ITU-T Rec. H.264
              // | ISO/IEC 14496-10 Video
   case 0x24: // H.265
   case 0x10: // ISO/IEC 14496-2 MPEG-4 Video Part2/H.263
       return true;
   }
   return false;
}

static bool isAudio (uint8_t type)
{
   switch (type) {
   case 0x03: // ISO/IEC 11172-3 Audio
   case 0x04: // ISO/IEC 13818-3 Audio
   case 0x81: // Dolby
   case 0x87: // Dolby Plus
   case 0x11: // ISO/IEC 14496-3 Audio - MPEG-4 Audio
   case 0x0F: // ISO/IEC 13818-7 Audio - MPEG-2 AAC
       return true;
   }
   return false;
}

void MediaTask::start ()
{
    l.info("start");
    // pump stream
    tsStreamReader = task.tsStreamer.createStreamReader(service->srvLoc);
    if (!tsStreamReader) {
        l.error("start failed : createStreamReader returned nullptr");
        return;
    }

    acba::Ref<ServiceMediaInfo> minfo = service->mediaInfo;
    if (!minfo) {
        l.error("start failed : mediaInfo not available");
        return;
    }

    uint16_t vid, aid;
    uint8_t vtype, atype;
    int mode = 0;
    vid = aid = 0;
    reqPids = {0, minfo->pmtPid, minfo->pcrPid};
    for (auto e: minfo->eleStreams) {
        reqPids.insert(e->pid);
        if ( vid == 0 && isVideo(e->streamType) ) {
            vid = e->pid;
            vtype = e->streamType;
            mode |= atsc1_MMODE_VIDEO;
        } else if ( aid == 0 && isAudio(e->streamType) ) {
            aid = e->pid;
            atype = e->streamType;
            mode |= atsc1_MMODE_AUDIO;
        }
    }
    for (auto p: reqPids) {
        tune->startStreaming(p, tsStreamReader);
    }
    
    char url[256];
    sprintf(url, "ts://localhost:8081/ts/%s", service->srvLoc.c_str());
    task.platform->rmpStart(url);
    task.platform->playStream(mode, minfo->pcrPid, vid, vtype, aid, atype);
}

void MediaTask::stop ()
{
    l.info("stop");
    if (!tsStreamReader) {
        l.info("nothing to stop");
        return;
    }
    task.platform->stopStream(atsc1_MMODE_AUDIO | atsc1_MMODE_VIDEO);
    task.platform->rmpStop();
    for (auto p: reqPids) {
        tune->stopStreaming(p, tsStreamReader);
    }
    reqPids.clear();
    task.tsStreamer.releaseStreamReader(service->srvLoc);
    tsStreamReader = 0;
}

void MediaTask::update ()
{
    l.info("update");
    if (!tsStreamReader) {
        l.error("update failed : tsStreamReader is nullptr");
        return;
    }

    acba::Ref<ServiceMediaInfo> minfo = service->mediaInfo;
    if (!minfo) {
        l.error("update failed : mediaInfo not available");
        return;
    }

    uint16_t vid, aid;
    uint8_t vtype, atype;
    int mode = 0;
    vid = aid = 0;
    std::set<uint16_t> newPids = {0, minfo->pmtPid, minfo->pcrPid};
    for (auto e: minfo->eleStreams) {
        newPids.insert(e->pid);
        if ( vid == 0 && isVideo(e->streamType) ) {
            vid = e->pid;
            vtype = e->streamType;
            mode |= atsc1_MMODE_VIDEO;
        } else if ( aid == 0 && isAudio(e->streamType) ) {
            aid = e->pid;
            atype = e->streamType;
            mode |= atsc1_MMODE_AUDIO;
        }
    }
    for (auto p: reqPids) {
        if (newPids.find(p) == newPids.end()) {
            tune->stopStreaming(p, tsStreamReader);
        }
    }
    for (auto p: newPids) {
        if (reqPids.find(p) == reqPids.end()) {
            tune->startStreaming(p, tsStreamReader);
        }
    }
    reqPids.swap(newPids);
    task.platform->playStream(mode, minfo->pcrPid, vid, vtype, aid, atype);
}
