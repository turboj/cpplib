#ifndef ATSC1_MULTIPLE_STRING_H
#define ATSC1_MULTIPLE_STRING_H

#include <vector>

#include "acba.h"

namespace atsc1 {

struct MultipleString {
    MultipleString (const byte_t *text, int sz) {
        decode(text, sz);
    }

    std::map<std::string, std::string> decoded;

private:
    void decode (const byte_t *text, int sz);
};

}
#endif
