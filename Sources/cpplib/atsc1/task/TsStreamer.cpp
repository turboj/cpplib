#include <set>
#include <string>
#include <sstream>
#include <pthread.h>

#include "acba.h"
#include "atsc1_task.h"
#include "ts_streamer.h"

#define OUT_BUFFER_LIMIT (8*1024*1024)

using namespace std;
using namespace acba;
using namespace atsc1;

static Log l("ts1");

namespace {

struct TsStreamReader: RefCountedStreamReader, Reactor {

    TsStreamReader (): mutex(PTHREAD_MUTEX_INITIALIZER) {}

    pthread_mutex_t mutex;
    Buffer buffer;
    set<HttpReactor*> webSockets;

    void processEvent (int events) override {
        ScopedLock sl(mutex);
        if (buffer.size() == 0) return;
        for (auto w: webSockets) {
            if (w->outBuffer.size() > OUT_BUFFER_LIMIT) {
                l.warn("TsStreamer buffer full. sz:%d", w->outBuffer.size());
                continue;
            }

            w->sendFrame(buffer.begin(), buffer.size());
        }
        buffer.clear();
    }

    int read (const byte_t *data, int sz, Chain *ctx) override {
        ScopedLock sl(mutex);

        buffer.writeBytes(data, sz);

        task.eventDriver.setTimeout(0, this);

        return sz;
    }

    void dispose () {
        task.eventDriver.unsetTimeout(this);

        ScopedLock sl(mutex);
        for (auto w: webSockets) w->abort();
    }
};

static map<string, Ref<TsStreamReader>> tsStreamReaders;

struct Connection: HttpRequestHandler {

    void handleWebsocketOpen (HttpReactor &r) override {
        l.info("open %p %s", &r, r.requestHandlerPath.c_str());

        auto &path = r.requestHandlerPath;
        auto tsri = tsStreamReaders.find(path);
        if (tsri == tsStreamReaders.end()) {
            r.abort("Invalid path");
            return;
        }

        tsStreamReader = tsri->second;

        ScopedLock sl(tsStreamReader->mutex);
        tsStreamReader->webSockets.insert(&r);
    }

    void handleWebsocketTextMessage (HttpReactor &r) override {
        l.warn("got text message");
    }

    void handleWebsocketMessage (HttpReactor &r) override {
        l.warn("got binary message");
    }

    void handleWebsocketClose (HttpReactor &r) override {
        l.info("close %p", &r);

        if (!tsStreamReader) return;

        ScopedLock sl(tsStreamReader->mutex);
        tsStreamReader->webSockets.erase(&r);
        tsStreamReader = nullptr;
    }

    Ref<TsStreamReader> tsStreamReader;
};

} // namespace

RefCountedStreamReader *TsStreamer::createStreamReader (const string &path)
{
    auto &tsr = tsStreamReaders[path];
    if (tsr) return nullptr; // already exists

    l.info("Created websocket stream %s", path.c_str());
    tsr = new TsStreamReader();
    return tsr.get();
}

void TsStreamer::releaseStreamReader (const string &path)
{
    auto tsri = tsStreamReaders.find(path);
    if (tsri == tsStreamReaders.end()) return;

    l.info("Removed websocket stream %s", path.c_str());

    tsri->second->dispose();
    tsStreamReaders.erase(tsri);
}

HttpRequestHandler *TsStreamer::acceptHttpRequest (const string &basePath,
        HttpReactor *r)
{
    if (!r->websocketVersion) return 0;
    return new Connection();
}
