#ifndef ATSC1_TASK_H
#define ATSC1_TASK_H

#include "acba.h"
#include "tune_manager.h"
#include "service_manager.h"
#include "atsc1_platform.h"
#include "EventDriver.h"
#include "http_server.h"
#include "ts_streamer.h"

namespace atsc1 {

bool init (int &argc, char **&argv, atsc1_Platform *p);

struct Task {
    TuneManager tuneManager;
    ServiceManager serviceManager;
    acba::EventDriver eventDriver;
    acba::HttpServerReactor httpServer;
    TsStreamer tsStreamer;
    atsc1_Platform *platform;
};
extern Task task;

}
#endif
