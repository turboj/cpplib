#include <pthread.h>
#include <fcntl.h>

#include "ts_reader.h"
#include "tssync_reader.h"
#include "section_reader.h"
#include "tune_manager.h"
#include "atsc1_task.h"
#include "EventDriver.h"
#include "acba.h"

using namespace std;
using namespace acba;
using namespace atsc1;

static Log l("tsfiletune");

struct TsFileTune: Tune {

    TsFileTune (const string &loc, const Ref<SICollector> &col): Tune(loc),
            l("tsfile", loc+" "), tsSyncReader(&tsReader),
            stopped(false), collector(col) {
        sectReader = new SectionReader(col.get());
        tuneContext.tune = this;
    }

    static void *run0 (void *arg) {
        auto self = reinterpret_cast<TsFileTune*>(arg);
        self->run();
        return 0;
    }

    void run () {
// location : "tsfile:xxx"
        int fd = open(location.substr(7).c_str(), O_RDONLY);
        if (fd < 0) {
            notify(EVENT_TUNER_UNLOCKED);
            l.error("Failed to open the file");
            return;
        }
        notify(EVENT_TUNER_LOCKED);
        l.info("file open");

        while (!stopped) read(fd);

        close(fd);
        notify(EVENT_TUNER_UNLOCKED); //nop. No listener
        l.info("file closed");
        deref();
    }

    bool start () {
        ref();
        pthread_t t;
        pthread_create(&t, NULL, run0, this);
        pthread_detach(t);
        {
            ScopedLock sl(mutex);
            locked = true;
        }
        collector->start(this);
        return true;
    }

    void stop () {
        collector->stop();
        {
            ScopedLock sl(mutex);
            locked = false;
        }
        stopped = true;
    }

    void startSF (uint16_t pid)
    {
        l.info("startSF pid=%d", pid);
        tsReader.addPidStreamReader(pid, sectReader);
    }

    void cancelSF (uint16_t pid)
    {
        l.info("cancelSF pid=%d", pid);
        tsReader.removePidStreamReader(pid, sectReader);
    }

    void startStreaming (uint16_t pid, const StreamReaderRef &sr)
    {
        l.info("starStreaming pid=%d", pid);
        tsReader.addPidStreamReader(pid, sr);
    }

    void stopStreaming (uint16_t pid, const StreamReaderRef &sr)
    {
        l.info("stopStreaming pid=%d", pid);
        tsReader.removePidStreamReader(pid, sr);
    }

    static const int READ_SIZE = 64 * 1024;
    void read (int fd) {
        ssize_t sz;

        while (buffer.size() > 0) {
            sz = tsSyncReader.read(buffer.begin(), buffer.size(), &tuneContext);

            if (sz <= 0) break;
            buffer.drop(sz);
        }

        buffer.ensureMargin(READ_SIZE);
        sz = ::read(fd, buffer.end(), READ_SIZE);
        l.debug("read %d", sz);
        if (sz > 0) {
            buffer.grow(sz);
        } else if (sz == 0) {
            l.info("Rewinding...");
            buffer.clear();
            lseek(fd, 0, SEEK_SET);
            task.eventDriver.setTimeout(0, &reselector);
        } else {
            l.error("Disconnected");
        }

        return;
    }

    struct Reselector: Reactor {
        void processEvent (int events) override {
            auto *self = container_of(this, &TsFileTune::reselector);
            if (model.currentService
                    && model.currentService->tsLoc == self->location) {
                task.serviceManager.reselect();
            }
        }
    } reselector;

    Buffer buffer;
    volatile bool stopped;

    TuneContext tuneContext;
    TsReader tsReader;
    TsSyncReader tsSyncReader;
    StreamReaderRef sectReader;
    acba::Ref<SICollector> collector;

    Log l;
};

struct TsFileTuneProvider: TuneProvider {

    TsFileTuneProvider (): TuneProvider("tsfile") {}

    Tune *create (const std::string &loc, const Ref<SICollector> &col)
            override {
        auto tft = new TsFileTune(loc, col);
        tft->start();
        return tft;
    }

    void destroy (const acba::Ref<Tune> &t) override {
        static_cast<TsFileTune*>(t.get())->stop();
    }
};

void TsFileTuneProvider_init ()
{
    task.tuneManager.registerTuneProvider(new TsFileTuneProvider());
}
