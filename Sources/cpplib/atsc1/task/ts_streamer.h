#ifndef ATSC1_TS_STREAMER_H
#define ATSC1_TS_STREAMER_H

#include <string.h>

#include "acba.h"
#include "http_server.h"

namespace atsc1 {

struct TsStreamer: acba::HttpRequestAcceptor {

    acba::HttpRequestHandler *acceptHttpRequest (const std::string &basePath,
        acba::HttpReactor *r) override;

    /**
     * Create a stream reader which transfers received bytes to the websocket
     * clients. May return null if given path is already being used.
     * This reader instance is managed by TsTreamer until releaseStreamReader
     * is called.
     */
    RefCountedStreamReader *createStreamReader (const std::string &path);

    /**
     * Release previously created stream reader.
     * As the stream reader is ref-counted, it may be still referenced by other
     * entiries, but not longiner sending stream bytes to peers.
     */
    void releaseStreamReader (const std::string &path);
};

}
#endif
