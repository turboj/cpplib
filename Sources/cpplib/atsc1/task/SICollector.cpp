#include "ts_reader.h"
#include "tune_manager.h"
#include "si_manager.h"
#include "atsc1_task.h"
#include "EventDriver.h"
#include "acba.h"
#include "atsc1_multiple_string.h"

using namespace std;
using namespace acba;
using namespace atsc1;

#define BITS32(x) ((x[0] << 24) | (x[1] << 16) | (x[2] << 8) | x[3])
#define SECTION_LENGTH(x) (((x[1] & 0x0F) << 8) | (x[2]))
#define TABLE_ID_EXTENSION(x) ((x[3] << 8) | (x[4]))
#define VERSION_NUMBER(x) ((x[5] >> 1) & 0x1F)
#define SECTION_NUMBER(x) (x[6])
#define LAST_SECTION_NUMBER(x) (x[7])
#define CRC32(x) BITS32((x + 3 + SECTION_LENGTH(x) - 4))

static Log l("sicollector");

static const uint16_t PAT_PID = 0;
static const uint16_t MGT_VCT_PID = 0x1FFB;

void SICollector::start (const acba::Ref<Tune> &t)
{
    l.info("start");
    this->tune = t;
    if (model.transportStreamByLocation.find(t->location)
            != model.transportStreamByLocation.end()) {
        tune->startSF(PAT_PID);
        reqPids.insert(PAT_PID);
    }
    tune->startSF(MGT_VCT_PID);
    reqPids.insert(MGT_VCT_PID);
}

void SICollector::stop ()
{
    l.info("stop");
    for(auto i: reqPids) {
        tune->cancelSF(i);
    }
    reqPids.clear();
}

int SICollector::read (const byte_t *data, int sz, Chain *ctx)
{
    auto tunectx = ctx->as<TuneContext>();
    auto tsctx = ctx->as<TsContext>();

    if (!tunectx) {
        l.error("No tunectx");
        return 0;
    }
    if (!tsctx) {
        l.error("No tsctx");
        return 0;
    }
    if ( sz < 3) {
        l.error("section size is less than 3");
        return 0;
    }

    l.info("section pid:0x%02X tid:0x%02X from %s", tsctx->pid, data[0],
            tunectx->tune->location.c_str());

    if (tsctx->pid == PAT_PID && data[0] == SI_PAT_TID) {
        processPAT(data, sz);
    } else if (data[0] == SI_PMT_TID) {
        processPMT(data, sz, tsctx->pid);
    } else if (tsctx->pid == MGT_VCT_PID) {
        if (data[0] == SI_VCT_TID) {
            processVCT(data, sz);
        } else if (data[0] == SI_MGT_TID) {
            processMGT(data, sz);
        } else if (data[0] == SI_RRT_TID) {
            processRRT(data, sz);
        }
    } else if (data[0] == SI_EIT_TID) {
        processEIT(data, sz, tsctx->pid);
    } else if (data[0] == SI_ETT_TID) {
        processETT(data, sz, tsctx->pid);
    }

    return 0;
}

void SICollector::processPAT (const byte_t *data, int sz)
{
    l.info("In processPAT");
    if (sz < 12) {
        l.error("PAT section size is less than 12 : %d", sz);
        return;
    }
    int len = SECTION_LENGTH(data);

    if (len+3 != sz) {
        l.error("PAT section size mismatch(%d/%d)", len+3, sz);
        return;
    }

    if (!pat) {
        pat = new SITable(SI_TAB_PAT, PAT_PID);
        pat->lsn = LAST_SECTION_NUMBER(data);
        pat->version = VERSION_NUMBER(data);
        pat->tabExt = TABLE_ID_EXTENSION(data); //tsid
        l.info("PAT created lsn=%d version=%d", pat->lsn, pat->version);
    }

    if (pat->version != VERSION_NUMBER(data)) {
        l.warn("PAT version was changed");
        pat = new SITable(SI_TAB_PAT, PAT_PID);
        pat->lsn = LAST_SECTION_NUMBER(data);
        pat->version = VERSION_NUMBER(data);
        pat->tabExt = TABLE_ID_EXTENSION(data); //tsid
    }

    if (pat->lsn != LAST_SECTION_NUMBER(data)) {
        l.warn("PAT lsn is updated");
        pat = new SITable(SI_TAB_PAT, PAT_PID);
        pat->lsn = LAST_SECTION_NUMBER(data);
        pat->version = VERSION_NUMBER(data);
        pat->tabExt = TABLE_ID_EXTENSION(data); //tsid
    }

    Ref<SISection> section = new SISection();
    section->secNum = SECTION_NUMBER(data);
    section->crc32 = CRC32(data);
    section->data = (byte_t*)malloc(sz);
    memcpy(section->data, data, sz);
    section->dataLen = sz;
    pat->sections[section->secNum] = section;
    l.info("PAT %d-th section : crc32=0x%08X", section->secNum, section->crc32);

    if (pat->lsn+1 == pat->sections.size()) {
        l.info("PAT COMPLETE");
        l.info("version=%d, lsn=%d", pat->version, pat->lsn);

        ScopedLock sl(mutex);
        auto &rq = reqQueues[tabQueueIndex];
        for (auto &s: pat->sections) {
            byte_t *buf = s.second->data;
            int numPID = (s.second->dataLen - 12) / 4;
            int pos = 8;
            for (int i = 0; i < numPID; i++) {
                uint16_t proNum = ((buf[pos]<<8) | buf[pos+1]);
                uint16_t pid = (((buf[pos+2] & 0x1F)<<8) | buf[pos+3]);
                l.info("%d-th loop : progNum=%d pid=%d", i, proNum, pid);
                if ( proNum != 0 ) {
                    rq.push_back(pid);
                }
                pos += 4;
            }
        }
        if (rq.size() > 0) {
            task.eventDriver.setTimeout(0, this);
        }
    }
}

void SICollector::processPMT (const byte_t *data, int sz, uint16_t pid)
{
    l.info("In processPMT");
    if (sz < 16) {
        l.error("PMT section size is less than 16 : %d", sz);
        return;
    }
    int len = SECTION_LENGTH(data);

    if (len+3 != sz) {
        l.error("PMT section size mismatch(%d/%d)", len+3, sz);
        return;
    }

// As per ISO 13818-1, lsn and sn shall be 0 (single section)
    if (LAST_SECTION_NUMBER(data) != 0 ||
            SECTION_NUMBER(data) != 0) {
        l.error("PMT corrupted(lsn=%d secNum=%d", LAST_SECTION_NUMBER(data),
                SECTION_NUMBER(data));
        return;
    }

    acba::Ref<SITable> pmt;
    int progNum = ((data[3] << 8) | data[4]);
    auto p = pmts.find(progNum);
    if (p == pmts.end()) {
        pmt = new SITable(SI_TAB_PMT, pid);
        pmt->lsn = 0;
        pmt->version = VERSION_NUMBER(data);
        l.info("PMT created lsn=%d version=%d", pmt->lsn, pmt->version);
        pmts[progNum] = pmt;
    } else {
        pmt = p->second;
    }

    if (pmt->version != VERSION_NUMBER(data)) {
        l.warn("PMT version was changed");
        pmt = new SITable(SI_TAB_PMT, pid);
        pmt->lsn = 0;
        pmt->version = VERSION_NUMBER(data);
        pmts[progNum] = pmt;
    }

    Ref<SISection> section = new SISection();
    section->secNum = 0;
    section->crc32 = CRC32(data);
    section->data = (byte_t*)malloc(sz);
    memcpy(section->data, data, sz);
    section->dataLen = sz;

    if (pmt->sections.empty()) {
        pmt->sections[0] = section;
    } else {
        if (pmt->sections[0]->crc32 == section->crc32) {
            l.warn("duplicate section");
            return;
        }
        l.warn("PMT crc32 was changed");
        pmt->sections[0] = section;
    }
    l.info("PMT section : crc32=0x%08X", section->crc32);

    l.info("PMT(%d/%d) COMPLETE(version=%d)", progNum, pid, pmt->version);

    ScopedLock sl(mutex);
    auto &tq = tabQueues[tabQueueIndex];
    tq.push_back(pmt);
    task.eventDriver.setTimeout(0, this);
}

void SICollector::processVCT (const byte_t *data, int sz)
{
    l.info("In processVCT");
    if (sz < 16) {
        l.error("VCT section size is less than 16 : %d", sz);
        return;
    }
    int len = SECTION_LENGTH(data);

    if (len+3 != sz) {
        l.error("VCT section size mismatch(%d/%d)", len+3, sz);
        return;
    }

    if (!vct) {
        vct = new SITable(SI_TAB_VCT, MGT_VCT_PID);
        vct->lsn = LAST_SECTION_NUMBER(data);
        vct->version = VERSION_NUMBER(data);
        vct->tabExt = TABLE_ID_EXTENSION(data); //tsid
        l.info("VCT created lsn=%d ver=%d, tsid=%d", vct->lsn,
                vct->version, vct->tabExt);
    }

    if (vct->version != VERSION_NUMBER(data)) {
        l.warn("VCT version was changed");
        vct = new SITable(SI_TAB_VCT, MGT_VCT_PID);
        vct->lsn = LAST_SECTION_NUMBER(data);
        vct->version = VERSION_NUMBER(data);
        vct->tabExt = TABLE_ID_EXTENSION(data); //tsid
    }

    if (vct->lsn != LAST_SECTION_NUMBER(data)) {
        l.warn("VCT lsn is updated");
        vct = new SITable(SI_TAB_VCT, MGT_VCT_PID);
        vct->lsn = LAST_SECTION_NUMBER(data);
        vct->version = VERSION_NUMBER(data);
        vct->tabExt = TABLE_ID_EXTENSION(data); //tsid
    }

    Ref<SISection> section = new SISection();
    section->secNum = SECTION_NUMBER(data);
    section->crc32 = CRC32(data);
    section->data = (byte_t*)malloc(sz);
    memcpy(section->data, data, sz);
    section->dataLen = sz;
    vct->sections[section->secNum] = section;
    l.info("VCT %d-th section : crc32=0x%08X", section->secNum, section->crc32);

    if (vct->lsn+1 == vct->sections.size()) {
        l.info("VCT COMPLETE");
        l.info("ver=%d, lsn=%d tsid=%d", vct->version, vct->lsn, vct->tabExt);

        ScopedLock sl(mutex);
        auto &tq = tabQueues[tabQueueIndex];
        tq.push_back(vct);
        for (auto &r: rrts) {
            tq.push_back(r.second);
        }
        vctProcessed = true;
        task.eventDriver.setTimeout(0, this);
    }
}

void SICollector::processMGT (const byte_t *data, int sz)
{
    l.info("In processMGT");
    if (sz < 17) {
        l.error("MGT section size is less than 17 : %d", sz);
        return;
    }
    int len = SECTION_LENGTH(data);

    if (len+3 != sz) {
        l.error("MGT section size mismatch(%d/%d)", len+3, sz);
        return;
    }

// As per A/65-B, lsn and sn shall be 0(single section)
    if (LAST_SECTION_NUMBER(data) != 0 ||
            SECTION_NUMBER(data) != 0) {
        l.error("MGT corrupted(lsn=%d secNum=%d", LAST_SECTION_NUMBER(data),
                SECTION_NUMBER(data));
        return;
    }

    if (!mgt) {
        mgt = new SITable(SI_TAB_MGT, MGT_VCT_PID);
        mgt->lsn = 0;
        mgt->version = VERSION_NUMBER(data);
        l.info("MGT created lsn=%d version=%d", mgt->lsn, mgt->version);
    }

    if (mgt->version != VERSION_NUMBER(data)) {
        l.warn("MGT version was changed");
        mgt = new SITable(SI_TAB_MGT, MGT_VCT_PID);
        mgt->lsn = 0;
        mgt->version = VERSION_NUMBER(data);
    }

    Ref<SISection> section = new SISection();
    section->secNum = 0;
    section->crc32 = CRC32(data);
    section->data = (byte_t*)malloc(sz);
    memcpy(section->data, data, sz);
    section->dataLen = sz;

    if (mgt->sections.empty()) {
        mgt->sections[0] = section;
    } else {
        if (mgt->sections[0]->crc32 == section->crc32) {
            l.warn("duplicate section");
            return;
        }
        l.warn("MGT crc32 was changed");
        mgt->sections[0] = section;
    }
    l.info("MGT section : crc32=0x%08X", section->crc32);

    l.info("MGT COMPLETE(version=%d)", mgt->version);

    std::map<uint32_t, acba::Ref<MgtEntry>> newEitInfo;
    std::map<uint32_t, acba::Ref<MgtEntry>> newEttInfo;
    uint16_t tdef = ((data[9]<<8) | data[10]);
    int idx = 11;
    l.info("MGT DUMP : version=%d tdefined=%d sec_len=%d", mgt->version, tdef,
            sz);
    ScopedLock sl(mutex);
    auto &rq = reqQueues[tabQueueIndex];
    for (int i = 0; i < tdef; i++) {
        uint16_t ttype = ((data[idx]<<8) | data[idx+1]);
        uint16_t tpid = (((data[idx+2] & 0x1F) << 8) | data[idx+3]);
        uint8_t tver = (data[idx+4] & 0x1F);
        uint32_t numbytes = ((data[idx+5] << 24) | (data[idx+6] << 16)
                | (data[idx+7] << 8) | data[idx+8]);
        uint32_t tdesclen = (((data[idx+9] & 0x0F) << 8) | data[idx+10]);
        l.info(" [%2d-th tType=0x%04X tPid=0x%04X ver=%2d bytes=%d descLen=%d]",
                i, ttype, tpid, tver, numbytes, tdesclen);
        if (ttype >= SI_MGT_EIT_BASE && ttype <= SI_MGT_EIT_MAX) {
            uint32_t pv = (tpid << 16) | tver;
            acba::Ref<MgtEntry> info = new MgtEntry(ttype-SI_MGT_EIT_BASE,
                    pv, numbytes);
            newEitInfo[pv] = info;

            // no effect when same pid SF was already requested
            rq.push_back(tpid);
        } else if (ttype >= SI_MGT_ETT_BASE && ttype <= SI_MGT_ETT_MAX) {
            uint32_t pv = (tpid << 16) | tver;
            acba::Ref<MgtEntry> info = new MgtEntry(ttype-SI_MGT_ETT_BASE,
                    pv, numbytes);
            newEttInfo[pv] = info;
            // no effect when same pid SF was already requested
            rq.push_back(tpid);
        }
        idx += (11 + tdesclen);
    }
    if (rq.size() > 0) {
        task.eventDriver.setTimeout(0, this);
    }

    mgtEitInfo.swap(newEitInfo);
    mgtEttInfo.swap(newEttInfo);
    l.info("MGT EIT Info DUMP");
    for (auto &ei: mgtEitInfo) {
        l.info("[%d] pidnver: 0x%08X, numBytes:%d", ei.second->idx,
                ei.second->pidnver, ei.second->numBytes);

        uint16_t pid = (ei.second->pidnver) >> 16;
        if (eitCaches.find(pid) != eitCaches.end() ) {
            if (eitCaches[pid]->pidnver == ei.second->pidnver
                    && eitCaches[pid]->rcvBytes >= ei.second->numBytes) {
                l.info("EIT(%d/%d) Complete", ei.second->idx,
                        ei.second->pidnver);
                auto &tq = tabQueues[tabQueueIndex];
                tq.push_back(eitCaches[pid]->eitInsts.begin()->second);
                task.eventDriver.setTimeout(0, this);
            }
        }
    }
    l.info("MGT ETT Info DUMP");
    for (auto &ei: mgtEttInfo) {
        l.info("[%d] pidnver: 0x%08X, numBytes:%d", ei.second->idx,
                ei.second->pidnver, ei.second->numBytes);

        uint16_t pid = (ei.second->pidnver) >> 16;
        if (ettCaches.find(pid) != ettCaches.end() ) {
            if (ettCaches[pid]->pidnver == ei.second->pidnver
                    && ettCaches[pid]->rcvBytes >= ei.second->numBytes) {
                l.info("ETT(%d/%d) Complete", ei.second->idx,
                        ei.second->pidnver);
                auto &tq = tabQueues[tabQueueIndex];
                tq.push_back(ettCaches[pid]->etts.begin()->second);
                task.eventDriver.setTimeout(0, this);
            }
        }
    }
}

void SICollector::processRRT (const byte_t *data, int sz)
{
    l.info("In processRRT");
    if (sz < 17) {
        l.error("RRT section size is less than 17 : %d", sz);
        return;
    }
    int len = SECTION_LENGTH(data);

    if (len+3 != sz) {
        l.error("RRT section size mismatch(%d/%d)", len+3, sz);
        return;
    }

// As per A/65-B, lsn and sn shall be 0(single section)
    if (LAST_SECTION_NUMBER(data) != 0 ||
            SECTION_NUMBER(data) != 0) {
        l.error("RRT corrupted(lsn=%d secNum=%d", LAST_SECTION_NUMBER(data),
                SECTION_NUMBER(data));
        return;
    }

    acba::Ref<SITable> rrt;
    uint8_t region = data[4];
    auto r = rrts.find(region);
    if (r == rrts.end()) {
        rrt = new SITable(SI_TAB_RRT, MGT_VCT_PID);
        rrt->lsn = 0;
        rrt->version = VERSION_NUMBER(data);
        rrt->tabExt = TABLE_ID_EXTENSION(data);
        l.info("RRT created ver=%d region=%d", rrt->version, region);
        rrts[region] = rrt;
    } else {
        rrt = r->second;
    }

    if (rrt->version != VERSION_NUMBER(data)) {
        rrt = new SITable(SI_TAB_RRT, MGT_VCT_PID);
        rrt->lsn = 0;
        rrt->version = VERSION_NUMBER(data);
        rrt->tabExt = TABLE_ID_EXTENSION(data);
        rrts[region] = rrt;
        l.info("RRT version changed: ver=%d region=%d", rrt->version, region);
    }

    Ref<SISection> section = new SISection();
    section->secNum = 0;
    section->crc32 = CRC32(data);
    section->data = (byte_t*)malloc(sz);
    memcpy(section->data, data, sz);
    section->dataLen = sz;

    if (rrt->sections.empty()) {
        rrt->sections[0] = section;
    } else {
        if (rrt->sections[0]->crc32 == section->crc32) {
            l.warn("duplicate section");
            return;
        }
        l.warn("RRT crc32 was changed");
        rrt->sections[0] = section;
    }
    l.info("RRT section : crc32=0x%08X", section->crc32);

    l.info("RRT(%d) COMPLETE(version=%d)", region, rrt->version);

    ScopedLock sl(mutex);
    if (vctProcessed == true) {
        auto &tq = tabQueues[tabQueueIndex];
        tq.push_back(rrt);
        task.eventDriver.setTimeout(0, this);
    }
}

void SICollector::processEIT (const byte_t *data, int sz, uint16_t pid)
{
    l.info("In processEIT");
    if (sz < 14) {
        l.error("EIT section size is less than 14 : %d", sz);
        return;
    }
    int len = SECTION_LENGTH(data);

    if (len+3 != sz) {
        l.error("EIT section size mismatch(%d/%d)", len+3, sz);
        return;
    }

    uint16_t srcid = TABLE_ID_EXTENSION(data);
    uint8_t ver = VERSION_NUMBER(data);
    uint8_t sn = SECTION_NUMBER(data);
    uint8_t lsn = LAST_SECTION_NUMBER(data);
    uint8_t numevt = data[9];
    uint32_t pv = (pid << 16) | ver;

    ScopedLock sl(mutex);
    auto &ec = eitCaches[pid];
    if (!ec || ec->pidnver != pv) {
        ec = new EitCache(pv);
    }
    auto &ei = ec->eitInsts[srcid];
    if (!ei) {
        ei = new SITable(SI_TAB_EIT, pid);
        ei->lsn = lsn;
        ei->version = ver;
        ei->tabExt = srcid;
    }
    Ref<SISection> section = new SISection();
    section->secNum = sn;
    section->crc32 = CRC32(data);
    section->data = (byte_t*)malloc(sz);
    memcpy(section->data, data, sz);
    section->dataLen = sz;
    ei->sections[sn] = section;
    ec->rcvBytes += sz;

    l.info("EIT(%d/%d) %d/%d/%d numEvt=%d", pid, srcid, ver, sn, lsn, numevt);

    if (mgtEitInfo.find(pv) != mgtEitInfo.end()) {
        if (ec->rcvBytes >= mgtEitInfo[pv]->numBytes) {
            l.info("EIT(%d/%d) Complete", mgtEitInfo[pv]->idx, pv);
            auto &tq = tabQueues[tabQueueIndex];
            tq.push_back(ei);
            task.eventDriver.setTimeout(0, this);
        }
    }
}

void SICollector::processETT (const byte_t *data, int sz, uint16_t pid)
{
    l.info("In processETT");
    if (sz < 17) {
        l.error("ETT section size is less than 14 : %d", sz);
        return;
    }
    int len = SECTION_LENGTH(data);

    if (len+3 != sz) {
        l.error("ETT section size mismatch(%d/%d)", len+3, sz);
        return;
    }

// As per A/65-B, lsn and sn shall be 0(single section)
    if (LAST_SECTION_NUMBER(data) != 0 ||
            SECTION_NUMBER(data) != 0) {
        l.error("ETT corrupted(lsn=%d secNum=%d", LAST_SECTION_NUMBER(data),
                SECTION_NUMBER(data));
        return;
    }

    uint16_t tabext = TABLE_ID_EXTENSION(data);
    uint8_t ver = VERSION_NUMBER(data);
    uint32_t eid = ((data[9] << 24) | (data[10] << 16) | (data[11] << 8)
            | data[12]);
    uint32_t pv = (pid << 16) | ver;

    l.info("ETT (pv=0x%08X tabext=%d etm_id=0x%08X) recevied", pv, tabext, eid);

    ScopedLock sl(mutex);
    auto &ec = ettCaches[pid];
    if (!ec || ec->pidnver != pv) {
        ec = new EttCache(pv);
    }
    auto &ei = ec->etts[eid];
    if (!ei) {
        ei = new SITable(SI_TAB_ETT, pid);
        ei->lsn = 0;
        ei->version = ver;
        ei->tabExt = tabext;
    }
    Ref<SISection> section = new SISection();
    section->secNum = 0;
    section->crc32 = CRC32(data);
    section->data = (byte_t*)malloc(sz);
    memcpy(section->data, data, sz);
    section->dataLen = sz;
    ei->sections[0] = section;
    ec->rcvBytes += sz;

    if (mgtEttInfo.find(pv) != mgtEttInfo.end()) {
        if (ec->rcvBytes >= mgtEttInfo[pv]->numBytes) {
            l.info("ETT(%d/%d) Complete", mgtEttInfo[pv]->idx, pv);
            auto &tq = tabQueues[tabQueueIndex];
            tq.push_back(ei);
            task.eventDriver.setTimeout(0, this);
        }
    }
}

void SICollector::processEvent (int evt)
{
    vector<Ref<SITable> > tq;
    vector<uint16_t> rq;
    {
        ScopedLock sl(mutex);
        tq.swap(tabQueues[tabQueueIndex]);
        rq.swap(reqQueues[tabQueueIndex]);
        tabQueueIndex ^=1;
        assert(tabQueues[tabQueueIndex].size() == 0);
        assert(reqQueues[tabQueueIndex].size() == 0);
    }

    l.info("rq size = %d", rq.size());
    for (uint16_t pid: rq) {
        tune->startSF(pid);
        reqPids.insert(pid);
    }

    l.info("tq size = %d", tq.size());
    for (auto &tab: tq) {
        l.info("Tab type=%d(pid=%d) from queue", tab->type, tab->pid);
        if (tab->type == SI_TAB_PMT) {
            if (tab->sections.size() == 0) continue;
            auto &fs = tab->sections[0];
            uint16_t progNum = ((fs->data[3] << 8) | fs->data[4]);
            uint16_t pcrPid = (((fs->data[8]&0x1F) << 8) | fs->data[9]);
            l.info("parse PMT(progNum=%d) : pcr=%d", progNum, pcrPid);
            acba::Ref<ServiceMediaInfo> minfo
                    = new ServiceMediaInfo(tab->pid, pcrPid);
            minfo->version = tab->version;
            minfo->crc32 = fs->crc32;
            minfo->rawPmt = (byte_t *)malloc(fs->dataLen);
            memcpy(minfo->rawPmt, fs->data, fs->dataLen);
// extract es info
            int progInfoLen = (((fs->data[10]&0x0F) << 8) | fs->data[11]);
            int esInfoLen = fs->dataLen - progInfoLen - 16;
            int pos = progInfoLen + 12;
            for (int i = 0; i < esInfoLen;) {
                uint8_t streamType = fs->data[pos+i];
                uint16_t pid = (((fs->data[pos+i+1]&0x1F) << 8)
                        | fs->data[pos+i+2]);
                int esDesLen = (((fs->data[pos+i+3]&0x0F) << 8)
                        | fs->data[pos+i+4]);
                i += esDesLen + 5;
                acba::Ref<ESInfo> es = new ESInfo(streamType, pid);
                minfo->eleStreams.push_back(es);
                l.info("ES added : type=%d pid=%d", streamType, pid);
            }

            //find the matached Service
            auto ts = model.transportStreamByLocation.find(tune->location);
            if (ts == model.transportStreamByLocation.end()) {
                l.warn("No matched TS found");
                continue;
            }
            auto svci = ts->second->services.find(progNum);
            if (svci == ts->second->services.end()) {
                l.warn("No matched Service found");
                continue;
            }
            auto &svc = ts->second->services[progNum];
            if (!(svc->mediaInfo) || (svc->mediaInfo->version != minfo->version
                    && svc->mediaInfo->crc32 != minfo->crc32)) {
                svc->mediaInfo = minfo;
                svc->mediaInfoUpdate.notify(svc);
            }
        } else if (tab->type == SI_TAB_VCT) {
            if (tab->sections.size() == 0) continue;

            auto &ts = model.transportStreamByLocation[tune->location];
            if (!ts) {
                l.info("TransportStream created(loc=%s)",
                        tune->location.c_str());
                ts = new TransportStream(tune->location);
                tune->startSF(PAT_PID);
                reqPids.insert(PAT_PID);
            }
            if (ts->vctInfo && ts->vctInfo->version == tab->version) {
                l.info("VCT is not updated, so ignore");
                continue;
            }
            ts->services.clear();

            acba::Ref<VCTInfo> vinfo = new VCTInfo(tab->version, tab->tabExt);
            ts->vctInfo = vinfo;

            for (auto &s: tab->sections) {
                task.serviceManager.processVct(s.second->data,
                        s.second->dataLen, tune->location);
                ts->vctInfo->rawVct.writeI32(s.second->dataLen);
                ts->vctInfo->rawVct.writeBytes(s.second->data,
                        s.second->dataLen);
            }
            model.transportStreamUpdate.notify(ts);
        } else if (tab->type == SI_TAB_RRT) {
            auto ts = model.transportStreamByLocation.find(tune->location);
            if (ts == model.transportStreamByLocation.end()) {
                l.warn("No matched TS found");
                continue;
            }
            // Update RRTInfo of model->TS
            byte_t *sect = tab->sections[0]->data;
            uint8_t region = sect[4];
            int pos = 9;
            int nameLen = sect[pos++];
            MultipleString rName(&(sect[pos]), nameLen);
            acba::Ref<RatingInfo> rInfo
                    = new RatingInfo(tab->version, region);
            if (rName.decoded.size() > 0) {
                rInfo->regionName = rName.decoded.begin()->second;
            }
            pos += nameLen;
            uint8_t nDim = sect[pos++];
            for (uint8_t i = 0; i < nDim; i++) {
                acba::Ref<RatingDimension> dim = new RatingDimension();
                int dNameLen = sect[pos++];
                MultipleString dName(&(sect[pos]), dNameLen);
                if (dName.decoded.size() > 0) {
                    dim->dimName = dName.decoded.begin()->second;
                }
                pos += dNameLen;
                uint8_t nVal = sect[pos++] & 0x0F;
                for (uint8_t j = 0; j < nVal; j++) {
                    string aVal;
                    int aValLen = sect[pos++];
                    MultipleString aValMs(&(sect[pos]), aValLen);
                    if (aValMs.decoded.size() > 0) {
                        aVal = aValMs.decoded.begin()->second;
                    }
                    dim->abbrRatingValues[j] = aVal;
                    pos += aValLen;

                    string rVal;
                    int rValLen = sect[pos++];
                    MultipleString rValMs(&(sect[pos]), rValLen);
                    if (rValMs.decoded.size() > 0) {
                        rVal = rValMs.decoded.begin()->second;
                    }
                    dim->ratingValues[j] = rVal;
                    pos += rValLen;
                }
                rInfo->dimensions[i] = dim;
            }
            ts->second->ratingInfos[region] = rInfo;
            l.info("RatingInfo addded(region:%d)", region);
            l.info("- regionName : %s", rInfo->regionName.c_str());
            l.info("- Dimensions(num=%d)", rInfo->dimensions.size());
            for (auto &d: rInfo->dimensions) {
                l.info(" <Dimension(%d) : name=%s>", d.first, d.second->dimName.c_str());
                l.info(" - abbrRatingValues(num=%d)", d.second->abbrRatingValues.size());
                for (auto &a: d.second->abbrRatingValues) {
                    l.info("   %d:%s", a.first, a.second.c_str());
                }
                l.info(" - ratingValues(num=%d)", d.second->ratingValues.size());
                for (auto &r: d.second->ratingValues) {
                    l.info("   %d:%s", r.first, r.second.c_str());
                }
            }
        } else if (tab->type == SI_TAB_EIT) {
            auto ts = model.transportStreamByLocation.find(tune->location);
            if (ts == model.transportStreamByLocation.end()) {
                l.warn("No matched TS found");
                continue;
            }

            ScopedLock sl(mutex);
            uint32_t pv = (tab->pid << 16) | tab->version;
            if (mgtEitInfo.find(pv) != mgtEitInfo.end()
                    && eitCaches.find(tab->pid) != eitCaches.end()) {
                std::map<uint16_t, acba::Ref<SITable>> eis;
                auto &ec = eitCaches[tab->pid];
                uint16_t idx = mgtEitInfo[pv]->idx;
                for (auto &ei: ec->eitInsts) {
                    eis[ei.first] = ei.second;
                }
                ts->second->eits[idx].swap(eis);
                model.eitUpdate.notify(ts->second);
            }
        } else if (tab->type == SI_TAB_ETT) {
            auto ts = model.transportStreamByLocation.find(tune->location);
            if (ts == model.transportStreamByLocation.end()) {
                l.warn("No matched TS found");
                continue;
            }

            ScopedLock sl(mutex);
            uint32_t pv = (tab->pid << 16) | tab->version;
            if (mgtEttInfo.find(pv) != mgtEttInfo.end()
                    && ettCaches.find(tab->pid) != ettCaches.end()) {
                std::map<uint32_t, acba::Ref<SITable>> eis;
                auto &ec = ettCaches[tab->pid];
                uint16_t idx = mgtEttInfo[pv]->idx;
                for (auto &ei: ec->etts) {
                    eis[ei.first] = ei.second;
                }
                ts->second->etts[idx].swap(eis);
                //model.eitUpdate.notify(ts->second);
            }
        }
    }
}
