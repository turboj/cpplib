#include "tune_manager.h"
#include "EventDriver.h"
#include "acba.h"

using namespace std;
using namespace acba;
using namespace atsc1;

static Log l("tunemgr1");

void Atsc1TuneProvider_init ();
void TsFileTuneProvider_init ();
void VirtualAtsc1TuneProvider_init ();

void TuneManager::init ()
{
#if USE_TUNER_CLIENT
    VirtualAtsc1TuneProvider_init();
#else
    Atsc1TuneProvider_init();
#endif
    TsFileTuneProvider_init();
}

void TuneManager::registerTuneProvider (const acba::Ref<TuneProvider> &prov)
{
    tuneProviders.push_back(prov);
}

Tune *TuneManager::requestTune (const string &loc)
{
    // reuse tune if exists
    auto ti = tunes.find(loc);
    if (ti != tunes.end()) {
        auto &th = ti->second;
        th.useCount++;
        l.info("Share tune %s count:%d", loc.c_str(), th.useCount);
        return th.tune.get();
    }

    // determine provider
    string prefix = loc.substr(0, loc.find_first_of(':'));
    auto provi = find_if(tuneProviders.begin(), tuneProviders.end(),
            [&prefix] (const Ref<TuneProvider> &p) { return p->type == prefix; });
    if (provi == tuneProviders.end()) {
        l.error("Unsupported location type %s", prefix.c_str());
        return nullptr;
    }
    auto &prov = *provi;

// request tune
    Ref<SICollector> col = new SICollector();

    Ref<Tune> t = prov->create(loc, col.get());
    if (!t) {
        l.error("Unable to tune %s", loc.c_str());
        return nullptr;
    }

    auto &th = tunes[loc];
    th.useCount++;
    th.tune = t;
    th.provider = prov;
    th.collector = col;
    l.info("Create tune %s", loc.c_str());

    return t.get();
}

void TuneManager::releaseTune (const acba::Ref<Tune> &t)
{
    auto thi = tunes.find(t->location);
    if (thi == tunes.end()) {
        l.error("Invalid tune %s", t->location.c_str());
        return;
    }

    auto &th = thi->second;
    assert(th.tune == t);
    th.useCount--;
    if (th.useCount > 0) {
        l.info("Unshare tune %s count:%d", th.tune->location.c_str(),
                th.useCount);
        return;
    }

    l.info("Destroy tune %s", th.tune->location.c_str());
    th.provider->destroy(th.tune);
    tunes.erase(thi);
}
