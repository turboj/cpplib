#ifndef SERVICE_MANAGER_1_H
#define SERVICE_MANAGER_1_H

#include <set>
#include "tune_manager.h"
#include "acba.h"
#include "ts_streamer.h"

namespace atsc1 {

struct MediaTask: acba::RefCounter<MediaTask> {

    MediaTask (const acba::Ref<Service> &s, const acba::Ref<Tune> &t):
            service(s), tune(t) {}

    void start ();
    void stop ();
    void update ();

    acba::Ref<Service> service;
    acba::Ref<Tune> tune;
    StreamReaderRef tsStreamReader;
    std::set<uint16_t> reqPids;
};

struct ServiceTask: acba::RefCounter<ServiceTask>,
        acba::Reactor, acba::Listener<Tune::EVENT>,
        acba::Listener<const acba::Ref<Service>&> {

    ServiceTask (const acba::Ref<Service> &s): service(s),
            mutex(PTHREAD_MUTEX_INITIALIZER) {}

    enum SVC_SEL_STATE {
        SVC_SEL_INIT,
        SVC_SEL_START,
        SVC_SEL_SELECTED,
        SVC_SEL_STOPPED,
        SVC_SEL_WAIT_TUNE,
        SVC_SEL_TUNED,
        SVC_SEL_WAIT_SI,
        SVC_SEL_SI_READY,
        SVC_SEL_FAIL
    };

    enum SVC_SEL_FAIL_REASON {
        SVC_SEL_FAIL_NO_RESOURCE,
        SVC_SEL_FAIL_TUNE_FAIL,
        SVC_SEL_FAIL_CONTENT_NOT_FOUND,
        SVC_SEL_FAIL_INVALID_SERVICE,
        SVC_SEL_FAIL_UNKNOWN
    };

    struct SvcSelectionStatus {
        SvcSelectionStatus (): state(SVC_SEL_INIT),
                reason(SVC_SEL_FAIL_UNKNOWN) {}
        SVC_SEL_STATE state;
        SVC_SEL_FAIL_REASON reason;
    };

    void start ();
    void stop ();
    void onUpdate (Tune::EVENT e) override;
    void onUpdate (const acba::Ref<Service>& srv) override;
    void processEvent (int events) override;

    acba::Ref<Service> service;
    acba::Ref<MediaTask> mediaTask;

    SvcSelectionStatus status; // Access inside mutex lock
    pthread_mutex_t mutex;
};

struct TuneHandler: acba::Listener<Tune::EVENT> {

    acba::Ref<Tune> tune;
    acba::Update<Tune::EVENT> update;

    enum TUNE_REQ_RES {
        TUNE_REQ_SUCCESS,
        TUNE_REQ_ALREADY_TUNED,
        TUNE_REQ_NO_RESOURCE
    };

    TUNE_REQ_RES requestTune (const std::string &loc);
    void releaseTune ();
    void onUpdate (Tune::EVENT e) override;
};

struct ServiceManager {

    acba::Ref<ServiceTask> currentServiceTask;
    TuneHandler tuneHandler;

    acba::Update<> currentServiceTaskUpdate;

    struct Selector: acba::Reactor, acba::Listener<>,
            acba::Listener<const acba::Ref<Service>&> {
        void onUpdate (const acba::Ref<Service>& srv) override;
        void onUpdate () override;
        void processEvent (int events) override;
    } selector;

    void init();
    void save ();
    void load ();
    void clear ();
    void reselect (); // for ts file stream rewind
    void processVct (const byte_t *data, int sz, const std::string &loc);

    struct AutoSaver: acba::Reactor {
        void processEvent (int events) override;
    } autoSaver;
};

}
#endif
