#include "ts_reader.h"
#include "section_reader.h"
#include "tune_manager.h"
#include "atsc1_task.h"
#include "EventDriver.h"
#include "acba.h"

using namespace std;
using namespace acba;
using namespace atsc1;

static Log l("atsc1tune");

struct Atsc1Tune: Tune {

    TuneContext tuneContext;
    TsReader tsReader;
    StreamReaderRef sectReader;
    acba::Ref<SICollector> collector;
    atsc1_Tuner *tuner;

    Log l;

    Atsc1Tune (atsc1_Tuner *t, const string &loc,
            const Ref<SICollector> &col): Tune(loc), tuner(t),
            l("atsc1tune", loc+" "), collector(col) {
        sectReader = new SectionReader(col.get());
        tuneContext.tune = this;
    }

    bool start () {
        atsc1_TuneParams params;
        params.frequency = atoi(location.c_str() + 6);
        params.callback = onTunerCallback0;
        params.context = this;

        bool ret = task.platform->startTune(tuner, &params);
        if (ret) {
            ref();
            collector->start(this);
        }

        return ret;
    }

    void stop () {
        task.platform->stopTune(tuner);
        collector->stop();
    }

    void startSF (uint16_t pid)
    {
        tsReader.addPidStreamReader(pid, sectReader);
    }

    void cancelSF (uint16_t pid)
    {
        tsReader.removePidStreamReader(pid, sectReader);
    }

    void startStreaming (uint16_t pid, const StreamReaderRef &sr)
    {
        l.info("startStreaming pid=%d", pid);
        tsReader.addPidStreamReader(pid, sr);
    }

    void stopStreaming (uint16_t pid, const StreamReaderRef &sr)
    {
        l.info("stopStreaming pid=%d", pid);
        tsReader.removePidStreamReader(pid, sr);
    }

    static void onTunerCallback0 (atsc1_CALLBACK_TYPE type, const byte_t *data,
            int sz, void *appData) {
        auto t = (Atsc1Tune*)appData;
        t->onTunerCallback(type, data, sz);
    }

    void onTunerCallback (atsc1_CALLBACK_TYPE type, const byte_t *data, int sz)
            {
        l.trace("onTunerCallback %d", type);

        switch (type) {
        case atsc1_CALLBACK_TUNER_LOCKED:
            {
                ScopedLock sl(mutex);
                if (locked) break;
                locked = true;
                update.notify(EVENT_TUNER_LOCKED);
            }
            break;

        case atsc1_CALLBACK_TUNER_UNLOCKED:
            {
                ScopedLock sl(mutex);
                if (!locked) break;
                locked = false;
                update.notify(EVENT_TUNER_UNLOCKED);
            }
            break;

        case atsc1_CALLBACK_TS:
            while (sz > 0) {
                int rsz = tsReader.read(data, sz, &tuneContext);
                if (rsz == 0) break;
                data += rsz;
                sz -= rsz;
            }
            break;

        case atsc1_CALLBACK_END:
            deref();
            break;

        default:
            l.warn("Unsupported tuner callback %d", type);
            break;
        }
    }
};

struct Atsc1TuneProvider: TuneProvider {

    set<atsc1_Tuner*> tunersInUse;

    Atsc1TuneProvider (): TuneProvider("atsc1") {
        for (int i = 0; i < 10; i++) uses[i] = false;
    }

    Tune *create (const std::string &loc, const Ref<SICollector> &col)
            override {

        // update tuner list
        atsc1_Tuner *tuners[10];
        int nTuners = sizeof(tuners) / sizeof(tuners[0]);
        task.platform->getTuners(tuners, &nTuners);

        for (int i = 0; i < nTuners; i++) {
            auto t = tuners[i];
            if (tunersInUse.count(t) == 1) continue;

            // try tune
            auto at = new Atsc1Tune(tuners[i], loc, col);
            if (!at->start()) {
                delete at;
                continue;
            }

            tunersInUse.insert(t);
            return at;
        }

        return nullptr;
    }

    void destroy (const acba::Ref<Tune> &t) override {
        auto at = static_cast<Atsc1Tune*>(t.get());
        assert(tunersInUse.count(at->tuner) == 1);

        at->stop();
        tunersInUse.erase(at->tuner);
    }

    bool uses[10];
};

void Atsc1TuneProvider_init ()
{
    task.tuneManager.registerTuneProvider(new Atsc1TuneProvider());
}
