#ifndef SI_MANAGER_1_H
#define SI_MANAGER_1_H

#include <set>
#include "tune_manager.h"
#include "acba.h"
#include "EventDriver.h"
#include "atsc1_si_type.h"

namespace atsc1 {

typedef struct Tune Tune;

enum SI_MGT_TT {
    SI_MGT_CNT_TVCT = 0x0000,
    SI_MGT_NXT_TVCT = 0x0001,
    SI_MGT_CNT_CVCT = 0x0002,
    SI_MGT_NXT_CVCT = 0x0003,
    SI_MGT_CHN_ETT  = 0x0004,
    SI_MGT_EIT_BASE = 0x0100,
    SI_MGT_EIT_MAX  = 0x017F,
    SI_MGT_ETT_BASE = 0x0200,
    SI_MGT_ETT_MAX  = 0x027F,
    SI_MGT_RRT_BASE = 0x0301,
    SI_MGT_RRT_MAX  = 0x03FF
};

struct MgtEntry: acba::RefCounter<MgtEntry> {
    MgtEntry (uint32_t i, uint32_t pv, uint32_t bytes): idx(i), pidnver(pv),
            numBytes(bytes) {};
    uint16_t idx;
    uint32_t pidnver;
    uint32_t numBytes;
};

struct EitCache: acba::RefCounter<EitCache> {
    EitCache (uint32_t pv): pidnver(pv), rcvBytes(0) {}
    uint32_t pidnver;
    uint32_t rcvBytes;
    std::map<uint16_t, acba::Ref<SITable>> eitInsts; // sourceId -> eit instance 
};

struct EttCache: acba::RefCounter<EttCache> {
    EttCache (uint32_t pv): pidnver(pv), rcvBytes(0) {}
    uint32_t pidnver;
    uint32_t rcvBytes;
    std::map<uint32_t, acba::Ref<SITable>> etts; // ETM_id -> ett
};

struct SICollector: acba::Reactor, RefCountedStreamReader {

    SICollector (): mutex(PTHREAD_MUTEX_INITIALIZER), tabQueueIndex(0),
            vctProcessed(false) {}

    void start (const acba::Ref<Tune> &t);
    void stop ();
    int read (const byte_t *data, int sz, Chain *ctx) override;
    void processEvent (int events) override;

private:
    void processPAT (const byte_t *data, int sz);
    void processPMT (const byte_t *data, int sz, uint16_t pid);
    void processVCT (const byte_t *data, int sz);
    void processMGT (const byte_t *data, int sz);
    void processRRT (const byte_t *data, int sz);
    void processEIT (const byte_t *data, int sz, uint16_t pid);
    void processETT (const byte_t *data, int sz, uint16_t pid);

    acba::Ref<Tune> tune;
    acba::Ref<SITable> pat;
    std::map<int, acba::Ref<SITable>> pmts; // program_number ->  pmt
    acba::Ref<SITable> vct;
    acba::Ref<SITable> mgt;
    std::map<uint8_t, acba::Ref<SITable>> rrts; // rating_region -> rrt
    std::set<uint16_t> reqPids;
    pthread_mutex_t mutex;
    std::vector<acba::Ref<SITable>> tabQueues[2];
    std::vector<uint16_t> reqQueues[2];
    int tabQueueIndex;
    bool vctProcessed; // flag to handle RRT after VCT processing
    // For EIT/ETT cache
    std::map<uint32_t, acba::Ref<MgtEntry>> mgtEitInfo; // pidnver -> mgt eit info
    std::map<uint16_t, acba::Ref<EitCache>> eitCaches; // pid -> eit cache
    std::map<uint32_t, acba::Ref<MgtEntry>> mgtEttInfo; // pidnver -> mgt ett info
    std::map<uint16_t, acba::Ref<EttCache>> ettCaches; // pid -> ett cache
};

}
#endif
