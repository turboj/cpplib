#include <string>
#include <codecvt>
#include <locale>
#include <sstream>

#include "service_manager.h"
#include "atsc1_task.h"
#include "acba.h"

#define AUTOSAVE_DELAY 10
#define SERVICE_LIST_TYPE "srv0"

using namespace std;
using namespace acba;
using namespace atsc1;

static Log l("srvmgr1");

#define NUM_CHANNELS(x) (x[9])
#define TRANSPORT_STREAM_ID(x) ((x[3] << 8) | (x[4]))

static bool isHiddenSvc (uint16_t flags) {
    if ( flags & 0x1200) { // check bits for hidden, hide_guide
        l.info("hidden or hide_guide is true");
        return true;
    }
    uint16_t svc_type = flags & 0x003F;
    switch (svc_type) {
    case 1:
    case 2:
    case 3:
        return false;
    }
    return true;
}

void ServiceManager::Selector::onUpdate (const acba::Ref<Service>& srv)
{
}

void ServiceManager::Selector::onUpdate ()
{
    task.eventDriver.setTimeout(0, this);
}

void ServiceManager::Selector::processEvent (int events)
{
    auto self = container_of(this, &ServiceManager::selector);

    l.info("In Selctor::processEvent");
    if (model.currentService) {
        if (self->currentServiceTask) {
            l.info("Stop currentServiceTask");
            self->currentServiceTask->stop();
        }
        l.info("Start currentServiceTask");
        self->currentServiceTask = new ServiceTask(model.currentService);
        self->currentServiceTaskUpdate.notify();
        self->currentServiceTask->start();
    } else {
        l.info("No currentService");
        if (self->currentServiceTask) {
            l.info("Stop currentServiceTask");
            self->currentServiceTask->stop();
        }
        self->tuneHandler.releaseTune();
        self->currentServiceTask = 0;
        self->currentServiceTaskUpdate.notify();
    }
}

void ServiceManager::init () {
}

void ServiceManager::save () {
    Buffer buf;

    buf.writeBytes((const uint8_t*)SERVICE_LIST_TYPE, 4);

    for (auto &t: model.transportStreamByLocation) {
        auto &loc = t.first;
        auto &ts = t.second;
        buf.writeI32(loc.size()); // ts location
        buf.writeBytes((uint8_t*)loc.c_str(), loc.size());

        // TSInfo including rawVct
        auto &vct = ts->vctInfo->rawVct;
        buf.writeI32(vct.size() + 3);
        buf.writeI8(ts->vctInfo->version); // vct version 
        buf.writeI16(ts->vctInfo->tsid); // tsid
        buf.writeBytes(vct.begin(), vct.size());
    }
    buf.preWriteI32(buf.size());

    stringstream ss;
    ss << model.storageRoot << "/service_list";
    FILE *fp = fopen(ss.str().c_str(), "w");
    if (!fp) {
        l.error("Failed to save service list: %s", strerror(errno));
        return;
    }

    fwrite(buf.begin(), 1, buf.size(), fp);
    fclose(fp);
    l.info("Saved service list");
}

void ServiceManager::load ()
{
    stringstream ss;
    ss << model.storageRoot << "/service_list";
    FILE *fp = fopen(ss.str().c_str(), "r");
    if (!fp) {
        l.info("Failed to load service list: %s", strerror(errno));
        return;
    }

    // load file
    Buffer buf;
    while (true) {
        buf.ensureMargin(512);
        auto sz = fread(buf.end(), 1, buf.margin(), fp);
        if (sz == 0) break;
        buf.grow(sz);
    }
    fclose(fp);

    // check header
    auto sz = buf.readI32();
    if (sz != buf.size() || sz < 4 ||
            memcmp(SERVICE_LIST_TYPE, buf.begin(), 4) != 0) {
        l.info("Invalid service list header");
        return;
    }
    buf.drop(4);

    // clear any existing service list from memory.
    clear();

    while (buf.size() > 0) {
        // read location
        int len = buf.readI32();
        string loc = {(char*)buf.begin(), (size_t)len};
        buf.drop(len);
        acba::Ref<TransportStream> ts = new TransportStream(loc);
        model.transportStreamByLocation[loc] = ts;

        // read TSInfo
        len = buf.readI32();
        uint8_t version = buf.readI8();
        uint16_t tsid = buf.readI16();
        acba::Ref<VCTInfo> vinfo = new VCTInfo(version, tsid);
        ts->vctInfo = vinfo;
        len -= 3;

        while ( len > 0 ) {
            int secLen  = buf.readI32();
            processVct(buf.begin(), secLen, loc);
            buf.drop(secLen);
            len = len - secLen - 4;
        }
        model.transportStreamUpdate.notify(ts);
    }

    // Suppress auto saver from triggering by this loading.
    task.eventDriver.unsetTimeout(&autoSaver);

    l.info("Loaded service list");
}

void ServiceManager::clear ()
{
    model.transportStreams.clear();
    model.transportStreamByLocation.clear();
    model.selectService(nullptr);
    model.transportStreamUpdate.notify(nullptr);

    l.info("Cleared service list");
}

void ServiceManager::reselect ()
{
    task.eventDriver.setTimeout(0, &selector);
}

void ServiceManager::processVct (const byte_t *data, int sz, const string &loc)
{
    auto tsi = model.transportStreamByLocation.find(loc);
    if (tsi == model.transportStreamByLocation.end()) {
        l.warn("processVct(loc=%s): Failed to find TS", loc.c_str());
        return;
    }
    auto &ts = model.transportStreamByLocation[loc];
    ts->services.clear();

    acba::Ref<Service> svc;
    int numCh = NUM_CHANNELS(data);
    l.info("num_channels_in_section : %d", numCh);
    int pos = 10;
    char16_t rawName[8];
    rawName[7] = 0;
    std::wstring_convert<std::codecvt_utf8_utf16<char16_t>, char16_t> converter;
    for (int i = 0; i < numCh; i++) {
        uint16_t progNum = ((data[pos+24] << 8) | (data[pos+25]));
        uint16_t flags = ((data[pos+26] << 8) | (data[pos+27]));
        uint16_t sourceId = ((data[pos+28] << 8) | (data[pos+29]));
        l.info("Flag 0x%04X", flags);
        if (isHiddenSvc(flags)) {
            int descLen = (((data[pos+30] & 0x03) << 8) | (data[pos+31]));
            pos += (descLen+32);
            continue;
        }
        svc = new Service(loc, progNum, sourceId);
        svc->majorNum = (((data[pos+14]&0x0F) << 6) | (data[pos+15] >> 2));
        svc->minorNum = (((data[pos+15]&0x03) << 8) | data[pos+16]);
        svc->tsid = TRANSPORT_STREAM_ID(data);
        for (int i = 0; i < 7; i++) {
            rawName[i] = (((data[pos+i*2]) << 8) | (data[pos+i*2+1]));
        }
        svc->name = converter.to_bytes(rawName);

        ts->services[progNum] = svc;
        l.info("Service[%d.%d:%s] addd(proNum=%d)", svc->majorNum, svc->minorNum, svc->name.c_str(), progNum);
        int descLen = (((data[pos+30] & 0x03) << 8) | (data[pos+31]));
        pos += (descLen+32);
    }
    ts->dump();
}

void ServiceManager::AutoSaver::processEvent (int events)
{
    l.info("Auto saving...");

    auto self = container_of(this, &ServiceManager::autoSaver);
    self->save();
}
