#include "service_manager.h"
#include "atsc1_task.h"
#include "acba.h"

using namespace std;
using namespace atsc1;
using namespace acba;

static Log l("srv1");

void ServiceTask::start ()
{
    auto &ts = model.transportStreamByLocation[service->tsLoc];
    assert(ts);

    ScopedLock sl(mutex);
    task.serviceManager.tuneHandler.update.addListener(this);
    TuneHandler::TUNE_REQ_RES res
            = task.serviceManager.tuneHandler.requestTune(ts->location);
    switch (res) {
    case TuneHandler::TUNE_REQ_SUCCESS:
        l.info("state = SVC_SEL_WAIT_TUNE");
        status.state = SVC_SEL_WAIT_TUNE;
        // will continue when tune event is delivered
        break;
    case TuneHandler::TUNE_REQ_ALREADY_TUNED:
        // will continue immediately
        l.info("state = SVC_SEL_TUNED");
        status.state = SVC_SEL_TUNED;
        task.eventDriver.setTimeout(0, this);
        break;
    case TuneHandler::TUNE_REQ_NO_RESOURCE:
        l.info("state = SVC_SEL_FAIL(NO RESC)");
        status.state = SVC_SEL_FAIL;
        status.reason = SVC_SEL_FAIL_NO_RESOURCE;
        task.serviceManager.currentServiceTaskUpdate.notify();
        break;
    }
}

void ServiceTask::stop ()
{
    ScopedLock sl(mutex);
    if (mediaTask) {
        mediaTask->stop();
        mediaTask = 0;
    }
    l.info("state = SVC_SEL_STOP");
    status.state = SVC_SEL_STOPPED;
    task.serviceManager.tuneHandler.update.removeListener(this);
}

void ServiceTask::onUpdate (Tune::EVENT e)
{
    static map<Tune::EVENT, const string> strs = {
        {Tune::EVENT_TUNER_LOCKED, "EVENT_TUNER_LOCKED"},
        {Tune::EVENT_TUNER_UNLOCKED, "EVENT_TUNER_UNLOCKED"},
        {Tune::EVENT_TS_LIST_CHANGED, "EVENT_TS_LIST_CHANGED"},
    };

    auto ni = strs.find(e);
    if (ni == strs.end()) {
        l.warn("Unknown tune event:%d", e);
        return;
    }
    l.info("%s", ni->second.c_str());

    ScopedLock sl(mutex);
    switch (e) {
    case Tune::EVENT_TUNER_LOCKED:
        l.info("Received Tuner Locked (state=%d)", status.state);
        if (status.state == SVC_SEL_WAIT_TUNE) {
            l.info("state = SVC_SEL_TUNED");
            status.state = SVC_SEL_TUNED;
            task.eventDriver.setTimeout(0, this);
        }
        break;

    case Tune::EVENT_TUNER_UNLOCKED:
        l.info("Received Tuner Unlocked (state=%d)", status.state);
        if (status.state == SVC_SEL_WAIT_TUNE) {
            l.info("state = SVC_SEL_FAIL(TUNE FAIL)");
            status.state = SVC_SEL_FAIL;
            status.reason= SVC_SEL_FAIL_TUNE_FAIL;
            task.serviceManager.currentServiceTaskUpdate.notify();
        }
// TODO: handle tuned away condition
        break;

    default:
        l.info("Received Tuner evt=%d (state=%d)", e, status.state);
        break;
    }
}

void ServiceTask::onUpdate (const acba::Ref<Service>& srv)
{
    ScopedLock sl(mutex);
    switch (status.state) {
    case SVC_SEL_WAIT_SI:
        l.info("state = SVC_SEL_SI_READY");
        status.state = SVC_SEL_SI_READY;
    case SVC_SEL_SI_READY:
    case SVC_SEL_SELECTED:
        task.eventDriver.setTimeout(0, this);
    }
}

void ServiceTask::processEvent (int events)
{
    l.info("In ServiceTask::processEvent(state=%d)", status.state);
    acba::Ref<ServiceMediaInfo> minfo;
    ScopedLock sl(mutex);

    switch (status.state) {
    case SVC_SEL_TUNED:
        service->mediaInfoUpdate.addListener(this);
        minfo = service->mediaInfo;
        if (!minfo) {
            l.info("state = SVC_SEL_WAIT_SI");
            status.state = SVC_SEL_WAIT_SI;
            return;
        }
        status.state = SVC_SEL_SI_READY;
        break;
    case SVC_SEL_SI_READY:
    case SVC_SEL_SELECTED:
        minfo = service->mediaInfo;
        if (!minfo) {
            l.warn("state is %d, but servce media info unavail", status.state);
            l.warn("state = SVC_SEL_FAIL(CONTENT NF)");
            status.state = SVC_SEL_FAIL;
            status.reason = SVC_SEL_FAIL_CONTENT_NOT_FOUND;
            task.serviceManager.currentServiceTaskUpdate.notify();
            return;
        }
        break;
    default:
        l.warn("state is invalid");
        return;
    }
    l.info("Selected Service Info");
    l.info("- pmt PID : %d", minfo->pmtPid);
    l.info("- PCR PID : %d", minfo->pcrPid);
    for (auto e: minfo->eleStreams) {
        l.info("- ES pid=%d, type=%d", e->pid, e->streamType);
    }

    if (!mediaTask) {
        mediaTask = new MediaTask(service,
                task.serviceManager.tuneHandler.tune);
        mediaTask->start();
    } else {
        mediaTask->update();
    }
    l.info("state = SVC_SEL_SELECTED");
    status.state = SVC_SEL_SELECTED;
    task.serviceManager.currentServiceTaskUpdate.notify();
}
