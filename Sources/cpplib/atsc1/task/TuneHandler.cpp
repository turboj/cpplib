#include "service_manager.h"
#include "atsc1_task.h"
#include "acba.h"

using namespace std;
using namespace atsc1;
using namespace acba;

static Log l("tnhdl1");

TuneHandler::TUNE_REQ_RES TuneHandler::requestTune (const std::string &loc)
{
    if (tune) {
        if (tune->location == loc) {
            l.info("Already tuned to %s", loc.c_str());
            return TUNE_REQ_ALREADY_TUNED;
        }
        tune->removeListener(this);
        task.tuneManager.releaseTune(tune);
        tune = nullptr;
    }
    tune = task.tuneManager.requestTune(loc);
    if (!tune) {
        l.error("Tuning %s failed.", loc.c_str());
        return TUNE_REQ_NO_RESOURCE;
    }
    tune->addListener(this);
    if (tune->isLocked()) {
        l.info("Already tuned to %s", loc.c_str());
        return TUNE_REQ_ALREADY_TUNED;
    }
    return TUNE_REQ_SUCCESS;
}

void TuneHandler::releaseTune ()
{
    if (tune) {
        tune->removeListener(this);
        task.tuneManager.releaseTune(tune);
        update.notify(Tune::EVENT_TUNER_UNLOCKED);
        tune = nullptr;
    }
}

void TuneHandler::onUpdate (Tune::EVENT e)
{
    static map<Tune::EVENT, const string> strs = {
        {Tune::EVENT_TUNER_LOCKED, "EVENT_TUNER_LOCKED"},
        {Tune::EVENT_TUNER_UNLOCKED, "EVENT_TUNER_UNLOCKED"},
        {Tune::EVENT_TS_LIST_CHANGED, "EVENT_TS_LIST_CHANGED"},
    };

    auto ni = strs.find(e);
    if (ni == strs.end()) {
        l.warn("Unknown tune event:%d", e);
        return;
    }
    l.info("%s", ni->second.c_str());

    update.notify(e);
}
