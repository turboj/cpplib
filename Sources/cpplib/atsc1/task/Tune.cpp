#include "acba.h"
#include "tune_manager.h"

using namespace atsc1;
using namespace acba;

void Tune::addListener (acba::Listener<EVENT> *l)
{
    ScopedLock sl(mutex);
    update.addListener(l);
}

void Tune::removeListener (acba::Listener<EVENT> *l)
{
    ScopedLock sl(mutex);
    update.removeListener(l);
}

void Tune::notify (EVENT e)
{
    ScopedLock sl(mutex);
    update.notify(e);
}
