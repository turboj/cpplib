#include <set>
#include <map>

#include "acba.h"
#include "nlohmann/json.hpp"
#include "JsonRef.h"
#include "unifiedtv.h"
#include "unifiedtv_atsc1.h"
#include "atsc1_multiple_string.h"
#include "atsc1_model.h"
#include "atsc1_task.h"

using namespace std;
using namespace acba;
using namespace unifiedtv;
using json = nlohmann::json;

static Log l("unifiedtv_atsc1", "esg: ");

static atsc1::RatingInfo *defRatingInfo = nullptr;

namespace {

struct CallbackNotifyEsgUpdate: RpcNotificationHandler, Reactor,
        Listener<const Ref<atsc1::TransportStream>&> {

    CallbackNotifyEsgUpdate (): listening(false) {}

    set<RpcPeer*> peers;
    set<string> serviceIds;

    bool subscribe (const string &type, const Ref<RpcPeer> &peer) override {
        if (!listening) {
            atsc1::model.eitUpdate.addListener(this);
            listening = true;
        }
        return peers.insert(peer.get()).second;
    }

    bool unsubscribe (const string &type, const Ref<RpcPeer> &peer) override {
        return peers.erase(peer.get()) > 0;
    }

    void onUpdate (const Ref<atsc1::TransportStream>& ts) override {
        for (auto &s: ts->services) {
            auto &gid = s.second->srvLoc;
            if (gid.empty()) continue;
            serviceIds.insert(gid);
        }
        atsc1::task.eventDriver.setTimeout(1, this);
    }

    void processEvent (int evt) override {
        json sids = json::array();
        for (auto &sid: serviceIds) sids.push_back("atsc1:" + sid);
        serviceIds.clear();

        json msg = {
            {"jsonrpc", "2.0"},
            {"method", "notifyEsgUpdate"},
            {"params", {
                {"serviceIds", sids}
            }}
        };

        for (auto p: peers) p->sendMessage(msg);
    }

private:
    bool listening;
};

}
#define GPS_UTC_BASE_IN_SECONDS 315964800
#define GPS_UTC_OFFSET 18

static uint32_t convert_gps2epoch (uint32_t gps) {
    if (gps > numeric_limits<uint32_t>::max() - GPS_UTC_BASE_IN_SECONDS) {
        return numeric_limits<uint32_t>::max();
    }
    return (gps + GPS_UTC_BASE_IN_SECONDS - GPS_UTC_OFFSET);
}

static uint32_t convert_epoch2gps (uint32_t ep) {
    if (ep < GPS_UTC_BASE_IN_SECONDS) return 0;
    return (ep - GPS_UTC_BASE_IN_SECONDS + GPS_UTC_OFFSET);
}

static void initDefRatingInfo() {
    l.info("initialize default rating info");
    defRatingInfo = new atsc1::RatingInfo(0, 1);
    defRatingInfo->regionName = "U.S. (50 states + possessions)";

    atsc1::RatingDimension *dim;
    dim = new atsc1::RatingDimension();
    dim->dimName = "Entire Audience";
    dim->abbrRatingValues[0] = "";
    dim->abbrRatingValues[1] = "None";
    dim->abbrRatingValues[2] = "TV-G";
    dim->abbrRatingValues[3] = "TV-PG";
    dim->abbrRatingValues[4] = "TV-14";
    dim->abbrRatingValues[5] = "TV-MA";
    dim->ratingValues[0] = "";
    dim->ratingValues[1] = "None";
    dim->ratingValues[2] = "TV-G";
    dim->ratingValues[3] = "TV-PG";
    dim->ratingValues[4] = "TV-14";
    dim->ratingValues[5] = "TV-MA";
    defRatingInfo->dimensions[0] = dim;

    dim = new atsc1::RatingDimension();
    dim->dimName = "Dialogue";
    dim->abbrRatingValues[0] = "";
    dim->abbrRatingValues[1] = "D";
    dim->ratingValues[0] = "";
    dim->ratingValues[1] = "D";
    defRatingInfo->dimensions[1] = dim;

    dim = new atsc1::RatingDimension();
    dim->dimName = "Language";
    dim->abbrRatingValues[0] = "";
    dim->abbrRatingValues[1] = "L";
    dim->ratingValues[0] = "";
    dim->ratingValues[1] = "L";
    defRatingInfo->dimensions[2] = dim;

    dim = new atsc1::RatingDimension();
    dim->dimName = "Sex";
    dim->abbrRatingValues[0] = "";
    dim->abbrRatingValues[1] = "S";
    dim->ratingValues[0] = "";
    dim->ratingValues[1] = "S";
    defRatingInfo->dimensions[3] = dim;

    dim = new atsc1::RatingDimension();
    dim->dimName = "Violence";
    dim->abbrRatingValues[0] = "";
    dim->abbrRatingValues[1] = "V";
    dim->ratingValues[0] = "";
    dim->ratingValues[1] = "V";
    defRatingInfo->dimensions[4] = dim;

    dim = new atsc1::RatingDimension();
    dim->dimName = "Children";
    dim->abbrRatingValues[0] = "";
    dim->abbrRatingValues[1] = "TV-Y";
    dim->abbrRatingValues[2] = "TV-Y7";
    dim->ratingValues[0] = "";
    dim->ratingValues[1] = "TV-Y";
    dim->ratingValues[2] = "TV-Y7";
    defRatingInfo->dimensions[5] = dim;

    dim = new atsc1::RatingDimension();
    dim->dimName = "Fantasy Violence";
    dim->abbrRatingValues[0] = "";
    dim->abbrRatingValues[1] = "FV";
    dim->ratingValues[0] = "";
    dim->ratingValues[1] = "FV";
    defRatingInfo->dimensions[6] = dim;

    dim = new atsc1::RatingDimension();
    dim->dimName = "MPAA";
    dim->abbrRatingValues[0] = "";
    dim->abbrRatingValues[1] = "N/A";
    dim->abbrRatingValues[2] = "G";
    dim->abbrRatingValues[3] = "PG";
    dim->abbrRatingValues[4] = "PG-13";
    dim->abbrRatingValues[5] = "R";
    dim->abbrRatingValues[6] = "NC-17";
    dim->abbrRatingValues[7] = "X";
    dim->abbrRatingValues[8] = "NR";
    dim->ratingValues[0] = "";
    dim->ratingValues[1] = "MPAA Rating Not Applicable";
    dim->ratingValues[2] = "Suitable for All Ages";
    dim->ratingValues[3] = "Parental Guidance Suggested";
    dim->ratingValues[4] = "Parents Strongly Cautioned";
    dim->ratingValues[5] = "Restricted, under 17 must be accompanied by adult";
    dim->ratingValues[6] = "No One 17 and Under Admitted";
    dim->ratingValues[7] = "No One 17 and Under Admitted";
    dim->ratingValues[8] = "Not Rated by MPAA";
    defRatingInfo->dimensions[7] = dim;
}

static unifiedtv_atsc1::Registry r[] = {

    {"queryEsg", [] (const JsonRef req, const Ref<RpcPeer> peer) {

        l.info("queryEsg called");
        auto params = req["params"];

        auto jSrvs = params["serviceIds"];
        set<string> sids;
        for (int i = 0; ; i++) {
            auto srv = jSrvs[i];
            if (!srv.isString()) break;
            sids.insert(srv.asString());
        }
        uint32_t startTime = params["startTime"].toU32(0);
        uint32_t endTime
                = params["endTime"].toU32(numeric_limits<uint32_t>::max());
        startTime = convert_epoch2gps(startTime);
        endTime = convert_epoch2gps(endTime);

        json result = json::object();
        for (auto &ts: atsc1::model.transportStreamByLocation) {
            for (auto &s: ts.second->services) {
                string sid = string("atsc1:") + s.second->srvLoc;
                if (sids.find(sid) == sids.end()) {
                    l.info("no matched service: %s", sid.c_str());
                    continue;
                }
                for (auto &ei: ts.second->eits) {
                    auto &eii = ei.second;
                    if (eii.find(s.second->sourceId) == eii.end()) {
                        l.info("no matched eit: sId=%d", s.second->sourceId);
                        continue;
                    }
                    auto &tab = eii[s.second->sourceId];
                    for (auto &sect: tab->sections) {
                        // parse section and create program events
                        byte_t *buf = sect.second->data;
                        uint8_t numevt = buf[9];
                        uint32_t pos = 10;
                        for (int i = 0; i < numevt; i++) {
                            uint32_t st = ((buf[pos+2] << 24)
                                    | (buf[pos+3] << 16) | (buf[pos+4] << 8)
                                    | buf[pos+5]);
                            uint32_t dur = (((buf[pos+6] & 0x0F) << 16)
                                    | (buf[pos+7] << 8) | buf[pos+8]);
                            uint32_t et = st + dur;
                            uint32_t b = max(startTime, st);
                            uint32_t e = min(endTime, et);
                            if (b >= e) {
                                l.info("st=%d et=%d doesn't match");
                                continue;
                            }
                            st = convert_gps2epoch(st);
                            et = convert_gps2epoch(et);

                            string title;
                            int tlen = buf[pos+9];
                            atsc1::MultipleString titleMs(&(buf[pos+10]), tlen);
                            if (titleMs.decoded.size() > 0) {
                                title = titleMs.decoded.begin()->second;
                            }
                            l.info("title: %s", title.c_str());

                            string desc; // description from ett
                            uint16_t evtId = (((buf[pos] & 0x3F) << 8)
                                    | buf[pos+1]);
                            uint32_t etmId = (s.second->sourceId << 16)
                                    | (evtId << 2) | 0x02;
                            l.info("etmId = 0x%08X", etmId);
                            byte_t *desc_ptr = nullptr;
                            int dlen = 0;
                            for (auto &eti: ts.second->etts) {
                                auto &etii = eti.second;
                                if (etii.find(etmId) == etii.end()) continue;
                                auto &ett_t = etii[etmId];
                                byte_t *ett_b = ett_t->sections[0]->data;
                                int secLen = ((ett_b[1] & 0x0F) | ett_b[2]);
                                dlen = ett_t->sections[0]->dataLen - 17;
                                l.debug("ETT found : dlen = %d", dlen);
                                if (dlen > 0) {
                                    desc_ptr = &(ett_b[13]);
                                    break;
                                }
                            }
                            if (desc_ptr != nullptr) {
                                atsc1::MultipleString descMs(desc_ptr, dlen);
                                if (descMs.decoded.size() > 0) {
                                    desc = descMs.decoded.begin()->second;
                                }
                            }
                            l.info("desc: %s", desc.c_str());

// TODO : get rating from RTT and eit content advisory descripor
                            string rating = "";
                            pos += (10 + tlen);
                            int deslen = (((buf[pos] & 0x0F) << 8)
                                    | buf[pos+1]);
                            pos += 2;
                            int despos = 0;
                            while (despos < deslen) {
                                if (buf[pos+despos] != 0x87) {
                                    despos += (buf[pos+despos+1] + 2);
                                    continue;
                                }
                                l.info("content advisory desc found");
                                uint8_t nreg = buf[pos+despos+2] & 0x3F;
                                despos += 3;
                                for (uint8_t j = 0; j < nreg; j++) {
                                    uint8_t rat = buf[pos+despos];
                                    uint8_t nd = buf[pos+despos+1];
                                    auto ri = ts.second->ratingInfos.find(rat);
                                    atsc1::RatingInfo *rif = nullptr;
                                    if (ri == ts.second->ratingInfos.end()) {
                                        if (rat == 1) {
                                            l.info("Use default RatingInfo");
                                            if (defRatingInfo == nullptr) {
                                                initDefRatingInfo();
                                            }
                                            rif = defRatingInfo;
                                        }
                                    } else {
                                        rif = ri->second;
                                    }

                                    if (rif == nullptr || nd == 0) {
                                        l.info("no assoc ratingInfo : %d", rat);
                                        despos += (2 + nd * 2);
                                        despos += (1 + buf[pos+despos]);
                                        continue;
                                    }

                                    if (rif->dimensions.find(buf[pos+despos+2]) != rif->dimensions.end()) {
                                        auto di = rif->dimensions[buf[pos+despos+2]];
                                        rating = di->abbrRatingValues[buf[pos+despos+3] & 0x0F];
                                        l.info("dim=%d val=%d", buf[pos+despos+2], buf[pos+despos+3] & 0x0F);
                                    }
                                    break;
                                }
                                break;
                            }
                            pos += deslen;

                            auto &arr = result[sid];
                            if (!arr.is_array()) arr = json::array();

                            arr.push_back({
                                {"title", title},
                                {"start", st},
                                {"end", et},
                                {"description", desc},
                                {"ratings", rating}
                            });
                        }
                    }
                }
            }
        }

        for (auto &ts: atsc1::model.transportStreamByLocation) {
            for (auto &s: ts.second->services) {
                string sid = string("atsc1:") + s.second->srvLoc;
                if (sids.count(sid) == 0) continue;

                auto &arr = result[sid];
                if (!arr.is_array()) arr = json::array();
            }
        }

        peer->sendMessage({
            {"jsonrpc", "2.0"},
            {"result", result},
            {"id", *req["id"]}
        });
    }},

    {"notifyEsgUpdate", new CallbackNotifyEsgUpdate()},
};
