#include "acba.h"
#include "atsc1_task.h"
#include "unifiedtv_atsc1.h"

using namespace acba;
using namespace unifiedtv_atsc1;

static Log l("unifiedtv_atsc1");

Adaptor &unifiedtv_atsc1::adaptor ()
{
    static Adaptor instance;
    return instance;
}

void unifiedtv_atsc1::start ()
{
    l.info("Starting UnifiedTV Adaptor...");
    ::atsc1::task.httpServer.bind("/unifiedtv_atsc1", &adaptor().rpcManager);
}
