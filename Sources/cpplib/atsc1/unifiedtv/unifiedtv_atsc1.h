#ifndef UNIFIEDTV_ATSC1_H
#define UNIFIEDTV_ATSC1_H

#include <string>
#include <functional>

#include "unifiedtv.h"

namespace unifiedtv_atsc1 {

struct Adaptor {
    unifiedtv::RpcManager rpcManager;
};

void start ();

Adaptor &adaptor();

struct Registry {
    Registry (const std::string &name, unifiedtv::RpcHandler h) {
        adaptor().rpcManager.registerHandler(name, h);
    }
    Registry (const std::string &name, unifiedtv::RpcNotificationHandler *rnh) {
        adaptor().rpcManager.registerNotificationHandler(name, rnh);
    }
};

}
#endif
