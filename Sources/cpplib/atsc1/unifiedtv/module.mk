$(call begin-target, unifiedtv_atsc1, staticlib)
    LOCAL_SRCS := $(wildcard $(LOCAL_PATH)/*.cpp)
    LOCAL_IMPORTS := acba-staticlib json-nlohmann unifiedtv-staticlib
    LOCAL_IMPORTS += atsc1_model-staticlib atsc1_task-staticlib
    LOCAL_CXXFLAGS := -DUSE_UNIFIEDTV_ATSC1=1

    LOCAL_WHOLE_ARCHIVE := true

    EXPORT_CXXFLAGS := -I$(LOCAL_PATH) $(LOCAL_CXXFLAGS)
    EXPORT_IMPORTS := $(LOCAL_IMPORTS)
$(end-target)
