#include <set>
#include <map>

#include "acba.h"
#include "nlohmann/json.hpp"
#include "JsonRef.h"
#include "unifiedtv.h"
#include "unifiedtv_atsc1.h"
#include "atsc1_model.h"
#include "atsc1_task.h"

using namespace std;
using namespace acba;
using namespace unifiedtv;
using json = nlohmann::json;

static Log l("unifiedtv_atsc1", "playback: ");

namespace {

struct CallbackUpdateComponents: RpcNotificationHandler {

    set<RpcPeer*> peers;

    bool subscribe (const string &type, const Ref<RpcPeer> &peer) override {
        return peers.insert(peer.get()).second;
    }

    bool unsubscribe (const string &type, const Ref<RpcPeer> &peer) override {
        return peers.erase(peer.get()) > 0;
    }
};

}

static unifiedtv_atsc1::Registry r[] = {

    {"setComponentsPreference", [] (const JsonRef req, const Ref<RpcPeer> peer) {
        peer->sendMessage({
            {"jsonrpc", "2.0"},
            {"result", {}},
            {"id", *req["id"]}
        });
    }},

    {"updateComponents", new CallbackUpdateComponents()},
};
