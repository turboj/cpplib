#include <set>
#include <map>

#include "acba.h"
#include "nlohmann/json.hpp"
#include "JsonRef.h"
#include "unifiedtv.h"
#include "unifiedtv_atsc1.h"
#include "atsc1_model.h"
#include "atsc1_task.h"

using namespace std;
using namespace acba;
using namespace unifiedtv;
using json = nlohmann::json;

static Log l("unifiedtv_atsc1", "servicelist: ");

namespace {

struct CallbackUpdateSelection: RpcNotificationHandler, Listener<> {

    set<RpcPeer*> peers;

    CallbackUpdateSelection (): listening(false) {}

    bool subscribe (const string &type, const Ref<RpcPeer> &peer) override {
        if (!listening) {
            atsc1::task.serviceManager.currentServiceTaskUpdate.addListener(this);
            listening = true;
        }

        return peers.insert(peer.get()).second;
    }

    bool unsubscribe (const string &type, const Ref<RpcPeer> &peer) override {
        return peers.erase(peer.get()) > 0;
    }

    void onUpdate () override {
        auto &smcst = atsc1::task.serviceManager.currentServiceTask;

        json sid = nullptr;
        json st = "selecting";
        json err = nullptr;
        if (smcst) {
            sid = "atsc1:" + smcst->service->srvLoc;
            switch (smcst->status.state) {
            case atsc1::ServiceTask::SVC_SEL_SELECTED:
                st = "selected";
                break;
            case atsc1::ServiceTask::SVC_SEL_FAIL:
                st = "error";
                switch (smcst->status.reason) {
                case atsc1::ServiceTask::SVC_SEL_FAIL_NO_RESOURCE:
                    err = {{"code", -2}, {"message", "Not enough resource"}};
                    break;
                case atsc1::ServiceTask::SVC_SEL_FAIL_TUNE_FAIL:
                case atsc1::ServiceTask::SVC_SEL_FAIL_CONTENT_NOT_FOUND:
                    err = {{"code", -4}, {"message", "Content not found"}};
                    break;
                default:
                    err = {{"code", -6}, {"message", "Service not found"}};
                    break;
                }
                break;
            }
        } else {
            st = "selected";
        }

        json msg;
        if (err != nullptr) {
            msg = {
                {"jsonrpc", "2.0"},
                {"method", "updateSelection"},
                {"params", {
                    {"serviceId", sid},
                    {"state", st},
                    {"error", err}
                }}
            };
        } else {
            msg = {
                {"jsonrpc", "2.0"},
                {"method", "updateSelection"},
                {"params", {
                    {"serviceId", sid},
                    {"state", st}
                }}
            };
        }

        for (auto p: peers) p->sendMessage(msg);
    }

private:
    bool listening;
};

struct CallbackUpdateServiceList: RpcNotificationHandler, Reactor,
        Listener<const Ref<atsc1::TransportStream>&> {

    struct ServiceInfo {
        ServiceInfo (): majorNumber(-1), minorNumber(-1) {}
        string name;
        int majorNumber;
        int minorNumber;
    };
    typedef map<string, ServiceInfo> ServiceList;

    map<RpcPeer*, ServiceList> peers;

    CallbackUpdateServiceList (): listening(false) {}

    bool subscribe (const string &type, const Ref<RpcPeer> &peer) override {
        if (!listening) {
            atsc1::model.transportStreamUpdate.addListener(this);
            listening = true;
        }

        auto pi = peers.find(peer.get());
        if (pi != peers.end()) return false;

        peers[peer.get()]; // create empty service list
        atsc1::task.eventDriver.setTimeout(0, this);
        return true;
    }

    bool unsubscribe (const string &type, const Ref<RpcPeer> &peer) override {
        return peers.erase(peer.get()) > 0;
    }

    void onUpdate (const Ref<atsc1::TransportStream> &ts) override {
        atsc1::task.eventDriver.setTimeout(0, this);
    }

    void processEvent (int evt) override {
        ServiceList lst;

        // contruct service list
        for (auto &ts: atsc1::model.transportStreamByLocation) {
            for (auto &s: ts.second->services) {
                auto &srv = *s.second;
                auto &gid = srv.srvLoc;
                if (gid.empty()) continue;
                auto &si = lst[gid];

                si.name = srv.name;
                si.majorNumber = srv.majorNum;
                si.minorNumber = srv.minorNum;
            }
        }

        for (auto &p: peers) syncPeer(p.first, p.second, lst);
    }

    void syncPeer (RpcPeer *p, ServiceList &lst, const ServiceList &nlst) {
        json adds = json::array();
        json mods = json::array();
        json dels = json::array();

        // check removals
        for (auto &si: lst) {
            if (nlst.find(si.first) != nlst.end()) continue;
            dels.push_back({
                {"id", "atsc1:" + si.first}
            });
        }

        // check adds/mods
        for (auto &nsi: nlst) {
            auto si = lst.find(nsi.first);
            auto &ns = nsi.second;
            bool exists = (si != lst.end());
            if (exists && si->second.name == ns.name && si->second.majorNumber == ns.majorNumber && si->second.minorNumber == ns.minorNumber) {
                // no change
                continue;
            }

            json s = {
                {"id", "atsc1:" + nsi.first},
                {"domainId", "atsc1"},
                {"name", ns.name}
            };
            if (ns.majorNumber >= 0) s["major"] = ns.majorNumber;
            if (ns.minorNumber >= 0) s["minor"] = ns.minorNumber;

            if (exists) {
                mods.push_back(s);
            } else {
                adds.push_back(s);
            }
        }

        // abort if no change occurred
        if (adds.size() == 0 && mods.size() == 0 && dels.size() == 0) {
            return;
        }

        lst = nlst;
        p->sendMessage({
            {"jsonrpc", "2.0"},
            {"method", "updateServiceList"},
            {"params", {
                {"add", adds},
                {"remove", dels},
                {"modify", mods}
            }}
        });
    }

private:
    bool listening;
};

}

static bool findAtsc1 (const JsonRef ids)
{
    if (ids.isNull()) return true;

    for (int i = 0; i < ids.length(); i++) {
        if (ids[i].asString() != "atsc1") continue;
        return true;
    }
    return false;
}

static unifiedtv_atsc1::Registry r[] = {

    {"saveServiceList", [] (const JsonRef req, const Ref<RpcPeer> peer) {

        if (findAtsc1(req["params"]["domainIds"])) {
            atsc1::task.serviceManager.save();
        }

        peer->sendMessage({
            {"jsonrpc", "2.0"},
            {"result", {}},
            {"id", *req["id"]}
        });
    }},

    {"loadServiceList", [] (const JsonRef req, const Ref<RpcPeer> peer) {

        if (findAtsc1(req["params"]["domainIds"])) {
            atsc1::task.serviceManager.load();
        }

        peer->sendMessage({
            {"jsonrpc", "2.0"},
            {"result", {}},
            {"id", *req["id"]}
        });
    }},

    {"clearServiceList", [] (const JsonRef req, const Ref<RpcPeer> peer) {

        if (findAtsc1(req["params"]["domainIds"])) {
            atsc1::task.serviceManager.clear();
        }

        peer->sendMessage({
            {"jsonrpc", "2.0"},
            {"result", {}},
            {"id", *req["id"]}
        });
    }},

    {"select", [] (const JsonRef req, const Ref<RpcPeer> peer) {
        auto &usid = req["params"].asString();

        atsc1::Service *srv = nullptr;

        if (usid.rfind("atsc1:", 0) == 0) {
            auto gsid = usid.substr(6);
            for (auto ts: atsc1::model.transportStreamByLocation) {
                for (auto s: ts.second->services) {
                    if (s.second->srvLoc == gsid) srv = s.second;
                }
            }
        }

        if (!usid.empty() && !srv) {
            l.warn("Service not found by %s. Unselecting.", usid.c_str());
        }

        // FIX ME: task.rmp.setScalePosition(100, 0, 0);
        atsc1::model.selectService(srv);
    }},

    {"updateSelection", new CallbackUpdateSelection()},
    {"updateSerivceList", new CallbackUpdateServiceList()},
};
