#include <set>
#include <map>

#include "acba.h"
#include "nlohmann/json.hpp"
#include "JsonRef.h"
#include "unifiedtv.h"
#include "unifiedtv_atsc1.h"
#include "atsc1_model.h"
#include "atsc1_task.h"

using namespace std;
using namespace acba;
using namespace unifiedtv;
using json = nlohmann::json;

static Log l("unifiedtv_atsc1", "scanning: ");

namespace {

static Reactor *notifier = nullptr;

struct Monitor: Listener<atsc1::Tune::EVENT> {
    Monitor (): useCount(0), locked(false) {}
    Ref<atsc1::Tune> tune;
    bool locked;
    int useCount;

    void onUpdate (atsc1::Tune::EVENT e) override {
        bool l;
        switch (e) {
        case atsc1::Tune::EVENT_TUNER_LOCKED:
            l = true;
            break;
        case atsc1::Tune::EVENT_TUNER_UNLOCKED:
            l = false;
            break;
        case atsc1::Tune::EVENT_TS_LIST_CHANGED:
            return;
        }

        if (locked == l) return;

        locked = l;
        atsc1::task.eventDriver.setTimeout(0, notifier);
    }
};

static map<string, Monitor> monitors;

struct CallbackUpdateTuneState: RpcNotificationHandler, Reactor {

    CallbackUpdateTuneState () {
        notifier = this;
    }

    typedef map<string, bool> TuneList;

    map<RpcPeer*, TuneList> peers;

    bool subscribe (const string &type, const Ref<RpcPeer> &peer) override {
        auto pi = peers.find(peer.get());
        if (pi != peers.end()) return false;

        peers[peer.get()]; // create empty slot
        atsc1::task.eventDriver.setTimeout(0, this);
        return true;
    }

    bool unsubscribe (const string &type, const Ref<RpcPeer> &peer) override {
        return peers.erase(peer.get()) > 0;
    }

    void processEvent (int evt) override {
        for (auto &p: peers) syncPeer(p.first, p.second);
    }

    void syncPeer (RpcPeer *p, TuneList &tl) {

        // Silently remove tunes which are released
        for (auto ti = tl.begin(); ti != tl.end(); ) {
            if (monitors.find(ti->first) == monitors.end()) {
                ti = tl.erase(ti);
            } else {
                ti++;
            }
        }

        // check adds/mods
        for (auto &m: monitors) {
            auto tli = tl.find(m.first);
            auto l = m.second.locked;
            if (tli != tl.end() && l == tli->second) continue;

            tl[m.first] = l;

            p->sendMessage({
                {"jsonrpc", "2.0"},
                {"method", "updateTuneState"},
                {"params", {
                    {"location", m.first},
                    {"domainId", "atsc1"},
                    {"state", l? "locked": "unlocked"},
                }}
            });
        }
    }
};

}

static unifiedtv_atsc1::Registry r[] = {

    {"requestTune", [] (const JsonRef req, const Ref<RpcPeer> peer) {

        l.info("requestTune is called");
        if (req["params"]["domainId"].asString() != "atsc1") {
            peer->sendMessage({
                {"jsonrpc", "2.0"},
                {"error", {
                    {"code", -1},
                    {"message", "Unsupported domain id"}
                }},
                {"id", *req["id"]}
            });
            l.info("domain id is unsupported");
            return;
        }

        auto &loc = req["params"]["location"].asString();

        l.info("loc = %s", loc.c_str());
        // reuse previous tune if exists
        auto mi = monitors.find(loc);
        if (mi != monitors.end()) {
            mi->second.useCount++;
            peer->sendMessage({
                {"jsonrpc", "2.0"},
                {"result", {}},
                {"id", *req["id"]}
            });
            return;
        }

        // request tune
        Ref<atsc1::Tune> t = atsc1::task.tuneManager.requestTune(loc);
        if (!t) {
            peer->sendMessage({
                {"jsonrpc", "2.0"},
                {"error", {
                    {"code", -1},
                    {"message", "failed to tune"}
                }},
                {"id", *req["id"]}
            });
            return;
        }

        // bind to monitor
        auto &m = monitors[loc];
        m.tune = t;
        m.useCount = 1;
        t->addListener(&m);

        // success
        peer->sendMessage({
            {"jsonrpc", "2.0"},
            {"result", {}},
            {"id", *req["id"]}
        });

        // send locked event immediately if the tune was being reused,
        // and also already locked.
        // TODO: This behavior is temporally necessary as we don't provide
        //      tuner status query api for now.
        if (t->isLocked()) {
            m.onUpdate(atsc1::Tune::EVENT_TUNER_LOCKED);
        }
    }},

    {"releaseTune", [] (const JsonRef req, const Ref<RpcPeer> peer) {

        if (req["params"]["domainId"].asString() != "atsc1") return;

        auto &loc = req["params"]["location"].asString();
        auto mi = monitors.find(loc);
        if (mi == monitors.end()) return;

        auto &m = mi->second;
        m.useCount--;
        if (m.useCount > 0) return;

        m.tune->removeListener(&m);
        atsc1::task.tuneManager.releaseTune(m.tune);
        monitors.erase(mi);
    }},

    {"updateTuneState", new CallbackUpdateTuneState()},
};
