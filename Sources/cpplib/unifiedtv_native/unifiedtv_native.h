#ifndef UNIFIEDTV_NATIVE_H
#define UNIFIEDTV_NATIVE_H

#include <stddef.h>
#include <stdbool.h>

#ifdef __cplusplus
extern "C" {
#endif

typedef enum {
    unifiedtv_ERROR_NONE = 0,
    unifiedtv_ERROR_GENERAL,
    unifiedtv_ERROR_INVALID_ARGUMENT,
    unifiedtv_ERROR_ILLEGAL_STATE,
    unifiedtv_ERROR_INSUFFICIENT_RESOURCE,
} unifiedtv_ERROR;

typedef enum {
    unifiedtv_SELECTION_STATE_SELECTING,
    unifiedtv_SELECTION_STATE_SELECTED,
    unifiedtv_SELECTION_STATE_ERROR,
} unifiedtv_SELECTION_STATE;

typedef struct {
    char *id;
    char *display;
    char *description;
} unifiedtv_Domain;

typedef struct {
    char *id;
    char *domainId;
    char *name;
    unsigned int major, minor;
    char *description;
} unifiedtv_Service;

typedef struct {
    struct Audio {
        /**
         * Concatenation of language codes defined in ISO 639-1
         */
        char *languages;
    } audio;

    struct Closedcaption {
        bool enabled;
        /**
         * Concatenation of language codes defined in ISO 639-1
         */
        char *languages;
    } closedcaption;
} unifiedtv_ComponentsPreference;

typedef enum {
    unifiedtv_COMPONENT_TYPE_VIDEO,
    unifiedtv_COMPONENT_TYPE_AUDIO,
    unifiedtv_COMPONENT_TYPE_CLOSEDCAPTION
} unifiedtv_COMPONENT_TYPE;

typedef struct {
    char *serviceId;
    bool selected;
    unifiedtv_COMPONENT_TYPE type;
    char *language; // language code defined in ISO 639-1
    char *mimeType;
} unifiedtv_Component;

typedef struct {
    char *serviceId;
    char *title;
    unsigned int start, end;
    char *description;
    char **ratings;
} unifiedtv_Program;

/**
 * A Collection of callback functions provided by application to be called
 * by stack. All callbacks are called in a dedicated callback thread.
 * Unless explicitly specified, All pointers in arguments are considered
 * to be invalidated after return.
 */
typedef struct {

    /**
     * Notifies resulting state of service selection.
     * @arg err is ERROR_NONE unless @arg state is error.
     * For each service selection,
     * App would usually receive state SELECTING and then SELECTED in order.
     * However, if multiple selection are requested in short intervals,
     * those events for earlier requests may be omitted.
     * Also note that state SELECTED may be received without prior state
     * SELECTING if no time consuming tasks were involved.
     * (especially when unselecting)
     * Note that this method is also called on channel change which was not
     * requested using "select" method. For instance, ATSC3.0 application may
     * request channel change as well.
     */
    void (*updateSelection) (const char *sid, unifiedtv_SELECTION_STATE st,
            unifiedtv_ERROR err);

    /**
     * Receives diffs between stack and app so that app can maintain
     * up-to-date service list information.
     * Note that updateServiceList will be called immeidately when callback is
     * set if the stack has non-empty service list already.
     * Note that removed services has only 'id' as valid field. other fields
     * should be ignored.
     */
    void (*updateServiceList) (const unifiedtv_Service **adds,
            const unifiedtv_Service **removes, const unifiedtv_Service **mods);

    /**
     * Notify currently available/selected components after service selection.
     */
    void (*updateComponents) (const unifiedtv_Component **comps);

    /**
     * Notifies locked/unlocked state for requested tune.
     */
    void (*updateTuneState) (const char *location, bool locked,
            const char *domainId);

    /**
     * Notifies that cached component information has changed for the given
     * services.
     * Updated component cache information can be retrieved using
     * "queryComponentCache".
     * When component information of services added by "updateServiceList" is
     * available, "notifyComponentCacheUpdate" will be called.
     */
    void (*notifyComponentCacheUpdate) (const char **sids);

    /**
     * Notifies that the some guide information has changed for given services.
     * Updated information can be retrieved using "queryEsg".
     * In case of service add/removal, this may not be called. instead,
     * You should monitor "updateServiceList" as well.
     */
    void (*notifyEsgUpdate) (const char **sids);
} unifiedtv_UnifiedTvCallbacks;

/**
 * A Collection of functions provided by stack to be called by app.
 * Every function call share a single mutex and returns immediately non-blocked.
 * Unless explicitly specified, All pointers in arguments are not used
 * after return.
 */
typedef struct {

    int apiVersion;

    /**
     * Sets callback functions to be called by this stack.
     * Function pointers are copied into the stack.
     * Note that if any pending notifications exist, corresponding callback
     * will be called immediately from callback thread.
     */
    unifiedtv_ERROR (*setCallbacks) (const unifiedtv_UnifiedTvCallbacks *cbs);

    /**
     * Query supported TV domains from the device. Domain id can be used to
     * find where the service comes from. (refer "updateServiceList")
     * On success, (*d) refers null-terminated array of type *Domain
     * If given buffer size is not sufficient, this returns
     * ERROR_INSUFFICIENT_RESOURCE. Then app can call this again with larger
     * buffer.
     */
    unifiedtv_ERROR (*queryDomains) (unifiedtv_Domain ***d,
            void *buf, size_t bufsz);

    /**
     * Request service selection by id.
     * If null is given, it is interpreted as unselect.
     * Result can be received via callback updateSelection.
     */
    unifiedtv_ERROR (*select) (const char *sid);

    /**
     * Request saving of current service list which can be loaded back
     * using "loadServiceList".
     */
    void (*saveServiceList) ();

    /**
     * Request the stack to load service list from previously saved service
     * list. Existing service list at runtime is removed ahead.
     */
    void (*loadServiceList) ();

    /**
     * Request the stack to flush out service list beinbg managed in runtime.
     */
    void (*clearServiceList) ();

    /**
     * Set preference for service components selection.
     * If no matching component is determined, component selection for
     * such type will be implementation dependent.
     * Note that this method is not required to be persistent. Thus app should
     * call ahead before starting any service.
     */
    void (*setComponentsPreference) (const unifiedtv_ComponentsPreference *cp);

    /**
     * Request tuning to given location.
     * Format of "location" is domain specifc.
     * Before getting initial call, App should regard tuner state as "unlocked".
     * Any tune request should be paired with the method "releaseTune".
     */
    void (*requestTune) (const char *location, const char *domainId);

    /**
     * Release resources allocated for prior tune request using "requestTune".
     */
    void (*releaseTune) (const char *location, const char *domainId);

    /**
     * Queries cached component information which meets the condition given
     * by service id list.
     * For the services which are no longer valid or valid but having no cached
     * component yet, result may not include component information for such
     * services.
     * On success, (*c) refers null-terminated array of type *Component
     * If given buffer size is not sufficient, this returns
     * ERROR_INSUFFICIENT_RESOURCE. Then app can call this again with larger
     * buffer.
     */
    unifiedtv_ERROR (*queryComponentCache) (unifiedtv_Component ***c,
            void *buf, size_t bufsz, const char **sids);

    /**
     * Queries program event list which meets the condition given by service id
     * list and time range(startTime and endTime).
     * For the services which are no longer valid, result should not include
     * them.
     * For the services which are valid but don't have related ESG information,
     * result should include them with empty array.
     * startTime/endTime/start/end : A 32-bit unsigned integer representing
     * EPOCH time since 00:00:00 UTC, January 1, 1970
     * On success, (*p) refers null-terminated array of type *Program
     * If given buffer size is not sufficient, this returns
     * ERROR_INSUFFICIENT_RESOURCE. Then app can call this again with larger
     * buffer.
     */
    unifiedtv_ERROR (*queryEsg) (unifiedtv_Program ***p,
            void *buf, size_t bufsz,
            const char **sids, unsigned int startTime, unsigned int endTime);
} unifiedtv_UnifiedTv;

#ifdef __cplusplus
}
#endif
#endif
