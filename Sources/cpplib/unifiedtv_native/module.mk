$(call begin-target, unifiedtv_native, staticlib)
    LOCAL_LABELS := OPTIONAL
    LOCAL_SRCS := $(wildcard $(LOCAL_PATH)/*.c)
    EXPORT_CFLAGS := -I$(LOCAL_PATH)
    EXPORT_CXXLFAGS := -I$(LOCAL_PATH)
$(end-target)

