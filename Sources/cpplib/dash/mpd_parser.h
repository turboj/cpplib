#ifndef MPD_PARSER_H
#define MPD_PARSER_H

#include <math.h>

#include <string>
#include <vector>
#include <map>
#include <functional>

#include "pugixml.hpp"

namespace dash {

struct RepresentationInfo {

    std::string id;

    enum SEGMENT_TYPE {
        SEGMENT_TYPE_UNSUPPORTED,
        SEGMENT_LIST,
        SEGMENT_TEMPLATE_BY_NUMBER,
        SEGMENT_TEMPLATE_BY_TIME,
    };

    SEGMENT_TYPE segmentType;

    // only for SEGMENT_TEMPLATE_BY_NUMBER or SEGMENT_TEMPLATE_BY_TIME.
    std::string templateHead;
    std::string templateTail;
    int templatePrecision; // 0 means no precision specified with '%0Nd'

    // segment filename to offset (only for SEGMENT_LIST)
    std::map<std::string, double> segments;

    std::vector<std::string> baseUrls;

    std::string mimeType;

    int bandwidth;
    std::string initialization;
    double availabilityTimeOffset;
    long timescale;
    long duration;
    int startNumber;

    std::map<uint64_t, std::pair<uint32_t, uint32_t>,
            std::greater<uint32_t> > segmentTimeline;

    RepresentationInfo (): segmentType(SEGMENT_TYPE_UNSUPPORTED),
            templatePrecision(-1), availabilityTimeOffset(0.0), timescale(1),
            bandwidth(0), startNumber(1), duration(-1) {
    }

    // Returns NAN if not found.
    double getSegmentStartTime (const std::string &str);

private:

    void setInitialization (const std::string &temp);
    void setTemplate (const std::string &temp);

    friend struct MpdInfo;
};

struct AdaptationSetInfo {
    std::vector<RepresentationInfo> representations;
};

struct PeriodInfo {
    double start;
    double end;
    std::vector<AdaptationSetInfo> adaptationSets;
};

struct MpdInfo {
    std::vector<PeriodInfo> periods;

    // returns 0 on success
    int read (const pugi::xml_document &doc);
};

}
#endif
