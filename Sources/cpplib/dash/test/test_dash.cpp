#include <stdio.h>
#include <vector>
#include <string>
#include <regex>

#include "acba.h"
#include "pugixml.hpp"
#include "mpd_parser.h"

using namespace std;
using namespace acba;
using namespace pugi;
using namespace dash;

static Log l("test_dash");

int main (int argc, char **argv)
{
    if (argc != 2) {
        l.error("Usage: %s <mpd.xml>", argv[0]);
        return -1;
    }

    xml_document doc;
    auto res = doc.load_file(argv[1]);
    if (!res) {
        l.error("Failed to parse %s", argv[1]);
        return -1;
    }

    MpdInfo mi;

    if (mi.read(doc)) {
        l.error("Failed to process mpd");
        return -1;
    }

    for (auto &pi: mi.periods) {
        l.info("Period %f-%f", pi.start, pi.end);
        for (auto &asi: pi.adaptationSets) {
            l.info("    AdaptationSet");
            for (auto &repi: asi.representations) {
                l.info("        Representation %s %s type:%d init:%s media:%s%s%s astoff:%f timescale:%ld dur:%ld",
                    repi.id.c_str(),
                    repi.mimeType.c_str(),
                    repi.segmentType,
                    repi.initialization.c_str(),
                    repi.templateHead.c_str(),
                    (repi.segmentType == RepresentationInfo::SEGMENT_TEMPLATE_BY_NUMBER) ? "$Number$":
                    (repi.segmentType == RepresentationInfo::SEGMENT_TEMPLATE_BY_TIME) ? "$Time$": "<non_template>",
                    repi.templateTail.c_str(),
                    repi.availabilityTimeOffset,
                    repi.timescale,
                    repi.duration);

                for (auto &stl: repi.segmentTimeline) {
                    l.info("            time %llu idx:%lu dur:%lu",
                            stl.first, stl.second.first, stl.second.second);
                }

                for (auto &bu: repi.baseUrls) {
                    l.info("            BaseURL: %s", bu.c_str());
                }
            }
        }
    }

    string fn = "1400kbps_00005.ts";
    for (auto &pi: mi.periods) {
        for (auto &asi: pi.adaptationSets) {
            for (auto &repi: asi.representations) {
                auto st = repi.getSegmentStartTime(fn);
                if (isnan(st)) continue;
                l.info("Found match on Representation %s with offset:%f",
                        repi.id.c_str(), st);
            }
        }
    }

    return 0;
}
