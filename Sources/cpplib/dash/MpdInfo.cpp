#include <stdio.h>
#include <vector>
#include <string>

#include "acba.h"
#include "datetime.h"
#include "mpd_parser.h"

using namespace std;
using namespace acba;
using namespace pugi;
using namespace dash;

#define DINF (std::numeric_limits<double>::infinity())

static Log l("dash");

namespace {

template <typename R>
struct Concat {

    typedef decltype(((R*)0)->begin()) iterator_;

    Concat (R &f, R &s): fbegin(f.begin()), fend(f.end()), sbegin(s.begin()),
            send(s.end()) {}

    iterator_ fbegin, fend;
    iterator_ sbegin, send;

    struct Iterator {
        Iterator (iterator_ &it, Concat &c): iter(it), concat(c) {}
        iterator_ iter;
        Concat &concat;

        decltype(*iter) &operator* () {
            return *iter;
        }

        bool operator!= (const Iterator &o) {
            return iter != o.iter;
        }

        Iterator &operator++ () {
            iter++;
            if (iter == concat.fend) iter = concat.sbegin;
            return *this;
        }
    };

    Iterator begin () {
        if (fbegin == fend) return {sbegin, *this};
        return {fbegin, *this};
    }

    Iterator end () {
        return {send, *this};
    }
};

struct SegmentInfo {
    vector<string> baseUrls;
    vector<xml_node> segmentLists;
    vector<xml_node> segmentTemplates;
    vector<xml_node> segmentTimelines;
    string mimeType;
    string initialization;
    string media;
    string availabilityTimeOffset;
    string timescale;
    string startNumber;
    string duration;

    void init () {
        baseUrls.push_back("");
    }

    void build (xml_node el, SegmentInfo &si) {

        // build baseUrls
        for (auto bu: el.children("BaseURL")) {
            auto url = bu.text().get();
            if (strncmp(url, "http:", 5) == 0 || strncmp(url, "https:", 6) == 0) {
                baseUrls.push_back(url);
            } else {
                for (auto &p: si.baseUrls) {
                    baseUrls.push_back(p + url);
                }
            }
        }
        if (baseUrls.size() == 0) baseUrls = si.baseUrls;

        auto sl = el.child("SegmentList");
        segmentLists = si.segmentLists;
        if (sl) segmentLists.push_back(sl);

        auto st = el.child("SegmentTemplate");
        segmentTemplates = si.segmentTemplates;
        if (st) segmentTemplates.push_back(st);

        if (!segmentLists.empty() && !segmentTemplates.empty()) {
            l.warn("SegmentList and SegmentTemplate conflicts");
        }

        auto mt = el.attribute("mimeType");
        mimeType = (mt)? mt.value(): si.mimeType;
    }

    void setOnce (string &o, const char *i) {
        if (!i || !i[0]) return;
        if (!o.empty()) return;
        o = i;
    }

    void finish () {
        for (auto mseg:
                Concat<vector<xml_node>>(segmentLists, segmentTemplates)) {
            setOnce(initialization,
                    mseg.child("Initialization").attribute("sourceURL").value());
            setOnce(initialization, mseg.attribute("initialization").value());
            setOnce(media, mseg.attribute("media").value());
            setOnce(availabilityTimeOffset,
                    mseg.attribute("availabilityTimeOffset").value());
            setOnce(timescale, mseg.attribute("timescale").value());
            setOnce(startNumber, mseg.attribute("startNumber").value());
            setOnce(duration, mseg.attribute("duration").value());

            auto tl = mseg.child("SegmentTimeline");
            if (tl) segmentTimelines.push_back(tl);
        }
    }
};

}

int MpdInfo::read (const xml_document &doc)
{
    SegmentInfo si0;
    si0.init();

    auto mpd = doc.document_element();

    auto type = mpd.attribute("type");
    if (!type) {
        l.error("No MPD@type");
        return -1;
    }
    if (strcmp(type.value(), "dynamic") != 0) {
        l.error("Not dynamic");
        return -1;
    }

    auto ast = mpd.attribute("availabilityStartTime");
    if (!ast) {
        l.error("No AST exist");
        return -1;
    }

    SegmentInfo si1;
    si1.build(mpd, si0);

    for (auto period: mpd.children("Period")) {
        auto pstart = period.attribute("start");
        auto pdur = period.attribute("duration");

        PeriodInfo *lpi = nullptr; // last PeriodInfo
        if (periods.size() > 0) lpi = &*(periods.end() - 1);

        PeriodInfo pi;
        if (pstart) {
            pi.start = parseDuration(pstart.value());
        } else if (!lpi) {
            // NOTE: Spec says this belongs to Skip Early Available Period.
            // but we see lots of MPD file without start attr in first Period.
            pi.start = 0.0;
        } else if (lpi->end != DINF) {
            pi.start = lpi->end;
        } else {
            l.warn("Skip period with unknown start time "
                    "(Could be Early Available Period");
            continue;
        }
        if (pdur) {
            pi.end = parseDuration(pdur.value());
        } else {
            pi.end = DINF;
        }
        l.debug("PeriodInfo start:%f dur:%f", pi.start, pi.end);

        SegmentInfo si2;
        si2.build(period, si1);

        for (auto adaptset: period.children("AdaptationSet")) {

            SegmentInfo si3;
            si3.build(adaptset, si2);

            AdaptationSetInfo asi;

            for (auto rep: adaptset.children("Representation")) {
                auto repid = rep.attribute("id").as_string(nullptr);
                if (!repid) {
                    l.error("No representation id");
                    return -1;
                }

                SegmentInfo si;
                si.build(rep, si3);
                si.finish();

                RepresentationInfo ri;

                ri.id = repid;

                ri.bandwidth = rep.attribute("bandwidth").as_int();
                if (ri.bandwidth == 0) {
                    l.error("No bandwidth");
                    return -1;
                }

                ri.setInitialization(si.initialization);
                if (!si.media.empty()) ri.setTemplate(si.media);
                ri.baseUrls.swap(si.baseUrls);

                ri.mimeType = si.mimeType;

                if (!si.startNumber.empty()) {
                    ri.startNumber = atol(si.startNumber.c_str());
                }

                if (!si.availabilityTimeOffset.empty()) {
                    ri.availabilityTimeOffset =
                            atof(si.availabilityTimeOffset.c_str());
                }

                if (!si.timescale.empty()) {
                    ri.timescale = atol(si.timescale.c_str());
                    if (ri.timescale <= 0) {
                        l.error("Invalid timescale:%ld", ri.timescale);
                        return -1;
                    }
                }

                if (!si.duration.empty()) {
                    ri.duration = atol(si.duration.c_str());
                    if (ri.duration <= 0) {
                        l.error("Invalid duration:%ld", ri.duration);
                        return -1;
                    }
                }

                if (si.segmentTimelines.size() > 0) {
                    uint64_t curtime = 0;
                    uint32_t pos = 0;
                    for (auto segtl: si.segmentTimelines) {
                        for (auto s: segtl.children("S")) {
                            auto attr_t = s.attribute("t");
                            if (attr_t) curtime = attr_t.as_ullong();
                            auto attr_d = s.attribute("d");
                            if (!attr_d) {
                                l.error("attribute @d is missing");
                                return -1;
                            }
                            uint32_t d = attr_d.as_uint();
                            int r = s.attribute("r").as_int(0);

                            ri.segmentTimeline[curtime] = { pos, d };
                            if (r < 0) break;
                            curtime += (r + 1) * d;
                            pos += (r + 1);
                        }
                    }
                }

                asi.representations.push_back(ri);
            }

            pi.adaptationSets.push_back(asi);
        }

        periods.push_back(pi);
    }

    return 0;
}
