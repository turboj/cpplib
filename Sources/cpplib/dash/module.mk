$(call begin-target, dash, staticlib)
    LOCAL_SRCS := $(wildcard $(LOCAL_PATH)/*.cpp)
    LOCAL_IMPORTS := acba-staticlib boost-staticlib pugixml-staticlib
    EXPORT_CXXFLAGS := -I$(LOCAL_PATH)
    EXPORT_IMPORTS := $(LOCAL_IMPORTS)
$(end-target)

$(call begin-target, test_dash, executable)
    LOCAL_LABELS := TEST
    LOCAL_SRCS := $(LOCAL_PATH)/test/test_dash.cpp
    LOCAL_IMPORTS := acba-staticlib dash-staticlib
$(end-target)
