#include <ctype.h>
#include <string.h>

#include <sstream>
#include <regex>

#include "acba.h"
#include "mpd_parser.h"

using namespace std;
using namespace acba;
using namespace dash;

static Log l("dash");

double RepresentationInfo::getSegmentStartTime (const string &str)
{
    switch (segmentType) {
    case SEGMENT_TYPE_UNSUPPORTED:
        return NAN;

    case SEGMENT_LIST:
        // TODO
        return NAN;

    case SEGMENT_TEMPLATE_BY_NUMBER:
    case SEGMENT_TEMPLATE_BY_TIME:
        // fall through
        break;

    default:
        assert(!"Invalid segment type");
        break;
    }

    // Only for segment template cases below.

    // abort unless startsWith(head) endsWith(tail)
    if (str.compare(0, templateHead.size(), templateHead) != 0) return NAN;
    if (str.compare(str.size() - templateTail.size(), templateTail.size(),
                templateTail) != 0) {
        return NAN;
    }

    // extract non head/tail part as number.
    auto body = str.substr(templateHead.size(),
            str.size() - templateHead.size() - templateTail.size());
    if (!isdigit(body[0])) return NAN;
    auto num = atoll(body.c_str());
    char buf[64];
    if (templatePrecision == 0) {
        sprintf(buf, "%lld", num);
    } else {
        char fmt[32];
        sprintf(fmt, "%%0%dlld", templatePrecision);
        sprintf(buf, fmt, num);
    }
    if (body.compare(buf) != 0) {
        l.warn("Template has unexpected body:%s expected:%s in %s",
                body.c_str(), buf, str.c_str());
        return NAN;
    }

    switch (segmentType) {
    case SEGMENT_TEMPLATE_BY_NUMBER:
        if (duration == -1) {
            l.warn("Need @duration");
            return NAN;
        }
        return double(num - startNumber) * duration / timescale;

    case SEGMENT_TEMPLATE_BY_TIME:
        if (duration == -1) {
            auto b = segmentTimeline.lower_bound(num);
            if (b == segmentTimeline.end()) {
                l.warn("No segment timeline for time:%llu", num);
                return NAN;
            }

            auto off = num - b->first;;
            if (off / b->second.second * b->second.second != off) {
                l.warn("No aligned with segment timeline. time:%llu", num);
                return NAN;
            }
        } else {
            // TODO: check
        }
        return (double)num / timescale;
    }
}

void RepresentationInfo::setInitialization (const string &temp) {
    string tail = temp;
    stringstream ss;

    smatch sm;
    regex re("([^$]*)\\$([a-zA-Z]+)?(?:%0([0-9]+)d)?\\$(.*)$");
    while (regex_match(tail, sm, re)) {
        auto head = sm[1].str();
        auto var = sm[2].str();
        auto precision = sm[3].str();
        tail = sm[4].str();

        ss << head;

        if (var.empty()) {
            ss << '$';
        } else if (var == "RepresentationID") {
            ss << id;
        } else if (var == "Bandwidth") {
            if (precision.empty()) {
                ss << bandwidth;
            } else {
                char fmt[32];
                char str[64];
                sprintf(fmt, "%%0%ldd", atol(precision.c_str()));
                sprintf(str, fmt, bandwidth);
                ss << str;
            }
        }
    }
    ss << tail;
    initialization = ss.str();
}

void RepresentationInfo::setTemplate (const string &temp) {
    string tail = temp;
    stringstream ss;

    bool readingTail = false;

    smatch sm;
    regex re("([^$]*)\\$([a-zA-Z]+)?(?:%0([0-9]+)d)?\\$(.*)$");
    while (regex_match(tail, sm, re)) {
        auto head = sm[1].str();
        auto var = sm[2].str();
        auto precision = sm[3].str();
        tail = sm[4].str();

        ss << head;

        if (var.empty()) {
            ss << '$';
        } else if (var == "RepresentationID") {
            ss << id;
        } else if (var == "Bandwidth") {
            if (precision.empty()) {
                ss << bandwidth;
            } else {
                char fmt[32];
                char str[64];
                sprintf(fmt, "%%0%ldd", atol(precision.c_str()));
                sprintf(str, fmt, bandwidth);
                ss << str;
            }
        } else if (var == "Number") {
            templateHead = ss.str();
            templatePrecision = atol(precision.c_str());
            ss.str({}); // clear
            readingTail = true;
            segmentType = SEGMENT_TEMPLATE_BY_NUMBER;
        } else if (var == "Time") {
            templateHead = ss.str();
            templatePrecision = atol(precision.c_str());
            ss.str({}); // clear
            readingTail = true;
            segmentType = SEGMENT_TEMPLATE_BY_TIME;
        }
    }
    ss << tail;
    if (readingTail) {
        templateTail = ss.str();
    } else {
        l.warn("Invalid template string:%s", temp.c_str());
        templateHead = ss.str();
    }
}
