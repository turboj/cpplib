$(call begin-target, boost, staticlib)
    EXPORT_CXXFLAGS := -I$(LOCAL_PATH)
    LOCAL_SRCS := $(LOCAL_PATH)/boost_dummy.cpp
$(end-target)

$(call begin-target, test_httpclient_sync, executable)
    LOCAL_LABELS := TEST
    LOCAL_SRCS := $(LOCAL_PATH)/libs/beast/example/http/client/sync/http_client_sync.cpp
    LOCAL_IMPORTS := boost-staticlib
$(end-target)

$(call begin-target, test_httpclient_async, executable)
    LOCAL_LABELS := TEST
    LOCAL_SRCS := $(LOCAL_PATH)/libs/beast/example/http/client/async/http_client_async.cpp
    LOCAL_IMPORTS := boost-staticlib
$(end-target)

$(call begin-target, test_httpclient_async_ssl, executable)
    LOCAL_LABELS := TEST
    LOCAL_SRCS := $(LOCAL_PATH)/libs/beast/example/http/client/async-ssl/http_client_async_ssl.cpp
    LOCAL_CXXFLAGS := -I$(LOCAL_PATH)/libs/beast
    LOCAL_LDFLAGS := -lssl -lcrypto
    LOCAL_IMPORTS := boost-staticlib
$(end-target)
