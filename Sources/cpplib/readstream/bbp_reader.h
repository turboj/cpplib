#ifndef BBP_READER_H
#define BBP_READER_H

#include <stdint.h>

#include "acba.h"
#include "stream_reader.h"

struct BbpReader: StreamReader {

    StreamReader *upper;

    int nextSequence;
    acba::Buffer buffer;

    BbpReader (StreamReader *u = nullptr): upper(u), nextSequence(-1) {}

    int read (const byte_t *d, int sz, Chain *ctx) override;
};



#endif
