#include "acba.h"
#include "acba_crc32.h"
#include "pes_reader.h"
#include "ts_reader.h"

using namespace std;
using namespace acba;

static Log l("pes");

int PesReader::read (const byte_t *d, int sz, Chain *ctx)
{
    const byte_t *e = d + sz;

    bool pusi = d[1] & 0x40; // PUSI: Payload unit start indicator
    if (d[3] & 0x20) {
        d += 5 + d[4]; // jump over adaptation field.
    } else {
        d += 4;
    }

    // TODO: gather payload using pusi
    if (pusi) l.info("pes header: %u %u %u", d[0], d[1], d[2]);

    return 188;
}
