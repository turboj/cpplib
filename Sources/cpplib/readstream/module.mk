$(call begin-target, readstream, staticlib)
    LOCAL_SRCS := $(wildcard $(LOCAL_PATH)/*.cpp)
    LOCAL_IMPORTS := acba-staticlib

    EXPORT_CXXFLAGS := -I$(LOCAL_PATH)
    EXPORT_IMPORTS := acba-staticlib
$(end-target)

$(call begin-target, test_alp, executable)
    LOCAL_LABELS := TEST
    LOCAL_SRCS := $(LOCAL_PATH)/test/test_alp.cpp
    LOCAL_IMPORTS := acba-staticlib readstream-staticlib
$(end-target)

$(call begin-target, test_stltp, executable)
    LOCAL_LABELS := TEST
    LOCAL_SRCS := $(LOCAL_PATH)/test/test_stltp.cpp
    LOCAL_IMPORTS := acba-staticlib readstream-staticlib
$(end-target)
