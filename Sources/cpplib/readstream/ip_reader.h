#ifndef IP_READER
#define IP_READER

#include <stdint.h>
#include <string>

#include "stream_reader.h"

struct IpReader: StreamReader {

    IpReader (StreamReader *u = 0): udpReader(u), tcpReader(0) {}
    int read (const byte_t *data, int sz, Chain *ctx);

    StreamReader *udpReader;
    StreamReader *tcpReader;
};

struct IpContext: public ChainNode<IpContext> {
    uint32_t sourceIp, destinationIp;
};

uint32_t ip2u (const char *addr);
std::string ip2hex (const char *addr);

#endif
