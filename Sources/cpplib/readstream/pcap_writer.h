#include <pthread.h>
#include <string>

#include "acba.h"
#include "CondVar.h"

struct PcapWriter: acba::RefCounter<PcapWriter> {

    std::string outputDirectory;
    pthread_mutex_t mutex;
    acba::CondVar outputAvailable;
    acba::Buffer outputBuffers[2];
    int outputBufferIndex;
    FILE *outputFile;
    bool closeRequested;
    uint32_t type;

    PcapWriter (uint32_t t = /*LINKTYPE_IPV4*/228);

    bool open (const std::string &odir);

    void feed (const byte_t *data, int sz);

    void close ();

    void run ();
};
