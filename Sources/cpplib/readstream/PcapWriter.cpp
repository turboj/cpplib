#include <pthread.h>

#ifdef __APPLE__
#include <sys/time.h>
#endif

#include "pcap_writer.h"
#include "datetime.h"

using namespace std;
using namespace acba;

static Log l("pcap_writer");

PcapWriter::PcapWriter (uint32_t t): mutex(PTHREAD_MUTEX_INITIALIZER),
        outputFile(nullptr), closeRequested(false), outputBufferIndex(0),
        type(t)
{
}

static void *_run (void *arg)
{
    Ref<PcapWriter> pw = static_cast<PcapWriter*>(arg);
    pw->run();
    return nullptr;
}

bool PcapWriter::open (const string &odir)
{
    outputDirectory = odir;

    char path[256];
    sprintf(path, "%s-%s.pcap", outputDirectory.c_str(),
            toDatetime(currentUnixTime()).c_str());

    outputFile = fopen(path, "w");
    if (!outputFile) return false;

    // write header
    {
        uint32_t magic = 0xa1b23c4d;
        fwrite(&magic, sizeof(magic), 1, outputFile);

        uint16_t major = 2, minor = 4;
        fwrite(&magic, sizeof(major), 1, outputFile);
        fwrite(&magic, sizeof(minor), 1, outputFile);

        uint32_t timezone = 0;
        fwrite(&timezone, sizeof(timezone), 1, outputFile);

        for (int i = 0; i < 4; i++) fputc(0, outputFile);

        uint32_t maxlen = 0xffff;
        fwrite(&maxlen, sizeof(maxlen), 1, outputFile);

        uint32_t network = type;
        fwrite(&network, sizeof(network), 1, outputFile);
    }

    pthread_t t;
    pthread_create(&t, nullptr, _run, this);
    pthread_detach(t);
    return true;
}

void PcapWriter::close ()
{
    ScopedLock sl(mutex);
    closeRequested = true;
    outputAvailable.signal();
}

void PcapWriter::feed (const byte_t *data, int sz)
{
    ScopedLock sl(mutex);
    Buffer &buf = outputBuffers[outputBufferIndex];
    auto presz = buf.size();

    uint32_t u;
#ifdef __APPLE__
    timeval t;
    gettimeofday(&t,0);
    u = t.tv_sec; buf.writeBytes((byte_t*)&u, sizeof(u));
    u = t.tv_usec*1000; buf.writeBytes((byte_t*)&u, sizeof(u));
#else
    timespec ts;
    clock_gettime(CLOCK_REALTIME, &ts);
    u = ts.tv_sec; buf.writeBytes((byte_t*)&u, sizeof(u));
    u = ts.tv_nsec; buf.writeBytes((byte_t*)&u, sizeof(u));
#endif
    u = sz; buf.writeBytes((byte_t*)&u, sizeof(u));
    u = sz; buf.writeBytes((byte_t*)&u, sizeof(u));
    buf.writeBytes(data, sz);

    if (presz == 0) outputAvailable.signal();
    if (presz >= 1000000) {
        l.warn("output buffer was stacked more than 1MB");
    }
}

void PcapWriter::run ()
{
    ScopedLock sl(mutex);

    while (!closeRequested) {
        Buffer &buf = outputBuffers[outputBufferIndex];
        int sz = buf.size();
        if (sz == 0) {
            outputAvailable.wait(&mutex);
            continue;
        }
        outputBufferIndex ^= 1; // flip output buffer

        ScopedUnlock su(mutex);
        int w = fwrite(buf.begin(), 1, sz, outputFile);
        buf.clear();
        if (w != sz) {
            l.error("file write error");
            closeRequested = true;
            continue;
        }
    }

    fclose(outputFile);
    outputFile = nullptr;
}
