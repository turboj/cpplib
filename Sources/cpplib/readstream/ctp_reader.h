#ifndef CTP_READER_H
#define CTP_READER_H

#include <stdint.h>

#include "acba.h"
#include "stream_reader.h"
#include "rtp_reader.h"
#include "ip_reader.h"
#include "udp_reader.h"

struct CtpReader: StreamReader {

    acba::Buffer buffer;
    int nextSequence;

    // for tunneled stream.
    IpReader ipReader;
    UdpReader udpReader;
    RtpReader rtpReader;

    CtpReader (StreamReader *u = nullptr): nextSequence(-1),
            ipReader(&udpReader), udpReader(&rtpReader), rtpReader(u) {}

    int read (const byte_t *d, int sz, Chain *ctx) override;
};

#endif
