#include "acba.h"
#include "rtp_reader.h"

using namespace acba;

static Log l("rtp");

#define SAFEREAD_LOG(args...) l.debug("expected " args)
#define SAFEREAD_RETURN return -1

// Packet size depends on sz.
int RtpReader::read (const byte_t *d, int sz, Chain *ctx)
{
    auto e = d + sz;

    RtpContext meta;
    meta.parent = ctx;
    meta.head = SAFEREAD_U32(d, e, "First 4 bytes");
    meta.timestamp = SAFEREAD_U32(d, e, "timestamp");
    meta.ssrc = SAFEREAD_U32(d, e, "SSRC");

#define READ_BITS(val,off,nbits) ((val >> (sizeof(val) * 8 - (off) - (nbits))) & ((1 << (nbits)) - 1))
#define H_VER READ_BITS(meta.head,0,2)
#define H_P READ_BITS(meta.head,2,1)
#define H_X READ_BITS(meta.head,3,1)
#define H_CC READ_BITS(meta.head,4,4)
#define H_M READ_BITS(meta.head,8,1)
#define H_PT READ_BITS(meta.head,9,7)
#define H_SEQ READ_BITS(meta.head,16,16)

    if (H_VER != 2) {
        if (IS_EACH_TIME(1000)) l.warn("Expected version 2, got %u", H_VER);
        return sz;
    }

    SAFEREAD_SKIP(d, e, H_CC * 4, "CSRC ids");
    if (H_X) {
        uint16_t hlen = SAFEREAD_U32(d, e, "Extension length") & 0xffff;
        SAFEREAD_SKIP(d, e, hlen * 4, "Extension header");
    }

    if (upper) {
        if (upper->read(d, e - d, &meta) <= 0) {
            l.warn("payload was not consumed. move on.");
        }
    }

    return sz;
}
