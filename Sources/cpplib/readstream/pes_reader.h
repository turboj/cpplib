#ifndef PES_READER
#define PES_READER

#include <set>
#include <map>

#include "stream_reader.h"
#include "acba.h"

struct PesReader: RefCountedStreamReader {

    PesReader (StreamReader *u = nullptr) {}

    int read (const byte_t *data, int sz, Chain *ctx) override;

private:

    StreamReader *upper;
};

struct PesContext: ChainNode<PesContext> {
};

#endif
