#ifndef GZIP_READER_H
#define GZIP_READER_H

#include "zlib.h"

#include "stream_reader.h"
#include "acba.h"

struct GzipReader: public StreamReader {
    GzipReader (StreamReader *u = 0): initialized(false), upper(u) {}
    ~GzipReader () {
        if (initialized) inflateEnd(&stream);
    }
    int read (const byte_t *data, int sz, Chain *ctx);

    bool initialized;
    z_stream stream;
    acba::Buffer outBuffer;
    StreamReader *upper;
};

#endif
