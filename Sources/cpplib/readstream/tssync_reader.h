#ifndef TSSYNC_READER
#define TSSYNC_READER

#include "stream_reader.h"

struct TsSyncReader: StreamReader {

    TsSyncReader (StreamReader *u = nullptr): upper(u) {}

    int read (const byte_t *data, int sz, Chain *ctx) override;

private:

    struct Sync {
        Sync (): pcr(0), time(0.0) {}
        uint64_t pcr;
        double time;
    };

    std::map<uint16_t, Sync> syncs; // key: pid

    StreamReader *upper;
};

#endif
