#include "acba.h"
#include "stream_reader.h"
#include "stltp_reader.h"
#include "ip_reader.h"
#include "udp_reader.h"

using namespace acba;

static Log l("stltp");

int StltpReader::read (const byte_t *d, int sz, Chain *ctx)
{
    auto rtp = ctx->as<RtpContext>();
    auto udp = rtp->as<UdpContext>();
    auto ip = udp->as<IpContext>();

    if (ip->destinationIp != 0xef003330) {
        if (IS_EACH_TIME(100)) l.info("drop ip %08x", ip->destinationIp);
        return sz;
    }

    // filter out non-bbp streams for now.
    if (((rtp->head >> 16) & 0x7f) != 78) return sz;
    int port = (int)udp->dstPort - 30000;
    if (port < 0) return sz;
    if (port / 100 >= 4) return sz; // exceeded number_of_channels max.
    if (port % 100 >= 64) return sz; // drop non-bbp stream

    auto &br = bbpReaders[port];
    if (!br) br = new RefCounted<BbpReader>(upper);
    return br->read(d, sz, ctx);
}
