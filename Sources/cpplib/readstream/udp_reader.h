#ifndef UDP_READER
#define UDP_READER

#include "stream_reader.h"

struct UdpReader: public StreamReader {

    UdpReader (StreamReader *u = 0): upper(u) {}
    int setStreamReader (const std::string &cond, StreamReader *r);

    StreamReader *upper;

    int read (const byte_t *data, int sz, Chain *ctx) override;
};

struct UdpContext: public ChainNode<UdpContext> {
    int srcPort;
    int dstPort;
};

#endif
