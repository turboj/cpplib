
#ifndef STLTP_READER_H
#define STLTP_READER_H

#include <stdint.h>

#include <map>

#include "acba.h"
#include "stream_reader.h"
#include "rtp_reader.h"
#include "bbp_reader.h"

struct StltpReader: StreamReader {

    StreamReader *upper;

    std::map<int, acba::Ref<acba::RefCounted<BbpReader> > > bbpReaders;

    StltpReader (StreamReader *u = nullptr): upper(u) {}

    int read (const byte_t *d, int sz, Chain *ctx) override;
};

#endif
