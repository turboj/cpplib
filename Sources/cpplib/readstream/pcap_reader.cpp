#include <time.h>
#include <unistd.h>

#ifdef __APPLE__
#include <sys/time.h>
#endif

#include "pcap_reader.h"
#include "acba.h"

using namespace acba;

static Log l("pcap");

static void read32_host (uint32_t *i, const byte_t *s)
{
    memcpy(i, s, 4);
}

static void read32_swap (uint32_t *i, const byte_t *s)
{
    byte_t *d = reinterpret_cast<byte_t*>(i);
    d[0] = s[3];
    d[1] = s[2];
    d[2] = s[1];
    d[3] = s[0];
}

static double now ()
{
#ifdef __APPLE__
    timeval t;
    gettimeofday(&t,0);
    return  (double)t.tv_sec + ((double)t.tv_usec / 1000000);
#else
    timespec ts;
    clock_gettime(CLOCK_REALTIME, &ts);
    return double(ts.tv_sec) + (ts.tv_nsec / 1000000000.);
#endif
}

int PcapReader::read (const byte_t *data, int sz, Chain *pctx)
{
    switch (state) {
    case 0: // initial state, read file header
        {
            if (sz < 24) return 0;

            uint32_t magic;
            memcpy(&magic, data, 4);
            switch (magic) {
            case 0xa1b2c3d4:
                read32 = read32_host;
                secondScale = 1000000.;
                break;
            case 0xa1b23c4d:
                read32 = read32_host;
                secondScale = 1000000000.;
                break;
            case 0xd4c3b2a1:
                read32 = read32_swap;
                secondScale = 1000000.;
                break;
            case 0x4d3cb2a1:
                read32 = read32_swap;
                secondScale = 1000000000.;
                break;
            default:
                l.error("Invalid magic: %08x. bypassing all streams", magic);
                state = 4;
                return -1;
            }

            {
                uint32_t thiszone;
                read32(&thiszone, data + 8);
                if (thiszone) l.warn("non-zero time offset: %d", thiszone);
            }

            {
                uint32_t network;
                read32(&network, data + 20);

                auto sri = streamReaders.find(network);
                if (sri != streamReaders.end()) {
                    upper = sri->second;
                } else {
                    upper = defaultStreamReader;
                }

                if (!upper) l.warn("Unknown network type:%d\n", network);
            }
            state++;
            return 24;
        }

    case 1: // read first timestamp
        {
            if (sz < 16) return 0;
            uint32_t sec;
            uint32_t ssec;
            read32(&sec, data);
            read32(&ssec, data + 4);
            timeOffset = now() - (double(sec) + ssec / secondScale);
            l.info("start time:%lf offset:%lf",
                    (double(sec) + ssec / secondScale), timeOffset);
            state++;
            return 0; // don't consume any bytes.
        }

    case 2: // read packet header
        {
            if (sz < 16) return 0;
            uint32_t sec;
            uint32_t ssec;
            read32(&sec, data);
            read32(&ssec, data + 4);
            nextTime = (double(sec) + ssec / secondScale) + timeOffset;

            read32(&inclen, data + 8);
            read32(&orglen, data + 12);
            if (inclen != orglen) {
                l.warn("inclen %d != orglen %d\n", inclen, orglen);
            }

            state++;
            return 16;
        }

    case 3: // read packet payload
        {
            if (sz < inclen) return 0;

            double wait = nextTime - now();
            if (wait > 0.0) {
                usleep(1000); // 1ms
                return 0;
            }
            if (wait < -0.5) {
                l.warn("Stream reading is behind %lf sec", -wait);
            }

            if (upper) {
                PcapContext meta;
                meta.parent = pctx;
                meta.timeOffset = timeOffset;
                upper->read(data, inclen, &meta);
            }

            state = 2;
            return inclen;
        }

    case 4: // errnous state
        usleep(300000); // 0.3 sec
        return 0;

    default:
        l.error("Unexpected state:%d", state);
        exit(1);
        break;
    }
}
