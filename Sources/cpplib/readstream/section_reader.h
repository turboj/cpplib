#ifndef SECTIOIN_READER
#define SECTIOIN_READER

#include <set>
#include <map>

#include "stream_reader.h"
#include "acba.h"

struct SectionReader: RefCountedStreamReader {

    SectionReader (StreamReader *u = nullptr): size(0), upper(u) {}

    int read (const byte_t *data, int sz, Chain *ctx) override;
    void send (Chain *ctx);

private:

    byte_t tableId;
    ssize_t size;
    acba::Buffer buffer;

    // bits are aligned to LSB.
    // key: bit concat of pid(13)+tableId(8)+tableIdExt(16)+sectionNum(8)+
    //          curNextIndicator(1)
    // value: (version(5), crc32(32))
    struct SectionChecksum {
        SectionChecksum (): version(0xff), crc32(0) {}
        SectionChecksum (uint8_t v, uint32_t c): version(v), crc32(c) {}
        SectionChecksum (uint8_t v, const uint8_t *c): version(v),
                crc32((c[0] << 24) | (c[1] << 16) | (c[2] << 8) | c[3]) {}
        bool operator== (const SectionChecksum &o) const {
            return version == o.version && crc32 == o.crc32;
        }
        uint8_t version;
        uint32_t crc32;
    };
    std::map<uint64_t, SectionChecksum> sectionChecksums;

    StreamReader *upper;
};

struct SectionContext: ChainNode<SectionContext> {
};

#endif
