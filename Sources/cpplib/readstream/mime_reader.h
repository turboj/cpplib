#ifndef MIME_READER
#define MIME_READER

#include <map>

#include "stream_reader.h"

struct MimeReader: StreamReader {
    MimeReader (StreamReader *u = 0): upper(u) {}
    int read (const byte_t *data, int sz, Chain *ctx);

    StreamReader *upper;
};


struct MimeContext: public ChainNode<MimeContext> {

    std::multimap<const std::string, const std::string> headers;
};

struct MultipartReader: StreamReader {
    MultipartReader (StreamReader *u = 0): upper(u) {}
    int read (const byte_t *data, int sz, Chain *ctx);

    StreamReader *upper;
    std::string boundary;
};

std::string mime_value (const char *s);

#endif
