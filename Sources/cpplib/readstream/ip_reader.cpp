#include <string>
#include "ip_reader.h"
#include "acba.h"

using namespace std;
using namespace acba;

static Log l("ip");

int IpReader::read (const byte_t *data, int sz, Chain *pctx)
{
    IpContext meta;

    if (sz < 4) return 0;

    int ver = data[0] >> 4;
    if (ver != 4) {
        l.warn("Unsupported ip ver:%d sz:%d\n", ver, sz);
        return -1;
    }

    int hlen = (data[0] & 0xf) * 4;
    int tlen = (data[2] << 8) + data[3];
    if (sz < tlen) return 0;

    int proto = data[9];

    meta.parent = pctx;
#define READ_U32(x) (((x)[0] << 24) + ((x)[1] << 16) + ((x)[2] << 8) + (x)[3]);
    meta.sourceIp = READ_U32(data + 12);
    meta.destinationIp = READ_U32(data + 16);

    /*
    const EthernetContext *ctx =
            reinterpret_cast<const EthernetContext*>(pctx);
    printf("%02x:%02x:%02x:%02x:%02x:%02x -> "
            "%02x:%02x:%02x:%02x:%02x:%02x\n",
            ctx->srcMac[0], ctx->srcMac[1], ctx->srcMac[2],
            ctx->srcMac[3], ctx->srcMac[4], ctx->srcMac[5],
            ctx->dstMac[0], ctx->dstMac[1], ctx->dstMac[2],
            ctx->dstMac[3], ctx->dstMac[4], ctx->dstMac[5]);
    */

    if (proto == 0x11) { // if udp
        if (udpReader->read(data + hlen, tlen - hlen, &meta) <= 0) {
            l.warn("invalid udp detected.");
        }
    }

    return tlen;
}

uint32_t ip2u (const char *addr)
{
    uint32_t a, b, c, d;
    if (sscanf(addr, "%d.%d.%d.%d", &a, &b, &c, &d) != 4) return -1;

    return (a << 24) + (b << 16) + (c << 8) + d;
}

string ip2hex (const char *addr)
{
    char s[32];
    sprintf(s, "%08x", ip2u(addr));
    return s;
}
