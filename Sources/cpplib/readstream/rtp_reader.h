#ifndef RTP_READER_H
#define RTP_READER_H

#include <stdint.h>

#include "acba.h"
#include "stream_reader.h"

struct RtpContext: ChainNode<RtpContext> {
    uint32_t head;
    uint32_t timestamp;
    uint32_t ssrc;
};

struct RtpReader: StreamReader {
    StreamReader *upper;

    RtpReader (StreamReader *u = nullptr): upper(u) {}

    int read (const byte_t *d, int sz, Chain *ctx) override;
};

#endif
