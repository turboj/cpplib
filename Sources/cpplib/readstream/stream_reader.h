#ifndef STREAM_READER_H
#define STREAM_READER_H

#include <map>
#include <string>

#include "acba.h"

struct Chain {
    Chain (Chain *p = nullptr): parent(p) {}

    Chain *parent;

    template <typename T> T *as () {
        for (Chain *c =  this; c; c = c->parent) {
            if (c->cid() == reinterpret_cast<void*>(T::sid)) {
                return static_cast<T*>(c);
            }
        }
        return 0;
    }

    virtual void *cid () const = 0;
};

template <typename T>
struct ChainNode: Chain {
    static void sid () {}
    virtual void *cid () const {
        return reinterpret_cast<void*>(&sid);
    }
};

struct StreamReader {
    /**
     * If successfully read, returns number of bytes consumed.
     * If more bytes are required, returns 0.
     * If error was encountered, returns -1.
     */
    virtual int read (const byte_t *data, int sz, Chain *ctx) = 0;
};

struct RefCountedStreamReader: acba::RefCounted<StreamReader> {
    // Make ref/deref virtual to enable custom implementation
    virtual void ref () {
        acba::RefCounted<StreamReader>::ref();
    }
    virtual void deref () {
        acba::RefCounted<StreamReader>::deref();
    }
};

typedef acba::Ref<RefCountedStreamReader> StreamReaderRef;

#endif
