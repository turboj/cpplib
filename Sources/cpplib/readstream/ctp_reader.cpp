#include "acba.h"
#include "ctp_reader.h"
#include "rtp_reader.h"

using namespace acba;

static Log l("ctp");

#define SAFEREAD_LOG(args...) l.debug("expected " args)
#define SAFEREAD_RETURN return -1

// Depends on sz for its size.
int CtpReader::read (const byte_t *d, int sz, Chain *ctx)
{
    auto e = d + sz;

    RtpContext meta;
    meta.parent = ctx;
    meta.head = SAFEREAD_U32(d, e, "First 4 bytes");
    meta.timestamp = SAFEREAD_U32(d, e, "timestamp");
    meta.ssrc = SAFEREAD_U32(d, e, "SSRC");

#define READ_BITS(val,off,nbits) ((val >> (sizeof(val) * 8 - (off) - (nbits))) & ((1 << (nbits)) - 1))
#define H_VER READ_BITS(meta.head,0,2)
#define H_P READ_BITS(meta.head,2,1)
#define H_X READ_BITS(meta.head,3,1)
#define H_CC READ_BITS(meta.head,4,4)
#define H_M READ_BITS(meta.head,8,1)
#define H_PT READ_BITS(meta.head,9,7)
#define H_SEQ READ_BITS(meta.head,16,16)
#define H_PROTOVER READ_BITS(meta.ssrc,0,2)
#define H_REDUNDANCY READ_BITS(meta.ssrc,2,2)
#define H_NUM_CHANNELS READ_BITS(meta.ssrc,4,2)
#define H_OFFSET READ_BITS(meta.ssrc,16,16)

    if (H_VER != 2) {
        l.warn("Expected version 2, got %u", H_VER);
        return sz;
    }

    if (H_PT != 97) {
        l.warn("Expected STLTP. payload_type:%u", H_PT);
        return sz;
    }

    if (H_PROTOVER != 1 && IS_EACH_TIME(10000)) {
        l.warn("Expected PROTOCOL_VERSION 1, got %u", H_PROTOVER);
    }

    SAFEREAD_SKIP(d, e, H_CC * 4, "CSRC ids");
    if (H_X) {
        auto hlen = SAFEREAD_U32(d, e, "Extension header header") & 0xffff;
        SAFEREAD_SKIP(d, e, hlen * 4, "Extension header");
    }

    if (H_NUM_CHANNELS != 0 && IS_EACH_TIME(10000)) {
        l.info("num channels is non-zero");
    }

    int alignment = 0;
    if (H_SEQ == nextSequence) {
        if (H_M) alignment = buffer.size() + H_OFFSET;
        buffer.writeBytes(d, e - d);
        nextSequence = (H_SEQ + 1) & 0xffff;
    } else if (H_M) {
        SAFEREAD_SKIP(d, e, H_OFFSET, "Skipped Offset bytes");
        buffer.clear();
        l.info("Start sequence from %u", H_SEQ);
        buffer.writeBytes(d, e - d);
        nextSequence = (H_SEQ + 1) & 0xffff;
    } else {
        buffer.clear();
        nextSequence = -1;
    }

    if (nextSequence != -1) {
        int rsz;
        do {
            rsz = ipReader.read(buffer.begin(), buffer.size(), &meta);
            if (rsz < 0) {
                if (alignment > 0) {
                    l.warn("invalid ip over ctp detected. try to re-align");
                    rsz = alignment;
                    alignment = 0;
                    buffer.drop(rsz);
                    continue;
                }

                l.warn("invalid ip over ctp detected. reset ctp buffer");
                buffer.clear();
                nextSequence = -1;
                break;
            }

            if (alignment > 0) {
                alignment -= rsz;
                if (alignment < 0) {
                    l.warn("ip over ctp not aligned with offset. "
                            "try to re-align");
                    rsz += alignment;
                    alignment = 0;
                }
            }

            buffer.drop(rsz);
        } while (rsz > 0 && buffer.size() > 0);
    }

    return sz;
}
