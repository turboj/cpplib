#include <stdio.h>
#include <string>

#include "udp_reader.h"
#include "ip_reader.h"
#include "acba.h"

using namespace std;
using namespace acba;

static Log l("udp");

int UdpReader::read (const byte_t *data, int sz, Chain *pctx)
{
    if (sz < 8) return 0;
    int len = (data[4] << 8) + data[5];
    if (len < sz) return 0;

    UdpContext meta;
    meta.parent = pctx;
    meta.srcPort = (data[0] << 8) + data[1];
    meta.dstPort = (data[2] << 8) + data[3];

#if 0
    char conid[256];
    IpContext *ipctx = pctx->as<IpContext>();
    sprintf(conid, "%08x-%08x:%d", ipctx->sourceIp, ipctx->destinationIp,
            meta.dstPort);
    ReaderMap::iterator i = readers.find(conid); // SSM
    if (i == readers.end()) i = readers.find(conid + 9); // no SSM

    if (i != readers.end()) {
        i->second->read(data + 8, len - 8, &meta);
    } else if (upper) {
        upper->read(data + 8, len - 8, &meta);
    }
#else
    if (upper) {
        if (upper->read(data + 8, len - 8, &meta) <= 0) {
            l.warn("udp payload was not consumed. move on.");
        }
    }
#endif

    return len;
}
