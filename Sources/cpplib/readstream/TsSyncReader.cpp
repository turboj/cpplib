#include <time.h>
#include <unistd.h>

#ifdef __APPLE__
#include <sys/time.h>
#endif

#include "tssync_reader.h"
#include "acba.h"

using namespace acba;

static Log l("tsfile");

static double now ()
{
#ifdef __APPLE__
    timeval t;
    gettimeofday(&t,0);
    return  (double)t.tv_sec + ((double)t.tv_usec / 1000000);
#else
    timespec ts;
    clock_gettime(CLOCK_MONOTONIC, &ts);
    return double(ts.tv_sec) + (ts.tv_nsec / 1000000000.);
#endif
}

int TsSyncReader::read (const byte_t *d, int sz, Chain *pctx)
{
    if (sz < 188) return 0;
    if (d[0] != 0x47) {
        l.warn("searching ts packet");
        return 1;
    }

    uint16_t pid = ((d[1] & 0x1F) << 8) | d[2];
    auto &sync = syncs[pid];

    if ((d[3] & 0x20) && d[4] && (d[5] & 0x10)) { // check PCR
        // adaption field offset: 4bytes
        uint64_t pcr = (((uint64_t)d[6] << 25) | ((uint64_t)d[7] << 17) |
                ((uint64_t)d[8] << 9) | ((uint64_t)d[9] << 1) |
                ((uint64_t)d[10] >> 7));

        double t = now();
        int64_t dpcr = pcr - sync.pcr;
        double waitTime = sync.time + dpcr / 90000.0 - t;

        if (waitTime < -0.5 || waitTime > 0.5) {
            sync.pcr = pcr;
            sync.time = t;
            l.info("sync pid:%u pcr:%lld", pid, pcr);
        } else if (waitTime > 0.01) {
            usleep(waitTime * 1000000);
        }
    }

    if (upper) upper->read(d, 188, pctx);

    return 188;
}
