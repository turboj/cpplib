#include <time.h>
#include <unistd.h>

#include "ts_reader.h"
#include "acba.h"

using namespace acba;

static Log l("ts");

int TsReader::read (const byte_t *d, int sz, Chain *pctx)
{
    if (sz < 188) return 0;

    TsContext meta;

    meta.pid = ((d[1] & 0x1F) << 8) | d[2];
    meta.parent = pctx;

    if (table != nextTable) table = nextTable;

    auto ei = table->entries.find(meta.pid);
    if (ei != table->entries.end()) {
        for (auto &sr: ei->second) sr->read(d, 188, &meta);
    }
    return 188;
}

void TsReader::addPidStreamReader (uint16_t pid, const StreamReaderRef &sr)
{
    ScopedLock sl(mutex);

    auto psrt = new PidStreamReaderTable(nextTable);
    psrt->entries[pid].insert(sr);
    nextTable = psrt;
}

void TsReader::removePidStreamReader (uint16_t pid, const StreamReaderRef &sr)
{
    ScopedLock sl(mutex);

    auto psrt = new PidStreamReaderTable(nextTable);
    psrt->entries[pid].erase(sr);
    nextTable = psrt;
}
