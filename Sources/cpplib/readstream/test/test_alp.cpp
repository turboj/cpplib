#include <unistd.h>
#include <sys/types.h>
#include <fcntl.h>
#include <stdio.h>

#include <map>
#include <string>

#include "acba.h"
#include "stream_reader.h"
#include "pcap_reader.h"
#include "alp_reader.h"

using namespace std;
using namespace acba;

static Log l("test");

struct MyReader: StreamReader {
    int read (const byte_t *d, int sz, Chain *ctx) {
        auto actx = ctx->as<AlpContext>();

        l.info("%d", actx->packetType);
        return sz;
    }
};

int main (int argc, char **argv)
{
    MyReader mr;
    AlpReader ar(&mr);
    PcapReader pr(&ar);

    int fd = open(argv[1], O_RDONLY);
    if (fd < 0) {
        l.error("Expected alp stream filename");
        return -1;
    }

    Buffer buf;
    static const int READ_SIZE = 64 * 1024;
    ssize_t sz;
    while (true) {
        // consume stream as much as possible.
        sz = pr.read(buf.begin(), buf.size(), nullptr);
        if (sz > 0) {
            buf.drop(sz);
            continue;
        } else if (buf.size() >= READ_SIZE / 2) {
            continue; // for time sync
        }

        buf.ensureMargin(READ_SIZE);
        sz = ::read(fd, buf.end(), READ_SIZE);
        if (sz <= 0) break;
        buf.grow(sz);
    }

    close(fd);
    return 0;
}


