#ifndef ALP_READER
#define ALP_READER

#include "acba.h"
#include "stream_reader.h"

struct AlpContext: ChainNode<AlpContext> {
    uint32_t packetType;
    int32_t subStreamId; // -1 if not available.
};

struct AlpReader: StreamReader {

    StreamReader *uppers[8]; // packet type -> reader

    acba::Buffer buffer;
    int lastSegmentNumber;

    AlpReader (StreamReader *u = nullptr): lastSegmentNumber(-1) {
        for (int i = 0; i < 8; i++) uppers[i] = u;
    }

    int read (const byte_t *d, int sz, Chain *ctx) override;

    void setStreamReader (uint8_t pktType, StreamReader *r) {
        uppers[pktType] = r;
    }
};

#endif
