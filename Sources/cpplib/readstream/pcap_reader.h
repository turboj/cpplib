#ifndef PCAP_READER
#define PCAP_READER

#include <stdint.h>

#include <map>

#include "stream_reader.h"
#include "ethernet_reader.h"

struct PcapReader: StreamReader {

    int state;
    double secondScale; // initialized on state == 0
    double timeOffset; // initialized on state == 1
    double nextTime; ; // initialized on state == 2
    uint32_t inclen, orglen;
    void (*read32)(uint32_t *i, const byte_t *s);

    StreamReader *upper;
    StreamReader *defaultStreamReader;
    std::map<uint32_t, StreamReader*> streamReaders; // network -> reader

    PcapReader (StreamReader *u = nullptr): state(0), upper(nullptr), read32(0),
            defaultStreamReader(u) {}

    int read (const byte_t *data, int sz, Chain *ctx) override;

    void reset () {
        state = 0;
    }

    void setStreamReader (uint32_t netid, StreamReader *r) {
        streamReaders[netid] = r;
    }
};

struct PcapContext: ChainNode<PcapContext> {
    double timeOffset;
};

#endif
