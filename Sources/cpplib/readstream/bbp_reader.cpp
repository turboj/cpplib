#include "acba.h"
#include "bbp_reader.h"
#include "rtp_reader.h"
#include "ip_reader.h"
#include "udp_reader.h"

using namespace acba;

// Strangely, all STLTP streams we got from interop or sinclair has
// tunneled RTP packet header with fixed sequence number.
// Allow such stream with this flag.
#define ALLOW_FIXED_SEQUENCE_NUMBER 1

static Log l("bbp"); // baseband packet

// Depends on sz for its size.
int BbpReader::read (const byte_t *d, int sz, Chain *ctx)
{
#define SAFEREAD_LOG(args...) l.debug("expected " args)
#define SAFEREAD_RETURN return -1

    auto rtp = ctx->as<RtpContext>();
    auto ip = ctx->as<IpContext>();
    auto udp = ctx->as<UdpContext>();

    auto e = d + sz;

    auto bf = SAFEREAD_U8(d, e, "bf");

    int ptr = bf & 0x7f;
    if (bf & 0x80) {
        auto bf2 = SAFEREAD_U8(d, e, "bf 2nd");
        ptr += (bf2 >> 2) << 7;

        int elen = 0;
        switch (bf2 & 0x3) {
        case 0: // No Extension Mode
            break;

        case 1: // Short Extension Mode
            elen = SAFEREAD_U8(d, e, "Short Extension") & 0x1f;
            break;

        case 2: // Long Extension Mode
        case 3: // Mixed Extension Mode
            elen = SAFEREAD_U8(d, e, "Long Extension 1st") & 0x1f;
            elen += SAFEREAD_U8(d, e, "Long Extension 2nd") << 5;
            break;
        }
        SAFEREAD_SKIP(d, e, elen, "Extension data");
    }
#define NO_PTR 8191
    if (ptr != NO_PTR) {
        SAFEREAD_CHECKSIZE(d, e, ptr, "Skipped offset bytes");
    }
    if (d == e) return sz; // empty payload.

    int alignment = 0;
    auto seq = rtp->head >> 16;
    if (seq == nextSequence) {
        if (ptr != NO_PTR) alignment = buffer.size() + ptr;
        buffer.writeBytes(d, e - d);
        nextSequence = (nextSequence + 1) & 0xffff;
#if ALLOW_FIXED_SEQUENCE_NUMBER
    } else if (((seq + 1) & 0xffff) == nextSequence) {
        if (IS_EACH_TIME(10000)) {
            l.warn("detected fixed sequence number %u", seq);
        }
        if (ptr != NO_PTR) alignment = buffer.size() + ptr;
        buffer.writeBytes(d, e - d);
#endif
    } else if (ptr != NO_PTR) {
        SAFEREAD_SKIP(d, e, ptr, "Skipped offset bytes");
        l.info("Reading %08x:%u from sequence %d. time:%u", ip->destinationIp,
                udp->dstPort, (rtp->head >> 16), rtp->timestamp);
        buffer.clear();
        buffer.writeBytes(d, e - d);
        nextSequence = ((rtp->head >> 16) + 1) & 0xffff;
    } else {
        buffer.clear();
        nextSequence = -1;
    }

    if (nextSequence != -1 && upper) {
        int rsz;
        do {
            rsz = upper->read(buffer.begin(), buffer.size(), ctx);
            if (rsz < 0) {
                if (alignment > 0) {
                    l.warn("invalid alp detected. try to re-align.");
                    rsz = alignment;
                    alignment = 0;
                    buffer.drop(rsz);
                    continue;
                }

                l.warn("invalid alp detected. reset bbp buffer.");
                buffer.clear();
                nextSequence = -1;
                return sz; // consume given bbp anyway.
            }

            if (alignment > 0) {
                alignment -= rsz;
                if (alignment < 0) {
                    l.warn("alp not aligned with bbp pointer. "
                            "try to re-align.");
                    rsz += alignment;
                    alignment = 0;
                }
            }

            buffer.drop(rsz);
        } while (rsz > 0 && buffer.size() > 0);
    }

    // ad-hoc check in case corrupted bbp buffer is stacked indefinitely.
    if (buffer.size() > 64*1024) {
        l.warn("bbp buffer grew more than 64k. reset.");
        buffer.clear();
        nextSequence = -1;
    }

    return sz;
}
