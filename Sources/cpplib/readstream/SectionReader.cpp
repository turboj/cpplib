#include "acba.h"
#include "acba_crc32.h"
#include "section_reader.h"
#include "ts_reader.h"

using namespace std;
using namespace acba;

static Log l("section");

int SectionReader::read (const byte_t *d, int sz, Chain *ctx)
{
    const byte_t *e = d + sz;

    bool pusi = d[1] & 0x40; // PUSI: Payload unit start indicator
    if (d[3] & 0x20) {
        d += 5 + d[4]; // jump over adaptation field.
    } else {
        d += 4;
    }

    if (pusi) {
        auto pointer_field = *d++;
        if (pointer_field && buffer.size() + pointer_field == size) {
            buffer.writeBytes(d, pointer_field);
            send(ctx);
        }
        d += pointer_field;
        buffer.clear();
        size = 0;
    }

    while (d < e) {
        if (size == 0) {
            byte_t tid = d[0];
            if (tid == 0xff) break;
            if (d + 3 > e) break;

            tableId = tid;
            size = (((d[1] << 8) + d[2]) & 0xfff) + 3;
            buffer.clear();
        }

        int psz = size - buffer.size();
        if (psz > e - d) psz = e - d;

        buffer.writeBytes(d, psz);
        d += psz;

        if (buffer.size() >= size) {
            send(ctx);
            buffer.clear();
            size = 0;
        }
    }
outer:

    return 188;
}

void SectionReader::send (Chain *ctx)
{
    auto ts = ctx->as<TsContext>();

    if (size < 13 || buffer.size() != size) {
        // siliently drop invalid size section.
        return;
    }

    auto b = buffer.begin();
    auto e = buffer.end();
    if (b[1] & 0x80) { // if common syntax section
        if (acba_crc32be(0, buffer.begin(), size)) {
            l.warn("CRC32 check failed");
            return;
        }

        uint64_t k = ts->pid; // pid
        k = (k << 13) + b[0]; // tableId
        k = (k << 16) + ((b[3] << 8) + b[4]); // tableIdExt
        k = (k << 8) + b[6]; // sectionNum
        k = (k << 1) + (b[5] & 1);

        SectionChecksum v = {uint8_t((b[5] >> 1) & 0x31), e - 4};

        auto &sc = sectionChecksums[k];
        if (sc == v) return; // skip identical section.

        sc = v;
        if (upper) {
            upper->read(b, size, ctx);
        } else {
            Buffer buf(b, size);
            buf.drop(3);
            auto tidext = (uint16_t)buf.readI16();
            auto vercur = buf.read();
            auto sn = buf.read();
            auto lsn = buf.read();

            l.info("section pid:%u tableId:%u-%u sn:%u/%u ver:%u cur:%d "
                    "size:%u crc32:%08x",
                    ts->pid, tableId, tidext, sn, lsn, (vercur >> 1) & 31,
                    vercur & 1, size, v.crc32);
        }
    } else { // if private section
        // TODO: use bit mask
        l.debug("private section pid:%u tableId:%u size:%d", ts->pid, tableId,
                int(size));
    }
}
