#include <algorithm>
#include <string>
#include <iostream>

#include "udp_reader.h"
#include "ip_reader.h"
#include "mime_reader.h"
#include "acba.h"

using namespace std;
using namespace acba;

static Log l("mime");

string mime_value (const char *s)
{
    const char *e = 0;

    while (*s == ' ' || *s == '\t') s++;
    if (*s == '"') {
        s++;
        e = strchr(s, '"');
        if (e) {
            return string(s, e - s);
        } else {
            return s;
        }
    } else {
        for (e = s; *e; e++) {
            if (strchr(";\t\r\n", *e)) break;
        }
        return string(s, e - s);
    }
}

#include <unistd.h>

int MimeReader::read (const byte_t *d, int sz, Chain *pctx)
{
    MimeContext meta;

    const byte_t *end = d + sz;

    while (d + 1 < end) {
        // check body start
        if (d[0] == '\r' && d[1] == '\n') {
            meta.parent = pctx;
            d += 2;
            if (upper) upper->read(d, end - d, &meta);
            break;
        }

        // detect header line
        const byte_t *col = 0;
        const byte_t *lend = d;
        const byte_t *prevd = d;
        for (; lend + 1 < end; lend++) {

            if (!col && *lend == ':') {
                col = lend;
                continue;
            }

            // check CRLF
            if (lend[0] == '\r' && lend[1] == '\n') {

                // check line folding
                if (lend + 2 < end && (lend[2] == ' ' || lend[2] == '\t')) {
                    lend += 2;
                    continue;
                }

                if (col) {
                    string k = string((char*)d, col - d);
                    std::transform(k.begin(), k.end(), k.begin(), ::tolower);

                    const byte_t *v = col + 1;
                    while (*v == ' ') v++;
                    meta.headers.insert({k, string((char*)v, lend - v)});
                }

                d = lend + 2;
                break;
            }
        }
        if (prevd == d) {
            l.warn("Invalid mime data skipped");
            break;
        }
    }

    return sz;
}

int MultipartReader::read (const byte_t *d, int sz, Chain *pctx)
{
    MimeContext *mctx = pctx->as<MimeContext>();
    if (!mctx) {
        l.error("Expected mime context for given data");
        return sz;
    }

    for (auto i: mctx->headers) {
        const string &k = i.first;
        const string &v = i.second;
        if (strcmp(k.c_str(), "content-type") == 0) {
            const char *b = strstr(v.c_str(), "boundary=");
            if (b) b += 9;
            if (b) {
                boundary = mime_value(b);
                boundary = string("--") + boundary;
            }
        }
    }

    const string start = boundary + "\r\n";
    const string sep = string("\r\n") + boundary;

    const byte_t *end = d + sz;

    d = (const byte_t*)memmem(d, sz, start.c_str(), start.length());
    if (!d) {
        l.error("Cannot find multipart start: %s", start.c_str());
        return sz;
    }

    for (const byte_t *b = d,
            *e = (const byte_t*)memmem(b, end - b, sep.c_str(), sep.size());
            e; //e && e + 1 < (char*)d + sz;
            b = e + sep.length() + 2,
            e = (const byte_t*)memmem(b, end - b, sep.c_str(), sep.size())) {
        MimeReader mr(upper);
        mr.read(b, e - b, pctx);
    }

    return sz;
}
