#include <unistd.h>
#include <sys/types.h>
#include <fcntl.h>
#include <stdio.h>

#include <map>
#include <string>

#include "acba.h"
#include "stream_reader.h"
#include "pcap_reader.h"
#include "alp_reader.h"

using namespace std;
using namespace acba;

static Log l("alp");

#define SAFEREAD_LOG(args...) l.debug("expected " args)
#define SAFEREAD_RETURN return 0

// Abort if size mismatch is detected as input date could be corrupted already.
#define READ_WHOLE(D,L,C) do {\
    auto rsz = upper->read(D, L, C);\
    if (rsz != L) {\
        l.warn("Payload size:%d consumed:%d", L, rsz);\
        return -1;\
    }\
} while (0)

int AlpReader::read (const byte_t *d, int sz, Chain *ctx)
{
    auto e = d + sz;
    // BaseHeader: packet_type(3) PC(1) HMorSC(1) Length(11)
    auto bh = SAFEREAD_U16(d, e, "ALP header");

    AlpContext meta;
    meta.packetType = bh >> 13;
    meta.subStreamId = -1;
    meta.parent = ctx;

    StreamReader *upper = uppers[meta.packetType];
    if (!upper) {
        struct NullReader: StreamReader {
            int read (const byte_t *data, int sz, Chain *ctx) override {
                return sz;
            }
        };
        static NullReader nullReader;
        upper = &nullReader;
    }

    unsigned int len = bh & ((1 << 11) - 1);
    switch ((bh >> 11) & 0x3) { // PCHM or PCSC

    case 0: // single packet, no additional header (pktsz < 2048).
        if (meta.packetType == 0x4) {
            SAFEREAD_SKIP(d, e, 5, "Additional Header for Signaling Info");
        } else if (meta.packetType == 0x6) {
            meta.packetType = SAFEREAD_U16(d, e, "extended_type");
        }

        SAFEREAD_CHECKSIZE(d, e, len, "payload");
        READ_WHOLE(d, len, &meta);
        d += len;
        break;

    case 1: // single packet w/ single_packet_hdr()
        {
            auto ah = SAFEREAD_U8(d, e, "single_packet_hdr()");
            len += (ah >> 3) << 11;

            if (meta.packetType == 0x4) {
                SAFEREAD_SKIP(d, e, 5, "Additional Header for Signaling Info");
            }

            if (ah & 0x2) { // SIF
                meta.subStreamId = SAFEREAD_U8(d, e, "SIF");
            }
            if (ah & 0x1) { // HEF
                auto et = SAFEREAD_U8(d, e, "extension_type");
                auto elm1 = SAFEREAD_U8(d, e, "extension_length_minus1");
                SAFEREAD_SKIP(d, e, elm1 + 1, "extension_bytes");
            }
            if (meta.packetType == 0x6) {
                meta.packetType = SAFEREAD_U16(d, e, "extended_type");
            }

            SAFEREAD_CHECKSIZE(d, e, len, "payload");
            READ_WHOLE(d, len, &meta);
            d += len;
        }
        break;

    case 2: // segment of packet w/ segmentation_hdr()
        {
            auto ah = SAFEREAD_U8(d, e, "segmentation_hder()");
            auto segNo = ah >> 3;
            int isLast = ah & 0x4;

            if (meta.packetType == 0x4) {
                SAFEREAD_SKIP(d, e, 5, "Additional Header for Signaling Info");
            }

            if (ah & 0x2) { // SIF
                meta.subStreamId = SAFEREAD_U8(d, e, "SIF");
            }
            if (ah & 0x1) { // HEF
                auto et = SAFEREAD_U8(d, e, "extension_type");
                auto elm1 = SAFEREAD_U8(d, e, "extension_length_minus1");
                SAFEREAD_SKIP(d, e, elm1 + 1, "extension_bytes");
            }
            if (meta.packetType == 0x6) {
                meta.packetType = SAFEREAD_U16(d, e, "extended_type");
            }

            SAFEREAD_CHECKSIZE(d, e, len, "segment payload");
            if (segNo == 0) {
                lastSegmentNumber = 0;
                buffer.clear();
                buffer.writeBytes(d, len);
            } else if (segNo == lastSegmentNumber + 1) {
                lastSegmentNumber++;
                buffer.writeBytes(d, len);
                if (isLast) {
                    READ_WHOLE(buffer.begin(), buffer.size(), &meta);
                    lastSegmentNumber = -1;
                }
            } else {
                lastSegmentNumber = -1;
            }
            d += len;
            break;
        }
        break;

    case 3: // concatenation of packets w/ concatenation_hdr()
        {
            auto ah = SAFEREAD_U8(d, e, "concatenation_hdr()");
            len += (ah >> 4) << 11;

            int compLengths[9 - 1];
            int compCount = ((ah >> 1) & 0x7) + 2;
            int lastCompLength = len;
            SAFEREAD_CHECKSIZE(d, e, compCount * 3 / 2 - 1,
                    "component_lengths");
            for (int i = 0; i < compCount - 1; i++) {
                if (i % 2 == 0) {
                    compLengths[i] = (d[i * 3] << 4) + (d[i * 3 + 1] >> 4);
                } else {
                    compLengths[i] = ((d[i * 3 + 1] & 0xf) << 8) +
                            d[i * 3 + 2];
                }
                lastCompLength -= compLengths[i];
            }
            d += compCount * 3 / 2 - 1;
            if (lastCompLength <= 0) {
                l.warn("Component length doesn't fit to total length");
                return -1;
            }

            if (meta.packetType == 0x4) {
                SAFEREAD_SKIP(d, e, 5, "Additional Header for Signaling Info");
            }

            if (ah & 0x1) { // SIF
                meta.subStreamId = SAFEREAD_U8(d, e, "SIF");
            }

            if (meta.packetType == 0x6) {
                meta.packetType = SAFEREAD_U16(d, e, "extended_type");
            }

            SAFEREAD_CHECKSIZE(d, e, len, "concatenated payload");
            for (int i = 0; i < compCount - 1; i++) {
                auto clen = compLengths[i];
                READ_WHOLE(d, clen, &meta);
                d += clen;
            }
            READ_WHOLE(d, lastCompLength, &meta);
            d += lastCompLength;
        }
        break;
    }

    return d - (e - sz);
}
