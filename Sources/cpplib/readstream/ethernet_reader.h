#ifndef ETHERNET_READER_H
#define ETHERNET_READER_H

#include "stream_reader.h"

struct EthernetReader: public StreamReader {

    EthernetReader (StreamReader *u = 0): upper(u) {}
    int read (const byte_t *data, int sz, Chain *ctx);

    StreamReader *upper;
};

struct EthernetContext: public ChainNode<EthernetContext> {
    const byte_t *srcMac;
    const byte_t *dstMac;
};

#endif
