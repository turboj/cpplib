#ifndef TS_READER
#define TS_READER

#include <pthread.h>

#include <set>

#include "acba.h"
#include "stream_reader.h"

struct TsReader: StreamReader {

    TsReader (): nextTable(new PidStreamReaderTable()),
            mutex(PTHREAD_MUTEX_INITIALIZER) {}

    int read (const byte_t *data, int sz, Chain *ctx) override;

    // MT-safe (Refer table/nextTable)
    void addPidStreamReader (uint16_t pid, const StreamReaderRef &sr);
    void removePidStreamReader (uint16_t pid, const StreamReaderRef &sr);

private:

    struct PidStreamReaderTable: acba::RefCounter<PidStreamReaderTable> {
        PidStreamReaderTable () {}
        PidStreamReaderTable (const acba::Ref<PidStreamReaderTable> &o):
                entries(o->entries) {}
        std::map<uint16_t, std::set<StreamReaderRef> > entries; // key: pid
    };

    // nextTable can be updated from other than reader thread.
    // then, table will follow nextTable in reader thread.
    acba::Ref<PidStreamReaderTable> table, nextTable;
    pthread_mutex_t mutex;
};

struct TsContext: ChainNode<TsContext> {
    uint16_t pid;
};

#endif
