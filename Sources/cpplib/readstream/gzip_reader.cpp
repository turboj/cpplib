#include "gzip_reader.h"
#include "zlib.h"
#include "acba.h"

using namespace acba;

static Log l("gzip");

int GzipReader::read (const byte_t *data, int sz, Chain *pctx)
{
    l.trace("read %p sz:%d", data, sz);

    if (!initialized) {
        memset(&stream, 0, sizeof(stream));
        inflateInit2(&stream, MAX_WBITS + 16);
        initialized = true;
    }

    if (sz == 0) {
        inflateEnd(&stream);
        initialized = false;
        return sz;
    }

    stream.avail_in = sz;
    stream.next_in = const_cast<byte_t*>(data);

    while (stream.avail_in > 0) {
#define DEFLATE_RATIO 10
        outBuffer.ensureMargin(stream.avail_in * DEFLATE_RATIO);
        stream.avail_out = outBuffer.margin();
        stream.next_out = outBuffer.end();

        int zrc = inflate(&stream, Z_SYNC_FLUSH);
        l.trace("inflate %p avail_in:%d avail_out:%d(%d) zrc:%d",
                data, stream.avail_in, stream.avail_out, outBuffer.margin(),
                zrc);
        if (zrc < 0) {
            l.warn("inflate failed. rc:%d", zrc);
            outBuffer.clear();
            return sz;
        }
        outBuffer.grow(outBuffer.margin() - stream.avail_out);

        if (upper) {
            int r = upper->read(outBuffer.begin(),
                    outBuffer.size(), pctx);
            if (r > 0) outBuffer.drop(r);
        }
    }

    return sz - stream.avail_in;
}
