#include "ethernet_reader.h"
#include "acba.h"

using namespace acba;

static Log l("eth");

// packet size depends on sz
int EthernetReader::read (const byte_t *data, int sz, Chain *pctx)
{
    if (sz < 12 + 2) {
        l.warn("Too short ethernet frame size:%d", sz);
        return -1;
    }

    EthernetContext meta;
    meta.parent = pctx;

    meta.dstMac = data;
    data += 6;
    meta.srcMac = data;
    data += 6;
    int etype = data[0] * 256 + data[1];
    data += 2;

    // NOTE: Accept ethernet type 0x0800(Ethernet II) only
    if (etype != 0x0800) return sz;

    // NOTE: Checksum 4bytes were not considered here.
    upper->read(data, sz - 14, &meta);
    return sz;
}
