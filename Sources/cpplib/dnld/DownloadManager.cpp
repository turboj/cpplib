#include <pthread.h>

#include <boost/algorithm/string/predicate.hpp>
#include <boost/beast/core.hpp>
#include <boost/beast/http.hpp>
#include <boost/beast/version.hpp>
#include <boost/asio/connect.hpp>
#include <boost/asio/ip/tcp.hpp>
#include <boost/asio/ssl/error.hpp>
#include <boost/asio/ssl/stream.hpp>
#include <cstdlib>
#include <functional>
#include <iostream>
#include <memory>
#include <string>
#include <list>

#include "acba.h"
#include "dnld.h"
#include "BufferBody.h"

using namespace std;
using namespace boost;
using namespace acba;
using tcp = asio::ip::tcp;
namespace ssl = asio::ssl;
namespace http = beast::http;

static Log l("dnld");

static bool parseUrl (const string &url, string &scheme, string &host,
        string &port, string &target)
{
    const char *s = url.c_str();
    const char *h = strstr(s, "://");
    if (!h) return false;

    scheme = {s, (unsigned long)(h - s)};
    h += 3;

    const char *t = strchr(h, '/');
    if (t) {
        target = t;
    } else {
        target = "/";
        t = url.c_str() + url.length();
    }

    const char *p = strchr(h, ':');
    if (p && p < t) {
        host = {h, (unsigned long)(p - h)};
        port = {p + 1, (unsigned long)(t - p - 1)};
    } else {
        host = {h, (unsigned long)(t - h)};
        // TODO: find default port number
        if (boost::starts_with(url, "https://")) {
            port = "443";
        } else {
            port = "80";
        }
    }
    return true;
}

namespace {

struct HttpDownloadResponseImpl: HttpDownloadResponse {

    HttpDownloadResponseImpl (http::response<BufferBody> &resp):
            response(resp) {}

    int status () override {
        return response.result_int();
    }

    string getHeader (const std::string &name) override {
        return string(response.base()[name]);
    }

    int bodyLength () override {
        return response.body()->size();
    }

    int readBody (byte_t *dst, int srcOffset, int sz) override {
        memcpy(dst, response.body()->begin() + srcOffset, sz);
        return sz;
    }

    http::response<BufferBody> &response;
};

struct Connection: RefCounter<Connection> {
    typedef function<void(system::error_code)> Handler;
    typedef function<void(system::error_code, size_t)> TransferHandler;

    virtual void abort () = 0;
    virtual void async_connect (tcp::resolver::results_type &resol,
            Handler h) = 0;
    virtual void async_write (http::request<http::empty_body> &request,
            TransferHandler h) = 0;
    virtual void async_read (beast::flat_buffer &buf,
            http::response<BufferBody> &resp,
            TransferHandler h) = 0;
    virtual void async_shutdown (Handler h) = 0;
};

struct TcpConnection: Connection {

    typedef TcpConnection C;

    asio::io_context &ioContext;
    tcp::socket socket;
    bool connected;

    TcpConnection (asio::io_context &ioc): ioContext(ioc), socket(ioc),
            connected(false) {}

    void abort () override {
        if (!connected) return;
        connected = false;
        socket.cancel();
    }

    void async_connect (tcp::resolver::results_type &resol, Handler h)
            override {
        asio::async_connect(socket, resol.begin(), resol.end(),
                bind(&C::onConnect, this, h, placeholders::_1));
    }

    void onConnect (Handler h, system::error_code ec) {
        if (!ec) connected = true;
        h(ec);
    }

    void async_write (http::request<http::empty_body> &request,
            TransferHandler h) override {
        // Send the HTTP request to the remote host
        http::async_write(socket, request, h);
    }

    void async_read (beast::flat_buffer &buf,
            http::response<BufferBody> &resp,
            TransferHandler h) override {
        // Receive the HTTP response
        http::async_read(socket, buf, resp, h);
    }

    void async_shutdown (Handler h) override {
        // Gracefully close the socket
        system::error_code ec;
        socket.shutdown(tcp::socket::shutdown_both, ec);
        ioContext.post(bind(h, ec));
    }
};

struct SslConnection: Connection {

    typedef SslConnection C;

    ssl::stream<tcp::socket> stream;
    bool aborted;
    bool connected;
    string host;

    SslConnection (asio::io_context &ioc, ssl::context &sslc, const string &h):
            stream(ioc, sslc), aborted(false), connected(false), host(h) {}

    void abort () override {
        if (aborted) return;
        aborted = true;
        if (connected) stream.next_layer().cancel();
    }

    void async_connect (tcp::resolver::results_type &resol, Handler h)
            override {

        // Set SNI Hostname (many hosts need this to handshake successfully)
        if(!SSL_set_tlsext_host_name(stream.native_handle(), host.c_str())) {
            system::error_code ec = {static_cast<int>(::ERR_get_error()),
                    asio::error::get_ssl_category()};
            asio::post(bind(h, ec));
            return;
        }

        // Make the connection on the IP address we get from a lookup
        asio::async_connect(stream.next_layer(), resol.begin(), resol.end(),
                bind(&C::onConnect, this, h, placeholders::_1));
    }

    void onConnect (Handler h, system::error_code ec) {
        if (!ec && aborted) {
            ec = system::errc::make_error_code(system::errc::operation_canceled);
        }
        if (ec) {
            h(ec);
            return;
        }
        connected = true;

        // Perform the SSL handshake
        stream.async_handshake(ssl::stream_base::client, h);
    }

    void async_write (http::request<http::empty_body> &request,
            TransferHandler h) override {
        // Send the HTTP request to the remote host
        http::async_write(stream, request, h);
    }

    void async_read (beast::flat_buffer &buf,
            http::response<BufferBody> &resp,
            TransferHandler h) override {
        // Receive the HTTP response
        http::async_read(stream, buf, resp, h);
    }

    void async_shutdown (Handler h) override {
        stream.async_shutdown(bind(&C::onShutdown, this, h, placeholders::_1));
    }

    void onShutdown (Handler h, system::error_code ec) {
#if 0
        if (ec == asio::error::eof) {
            // Rationale:
            // http://stackoverflow.com/questions/25587403/boost-asio-ssl-async-shutdown-always-finishes-with-an-error
            ec.assign(0, ec.category());
        }
        if(ec) {
            l.warn("Failed to shutdown socket");
            cout << ec << endl;
        }
#endif
        h({});
    }
};

struct HttpDownload: Download {

    HttpDownload (asio::io_context &ioc, ssl::context &sslc): ioContext(ioc),
            sslContext(sslc), resolver(ioc), aborted(false) {}

    asio::io_context &ioContext;
    ssl::context &sslContext;
    tcp::resolver resolver;
    beast::flat_buffer buffer;
    Ref<Connection> connection;
    DownloadManager *downloadManager;
    http::request<http::empty_body> request;
    http::response<BufferBody> response;
    Ref<HttpDownloadCompletionHandler> completionHandler;
    bool aborted;
    Buffer internalBodyBuffer;

    typedef HttpDownload C;

    void _abort () {
        if (aborted) return;
        aborted = true;
        if (connection) {
            connection->abort();
            connection = nullptr;
        }
    }

    void abort () override {
        asio::post(bind(&C::_abort, Ref<C>{this}));
    }

    bool start (const HttpDownloadRequest &req) {
        string scheme, host, port, target;
        if (!parseUrl(req.url, scheme, host, port, target)) return false;
        // Set up an HTTP GET request message
        request.version(11);
        request.method(http::verb::get);
        request.target(target.c_str());
        request.set(http::field::host, host);
        request.set(http::field::user_agent, BOOST_BEAST_VERSION_STRING);
        response.body() = req.bodyBuffer ? req.bodyBuffer : &internalBodyBuffer;

        if (scheme == "http") {
            connection = new TcpConnection(ioContext);
        } else if (scheme == "https") {
            connection = new SslConnection(ioContext, sslContext, host);
        } else {
            return false;
        }

        // any failure goes to handler from now on
        completionHandler = req.completionHandler;
        ref();

        // Look up the domain name
        resolver.async_resolve(host, port, bind(&C::onResolve, this,
                placeholders::_1, placeholders::_2));

        l.info("download %s", req.url.c_str());
        return true;
    }

    void onResolve (system::error_code ec,
            tcp::resolver::results_type results) {
        if (handleError(ec)) return;

        // Make the connection on the IP address we get from a lookup
        connection->async_connect(results, bind(&C::onConnect,
                this, placeholders::_1));
    }

    void onConnect (system::error_code ec) {
        if (handleError(ec)) return;

        // Send the HTTP request to the remote host
        connection->async_write(request, bind(&C::onWrite, this,
                placeholders::_1, placeholders::_2));
    }

    void onWrite (system::error_code ec, size_t bytes_transferred) {
            ignore_unused(bytes_transferred);
        if (handleError(ec)) return;

        // Receive the HTTP response
        connection->async_read(buffer, response, bind(&C::onRead, this,
                placeholders::_1, placeholders::_2));
    }

    void onRead (system::error_code ec, size_t bytes_transferred) {
        ignore_unused(bytes_transferred);
        if (handleError(ec)) return;

        // Gracefully close the stream
        connection->async_shutdown(
                bind(&C::onShutdown, this, placeholders::_1));
    }

    void onShutdown (system::error_code ec) {
#if 0
        if (ec == asio::error::eof) {
            // Rationale:
            // http://stackoverflow.com/questions/25587403/boost-asio-ssl-async-shutdown-always-finishes-with-an-error
            ec.assign(0, ec.category());
        }
        if(ec) {
            l.warn("Failed to shutdown socket");
            cout << ec << endl;
        }
#endif

        // handle redirect
        if (response.result_int() / 100 == 3 &&
                !response.base()["location"].empty()) {
            string loc(response.base()["location"]);
            string scheme, host, port, target;
            if (parseUrl(loc, scheme, host, port, target) && (
                    scheme == "http" || scheme == "https")) {
                l.info("redirecting to %s", loc.c_str());
                request.set(http::field::host, host);
                request.target(target.c_str());

                // clear previous states
                buffer = {};
                auto b = response.body();
                response = {};
                b->clear();
                response.body() = b;

                if (scheme == "http") {
                    connection = new TcpConnection(ioContext);
                } else {
                    connection = new SslConnection(ioContext, sslContext, host);
                }

                // restart!
                resolver.async_resolve(host, port, bind(&C::onResolve, this,
                        placeholders::_1, placeholders::_2));
                return;
            }
        }

        HttpDownloadResponseImpl resp(response);
        completionHandler->onCompletion(resp);
        completionHandler = nullptr;
        deref();
    }

    bool handleError (system::error_code ec) {
        if (!ec && aborted) {
            ec = system::errc::make_error_code(system::errc::operation_canceled);
        }
        if (ec) {
            onError(ec);
            return true;
        }
        return false;
    }

    void onError (system::error_code ec) {
        l.error("got error %s", ec.message().c_str());
        completionHandler->onError(ec.value());
        completionHandler = nullptr;
        deref();
    }
};

} // namespace

// FIXME: Add into class (or use single dnld mgr only)
static asio::io_context ioContext;
static asio::executor_work_guard<decltype(ioContext.get_executor())> work {ioContext.get_executor()};
static ssl::context sslContext {ssl::context::sslv23_client};

void *_start (void *)
{
    ioContext.run();
    return nullptr;
}

void DownloadManager::start ()
{
    sslContext.add_verify_path("/etc/ssl/certs");
    //sslContext.set_verify_mode(ssl::verify_peer);

    pthread_t t;
    pthread_create(&t, NULL, _start, NULL);
    pthread_detach(t);
}

Download *DownloadManager::download (const HttpDownloadRequest &req)
{
    if (!req.completionHandler) return nullptr;

    Ref<HttpDownload> hdl = new HttpDownload(ioContext, sslContext);

    if (!hdl->start(req)) return nullptr;

    return hdl.get();
}
