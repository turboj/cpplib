# TODO: This is not being used as ndk build cannot propagate export flags properly.

LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)
LOCAL_MODULE := dnld
LOCAL_SRC_FILES += $(wildcard $(LOCAL_PATH)/*.cpp $(LOCAL_PATH)/*.c)
LOCAL_EXPORT_C_INCLUDES := $(LOCAL_PATH)
LOCAL_C_INCLUDES := $(LOCAL_PATH)/../boost
LOCAL_STATIC_LIBRARIES := acba openssl_static opencrypto_static
include $(BUILD_STATIC_LIBRARY)
