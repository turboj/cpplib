#ifndef ACBA_DNLD_H
#define ACBA_DNLD_H

#include <functional>
#include <string>

#include "acba.h"

namespace acba {

struct HttpDownloadResponse {
    virtual int status () = 0;
    virtual std::string getHeader (const std::string &name) = 0;
    virtual int bodyLength () = 0;
    virtual int readBody (byte_t *dst, int srcOffset, int sz) = 0;
};

struct HttpDownloadCompletionHandler:
        acba::RefCounter<HttpDownloadCompletionHandler> {
    virtual void onCompletion (HttpDownloadResponse &resp) = 0;
    virtual void onError (int ec) = 0;
};

struct HttpDownloadRequest {
    HttpDownloadRequest (): bodyBuffer(nullptr) {}

    /**
     * Location of a file to download.
     * It should starts with either 'http://' or 'https://'
     */
    std::string url;

    /**
     * External buffer to fill in downloaded data.
     * If null is given, internal buffer will be used.
     */
    Buffer *bodyBuffer;

    /**
     * Once DownloadManager::download returns non-null download object,
     * It's guaranteed that either onCompletion or onError is called
     * from downloader's thread.
     */
    Ref<HttpDownloadCompletionHandler> completionHandler;
};

struct Download: RefCounter<Download> {
    virtual void abort () = 0;
};

struct DownloadManager {
    void start ();

    /**
     * Request to download a files located by either http or https.
     * This may return null when the given req is invalid
     * (e.g. url doesn't start with http:// or https://)
     * Once download object is returned, it can be aborted anytime,
     * and it's guaranteed that HttpDownloadCompletionHandler is invoked
     * at the end of download operation from internal thread.
     * Note that once HttpDownloadCompletionHandler is called, internal
     * reference to the Download object is gone.
     */
    Download *download (const HttpDownloadRequest &req);
};

}
#endif
