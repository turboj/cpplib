#ifndef ACBA_BUFFER_BODY_H
#define ACBA_BUFFER_BODY_H

#include <boost/beast/core/detail/config.hpp>
#include <boost/beast/http/error.hpp>
#include <boost/beast/http/message.hpp>
#include <boost/beast/core/detail/type_traits.hpp>
#include <boost/asio/buffer.hpp>
#include <boost/optional.hpp>
#include <cstdint>
#include <limits>
#include <memory>
#include <stdexcept>
#include <string>
#include <utility>

#include "acba.h"

namespace acba {

struct BufferBody {
    using value_type = Buffer*;

    static std::uint64_t size(value_type const& body) {
        if (!body) return 0;
        return body->size();
    }

    struct reader {
        value_type& body_;

        template<bool isRequest, class Fields>
        explicit reader (boost::beast::http::header<isRequest, Fields>&, value_type& b): body_(b) {}

        void init (boost::optional<std::uint64_t> const& length,
                boost::system::error_code& ec) {
            if (length) {
                if(static_cast<std::size_t>(*length) != *length) {
                    ec = boost::beast::http::error::buffer_overflow;
                    return;
                }
                body_->ensureMargin(static_cast<std::size_t>(*length));
            }
            ec.assign(0, ec.category());
        }

        template<class ConstBufferSequence>
        std::size_t put (ConstBufferSequence const& buffers, boost::system::error_code& ec) {
            using boost::asio::buffer_size;
            using boost::asio::buffer_copy;
            auto const n = buffer_size(buffers);
            body_->ensureMargin(n);
            ec.assign(0, ec.category());
            auto sz = buffer_copy(boost::asio::buffer(body_->end(), n), buffers);
            body_->grow(sz);
            return sz;
        }

        void finish (boost::system::error_code& ec) {
            ec.assign(0, ec.category());
        }
    };

    struct writer {
        value_type const& body_;

        using const_buffers_type = boost::asio::const_buffer;

        template<bool isRequest, class Fields>
        explicit writer(boost::beast::http::header<isRequest, Fields> const&, value_type const& b):
                body_(b) {}

        void init (boost::system::error_code& ec) {
            ec.assign(0, ec.category());
        }

        boost::optional<std::pair<const_buffers_type, bool>> get (
                boost::system::error_code& ec) {
            ec.assign(0, ec.category());
            return {{const_buffers_type{
                body_->begin(), (size_t)body_->size()}, false}};
        }
    };
};

}
#endif
