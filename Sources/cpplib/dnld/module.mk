$(call begin-target, dnld, staticlib)
    LOCAL_SRCS := $(wildcard $(LOCAL_PATH)/*.cpp)
    LOCAL_IMPORTS := acba-staticlib boost-staticlib
    EXPORT_CXXFLAGS := -I$(LOCAL_PATH)
    EXPORT_LDFLAGS := -lssl -lcrypto
$(end-target)

$(call begin-target, test_dnld, executable)
    LOCAL_LABELS := TEST
    LOCAL_SRCS := $(LOCAL_PATH)/test/test_dnld.cpp
    LOCAL_IMPORTS := acba-staticlib boost-staticlib dnld-staticlib
$(end-target)
