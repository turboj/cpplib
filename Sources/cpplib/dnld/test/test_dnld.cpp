#include <pthread.h>
#include <unistd.h>

#include <boost/beast/core.hpp>
#include <string>

#include "acba.h"
#include "dnld.h"

using namespace std;
using namespace acba;

static Log l("test");

DownloadManager dm;

struct SimpleDownloader: HttpDownloadCompletionHandler {
    SimpleDownloader (const string &url0): url(url0),
            l("test", "[" + url0 + "] ") {}

    string url;
    Log l;
    Ref<Download> task;
    Buffer body;

    void start () {
        HttpDownloadRequest req;
        req.url = url;
        req.bodyBuffer = &body;
        req.completionHandler = this;
        task = dm.download(req);
    }

    void abort () {
        if (task) task->abort();
    }

    void onCompletion (HttpDownloadResponse &resp) override {
        l.info("status:%d", resp.status());
        int len = resp.bodyLength();
        l.info("length:%d", len);
        assert(len == body.size());

        //body.dump();
    }

    void onError (int ec) override {
        l.error("Failed to download! ec:%d", ec);
    }
};

int main (int argc, char **argv)
{
    dm.start();

    Ref<SimpleDownloader> sd;

    sd = new SimpleDownloader("https://google.com");
    sd->start(); // runs asynchronously

    sd = new SimpleDownloader("https://wikipedia.org");
    sd->start(); // runs asynchronously

    sd = new SimpleDownloader("https://naver.com");
    sd->start(); // runs asynchronously

    sleep(2);

    sd->abort();

    sleep(2);

    return 0;
}
