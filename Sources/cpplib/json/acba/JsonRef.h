#ifndef ACBA_JSONREF_H
#define ACBA_JSONREF_H

#include <stdint.h>
#include <string>

#include "nlohmann/json.hpp"

namespace acba {

struct JsonRef {
    JsonRef (): value(nullptr) {}
    JsonRef (const nlohmann::json *j): value(j) {}
    JsonRef (const nlohmann::json &j): value(&j) {}
    JsonRef (const JsonRef &r): value(r.value) {}

    JsonRef &operator= (const JsonRef &r) {
        value = r.value;
        return *this;
    }

    const nlohmann::json *value;

    const std::string &asString () const {
        static std::string emptyString;
        if (!value || !value->is_string()) return emptyString;

        return value->get_ref<const nlohmann::json::string_t&>();
    }

    bool toBool (bool fallback = false) const {
        if (!value || value->is_null()) return fallback;
        if (value->is_boolean()) return value->get<bool>();
        return true;
    }

    uint32_t toU64 (uint64_t fallback = 0) const {
        if (!value || !value->is_number()) return fallback;
        return value->get<uint64_t>();
    }

    uint32_t toU32 (uint32_t fallback = 0) const {
        if (!value || !value->is_number()) return fallback;
        return value->get<uint32_t>();
    }

    uint32_t toI64 (uint64_t fallback = 0) const {
        if (!value || !value->is_number()) return fallback;
        return value->get<int64_t>();
    }

    uint32_t toI32 (uint32_t fallback = 0) const {
        if (!value || !value->is_number()) return fallback;
        return value->get<int32_t>();
    }

    float toF32 (float fallback = 0.0f) const {
        if (!value || !value->is_number()) return fallback;
        return value->get<float>();
    }

    double toF64 (double fallback = 0.0) const {
        if (!value || !value->is_number()) return fallback;
        return value->get<double>();
    }

    // Returns length of array if the json is array.
    // Returns 0 otherwise.
    int length () const {
        if (!value || !value->is_array()) return 0;
        return value->size();
    }

    bool isNull () const {
        return (!value || value->is_null());
    }

    bool isString () const {
        return (value && value->is_string());
    }

    bool isObject () const {
        return (value && value->is_object());
    }

    bool isArray () const {
        return (value && value->is_array());
    }

    bool isNumber () const {
        return (value && value->is_number());
    }

    JsonRef operator[] (const std::string &k) const {
        if (!value || !value->is_object()) return nullptr;
        auto i = value->find(k);
        if (i == value->end()) return JsonRef(nullptr);
        return *i;
    }

    JsonRef operator[] (uint32_t i) const {
        if (!value || !value->is_array()) return nullptr;
        if (i >= value->size()) return nullptr;
        return value->at(i);
    }

    const nlohmann::json &operator* () const {
        static nlohmann::json jnull = nullptr;
        return value ? *value : jnull;
    }

    std::string dump (int indent) {
        if (!value) return "null";
        return value->dump(indent);
    }
};

}
#endif
