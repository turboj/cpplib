#include <set>
#include <map>

#include "acba.h"
#include "nlohmann/json.hpp"
#include "JsonRef.h"
#include "unifiedtv.h"
#include "unifiedtv_atsc3.h"
#include "atsc3_model.h"
#include "atsc3_task.h"

using namespace std;
using namespace acba;
using namespace unifiedtv;
using json = nlohmann::json;

static Log l("unifiedtv_atsc3", "scanning: ");

namespace {

static Reactor *notifier = nullptr;

struct Monitor: Listener<atsc3::Tune::EVENT> {
    Monitor (): useCount(0), locked(false) {}
    Ref<atsc3::Tune> tune;
    bool locked;
    int useCount;

    void onUpdate (atsc3::Tune::EVENT e) override {
        bool l;
        switch (e) {
        case atsc3::Tune::EVENT_TUNER_LOCKED:
            l = true;
            break;
        case atsc3::Tune::EVENT_TUNER_UNLOCKED:
            l = false;
            break;
        case atsc3::Tune::EVENT_SERVICE_LIST_RECEIVED:
            return;
        }

        if (locked == l) return;

        locked = l;
        atsc3::task.eventDriver.setTimeout(0, notifier);
    }
};

static map<string, Monitor> monitors;

struct CallbackUpdateTuneState: RpcNotificationHandler, Reactor {

    CallbackUpdateTuneState () {
        notifier = this;
    }

    typedef map<string, bool> TuneList;

    map<RpcPeer*, TuneList> peers;

    bool subscribe (const string &type, const Ref<RpcPeer> &peer) override {
        auto pi = peers.find(peer.get());
        if (pi != peers.end()) return false;

        peers[peer.get()]; // create empty slot
        atsc3::task.eventDriver.setTimeout(0, this);
        return true;
    }

    bool unsubscribe (const string &type, const Ref<RpcPeer> &peer) override {
        return peers.erase(peer.get()) > 0;
    }

    void processEvent (int evt) override {
        for (auto &p: peers) syncPeer(p.first, p.second);
    }

    void syncPeer (RpcPeer *p, TuneList &tl) {

        // Silently remove tunes which are released
        for (auto ti = tl.begin(); ti != tl.end(); ) {
            if (monitors.find(ti->first) == monitors.end()) {
                ti = tl.erase(ti);
            } else {
                ti++;
            }
        }

        // check adds/mods
        for (auto &m: monitors) {
            auto tli = tl.find(m.first);
            auto l = m.second.locked;
            if (tli != tl.end() && l == tli->second) continue;

            tl[m.first] = l;

            p->sendMessage({
                {"jsonrpc", "2.0"},
                {"method", "updateTuneState"},
                {"params", {
                    {"location", m.first},
                    {"domainId", "atsc3"},
                    {"state", l? "locked": "unlocked"},
                }}
            });
        }
    }
};

}

static unifiedtv_atsc3::Registry r[] = {

    {"requestTune", [] (const JsonRef req, const Ref<RpcPeer> peer) {

        if (req["params"]["domainId"].asString() != "atsc3") {
            peer->sendMessage({
                {"jsonrpc", "2.0"},
                {"error", {
                    {"code", -1},
                    {"message", "Unsupported domain id"}
                }},
                {"id", *req["id"]}
            });
            return;
        }

        auto &loc = req["params"]["location"].asString();

        // reuse previous tune if exists
        auto mi = monitors.find(loc);
        if (mi != monitors.end()) {
            mi->second.useCount++;
            peer->sendMessage({
                {"jsonrpc", "2.0"},
                {"result", {}},
                {"id", *req["id"]}
            });
            return;
        }

        // request tune
        Ref<atsc3::Tune> t = atsc3::task.tuneManager.requestTune(loc);
        if (!t) {
            peer->sendMessage({
                {"jsonrpc", "2.0"},
                {"error", {
                    {"code", -1},
                    {"message", "failed to tune"}
                }},
                {"id", *req["id"]}
            });
            return;
        }

        // bind to monitor
        auto &m = monitors[loc];
        m.tune = t;
        m.useCount = 1;
        t->addListener(&m);

        // success
        peer->sendMessage({
            {"jsonrpc", "2.0"},
            {"result", {}},
            {"id", *req["id"]}
        });

        // send locked event immediately if the tune was being reused,
        // and also already locked.
        // TODO: This behavior is temporally necessary as we don't provide
        //      tuner status query api for now.
        if (t->isLocked()) {
            m.onUpdate(atsc3::Tune::EVENT_TUNER_LOCKED);
        }
    }},

    {"releaseTune", [] (const JsonRef req, const Ref<RpcPeer> peer) {

        if (req["params"]["domainId"].asString() != "atsc3") return;

        auto &loc = req["params"]["location"].asString();
        auto mi = monitors.find(loc);
        if (mi == monitors.end()) return;

        auto &m = mi->second;
        m.useCount--;
        if (m.useCount > 0) return;

        m.tune->removeListener(&m);
        atsc3::task.tuneManager.releaseTune(m.tune);
        monitors.erase(mi);
    }},

    {"updateTuneState", new CallbackUpdateTuneState()},
};
