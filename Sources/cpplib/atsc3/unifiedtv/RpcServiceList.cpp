#include <set>
#include <map>

#include "acba.h"
#include "nlohmann/json.hpp"
#include "JsonRef.h"
#include "unifiedtv.h"
#include "unifiedtv_atsc3.h"
#include "atsc3_model.h"
#include "atsc3_task.h"

using namespace std;
using namespace acba;
using namespace unifiedtv;
using json = nlohmann::json;

static Log l("unifiedtv_atsc3", "servicelist: ");

namespace {

struct CallbackUpdateSelection: RpcNotificationHandler, Listener<> {

    set<RpcPeer*> peers;

    Ref<atsc3::ServiceTask> currentServiceTask; // trace current service task
    bool wasPlayReady; // valid only when currentServiceTask is not null

    CallbackUpdateSelection (): listening(false) {}

    bool subscribe (const string &type, const Ref<RpcPeer> &peer) override {
        if (!listening) {
            atsc3::task.serviceManager.currentServiceTaskUpdate.addListener(this);
            listening = true;
        }

        return peers.insert(peer.get()).second;
    }

    bool unsubscribe (const string &type, const Ref<RpcPeer> &peer) override {
        return peers.erase(peer.get()) > 0;
    }

    void onUpdate () override {
        auto &smcst = atsc3::task.serviceManager.currentServiceTask;

        if (smcst == currentServiceTask) {
            if (smcst->playReady == wasPlayReady) return; // no change?
            wasPlayReady = smcst->playReady;
        } else {
            if (currentServiceTask) {
                currentServiceTask->playReadyUpdate.removeListener(this);
                currentServiceTask = nullptr;
            }

            if (smcst) {
                smcst->playReadyUpdate.addListener(this);
                currentServiceTask = smcst;
                wasPlayReady = smcst->playReady;
            }
        }

        json sid = nullptr;
        json st = "selected";
        if (currentServiceTask) {
            sid = "atsc3:" + smcst->service->globalId;
            if (!wasPlayReady) st = "selecting";
        }
        json msg = {
            {"jsonrpc", "2.0"},
            {"method", "updateSelection"},
            {"params", {
                {"serviceId", sid},
                {"state", st}
            }}
        };

        for (auto p: peers) p->sendMessage(msg);
    }

private:
    bool listening;
};

struct CallbackUpdateServiceList: RpcNotificationHandler, Reactor,
        Listener<atsc3::ESG_FRAGMENT_EVENT, const string&,
        const acba::Ref<atsc3::EsgFragmentSlide>&,
        const acba::Ref<atsc3::BroadcastStream>&>,
        Listener<const Ref<atsc3::Service>&> {

    struct ServiceInfo {
        ServiceInfo (): majorNumber(-1), minorNumber(-1) {}
        string name;
        int majorNumber;
        int minorNumber;
        Ref<atsc3::XmlDocument> fragment;
    };
    typedef map<string, ServiceInfo> ServiceList;

    map<RpcPeer*, ServiceList> peers;

    CallbackUpdateServiceList (): listening(false) {}

    bool subscribe (const string &type, const Ref<RpcPeer> &peer) override {
        if (!listening) {
            atsc3::model.serviceUpdate.addListener(this);
            atsc3::model.esgFragmentUpdate.addListener(this);
            listening = true;
        }

        auto pi = peers.find(peer.get());
        if (pi != peers.end()) return false;

        peers[peer.get()]; // create empty service list
        atsc3::task.eventDriver.setTimeout(0, this);
        return true;
    }

    bool unsubscribe (const string &type, const Ref<RpcPeer> &peer) override {
        return peers.erase(peer.get()) > 0;
    }

    void onUpdate (atsc3::ESG_FRAGMENT_EVENT evt, const string &fid,
            const Ref<atsc3::EsgFragmentSlide>& fs,
            const Ref<atsc3::BroadcastStream>& bs) override {
        if (fs->type != /*service*/1) return;
        atsc3::task.eventDriver.setTimeout(0, this);
    }

    void onUpdate (const Ref<atsc3::Service> &srv) override {
        atsc3::task.eventDriver.setTimeout(0, this);
    }

    void processEvent (int evt) override {
        ServiceList lst;

        // contruct service list from slts
        for (auto &bs: atsc3::model.broadcastStreams) {
            for (auto &sg: bs.second->serviceGroups) {
                for (auto &s: sg.second->services) {
                    auto &srv = *s.second;
                    auto &gid = srv.globalId;
                    if (gid.empty()) continue;
                    auto &si = lst[gid];

                    auto name = srv.xml.attribute("shortServiceName").
                            as_string(nullptr);
                    si.name = name ? name : gid;
                    si.majorNumber = srv.xml.attribute("majorChannelNo").as_int(-1);
                    si.minorNumber = srv.xml.attribute("minorChannelNo").as_int(-1);
                }
            }
        }

        // find associated fragment
        for (auto bs: atsc3::model.broadcastStreams) {
            for (auto fs: bs.second->esgFragmentSlides) {
                if (!fs.second || fs.second->type != 1 ||
                        !fs.second->currentFragment) continue;

                auto &cf = fs.second->currentFragment;
                auto xml = cf->document_element();
                string gid = xml.attribute("globalServiceID").value();

                auto sii = lst.find(gid);
                if (sii == lst.end()) continue;
                auto &si = sii->second;

                si.fragment = cf;
                auto name = xml.child("Name").attribute("text").as_string(nullptr);
                if (name) si.name = name;

                auto maj = xml.child("PrivateExt").
                        child("sa:ATSC3ServiceExtension").
                        child("sa:MajorChannelNum").text().as_int(-1);
                if (maj != -1) si.majorNumber = maj;

                auto min = xml.child("PrivateExt").
                        child("sa:ATSC3ServiceExtension").
                        child("sa:MinorChannelNum").text().as_int(-1);
                if (min != -1) si.minorNumber = min;
            }
        }

        for (auto &p: peers) syncPeer(p.first, p.second, lst);
    }

    void syncPeer (RpcPeer *p, ServiceList &lst, const ServiceList &nlst) {
        json adds = json::array();
        json mods = json::array();
        json dels = json::array();

        // check removals
        for (auto &si: lst) {
            if (nlst.find(si.first) != nlst.end()) continue;
            dels.push_back({
                {"id", "atsc3:" + si.first}
            });
        }

        // check adds/mods
        for (auto &nsi: nlst) {
            auto si = lst.find(nsi.first);
            auto &ns = nsi.second;
            bool exists = (si != lst.end());
            if (exists && si->second.fragment == ns.fragment) {
                // no change
                continue;
            }

            json s = {
                {"id", "atsc3:" + nsi.first},
                {"domainId", "atsc3"},
                {"name", ns.name}
            };
            if (ns.majorNumber >= 0) s["major"] = ns.majorNumber;
            if (ns.minorNumber >= 0) s["minor"] = ns.minorNumber;

            if (exists) {
                mods.push_back(s);
            } else {
                adds.push_back(s);
            }
        }

        // abort if no change occurred
        if (adds.size() == 0 && mods.size() == 0 && dels.size() == 0) {
            return;
        }

        lst = nlst;
        p->sendMessage({
            {"jsonrpc", "2.0"},
            {"method", "updateServiceList"},
            {"params", {
                {"add", adds},
                {"remove", dels},
                {"modify", mods}
            }}
        });
    }

private:
    bool listening;
};

}

static bool findAtsc3 (const JsonRef ids)
{
    if (ids.isNull()) return true;

    for (int i = 0; i < ids.length(); i++) {
        if (ids[i].asString() != "atsc3") continue;
        return true;
    }
    return false;
}

static unifiedtv_atsc3::Registry r[] = {

    {"saveServiceList", [] (const JsonRef req, const Ref<RpcPeer> peer) {

        if (findAtsc3(req["params"]["domainIds"])) {
            atsc3::task.serviceManager.save();
        }

        peer->sendMessage({
            {"jsonrpc", "2.0"},
            {"result", {}},
            {"id", *req["id"]}
        });
    }},

    {"loadServiceList", [] (const JsonRef req, const Ref<RpcPeer> peer) {

        if (findAtsc3(req["params"]["domainIds"])) {
            atsc3::task.serviceManager.load();
        }

        peer->sendMessage({
            {"jsonrpc", "2.0"},
            {"result", {}},
            {"id", *req["id"]}
        });
    }},

    {"clearServiceList", [] (const JsonRef req, const Ref<RpcPeer> peer) {

        if (findAtsc3(req["params"]["domainIds"])) {
            atsc3::task.serviceManager.clear();
        }

        peer->sendMessage({
            {"jsonrpc", "2.0"},
            {"result", {}},
            {"id", *req["id"]}
        });
    }},

    {"select", [] (const JsonRef req, const Ref<RpcPeer> peer) {
        auto &usid = req["params"].asString();

        atsc3::Service *srv = nullptr;

        if (usid.rfind("atsc3:", 0) == 0) {
            auto gsid = usid.substr(6);
            for (auto bs: atsc3::model.broadcastStreams) {
                for (auto sg: bs.second->serviceGroups) {
                    for (auto s: sg.second->services) {
                        if (s.second->globalId == gsid) srv = s.second;
                    }
                }
            }
        }

        if (!usid.empty() && !srv) {
            l.warn("Service not found by %s. Unselecting.", usid.c_str());
        }

        // FIX ME: task.rmp.setScalePosition(100, 0, 0);
        atsc3::model.selectService(srv);
    }},

    {"updateSelection", new CallbackUpdateSelection()},
    {"updateSerivceList", new CallbackUpdateServiceList()},
};
