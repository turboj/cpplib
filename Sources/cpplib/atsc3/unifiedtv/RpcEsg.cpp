#include <set>
#include <map>

#include "acba.h"
#include "nlohmann/json.hpp"
#include "JsonRef.h"
#include "unifiedtv.h"
#include "unifiedtv_atsc3.h"
#include "atsc3_model.h"
#include "atsc3_task.h"

using namespace std;
using namespace acba;
using namespace unifiedtv;
using json = nlohmann::json;

static Log l("unifiedtv_atsc3", "esg: ");

namespace {

struct CallbackNotifyEsgUpdate: RpcNotificationHandler, Reactor,
        Listener<atsc3::ESG_FRAGMENT_EVENT, const string&,
        const Ref<atsc3::EsgFragmentSlide>&,
        const Ref<atsc3::BroadcastStream>&> {

    CallbackNotifyEsgUpdate (): listening(false) {}

    set<RpcPeer*> peers;
    set<string> serviceIds;

    bool subscribe (const string &type, const Ref<RpcPeer> &peer) override {
        if (!listening) {
            atsc3::model.esgFragmentUpdate.addListener(this);
            listening = true;
        }

        return peers.insert(peer.get()).second;
    }

    bool unsubscribe (const string &type, const Ref<RpcPeer> &peer) override {
        return peers.erase(peer.get()) > 0;
    }

    void onUpdate (atsc3::ESG_FRAGMENT_EVENT evt, const string &fid,
            const Ref<atsc3::EsgFragmentSlide>& fs,
            const Ref<atsc3::BroadcastStream>& bs) override {

        atsc3::XmlDocument *sfrag = nullptr; // service fragment

        switch (fs->type) {
        case 1: // service
            sfrag = fs->currentFragment.get();
            break;

        case 2: // content
        case 3: // schedule
            {
                if (!fs->currentFragment) break;
                auto sfragid = fs->currentFragment->document_element().
                        child("ServiceReference").attribute("idRef").
                        as_string(nullptr);
                if (!sfragid) break;
                auto sfsi = bs->esgFragmentSlides.find(sfragid);
                if (sfsi == bs->esgFragmentSlides.end()) break;
                sfrag = sfsi->second->currentFragment.get();
            }
            break;
        }

        // trigger notification only when new service id is added.
        if (!sfrag) return;
        auto gid = sfrag->document_element().attribute("globalServiceID").
                as_string(nullptr);
        if (gid && serviceIds.insert(gid).second) {
            atsc3::task.eventDriver.setTimeout(1, this);
        }
    }

    void processEvent (int evt) override {
        json sids = json::array();
        for (auto &sid: serviceIds) sids.push_back("atsc3:" + sid);
        serviceIds.clear();

        json msg = {
            {"jsonrpc", "2.0"},
            {"method", "notifyEsgUpdate"},
            {"params", {
                {"serviceIds", sids}
            }}
        };

        for (auto p: peers) p->sendMessage(msg);
    }

private:
    bool listening;
};

}

static unifiedtv_atsc3::Registry r[] = {

    {"queryEsg", [] (const JsonRef req, const Ref<RpcPeer> peer) {

        auto params = req["params"];

        auto jSrvs = params["serviceIds"];
        set<string> sids;
        for (int i = 0; ; i++) {
            auto srv = jSrvs[i];
            if (!srv.isString()) break;
            sids.insert(srv.asString());
        }
        uint32_t startTime = params["startTime"].toU32(0);
        uint32_t endTime = params["endTime"].toU32(numeric_limits<uint32_t>::max());

        json result = json::object();
        for (auto bs: atsc3::model.broadcastStreams) {
            for (auto fs: bs.second->esgFragmentSlides) {
                // filter-out non-schedule fragments
                if (!fs.second || fs.second->type != 3 ||
                        !fs.second->currentFragment) continue;

                // filter-out by the given service list
                auto frag = fs.second->currentFragment->document_element();
                auto sfsi = bs.second->esgFragmentSlides.find(frag.child(
                        "ServiceReference").attribute("idRef").value());
                if (sfsi == bs.second->esgFragmentSlides.end()) continue;
                auto &sfs = sfsi->second;
                if (sfs->type != 1 || !sfs->currentFragment) continue;
                string sid = string("atsc3:") +
                        sfs->currentFragment->document_element().
                        attribute("globalServiceID").value();

                if (sids.find(sid) == sids.end()) continue;

                for (auto cr: frag.children("ContentReference")) {
                    auto cfragi = bs.second->esgFragmentSlides.find(
                            cr.attribute("idRef").value());
                    if (cfragi == bs.second->esgFragmentSlides.end()) continue;
                    auto &cfrag = cfragi->second;
                    if (cfrag->type != 2 || !cfrag->currentFragment) continue;

                    for (auto pw: cr.children("PresentationWindow")) {

                        // Read esg time of NTP as EPOCH
                        uint32_t st = pw.attribute("startTime").as_ullong(0);
                        uint32_t et = pw.attribute("endTime").as_ullong(-1);
                        if (st < 2208988800) {
                            st = 0;
                        } else {
                            st -= 2208988800;
                        }
                        if (et != 0xffffffff && et > 2208988800) {
                            et -= 2208988800;
                        }

                        // NOTE:
                        // Condition is satisfied only when
                        // non-zero time is overlapping.
                        uint32_t b = max(startTime, st);
                        uint32_t e = min(endTime, et);
                        if (b >= e) continue;

                        auto &arr = result[sid];
                        if (!arr.is_array()) arr = json::array();

                        auto ct = cfrag->currentFragment->document_element();
                        arr.push_back({
                            {"title", ct.child("Name").attribute("text").value()},
                            {"start", st},
                            {"end", et},
                            {"description", ct.child("Description").attribute("text").value()},
                            {"ratings", ct.child("ParentalRating").text().get()}
                        });
                    }
                }
            }
        }

        // add empty array for services whose ESG is not yet available.
        for (auto &bs: atsc3::model.broadcastStreams) {
            for (auto &sg: bs.second->serviceGroups) {
                for (auto &s: sg.second->services) {
                    if (s.second->globalId.empty()) continue;
                    auto sid = "atsc3:" + s.second->globalId;
                    if (sids.count(sid) == 0) continue;

                    auto &arr = result[sid];
                    if (!arr.is_array()) arr = json::array();
                }
            }
        }

        peer->sendMessage({
            {"jsonrpc", "2.0"},
            {"result", result},
            {"id", *req["id"]}
        });
    }},

    {"notifyEsgUpdate", new CallbackNotifyEsgUpdate()},
};
