#include <set>

#include "acba.h"
#include "nlohmann/json.hpp"
#include "JsonRef.h"
#include "unifiedtv.h"
#include "unifiedtv_atsc3.h"

using namespace std;
using namespace acba;
using namespace unifiedtv;
using json = nlohmann::json;

static Log l("unifiedtv_atsc3", "general: ");

static unifiedtv_atsc3::Registry r[] = {

    {"subscribe", [] (const JsonRef req, const Ref<RpcPeer> peer) {

        auto jMethods = JsonRef(req)["params"]["methods"];
        set<string> methods;

        if (jMethods.isNull()) {
            for (auto nhi: peer->manager.notificationHandlers) {
                methods.insert(nhi.first);
            }
        } else if (jMethods.isArray()) {
            for (int i = 0; i < jMethods.length(); i++) {
                auto mt = jMethods[i];
                if (!mt.isString()) {
                    l.warn("msgType contains non-string item");
                    continue;
                }
                methods.insert(mt.asString());
            }
        }

        auto subscribes = json::array();
        for (auto m: methods) {
            if (!peer->subscribe(m)) continue;
            subscribes.push_back(m);
        }

        peer->sendMessage({
            {"jsonrpc", "2.0"},
            {"id", *req["id"]},
            {"result", {
                {"methods", subscribes}
            }}
        });
    }},

    {"unsubscribe", [] (const JsonRef req, const Ref<RpcPeer> peer) {

        auto jMethods = req["params"]["methods"];
        set<string> methods;

        if (jMethods.isNull()) {
            methods = peer->subscriptions;
        } else if (jMethods.isArray()) {
            for (int i = 0; i < jMethods.length(); i++) {
                auto mt = jMethods[i];
                if (!mt.isString()) {
                    l.warn("msgType contains non-string item");
                    continue;
                }
                methods.insert(mt.asString());
            }
        }

        auto unsubscribes = json::array();
        for (auto mt: methods) {
            if (!peer->unsubscribe(mt)) continue;
            unsubscribes.push_back(mt);
        }

        peer->sendMessage({
            {"jsonrpc", "2.0"},
            {"id", *req["id"]},
            {"result", {
                {"methods", unsubscribes}
            }}
        });
    }},

    {"queryDomains", [] (const JsonRef req, const Ref<RpcPeer> peer) {
        peer->sendMessage({
            {"jsonrpc", "2.0"},
            {"result", {
                {
                    {"id", "atsc3"},
                    {"display", "ATSC 3.0"},
                    {"description", "NextGenTv"}
                }
            }},
            {"id", *req["id"]}
        });
        return;
    }},
};
