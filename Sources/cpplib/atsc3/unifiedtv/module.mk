$(call begin-target, unifiedtv_atsc3, staticlib)
    LOCAL_SRCS := $(wildcard $(LOCAL_PATH)/*.cpp)
    LOCAL_IMPORTS := acba-staticlib json-nlohmann unifiedtv-staticlib
    LOCAL_IMPORTS += atsc3_model-staticlib atsc3_task-staticlib atsc3_web-staticlib
    LOCAL_CXXFLAGS := -DUSE_UNIFIEDTV_ATSC3=1

    LOCAL_WHOLE_ARCHIVE := true

    EXPORT_CXXFLAGS := -I$(LOCAL_PATH) $(LOCAL_CXXFLAGS)
    EXPORT_IMPORTS := $(LOCAL_IMPORTS)
$(end-target)
