#include "acba.h"
#include "atsc3_web.h"
#include "unifiedtv_atsc3.h"

using namespace acba;
using namespace unifiedtv_atsc3;

static Log l("unifiedtv_atsc3");

Adaptor &unifiedtv_atsc3::adaptor ()
{
    static Adaptor instance;
    return instance;
}

void unifiedtv_atsc3::start ()
{
    l.info("Starting UnifiedTV Adaptor...");
    ::atsc3::web.httpServer.bind("/unifiedtv_atsc3", &adaptor().rpcManager);
}
