LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)

LOCAL_MODULE := unifiedtv_atsc3

LOCAL_SRC_FILES += $(wildcard $(LOCAL_PATH)/*.cpp)
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../json/single_include
LOCAL_STATIC_LIBRARIES := unifiedtv atsc3 acba

LOCAL_EXPORT_C_INCLUDES := $(LOCAL_PATH)
LOCAL_EXPORT_CFLAGS := -DUSE_UNIFIEDTV_ATSC3=1

include $(BUILD_STATIC_LIBRARY)
