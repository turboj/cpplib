#ifndef ATSC3_WEB_H
#define ATSC3_WEB_H

#include "rpc_manager.h"
#include "file_server.h"
#include "dash_streamer.h"
#include "app_server.h"
#include "http_server.h"
#include "mpu_streamer.h"
#include "cd_server.h"

namespace atsc3 {

struct Web {
    acba::HttpServerReactor httpServer;
    RpcManager rpcManager;
    FileServer fileServer;
    AppServer appServer;
    DashStreamer dashStreamer;
    MpuStreamer mpuStreamer;
    CdServer cdServer;
};

extern Web web;

}
#endif
