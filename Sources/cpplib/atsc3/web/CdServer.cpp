#include <map>
#include <set>
#include <string>
#include <sstream>
#include <vector>
#include <thread>
#include <boost/asio.hpp>

#include "acba.h"
#include "atsc3_model.h"
#include "atsc3_task.h"
#include "cd_server.h"

using namespace std;
using namespace acba;
using namespace atsc3;

static Log l("cdsvr");

static pthread_mutex_t activeHandlers_mutex = PTHREAD_MUTEX_INITIALIZER;
static set<int> activeHandlers;

struct CdHttpHandler: HttpRequestHandler {
    void handleHttpRequest (HttpReactor &r) override {
        auto cdm = CdManager::getInstance();

        const string &path = r.requestHandlerPath;
        for(auto it = r.headers.cbegin(); it != r.headers.cend(); ++it)
        {
            stringstream ss;
            ss << it->first << " " << it->second ;
            l.debug("header : %s", ss.str().c_str());
        }
        if (path == CD_DESC_XML) {
            // Description.xml
            string xml = cdm->getUpnpDescriptionXml();
            vector<string> headers;
            headers.push_back("CONTENT-TYPE: text/xml; charset=\"utf-8\"");
            headers.push_back("CONTENT-LANGUAGE: en-US");
            stringstream ss;
            ss << "Application-URL: " << cdm->getWebEndPoint();
            headers.push_back(ss.str());
            headers.push_back("Access-Control-Allow-Origin:*");
            r.sendHead(200, headers, xml.size());
            r.sendBody(xml);
            r.endResponse();
        } else if (path == CD_WEB_ENDPOINT) {
            // App2AppURL
            string xml = cdm->getAppToAppUrlXml();
            vector<string> headers;
            headers.push_back("CONTENT-TYPE: text/xml; charset=\"utf-8\"");
            headers.push_back("CONTENT-LANGUAGE: en-US");
            r.sendHead(200, headers, xml.size());
            r.sendBody(xml);
            r.endResponse();
        } else {
            l.debug("CD Web server returns 404 Invalid Path");
            r.sendResponse(404
                , {"Content-Type: text/plain"}, "Invalid Path");
        }
    }
};

static nlohmann::json getErrorResult(int id, int errorCode, string errorMsg) {
    nlohmann::json result = {
        {"jsonrpc", "2.0"}
        , {"result", {{"code", errorCode}, {"message", errorMsg}}}
        , {"id", id}
    };
    return result;
}

static json org_atsc_cdApp_launch(const json &req) {
    auto cdm = CdManager::getInstance();

    l.info("org_atsc_cdApp_launch(json)");
    json result = {};
    int id = -1;
    try {
        id = req["id"];
        json params = req["params"];
        bool includeWsEndpoints = false;
        string appliactionUrlOfLauncher;
        bool isValid;
        if (params.empty()) {
            return getErrorResult(id, -18, "No params");
        } else {
            int launcherId = params["id"];
            {
                ScopedLock sl(cdm->connected_launchers_mutex);
                set <LauncherInfo *>& launchers
                    = cdm->getConnectedLaunchers();
                if (launchers.size() > 0) {
                    for (auto iter = launchers.begin();
                            iter != launchers.end() ; iter++) {
                        LauncherInfo &tmp = **iter;
                        if (tmp.launcherId == launcherId) {
                            l.info("target launcherId = %d", launcherId);
                            appliactionUrlOfLauncher = tmp.applicationUrl;
                            isValid = tmp.valid;
                            break;
                        }
                    }
                }
            }
            if (appliactionUrlOfLauncher.size() == 0) {
                return getErrorResult(id, -18, "cannot find the launcher");
            } else if (!isValid) {
                return getErrorResult(id, -18, "Launcher is invalid");
            }
            json parameters = params["parameters"];
            if (parameters.empty()) {
                return getErrorResult(id, -18, "No payload");
            }
            string payload = parameters.dump(4);
            l.debug("payload = %s", payload.c_str());

            Url url = cdm->parse_url(appliactionUrlOfLauncher);
            // ex) "10.0.2.15", "7001", "/launcherEndPoint"
            string response = CdManager::getInstance()
                    ->doSyncHttp("POST", url.domain, url.port,
                        url.path, payload);
            l.debug("POST response from CD = %s", response.c_str());
            stringstream response_stream(response);
            string http_version;
            response_stream >> http_version;
            unsigned int status_code;
            response_stream >> status_code;
            string status_message;
            l.debug("status_message = %s" ,cdm->trim(status_message).c_str());
            if (!response_stream || http_version.substr(0, 5) != "HTTP/")
            {
                l.error("Invalid response");
                return getErrorResult(id, -18, "Invalid HTTP response");
            }
            if (status_code != 200)
            {
                l.error("Response returned with status code : %d"
                    , status_code);
                return getErrorResult(id, -18, "Invalid HTTP status");
            }
            string aLine;
            string errorCode;
            while (getline(response_stream, aLine) && aLine!= "\r") {
                //Error-Code :ALTICAST specific header by alticast launcher
                if (cdm->findStrIc(aLine, "Error-Code") >= 0) {
                    errorCode = cdm->getHeaderValue(aLine);
                    break;
                }
            }
            if (errorCode.size() == 0) {
                return getErrorResult(id, -18, "Parse Error");
            }
            int code = stoi(errorCode);
            if (code == -16) {
                // OK
                result = {
                    {"jsonrpc", "2.0"}
                    , {"result", {}}
                    , {"id", id}
                };
            } else {
                // Error
                return getErrorResult(id, code, "Error from Launcher");
            }
        }
    } catch (exception &e) {
        l.error("Error during org_atsc_cdApp_launch: %s", e.what());
        return getErrorResult(id, 18, "Error");
    } catch (...) {
        l.error("Error during org_atsc_cdApp_launch");
        return getErrorResult(id, 18, "Error");
    }
    return result;
}

static void doLaunch (HttpReactor *r, const json &req, int handlerId) {
    l.debug("doLaunch()");
    try {
        ScopedLock sl(activeHandlers_mutex);
        if (activeHandlers.count(handlerId) == 0) {
            l.debug("CD handler is already removed, so do nothing");
            return;
        }
        nlohmann::json result = org_atsc_cdApp_launch(req);
        r->sendFrame(result.dump(4));
    } catch (...) {
        l.info("EXCEPTION occurred during doLaunch(HttpReactor)");
    }
}

static json org_atsc_query_companionDevices(json req) {
    auto cdm = CdManager::getInstance();

    l.info("org_atsc_query_companionDevice(json)");
    json result = {};
    try {
        int id = req["id"];
        json params = req["params"];
        bool includeWsEndpoints = false;
        if (!params.empty()) {
            string paramsStr = params.dump(4);
            if (paramsStr.find("includeWsEndpoints") != string::npos
                    && paramsStr.find("true") != string::npos) {
                includeWsEndpoints = true;
            }
        } else {
            nlohmann::json result = {
                {"jsonrpc", "2.0"}
                , {"result", {}}
                , {"id", id}
            };
            return result;
        }

        json launcherArr = json::array();
        {
            ScopedLock sl(cdm->connected_launchers_mutex);
            auto &launchers = cdm->getConnectedLaunchers();
            if (launchers.size() > 0) {
                for (auto iter = launchers.begin();
                        iter != launchers.end() ; iter++) {
                    LauncherInfo &tmp = **iter;
                    json obj = json::object();
                    obj.push_back({"id", tmp.launcherId});
                    obj.push_back({"name", tmp.friendlyName});
                    obj.push_back({"cdOs", tmp.csOsId});
                    if (includeWsEndpoints) {
                        obj.push_back({"localEndpointUrl",
                            cdm->getLocalWsEndPoint()});
                        obj.push_back({"remoteEndpointUrl",
                            cdm->getRemoteWsEndPoint()});
                    }
                    launcherArr.push_back(obj);
                }
            }
        }
        result = {
            {"jsonrpc", "2.0"}
            , {"result", {{"launchers", launcherArr}}}
            , {"id", id}
        };
    } catch (exception &e) {
        l.error("Error during org_atsc_query_companionDevices: %s", e.what());
    } catch (...) {
        l.error("Error during org_atsc_query_companionDevices");
    }
    return result;
}

struct CdWsApiHandler: HttpRequestHandler {
    int myId;
    int getNewHandlerId() {
        static int id = 0;
        if (id > 100000000) id = 0;
        ++id;
        return id;
    }

    void handleWebsocketOpen (HttpReactor &r) override {
        ScopedLock sl(activeHandlers_mutex);
        myId = getNewHandlerId();
        l.info("atscCD OPENED : id = %d, %s", myId, r.requestLine.c_str());
        activeHandlers.insert(myId);
    }

    void handleWebsocketTextMessage (HttpReactor &r) override {
        json req = json::parse(
                {r.contentBuffer.begin(), r.contentBuffer.end()}
                , nullptr, false);
        string reqStr = req.dump(4);
        l.debug("atscCD MSG : id = %d, %s", myId, reqStr.c_str());
        // NEW A338S38
        if (reqStr.find(JSON_FUNC_org_atsc_query_companionDevices)
                != string::npos){
            nlohmann::json result = org_atsc_query_companionDevices(req);
            r.sendFrame(result.dump(4));
        } else if (reqStr.find(JSON_FUNC_org_atsc_cdApp_launch)
                != string::npos) {
            thread t(doLaunch, &r, req, myId);
            t.detach();
        }
    }

    void handleWebsocketClose (HttpReactor &r) override {
        ScopedLock sl(activeHandlers_mutex);
        l.info("atscCD CLOSED: id = %d, %s", myId, r.requestLine.c_str());
        activeHandlers.erase(myId);
    }
};

struct App2AppConnection;
static multimap<string, Ref<App2AppConnection> > pendingConnections[2];

#define WELCOME_MESSAGE "pairingcompleted"

struct App2AppConnection: HttpRequestHandler {

    int group; // 0: local, 1: remote
    HttpReactor *webSocket; // underlying connection handle
    Ref<App2AppConnection> peer; // paired connection

    App2AppConnection (int grp): group(grp), webSocket(nullptr) {}

    void handleWebsocketOpen (HttpReactor &r) override {
        webSocket = &r;

        // search for corresponding connection
        auto &pcs = pendingConnections[1 - group];
        auto pi = pcs.find(r.requestHandlerPath);
        if (pi == pcs.end()) {
            pendingConnections[group].insert({r.requestHandlerPath, this});
            return;
        }

        // make pair with each other
        peer = pi->second;
        pcs.erase(pi);
        peer->peer = this;
        webSocket->sendFrame(WELCOME_MESSAGE);
        peer->webSocket->sendFrame(WELCOME_MESSAGE);
    }

    void handleWebsocketTextMessage (HttpReactor &r) override {
        if (!peer) return;
        auto &buf = r.contentBuffer;
        peer->webSocket->sendFrame(buf.begin(), buf.size(), true);
    }

    void handleWebsocketClose (HttpReactor &r) override {
        webSocket = nullptr;

        if (peer) {
            peer->webSocket->abort("closed by peer");
            peer->peer = nullptr;
            peer = nullptr;
            return;
        }

        // if closed prior to paring.
        auto &pcs = pendingConnections[group];
        auto range = pcs.equal_range(r.requestHandlerPath);
        for (auto pi = range.first; pi != range.second; pi++) {
            if (pi->second.get() == this) {
                pcs.erase(pi);
                break;
            }
        }
    }
};

HttpRequestHandler *CdServer::acceptHttpRequest (const string &basePath,
        HttpReactor *r)
{
    //l.setLevel(ACBA_LOG_DEBUG);
    l.info("requestLine = %s", r->requestLine.c_str());

    if (!r->websocketVersion) return new CdHttpHandler(); // Web

    if (basePath == ATSC_CD) {
        // Companion API
        return new CdWsApiHandler();
    } else if (basePath == CD_LOCAL_WS_ENDPOINT) {
        return new App2AppConnection(/* local */ 0);
    } else if (basePath == CD_REMOTE_WS_ENDPOINT) {
        return new App2AppConnection(/* remote */ 1);
    } else {
        assert(!"Unexpected websocket request");
    }
}
