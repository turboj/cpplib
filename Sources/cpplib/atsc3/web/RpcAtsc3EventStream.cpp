#include <iostream>

#include "acba.h"
#include "rpc_manager.h"
#include "atsc3_web.h"
#include "atsc3_task.h"
#include "nlohmann/json.hpp"

using namespace std;
using namespace acba;
using namespace atsc3;
using json = nlohmann::json;

static Log l("rpc_atsc3_eventstream");

struct EventStreamNotifier: RefCounter<EventStreamNotifier>,
        Listener<const string&, double, const string&,
            double, double, const string&, double> {

    EventStreamNotifier (const Ref<RpcPeer> &p): peer(p) {}

    Ref<RpcPeer> peer;
    map<string,set<string> > filters; // schemeIdUri -> [value]

    void addFilter (const string &siduri, const string &val) {
        auto &f = filters[siduri];
        if (val.empty()) {
            f.clear();
        } else {
            f.insert(val);
        }
    }

    void removeFilter (const string &siduri, const string &val) {
        auto fi = filters.find(siduri);
        if (fi == filters.end()) return;
        auto &vs = fi->second;
        if (val.empty()) filters.erase(fi);
        if (vs.erase(val) && vs.empty()) filters.erase(fi);
    }

    void onUpdate (const string& _schemeIdUri, double _id, const string& _value,
            double _presentationTimeUs, double _durationMs, const string& _data,
            double _mediaStartMs) override {
        l.info("onUpdate for eventStream schemeIdUri:%s value:%s data:%s",
            _schemeIdUri.c_str(), _value.c_str(), _data.c_str());

        auto fi = filters.find(_schemeIdUri);
        if (fi == filters.end()) return;
        auto &values = fi->second;

        l.info("size of values %d", values.size());
        for (auto s: values)
            l.info("values %s", s.c_str());

        if (!_value.empty() && values.size() > 0) {
            if (values.find(_value) == values.end()) {
                l.info("The event value is not subscribed by peer");
                return;
            }
        }

        l.info("presentationTimeUs %lf", _presentationTimeUs);

        atsc3_RMPMediaTime mediaTime;
        task.rmp.getRMPMediaTime(&mediaTime);
        double eventTimeOffset;

        /** mediaStartMs exists if this event comes from a static MPS.
        * (meidaStartMs = availibilityStartMs + period.startMs)
        * otherwise mediStartMs is -1.
        **/
        if (_mediaStartMs != -1) {
            double eventTime = _presentationTimeUs/1000000 +
                                _mediaStartMs/1000;
            eventTimeOffset = eventTime - (mediaTime.startDateTimeMs/1000 +
                                mediaTime.currentTime);
        } else {
            eventTimeOffset = mediaTime.currentTime + _presentationTimeUs/1000000;
        }
        l.info("eventTime %lf", eventTimeOffset);
        if (eventTimeOffset < 0)
            return;

        json params = json::object();
        params["schemeIdUri"] = _schemeIdUri;
        params["value"] = _value; //optional
        params["id"] = _id;
        params["eventTime"] = eventTimeOffset;
        params["duration"] = _durationMs/1000; //optional
        params["data"] = _data; //optional

        peer->sendMessage({
            {"jsonrpc", "2.0"},
            {"method", "org.atsc.eventStream.event"},
            {"params", params}
        });
    }

    void dispose () {
        l.info("dispose EventStremNotifier");
        task.rmp.eventStreamEvent.removeListener(this);
    }
};

struct EventStreamNotificationHandler: RpcNotificationHandler
{
    map<RpcPeer*, Ref<EventStreamNotifier>> notifiers;

    bool subscribe (const string &type, const json &opt,
            const Ref<RpcPeer> &peer) override {
        if (!opt.is_structured()) return false;
        string schemeIdUri = opt["schemeIdUri"];
        if (schemeIdUri.empty()) return false;
        string value;
        if (opt.find("value") != opt.end()) value = opt["value"];

        auto &n = notifiers[peer.get()];
        if (!n) {
            n = new EventStreamNotifier(peer);
            task.rmp.eventStreamEvent.addListener(n);
        }

        n->addFilter(schemeIdUri, value);
        l.info("subscribed %s value:%s", schemeIdUri.c_str(), value.c_str());

        task.rmp.checkStoredEventMessage(schemeIdUri, value);

        return true;
    }

    bool unsubscribe (const string &type, const json &opt,
            const Ref<RpcPeer> &peer) override {
        auto ni = notifiers.find(peer.get());
        if (ni == notifiers.end()) return false;
        auto &n = ni->second;

        if (opt == nullptr) {
            // on disconnect
            n->filters.clear();
            n->dispose();
            notifiers.erase(ni);
            return true;
        }

        if (!opt.is_structured()) return false;
        string schemeIdUri = opt["schemeIdUri"];
        if (schemeIdUri.empty()) return false;
        string value;
        if (opt.find("value") != opt.end()) value = opt["value"];

        n->removeFilter(schemeIdUri, value);
        l.info("unsubscribed %s value:%s", schemeIdUri.c_str(), value.c_str());

        if (!n->filters.empty()) return false;

        n->dispose();
        notifiers.erase(ni);
        return true;
    }
};

static void subscribe (const json &req, const Ref<RpcPeer> peer)
{
    peer->subscribe("atsc3eventstream", req["params"]);

    peer->sendMessage({
        {"jsonrpc", "2.0"},
        {"result", json::object()},
        {"id", req["id"]}
    });
}

static void unsubscribe (const json &req, const Ref<RpcPeer> peer)
{
    peer->unsubscribe("atsc3eventstream", req["params"]);

    peer->sendMessage({
        {"jsonrpc", "2.0"},
        {"result", json::object()},
        {"id", req["id"]}
    });
}

void RpcAtsc3EventStream_init ()
{
    web.rpcManager.registerHandler("org.atsc.eventStream.subscribe",
            subscribe);
    web.rpcManager.registerHandler("org.atsc.eventStream.unsubscribe",
            unsubscribe);

    static EventStreamNotificationHandler esnh;
    web.rpcManager.registerNotificationHandler("atsc3eventstream", &esnh);
}
