#ifndef ATSC3_CD_SERVER_H
#define ATSC3_CD_SERVER_H

#include <sstream>

#include "acba.h"
#include "http_server.h"
#include "cd_manager.h"

namespace atsc3 {

struct CdServer: acba::HttpRequestAcceptor {
    acba::HttpRequestHandler *acceptHttpRequest (const std::string &basePath,
            acba::HttpReactor *r) override;
};

}
#endif
