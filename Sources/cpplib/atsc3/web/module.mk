$(call begin-target, atsc3_web, staticlib)
    LOCAL_SRCS := $(wildcard $(LOCAL_PATH)/*.cpp)
    LOCAL_IMPORTS := atsc3_model-staticlib atsc3_task-staticlib
    LOCAL_IMPORTS += json-nlohmann dnld-staticlib

    EXPORT_CXXFLAGS := -I$(LOCAL_PATH)
    EXPORT_IMPORTS := $(LOCAL_IMPORTS)
$(end-target)
