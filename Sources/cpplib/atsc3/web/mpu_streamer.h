#ifndef ATSC3_MPU_STREAMER_H
#define ATSC3_MPU_STREAMER_H

#include "acba.h"
#include "http_server.h"
#include "atsc3_model.h"

namespace atsc3 {

struct MpuStreamer: acba::HttpRequestAcceptor {
    acba::HttpRequestHandler *acceptHttpRequest (const std::string &basePath,
            acba::HttpReactor *r) override;
};

}
#endif
