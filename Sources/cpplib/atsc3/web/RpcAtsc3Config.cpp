#include <iostream>

#include "acba.h"
#include "rpc_manager.h"
#include "atsc3_web.h"
#include "atsc3_task.h"
#include "nlohmann/json.hpp"

using namespace std;
using namespace acba;
using namespace atsc3;
using json = nlohmann::json;

static Log l("rpc_atsc3_config_subscribe");

struct BlockedByRatingNotifier: RefCounter<BlockedByRatingNotifier>,
        Listener<bool> {

    BlockedByRatingNotifier (const Ref<RpcPeer> &p):
        peer(p) {};

    Ref<RpcPeer> peer;

    void onUpdate (bool blocked) override {
        // 9.3.2
        json ret = {};
        string contentRating;
        task.ratingHandler.retrieveContentAdvisoryRating(
        model.currentService.get(), contentRating);

        ret["msgType"] = "ratingBlock";
        ret["blocked"] = blocked;
        ret["contentRating"] = contentRating;
        peer->sendMessage({
            {"jsonrpc", "2.0"},
            {"method", "org.atsc.notify"},
            {"params", ret }
        });
    }

    void dispose () {
        task.ratingHandler.blockedByRatingUpdate.removeListener(this);
    }
};

struct CCStateNotifier: RefCounter<CCStateNotifier>, Listener<bool> {
    CCStateNotifier (const Ref<RpcPeer> &p): peer(p) {};

    Ref<RpcPeer> peer;

    void onUpdate (bool enabled) override {
       // 9.3.4
        json ret = {};
        ret["msgType"] = "captionState";
        ret["captionDisplay"] = enabled;
        peer->sendMessage({
            {"jsonrpc", "2.0"},
            {"method", "org.atsc.notify"},
            {"params", ret }
        });
    }

    void dispose () {
        task.rmp.captionStateUpdate.removeListener(this);
    }
};

struct ConfigChangeNotifier: RefCounter<ConfigChangeNotifier>,
        Listener<const string&> {

    ConfigChangeNotifier (const Ref<RpcPeer> &p, const string type):
        peer(p), subscribedType(type) {};

    Ref<RpcPeer> peer;
    const string subscribedType;

    void onUpdate (const string &key) override {
        l.info("onUpdate for configChangeUpdate key:%s type:%s",
            key.c_str(), subscribedType.c_str());

        if ((key == "preferredAudioLang" ||
                key == "preferredUiLang" ||
                key == "preferredCaptionSubtitleLang") &&
                subscribedType == "atsc3:languagePref") {
            // 9.3.5
            json ret = {};
            string value;
            model.preference.get(key, value);
            if (key == "preferredAudioLang")
                ret["preferredAudioLang"] = value;
            else if (key == "preferredUiLang")
                ret["preferredUiLang"] = value;
            else
                ret["preferredCaptionSubtitleLang"] = value;

            ret["msgType"] = "langPref";
            peer->sendMessage({
                {"jsonrpc", "2.0"},
                {"method", "org.atsc.notify"},
                {"params", ret }
            });

        } else if (key == "captionDisplay" &&
                subscribedType == "atsc3:CCDisplayPref") {
            // 9.3.6
            json ret = {};
            string value;
            model.preference.get(key, value);
            ret["displayPrefs"] = json::parse(value);
            ret["msgType"] = "captionDisplayPrefs";
            peer->sendMessage({
                {"jsonrpc", "2.0"},
                {"method", "org.atsc.notify"},
                {"params", ret }
            });
        } else if ((key == "videoDescriptionService" ||
                key == "audioEIService") &&
                subscribedType == "atsc3:audioAccessPref") {
            // 9.3.7
            json ret = {};
            if (key == "videoDescriptionService") {
                ret["videoDescriptionService"] = {};
                string videoDesc;
                model.preference.get("videoDescriptionService", videoDesc);
                ret["videoDescriptionService"] = json::parse(videoDesc);
            } else {
                ret["audioEIService"] = {};
                string audioEI;
                model.preference.get("audioEIService", audioEI);
                ret["audioEIService"]["enabled"] = json::parse(audioEI);
            }
            ret["msgType"] = "audioAccessibilityPref";
            peer->sendMessage({
                {"jsonrpc", "2.0"},
                {"method", "org.atsc.notify"},
                {"params", ret }
            });
        } else if (key == "rating" &&
                subscribedType == "atsc3:ratingChange") {
            // 9.3.1
            json ret = {};
            string value;
            model.preference.get(key, value);
            ret["msgType"] = "ratingChange";
            ret["rating"] = value;
            peer->sendMessage({
                {"jsonrpc", "2.0"},
                {"method", "org.atsc.notify"},
                {"params", ret }
            });
        }/*  else if (key == model.preference.RATING_BLOCK_CHANGE &&
                subscribedType == "atsc3:ratingBlock") {
            // 9.3.2
            json ret = {};

            ret["msgType"] = "ratingBlock";
            ret["blocked"] = true; //TODO get service status from Service
            ret["contentRating"] = ""; //TODO get content rating from Service
            peer->sendMessage({
                {"jsonrpc", "2.0"},
                {"method", "org.atsc.notify"},
                {"params", ret }
            });

        } else if (key == model.preference.CC_STATE_CHANGE &&
                subscribedType == "atsc3:captionState") {
            // 9.3.4
            json ret = {};
            ret["msgType"] = "captionState";
            ret["captionDisplay"] = true; //TODO get status from RMP
            peer->sendMessage({
                {"jsonrpc", "2.0"},
                {"method", "org.atsc.notify"},
                {"params", ret }
            });
        }*/
    }

    void dispose () {
        model.preference.update.removeListener(this);
        l.info("ConfigChangeNotifier dispose()");
    }
};

struct ConfigChangeNotificationHandler: RpcNotificationHandler {
    map<std::pair<RpcPeer*, string>,
        Ref<ConfigChangeNotifier>> configNotifiers;

    bool subscribe (const std::string &type, const nlohmann::json &opt,
            const acba::Ref<RpcPeer> &peer) override {

        l.info("subscribe %s", type.c_str());
        auto &n = configNotifiers[{peer.get(), type}];
        if (n) return true;

        n = new ConfigChangeNotifier(peer, type);
        model.preference.update.addListener(n);
        return true;
    }

    bool unsubscribe (const std::string &type, const acba::Ref<RpcPeer> &peer)
            override {

        l.info("unsubscribe %s", type.c_str());
        auto ni = configNotifiers.find({peer.get(), type});
        if (ni == configNotifiers.end()) return false;

        auto &n = ni->second;
        n->dispose();
        configNotifiers.erase(ni);
        return true;
    }
};

struct BlockedByRatnigNotificationHandler: RpcNotificationHandler {
    map<RpcPeer*, Ref<BlockedByRatingNotifier>> blockedByRatingNotifiers;

   bool subscribe (const std::string &type, const nlohmann::json &opt,
            const acba::Ref<RpcPeer> &peer) override {

        auto &n = blockedByRatingNotifiers[peer.get()];
        if (n) return true;

        n = new BlockedByRatingNotifier(peer);
        task.ratingHandler.blockedByRatingUpdate.addListener(n);
        return true;
    }

    bool unsubscribe (const std::string &type, const acba::Ref<RpcPeer> &peer)
            override {

        auto ni = blockedByRatingNotifiers.find(peer.get());
        if (ni == blockedByRatingNotifiers.end()) return false;

        auto &n = ni->second;
        n->dispose();
        blockedByRatingNotifiers.erase(ni);
        return true;
    }
};

struct CcStateNotificationHandler: RpcNotificationHandler {
    map<RpcPeer*, Ref<CCStateNotifier>> ccStateNotifiers;

    bool subscribe (const std::string &type, const nlohmann::json &opt,
            const acba::Ref<RpcPeer> &peer) override {

        auto &n = ccStateNotifiers[peer.get()];
        if (n) return true;

        n = new CCStateNotifier(peer);
        task.rmp.captionStateUpdate.addListener(n);
        return true;
    }

    bool unsubscribe (const std::string &type, const acba::Ref<RpcPeer> &peer)
            override {

        auto ni = ccStateNotifiers.find(peer.get());
        if (ni == ccStateNotifiers.end()) return false;

        auto &n = ni->second;
        n->dispose();
        ccStateNotifiers.erase(ni);
        return true;
    }
};

void RpcAtsc3Config_init ()
{
    static ConfigChangeNotificationHandler configChangeNotificationHandler;
    web.rpcManager.registerNotificationHandler("atsc3:ratingChange",
        &configChangeNotificationHandler);
    web.rpcManager.registerNotificationHandler("atsc3:languagePref",
        &configChangeNotificationHandler);
    web.rpcManager.registerNotificationHandler("atsc3:CCDisplayPref",
        &configChangeNotificationHandler);
    web.rpcManager.registerNotificationHandler("atsc3:audioAccessPref",
        &configChangeNotificationHandler);

    static CcStateNotificationHandler csnh;
    web.rpcManager.registerNotificationHandler("atsc3:captionState", &csnh);

    static BlockedByRatnigNotificationHandler bbrnh;
    web.rpcManager.registerNotificationHandler("atsc3:ratingBlock", &bbrnh);
}
