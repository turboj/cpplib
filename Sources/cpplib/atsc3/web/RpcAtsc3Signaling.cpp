#include <functional>
#include <iostream>
#include <list>

#include "acba.h"
#include "rpc_manager.h"
#include "atsc3_web.h"
#include "atsc3_task.h"
#include "JsonRef.h"
#include "nlohmann/json.hpp"
#include "pugixml.hpp"
#include "gzip_reader.h"

using namespace std;
using namespace acba;
using namespace atsc3;
using namespace pugi;
using json = nlohmann::json;
using namespace placeholders;

static Log l("rpc_signaling");

struct SignalingNotificationHandler: RpcNotificationHandler, Listener<> {

    Ref<Service> currentService;
    Ref<BroadcastStream> currentBroadcastStream;
    set<RpcPeer*> peers;

    bool subscribe (const string &type, const json &opt,
            const Ref<RpcPeer> &peer) override {
        peers.insert(peer.get());
        return true;
    }

    bool unsubscribe (const string &type, const Ref<RpcPeer> &peer) override {
        return peers.erase(peer.get()) > 0;
    }

    // on current service change
    void onUpdate () override {
        if (currentService == model.currentService) return;

        // remove listener
        if (currentBroadcastStream) {
            currentBroadcastStream->llsUpdate.removeListener(&notifier);
            currentBroadcastStream = nullptr;
        }
        if (currentService) {
            if (currentService->protocol == /*route*/1) {
                auto rsrv = static_cast<RouteService*>(currentService.get());
                rsrv->heldDocumentUpdate.removeListener(&notifier);
                rsrv->usdDocumentUpdate.removeListener(&notifier);
                rsrv->resolvedMpdFileUpdate.removeListener(&notifier);
                currentService = nullptr;
            }
        }

        if (!model.currentService) return;

        auto bsi = model.broadcastStreams.find(model.currentService->bsid);
        if (bsi == model.broadcastStreams.end()) {
            l.error("No broadcast stream found for current service");
            return;
        }
        currentBroadcastStream = bsi->second;

        // add listener
        currentBroadcastStream->llsUpdate.addListener(&notifier);

        currentService = model.currentService;
        if (currentService->protocol == /*route*/1) {
            auto rsrv = static_cast<RouteService*>(currentService.get());
            rsrv->heldDocumentUpdate.addListener(&notifier);
            rsrv->usdDocumentUpdate.addListener(&notifier);
            rsrv->resolvedMpdFileUpdate.addListener(&notifier);
        }
    }

    // notifier
    struct Notifier: Reactor, Listener<>,
            Listener<uint8_t, uint8_t, const Ref<File>> {

        void onUpdate () override {
            task.eventDriver.setTimeout(0, this);
        }

        void onUpdate (uint8_t tblid, uint8_t grpid, const Ref<File>) override {
            task.eventDriver.setTimeout(0, this);
        }

        void processEvent (int evt) override {
            auto self = container_of(this,
                    &SignalingNotificationHandler::notifier);

            json msg = {
                {"jsonrpc", "2.0"},
                {"method", "org.atsc.notify"},
                {"params", {
                    {"msgType", "signalingData"},
                }}
            };

            for (auto &p: self->peers) p->sendMessage(msg);
        }
    } notifier;
};

static void getLls (int tblid, const string &name, json &objs)
{
    if (!model.currentService) return;
    auto &srv = model.currentService;

    auto bsi = model.broadcastStreams.find(srv->bsid);
    if (bsi == model.broadcastStreams.end()) return;
    auto &bs = bsi->second;

    for (auto &sgi: bs->serviceGroups) {
        auto &sg = sgi.second;

        auto lfi = sg->llsFiles.find(tblid);
        if (lfi == sg->llsFiles.end()) continue;
        auto &lf = lfi->second;
        auto &buf = lf->content;

        if (lf->attributes["compressed"].asBool()) {
            GzipReader gr;
            if (gr.read(buf.begin(), buf.size(), nullptr) != buf.size()) {
                l.warn("gunzip failed");
                continue;
            }
            auto &buf2 = gr.outBuffer;

            objs.push_back({
                {"name", name},
                {"group", to_string(srv->sgid)},
                {"version", lf->attributes["version"].toString()},
                {"table", string((char*)buf2.begin(), buf2.size())},
            });
        } else {
            objs.push_back({
                {"name", name},
                {"group", to_string(srv->sgid)},
                {"version", lf->attributes["version"].toString()},
                {"table", string((char*)buf.begin(), buf.size())},
            });
        }
    }
}

static void getResolvedMpd (const string &name, json &objs)
{
    if (!model.currentService) return;
    if (model.currentService->protocol != /*route*/1) return;
    auto rsrv = static_cast<RouteService*>(model.currentService.get());

    if (!rsrv->resolvedMpdFile) return;

    // FIXME: resolved MPD should have its own version.
    string ver;
    for (auto &sfi: rsrv->slsFiles) {
        auto &sf = sfi.second;
        if (sf->type == "application/dash+xml") {
            ver = sf->attributes["version"].toString();
            break;
        }
    }

    auto &buf = rsrv->resolvedMpdFile->content;
    objs.push_back({
        {"name", name},
        {"version", ver},
        {"table", string((char*)buf.begin(), buf.size())},
    });
}

static void getRouteSls (const string &type, const string &name, json &objs)
{
    if (!model.currentService) return;
    if (model.currentService->protocol != /*route*/1) return;
    auto rsrv = static_cast<RouteService*>(model.currentService.get());

    for (auto &sfi: rsrv->slsFiles) {
        auto &sf = sfi.second;
        if (sf->type == type) {
            objs.push_back({
                {"name", name},
                {"version", sf->attributes["version"].toString()},
                {"table", string((char*)sf->content.begin(), sf->content.size())},
            });
        }
    }
}

static map<string, function<void(const string&, json&)> > signalGetters = {

    // LLS

    {/*slt*/"1", bind(getLls, 1, _1, _2)},
    {/*aeat*/"4", bind(getLls, 4, _1, _2)},
    {/*onscreenmessagenotification*/"5", bind(getLls, 5, _1, _2)},

    // ROUTE

    {"MPD", getResolvedMpd},
    {"USBD", bind(getRouteSls, "application/route-usd+xml", _1, _2)},
    {"STSID", bind(getRouteSls, "application/route-s-tsid+xml", _1, _2)},
    {"HELD", bind(getRouteSls, "application/atsc-held+xml", _1, _2)},
    //{"DWD",},
    //{"EMSG",},
    //{"APD",},

    // MMT

    //{"PAT",},
    //{"MPT",},
    //{"MPIT",},
    //{"CRIT",},
    //{"DCIT",},
    //{"AEI",},
    //{"EVTI",}
};

static void org_atsc_query_signaling (const json &req, const Ref<RpcPeer> peer)
{
    auto nameList = JsonRef(req)["params"]["nameList"];
    if (!nameList.isArray()) {
        peer->sendMessage({
            {"jsonrpc", "2.0"},
            {"id", req["id"]},
            {"result", {}}
        });
        return;
    }

    // Spec requires emtpy list to mean every signal names
    set<string> names;
    if (nameList.length() == 0) {
        for (auto sgi: signalGetters) names.insert(sgi.first);
    } else {
        for (int i = 0; i < nameList.length(); i++) {
            names.insert(nameList[i].asString());
        }
    }

    json objs = json::array();
    for (auto &name: names) {
        auto sgi = signalGetters.find(name);
        if (sgi == signalGetters.end()) {
            l.warn("Ignored unknown signal name %s", name.c_str());
            continue;
        }

        sgi->second(name, objs);
    }

    peer->sendMessage({
        {"jsonrpc", "2.0"},
        {"id", req["id"]},
        {"result", objs}
    });
}

void RpcAtsc3Signaling_init ()
{
    static SignalingNotificationHandler snh;
    web.rpcManager.registerNotificationHandler("atsc3:signalingData", &snh);
    model.currentServiceUpdate.addListener(&snh);

    web.rpcManager.registerHandler("org.atsc.query.signaling",
            org_atsc_query_signaling); // 9.2.10
}
