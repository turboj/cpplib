#include "acba.h"
#include "rpc_manager.h"
#include "atsc3_task.h"
#include "atsc3_web.h"
#include "nlohmann/json.hpp"
#include "JsonRef.h"
#include "pugixml.hpp"
#include "http_server.h"

// Set 0 only when testing
#define CHECK_APPCONTEXT_ID 1

using namespace std;
using namespace acba;
using namespace atsc3;
using namespace pugi;
using json = nlohmann::json;

static Log l("rpc_esg");

static void query (const json &req, const Ref<RpcPeer> peer) {
    json res = json::object();

#if CHECK_APPCONTEXT_ID
    auto &acc = peer->applicationContextCache;
    if (!acc) {
        // send empty list when not bound to app
        peer->sendMessage({
            {"jsonrpc", "2.0"},
            {"result", {
                {"urlList", json::array()},
            }},
            {"id", req["id"]}
        });
        return;
    }

    string appid = " " + acc->id + " ";

    char s[128];
    sprintf(s, "http://%d.%d.%d.%d:8080/esg/fragment/",
            acc->hostIp >> 24,
            (acc->hostIp >> 16) & 0xff,
            (acc->hostIp >> 8) & 0xff,
            acc->hostIp & 0xff);
    string baseUri = s;
#else
    string baseUri = "http://127.0.0.1:8080/esg/fragment/";
#endif

    auto sid = JsonRef(req)["params"]["service"];
    xml_node sfrag;
    if (sid.isString()) {
        for (auto bs: model.broadcastStreams) {
            for (auto fsi: bs.second->esgFragmentSlides) {
                auto &fs = fsi.second;
                if (!fs || fs->type != 1 || !fs->currentFragment) continue;

                auto frag = fs->currentFragment->document_element();
                if (sid.asString() == frag.attribute("globalServiceID").value()) {
                    sfrag = frag;
                    break;
                }
            }
        }

        if (!sfrag) {
            l.warn("No such service id");
            peer->sendMessage({
                {"jsonrpc", "2.0"},
                {"result", {
                    {"urlList", json::array()}
                }},
                {"id", req["id"]}
            });
            return;
        }
    }

    json urlList = json::array();
    for (auto bs: model.broadcastStreams) {
        for (auto fsi: bs.second->esgFragmentSlides) {
            auto &fs = fsi.second;
            if (!fs || !fs->currentFragment) continue;
#if CHECK_APPCONTEXT_ID
            if (fs->applicationContextIds.find(appid) == string::npos) continue;
#endif

            auto frag = fs->currentFragment->document_element();

            switch (fs->type) {
            case 1: // Service Fragment
                if (sfrag && sfrag != frag) break;

                urlList.push_back({
                    {"sgType", frag.name()},
                    {"sgUrl", baseUri +
                            url_encode(frag.attribute("id").as_string(""))},
                    {"service", frag.attribute("globalServiceID").value()}
                });
                break;

            case 2: // Content Fragment
                if (sfrag) {
                    bool found = false;
                    for (auto sri: frag.children("ServiceReference")) {
                        auto sfi = bs.second->esgFragmentSlides.find(
                                sri.attribute("idRef").value());
                        if (sfi != bs.second->esgFragmentSlides.end() &&
                                sfi->second && sfi->second->currentFragment &&
                                sfi->second->currentFragment->
                                document_element() == sfrag) {
                            found = true;
                            break;
                        }
                    }
                    if (!found) break;
                }

                // NOTE: Since multiple services can be associated with
                // this content fragment, omitted service field here.
                urlList.push_back({
                    {"sgType", frag.name()},
                    {"sgUrl", baseUri +
                            url_encode(frag.attribute("id").as_string(""))},
                    {"content", frag.attribute("globalContentID").value()}
                });
                break;

            case 3: // Schedule Fragment
                {
                    // find service fragment referenced by this schedule frag.
                    xml_node sf;
                    auto sfi = bs.second->esgFragmentSlides.find(
                            frag.child("ServiceReference").attribute("idRef").
                            value());
                    if (sfi != bs.second->esgFragmentSlides.end() &&
                            sfi->second && sfi->second->currentFragment) {
                        sf = sfi->second->currentFragment->document_element();
                    }
                    if (!sf) {
                        l.warn("Service fragment %s referenced from "
                                "schedule fragment %s was not found.",
                                frag.child("ServiceReference").
                                attribute("idRef").value(),
                                frag.attribute("id").value());
                        break;
                    }

                    if (sfrag && sfrag != frag) break;

                    urlList.push_back({
                        {"sgType", frag.name()},
                        {"sgUrl", baseUri +
                                url_encode(frag.attribute("id").as_string(""))},
                        {"service", sf.attribute("globalServiceID").value()}
                    });
                }
                break;

            default:
                l.warn("Skipped unsupported fragment type:%u", fs->type);
                break;
            }
        }
    }

    peer->sendMessage({
        {"jsonrpc", "2.0"},
        {"result", {
            {"urlList", urlList}
        }},
        {"id", req["id"]}
    });
}

struct EsgChangeHandler: RpcNotificationHandler, Reactor,
        Listener<ESG_FRAGMENT_EVENT, const string&,
        const Ref<EsgFragmentSlide>&, const Ref<BroadcastStream>&>
{
    set<RpcPeer*> peers;
    set<Ref<EsgFragmentSlide> > updates;

    bool subscribe (const string &type, const nlohmann::json &opt,
            const Ref<RpcPeer> &peer) override {
        if (!peer->applicationContextCache) {
            l.warn("serviceGuideChange requires app context id");
#if CHECK_APPCONTEXT_ID
            return false;
#endif
        }

        return peers.insert(peer.get()).second;
    }

    bool unsubscribe (const string &type, const Ref<RpcPeer> &peer) override {
        return peers.erase(peer.get()) > 0;
    }

    void onUpdate (ESG_FRAGMENT_EVENT evt, const string &fid,
            const Ref<EsgFragmentSlide>& fs, const Ref<BroadcastStream>& bs)
            override {

#if CHECK_APPCONTEXT_ID
        if (fs->applicationContextIds.empty()) return;
#endif

        updates.insert(fs);
        task.eventDriver.setTimeout(1000000, this);
    }

    void processEvent (int evt) override {
        for (auto &p: peers) {
#if CHECK_APPCONTEXT_ID
            if (!p->applicationContextCache) continue;
            string id = " " + p->applicationContextCache->id + " ";
#endif

            json urlList = json::array();
            for (auto &fs: updates) {
                if (!fs->currentFragment) continue;
#if CHECK_APPCONTEXT_ID
                if (fs->applicationContextIds.find(id) == string::npos) continue;
#endif
                auto frag = fs->currentFragment->document_element();

                urlList.push_back({
                    {"sgType", frag.name()},
                    {"sgUrl", string("http://localhost:8080/esg/fragment/") +
                    url_encode(frag.attribute("id").as_string(""))},
                });
            }

            p->sendMessage({
                {"jsonrpc", "2.0"},
                {"method", "org.atsc.notify"},
                {"params", {
                    {"msgType", "serviceGuideChange"},
                    {"urlList", urlList}
                }},
            });
        }
        updates.clear();
    }
};

struct EsgServer: HttpRequestHandler {
    void handleHttpRequest (HttpReactor &r) {
        const char *path = r.requestHandlerPath.c_str();
        for (auto bs: model.broadcastStreams) {
            auto fsi = bs.second->esgFragmentSlides.find(path);
            if (fsi == bs.second->esgFragmentSlides.end()) continue;
            auto &fs = fsi->second;

            if (!fs || !fs->currentFragment) break;

            stringstream ss;
            fs->currentFragment->save(ss);
            r.sendResponse(200, {"Content-Type: text/xml"}, ss.str());
            return;
        }

        // not found
        r.sendResponse(404, {"Content-Type: text/plain"},
                "No corresponding esg fragment");
    }
};

struct EsgAcceptor: HttpRequestAcceptor {
    HttpRequestHandler *acceptHttpRequest (const string &basePath,
            HttpReactor *r) override {
        if (r->websocketVersion) return 0;
        return new EsgServer();
    }
};

void RpcAtsc3Esg_init ()
{
    web.rpcManager.registerHandler("org.atsc.query.serviceGuideUrls", query);

    static EsgChangeHandler ech;
    model.esgFragmentUpdate.addListener(&ech);
    web.rpcManager.registerNotificationHandler("atsc3:serviceGuideChange", &ech);

    static EsgAcceptor esgacc;
    web.httpServer.bind("/esg/fragment/", &esgacc);
}
