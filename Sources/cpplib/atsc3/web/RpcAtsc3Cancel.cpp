#include <iostream>

#include "acba.h"
#include "rpc_manager.h"
#include "atsc3_web.h"
#include "atsc3_task.h"
#include "nlohmann/json.hpp"
#include "JsonRef.h"

using namespace std;
using namespace acba;
using namespace atsc3;
using json = nlohmann::json;

static Log l("rpc_atsc3_cancel");

static void cancel (const json &req, const Ref<RpcPeer> peer)
{
    // retrieve reqIds to cancel
    set<int> ids;
    auto reqIds = JsonRef(req)["params"]["requestIDs"];
    if (reqIds.length() > 0) {
        for (int i = 0; i < reqIds.length(); i++) ids.insert(reqIds[i].toI32());
    } else {
        peer->getRequests(ids);
    }

    // cancel request
    json cancelList = json::array();
    for (auto id: ids) {
        if (!peer->isKnownRequest(id)) {
            cancelList.push_back({
                {"requestID", id},
                {"disposition", "UNKNOWN"},
                {"description", "Request is not currently pending"}
            });
        } else if (peer->cancelRequest(id)) {
            // send response for canceled request.
            peer->sendMessage({
                {"jsonrpc", "2.0"},
                {"id", id},
                {"error", {
                    {"code", -20}, {"message", "Request Canceled"}
                }}
            });

            cancelList.push_back({
                {"requestID", id},
                {"disposition", "CANCELED"},
                {"description", "Request canceled successfully"}
            });
        } else {
            cancelList.push_back({
                {"requestID", id},
                {"disposition", "FAILED"},
                {"description", "Request is currently pending"}
            });
        }
    }

    // send resonse
    peer->sendMessage({
        {"jsonrpc", "2.0"},
        {"result", {
            {"cancelList", cancelList}
        }},
        {"id", req["id"]}
    });
}

void RpcAtsc3Cancel_init ()
{
    web.rpcManager.registerHandler("cancel", cancel);
}
