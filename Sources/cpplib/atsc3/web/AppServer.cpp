#include <set>
#include <string>
#include <sstream>

#include "acba.h"
#include "atsc3_model.h"
#include "app_server.h"

using namespace std;
using namespace acba;
using namespace atsc3;

static Log l("app_server");

struct AppHandler: HttpRequestHandler {
    void handleHttpRequest (HttpReactor &r) {

        const char *path = r.requestHandlerPath.c_str();
        l.info("request %s", path);
        if (path[0] == '/') path++;

        // find cache
        Ref<ApplicationContextCache> cache;
        for (auto cc: model.applicationContextCaches) {
            if (strncmp(path, cc.first.c_str(), cc.first.size()) == 0) {
                cache = cc.second;
                break;
            }
        }
        if (!cache) {
            r.sendResponse(404, {"Content-Type: text/plain"},
                    "No corresponding app context cache");
            return;
        }
        path += cache->id.size();
        if (path[0] == '/') path++;

        // search for the file
        auto f = cache->files.find(path);
        if (f == cache->files.end()) {
            r.sendResponse(404, {"Content-Type: text/plain"}, "No such file");
            return;
        }
        File &file = *f->second;

        // send file
        vector<string> headers;
        if (file.type.empty()) {
            headers.push_back("Content-Type: text/plain");
        } else {
            headers.push_back(string("Content-Type: ") + file.type);
        }
        // TODO: Should use expire info from each file.
        headers.push_back("Cache-Control: nocache, no-store, must-revalidate");
        r.sendHead(200, headers, file.content.size());
        r.sendBody(file.content.begin(), file.content.size());
        r.endResponse();
    }
};

HttpRequestHandler *AppServer::acceptHttpRequest (const string &basePath,
        HttpReactor *r)
{
    if (r->websocketVersion) return 0;
    return new AppHandler();
}
