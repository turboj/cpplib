#include <unistd.h>

#include "acba.h"
#include "rpc_manager.h"
#include "atsc3_web.h"
#include "atsc3_task.h"
#include "nlohmann/json.hpp"
#include "JsonRef.h"
#include "pugixml.hpp"

using namespace std;
using namespace acba;
using namespace atsc3;
using namespace pugi;
using json = nlohmann::json;

static Log l("rpc_tune");

struct TuneNotifier;
static map<pair<RpcPeer*, string>, Ref<TuneNotifier>> notifiers;

struct TuneNotifier: RefCounter<TuneNotifier>, Listener<RpcPeer&>,
        Listener<Tune::EVENT> {

    TuneNotifier (const Ref<RpcPeer> &p, const Ref<Tune> &t): peer(p), tune(t)
            {};

    Ref<RpcPeer> peer;
    Ref<Tune> tune;

    void onUpdate (Tune::EVENT e) override {
        peer->sendMessage({
            {"jsonrpc", "2.0"},
            {"method", "com.alticast.tune.notify"},
            {"params", {
                {"location", tune->location},
                {"event", e}
            }},
        });
    }

    void onUpdate (RpcPeer &peer) override {
        dispose ();
    }

    void dispose () {
        tune->removeListener(this);
        peer->webSocketUpdate.removeListener(this);
        task.tuneManager.releaseTune(tune);

        notifiers.erase({peer.get(), tune->location});
    }
};

static void com_alticast_tune_request (const json &req, const Ref<RpcPeer> peer)
{
    // parse params
    auto &loc = JsonRef(req)["params"]["location"].asString();
    if (loc.empty()) {
        peer->sendMessage({
            {"jsonrpc", "2.0"},
            {"error", {{"code", -1}, {"message","error"}}},
            {"id", req["id"]}
        });
        return;
    }

    // request tune
    Ref<Tune> t = task.tuneManager.requestTune(loc);
    if (!t) {
        peer->sendMessage({
            {"jsonrpc", "2.0"},
            {"error", {{"code", -1}, {"message", "failed to tune"}}},
            {"id", req["id"]}
        });
        return;
    }

    // bind to tuneNotifier
    auto &tn = notifiers[{peer.get(), loc}];
    if (!tn) {
        tn = new TuneNotifier(peer, t);
        peer->webSocketUpdate.addListener(tn.get());
        t->addListener(tn.get());
    }

    // success
    peer->sendMessage({
        {"jsonrpc", "2.0"},
        {"result", true},
        {"id", req["id"]}
    });

    // send locked event immediately if the tune was being reused,
    // and also already locked.
    // TODO: This behavior is temporally necessary as we don't provide
    //      tuner status query api for now.
    if (t->isLocked()) tn->onUpdate(Tune::EVENT_TUNER_LOCKED);
}

static void com_alticast_tune_release (const json &req, const Ref<RpcPeer> peer)
{
    // parse params
    auto &loc = JsonRef(req)["params"]["location"].asString();
    if (loc.empty()) {
        peer->sendMessage({
            {"jsonrpc", "2.0"},
            {"error", {{"code", -1}, {"message","error"}}},
            {"id", req["id"]}
        });
        return;
    }

    // find tuneNotifier
    auto tni = notifiers.find({peer.get(), loc});
    if (tni == notifiers.end()) {
        peer->sendMessage({
            {"jsonrpc", "2.0"},
            {"result", false},
            {"id", req["id"]}
        });
        return;
    }

    tni->second->dispose();

    peer->sendMessage({
        {"jsonrpc", "2.0"},
        {"result", true},
        {"id", req["id"]}
    });
}

static void servicelist_save (const json &req, const Ref<RpcPeer> peer)
{
    task.serviceManager.save();

    peer->sendMessage({
        {"jsonrpc", "2.0"},
        {"result", {}},
        {"id", req["id"]}
    });
}

static void servicelist_load (const json &req, const Ref<RpcPeer> peer)
{
    task.serviceManager.load();

    peer->sendMessage({
        {"jsonrpc", "2.0"},
        {"result", {}},
        {"id", req["id"]}
    });
}

static void servicelist_clear (const json &req, const Ref<RpcPeer> peer)
{
    task.serviceManager.clear();

    peer->sendMessage({
        {"jsonrpc", "2.0"},
        {"result", {}},
        {"id", req["id"]}
    });
}

void RpcAlticastTune_init () {
    web.rpcManager.registerHandler("com.alticast.tune.request", com_alticast_tune_request);
    web.rpcManager.registerHandler("com.alticast.tune.release", com_alticast_tune_release);
    web.rpcManager.registerHandler("com.alticast.servicelist.save", servicelist_save);
    web.rpcManager.registerHandler("com.alticast.servicelist.load", servicelist_load);
    web.rpcManager.registerHandler("com.alticast.servicelist.clear", servicelist_clear);
};
