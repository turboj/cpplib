#include <unistd.h>

#include "acba.h"
#include "rpc_manager.h"
#include "atsc3_model.h"
#include "atsc3_web.h"
#include "atsc3_task.h"
#include "nlohmann/json.hpp"
#include "JsonRef.h"
#include "pugixml.hpp"

using namespace std;
using namespace acba;
using namespace atsc3;
using namespace pugi;
using json = nlohmann::json;

static Log l("rpc_alticast");

static void echo (const json &req, const Ref<RpcPeer> peer) {
    peer->sendMessage({
        {"jsonrpc", "2.0"},
        {"result", req["params"]},
        {"id", req["id"]}
    });
}

static void getslts (const json &req, const Ref<RpcPeer> peer) {
    set<XmlDocument*> slts;
    for (auto &bs: model.broadcastStreams) {
        for (auto &sg: bs.second->serviceGroups) {
            for (auto &s: sg.second->services) {
                slts.insert(s.second->sltDocument.get());
            }
        }
    }
    json sltsJson = json::array();
    for (auto slt: slts) {
        stringstream ss;
        slt->save(ss);
        sltsJson.push_back(ss.str());
    }

    peer->sendMessage({
        {"jsonrpc", "2.0"},
        {"result", sltsJson},
        {"id", req["id"]}
    });
}

static void get_selected_service_id (const json &req, const Ref<RpcPeer> peer) {
    json gsid = nullptr;
    if (model.currentService) gsid = model.currentService->globalId;

    peer->sendMessage({
        {"jsonrpc", "2.0"},
        {"id", req["id"]},
        {"result", gsid}
    });
}

static void select_service (const json &req, const Ref<RpcPeer> peer) {

    auto &gsid = JsonRef(req)["params"].asString();

    Service *srv = nullptr;
    if (!gsid.empty()) {
        for (auto &bs: model.broadcastStreams) {
            for (auto &sg: bs.second->serviceGroups) {
                for (auto &s: sg.second->services) {
                    if (s.second->globalId == gsid) srv = s.second;
                }
            }
        }
    }

    task.rmp.setScalePosition(100, 0, 0);
    model.selectService(srv);
    peer->sendMessage({
        {"jsonrpc", "2.0"},
        {"id", req["id"]},
        {"result", !!srv}
    });
}

static void add_background_service (const json &req, const Ref<RpcPeer> peer)
{
    auto &gsid = JsonRef(req)["params"].asString();

    Service *srv = nullptr;
    for (auto &bs: model.broadcastStreams) {
        for (auto &sg: bs.second->serviceGroups) {
            for (auto &s: sg.second->services) {
                if (s.second->globalId == gsid) srv = s.second;
            }
        }
    }

    if (srv) {
        model.backgroundServices.insert(srv);
        model.backgroundServicesUpdate.notify();
    }
    peer->sendMessage({
        {"jsonrpc", "2.0"},
        {"id", req["id"]},
        {"result", !!srv}
    });
}

static void remove_background_service (const json &req,
        const Ref<RpcPeer> peer) {
    auto &gsid = JsonRef(req)["params"].asString();

    Service *srv = nullptr;
    for (auto s: model.backgroundServices) {
        if (s->globalId == gsid) srv = s.get();
    }

    if (srv) {
        model.backgroundServices.erase(srv);
        model.backgroundServicesUpdate.notify();
    }
    peer->sendMessage({
        {"jsonrpc", "2.0"},
        {"id", req["id"]},
        {"result", !!srv}
    });
}

static void overrideAllKeys (const json &req, const Ref<RpcPeer> peer) {
    task.applicationManager.setKeyOverride(JsonRef(req)["params"].toBool());
    peer->sendMessage({
        {"jsonrpc", "2.0"},
        {"result", true},
        {"id", req["id"]}
    });
}

static void com_alticast_set_rating (const json &req, const Ref<RpcPeer> peer) {
    auto &rating = JsonRef(req)["params"].asString();

    if (!rating.empty()) {
        model.preference.set("rating", rating);
        peer->sendMessage({
            {"jsonrpc", "2.0"},
            {"id", req["id"]},
            {"result", {}}
        });
    } else {
       peer->sendMessage({
            {"jsonrpc", "2.0"},
            {"id", req["id"]},
            {"error", {{"code", -32602}, {"message", "Invalid params"}}}
        });
    }
}


static void com_alticast_set_languages (const json &req, const Ref<RpcPeer> peer) {

    auto params = JsonRef(req)["params"];

    if (!params.isObject()) {
        peer->sendMessage({
            {"jsonrpc", "2.0"},
            {"id", req["id"]},
            {"error", {{"code", -32602}, {"message", "Invalid params"}}}
        });
        return;
    }

    auto &audio = params["preferredAudioLang"].asString();
    if (!audio.empty()) model.preference.set("preferredAudioLang", audio);

    auto &ui = params["preferredUiLang"].asString();
    if (!ui.empty()) model.preference.set("preferredUiLang", ui);

    auto &cc = params["preferredCaptionSubtitleLang"].asString();
    if (!cc.empty()) model.preference.set("preferredCaptionSubtitleLang", cc);

    peer->sendMessage({
        {"jsonrpc", "2.0"},
        {"id", req["id"]},
        {"result", {}}
    });
}

static void com_alticast_set_captionDisplay (const json &req, const Ref<RpcPeer> peer) {
    // extend: caption display preference
    auto captionDisplay = JsonRef(req)["params"];

    // TODO: check error
    model.preference.set("captionDisplay", captionDisplay.dump(4));

    peer->sendMessage({
        {"jsonrpc", "2.0"},
        {"id", req["id"]},
        {"result", {}}
    });
}

static void com_alticast_set_audioAccessibility (const json &req, const Ref<RpcPeer> peer) {
    auto params = JsonRef(req)["params"];

    // extend: set audio accessibility preference
    if (!params.isObject()) {
        peer->sendMessage({
            {"jsonrpc", "2.0"},
            {"id", req["id"]},
            {"error", {{"code", -32602}, {"message", "Invalid params"}}}
        });
        return;
    }

    auto videoDesc = params["videoDescriptionService"];
    if (!videoDesc.isNull()) {
        model.preference.set("videoDescriptionService", videoDesc.dump(4));
    }

    auto audioEI = params["audioEIService"];
    if (!audioEI.isNull()) {
        model.preference.set("audioEIService", audioEI.dump(4));
    }

    peer->sendMessage({
        {"jsonrpc", "2.0"},
        {"id", req["id"]},
        {"result", {}}
    });
}

static void com_alticast_set_ccEnabled (const json &req, const Ref<RpcPeer> peer) {
    // extend: set cc enabled preference
    auto ccEnabled = JsonRef(req)["params"]["enabled"].toBool();

    model.preference.set("ccEnabled", ccEnabled? "true": "false");
    peer->sendMessage({
        {"jsonrpc", "2.0"},
        {"id", req["id"]},
        {"result", {}}
    });
}
static void com_alticast_tracks_query (const json &req, const Ref<RpcPeer> peer) {
    const char *tracks = task.platform->rmpGetTracks();
    json jTrack = json::parse(string(tracks));
    peer->sendMessage({
        {"jsonrpc", "2.0"},
        {"id", req["id"]},
        {"result", jTrack}
    });
}

static void dump (const json &req, const Ref<RpcPeer> peer) {
    model.dump();
    peer->sendMessage({
        {"jsonrpc", "2.0"},
        {"result", true},
        {"id", req["id"]}
    });
}

static void com_alticast_set_track(const json &req, const Ref<RpcPeer> peer) {
    int errorCode = -32603;

    auto jTrackId = JsonRef(req)["params"]["trackId"].asString();
    auto jSelected = JsonRef(req)["params"]["selected"].toBool();

    bool result = task.platform->rmpSelectTrackExt(
            jSelected, jTrackId.c_str());

    if (result) {
        peer->sendMessage({
            {"jsonrpc", "2.0"},
            {"id", req["id"]},
            {"result", {}}
        });
    } else {
        peer->sendMessage({
            {"jsonrpc", "2.0"},
            {"id", req["id"]},
            {"error", {{"code", -10},
                    {"message", "Track can not be selected"}}}
        });
    }
}

void RpcAlticast_init () {
    web.rpcManager.registerHandler("echo", echo);
    web.rpcManager.registerHandler("getslts", getslts);
    web.rpcManager.registerHandler("get_selected_service_id", get_selected_service_id);
    web.rpcManager.registerHandler("select_service", select_service);
    web.rpcManager.registerHandler("add_background_service", add_background_service);
    web.rpcManager.registerHandler("remove_background_service", remove_background_service);
    web.rpcManager.registerHandler("overrideAllKeys", overrideAllKeys);
    web.rpcManager.registerHandler("com.alticast.set.rating", com_alticast_set_rating);
    web.rpcManager.registerHandler("com.alticast.set.languages", com_alticast_set_languages);
    web.rpcManager.registerHandler("com.alticast.set.captionDisplay", com_alticast_set_captionDisplay);
    web.rpcManager.registerHandler("com.alticast.set.audioAccessibility", com_alticast_set_audioAccessibility);
    web.rpcManager.registerHandler("com.alticast.set.ccEnabled", com_alticast_set_ccEnabled);
    web.rpcManager.registerHandler("com.alticast.query.tracks", com_alticast_tracks_query);
    web.rpcManager.registerHandler("com.alticast.dump", dump);
    web.rpcManager.registerHandler("com.alticast.set.track",
            com_alticast_set_track);
};
