#include <set>
#include <string>
#include <sstream>
#include "acba.h"
#include "atsc3_model.h"
#include "atsc3_web.h"
#include "mpu_streamer.h"

using namespace std;
using namespace acba;
using namespace atsc3;

static Log l("mpu_ws");

namespace {

struct Connection: HttpRequestHandler,
        Listener<const acba::Ref<Mpu>&, uint8_t, const void*> {

    Connection (): reactor(nullptr) {}

    void handleWebsocketOpen (HttpReactor &r) override {
        l.info("open %p", &r);

        int bsid = -1;
        int sid = -1;

        const char *path = r.requestHandlerPath.c_str();
        l.info("request %s", path);

        // parse bsid/sid
        if (sscanf(path, "/%d/%d/", &bsid, &sid) != 2) {
            r.abort("Invalid path");
            return;
        }
        char path2[128];
        int len2 = sprintf(path2, "/%d/%d/", bsid, sid);
        if (strncmp(path, path2, len2) != 0) {
            r.abort("Invalid path2");
            return;
        }
        path += len2;

        // locate service object
        Service *srv = nullptr;
        auto bs = model.broadcastStreams.find(bsid);
        if (bs != model.broadcastStreams.end()) {
            for (auto sg: bs->second->serviceGroups) {
                auto s = sg.second->services.find(sid);
                if (s != sg.second->services.end()) {
                    srv = s->second.get();
                }
            }
        }

        // Check if the service is mmtp
        if (!srv || srv->protocol != 2) {
            r.abort("Non-existant path");
            return;
        }
        MmtpService *msrv = static_cast<MmtpService*>(srv);

        // select component
        int pktid = -1;
        if (strcmp(path, "video") == 0) {
            pktid = msrv->getDefaultVideoPacketId();
            l.info("defaultVideoPacketId %d", pktid);
        } else if (strcmp(path, "audio") == 0) {
            pktid = msrv->getDefaultAudioPacketId();
            l.info("defaultAudioPacketId %d", pktid);
        } else if (strcmp(path, "text") == 0) {
            pktid = msrv->getDefaultTextPacketId();
            l.info("defaultTextPacketId %d", pktid);
        }
        if (pktid == -1) {
            r.abort("No specified stream available");
            return;
        }

        // start feeding
        packetId = (uint16_t)pktid;
        reactor = &r;
        msrv->mpuUpdate.addListener(this);
        service = msrv;
    }

    void handleWebsocketTextMessage (HttpReactor &r) override {
        l.warn("got text message");
    }

    void handleWebsocketMessage (HttpReactor &r) override {
        l.warn("got binary message");
    }

    void handleWebsocketClose (HttpReactor &r) override {
        l.info("close %p", &r);
        if (service) service->mpuUpdate.removeListener(this);
    }

    void onUpdate (const acba::Ref<Mpu> &mpu, uint8_t ft, const void *param)
            override {
        if (!reactor) {
            l.error("Got MPU Update while connection is no longer available");
            return;
        }

        if (mpu->packetId != packetId) return;
        if (mpu == lastSentMpu) {
            l.warn("Ignored update from already sent MPU %u.%u", mpu->packetId,
                    mpu->sequenceNumber);
            return;
        }

        // wait until complete MPU is available
        if (mpu->flags != Mpu::FLAGS_ALL) return;

        // send mpu presentation time ahead
        buffer.writeI32(mpu->sequenceNumber);
        buffer.writeI8(4);
        buffer.writeF64(mpu->presentationTime);
        reactor->sendFrame(buffer.begin(), buffer.size());
        buffer.clear();

        // send ft0
        buffer.writeI32(mpu->sequenceNumber);
        buffer.writeI8(0);
        buffer.writeBytes(mpu->mpuMetadata.begin(), mpu->mpuMetadata.size());
        reactor->sendFrame(buffer.begin(), buffer.size());
        buffer.clear();

        // send ft1
        buffer.writeI32(mpu->sequenceNumber);
        buffer.writeI8(1);
        buffer.writeBytes(mpu->movieFragmentMetadata.begin(),
                mpu->movieFragmentMetadata.size());
        reactor->sendFrame(buffer.begin(), buffer.size());
        buffer.clear();

        // send whole ft2
        buffer.writeI32(mpu->sequenceNumber);
        buffer.writeI8(2);
        buffer.writeBytes(mpu->mediaData.begin(), mpu->mediaData.size());
        reactor->sendFrame(buffer.begin(), buffer.size());
        buffer.clear();

        lastSentMpu = mpu;
    }

    HttpReactor *reactor;
    Ref<MmtpService> service;
    uint16_t packetId;
    Ref<Mpu> lastSentMpu;
    Buffer buffer;
};

} // namespace

HttpRequestHandler *MpuStreamer::acceptHttpRequest (const string &basePath,
        HttpReactor *r)
{
    if (!r->websocketVersion) return 0;
    return new Connection();
}
