#include "acba.h"
#include "rpc_manager.h"
#include "atsc3_task.h"
#include "atsc3_web.h"
#include "nlohmann/json.hpp"
#include "JsonRef.h"
#include "pugixml.hpp"

using namespace std;
using namespace acba;
using namespace atsc3;
using namespace pugi;
using json = nlohmann::json;

static Log l("rpc_esg");

struct EsgNotifier: RefCounter<EsgNotifier>, Reactor,
        Listener<ESG_FRAGMENT_EVENT, const string&,
        const Ref<EsgFragmentSlide>&, const Ref<BroadcastStream>&>,
        Listener<const Ref<Service>&> {

    EsgNotifier (const Ref<RpcPeer> &p): peer(p),
            serviceFragments(json::array()), contentFragments(json::array()),
            scheduleFragments(json::array()), sltServices(json::array()) {}

    Ref<RpcPeer> peer;

    json serviceFragments;
    json contentFragments;
    json scheduleFragments;
    json sltServices;

    void clear () {
        serviceFragments.clear();
        contentFragments.clear();
        scheduleFragments.clear();
        sltServices.clear();
    }

    void onUpdate (ESG_FRAGMENT_EVENT evt, const string &fid,
            const Ref<EsgFragmentSlide>& fs, const Ref<BroadcastStream>& bs)
            override {
        switch (fs->type) {
        case 1: // service
            serviceFragments.push_back(fid);
            break;

        case 2: // content
            contentFragments.push_back(fid);
            break;

        case 3: // schedule
            scheduleFragments.push_back(fid);
            break;
        }

        task.eventDriver.setTimeout(500, this);
    }

    void onUpdate (const Ref<Service> &srv) override {
        if (!srv || srv->globalId.empty()) return;

        sltServices.push_back(srv->globalId);
        task.eventDriver.setTimeout(500, this);
    }

    void dispose () {
        model.esgFragmentUpdate.removeListener(this);
        model.serviceUpdate.removeListener(this);
        task.eventDriver.unsetTimeout(this);
    }

    void processEvent (int evt) override {
        if (serviceFragments.size() == 0 && contentFragments.size() == 0 &&
                scheduleFragments.size() == 0 && sltServices.size() == 0) {
            return;
        }

        l.info("Sending com.alticast.esg.update "
                "services:%d contents:%d schedules:%d sltsrvs:%d",
                serviceFragments.size(), contentFragments.size(),
                scheduleFragments.size(), sltServices.size());
        peer->sendMessage({
            {"jsonrpc", "2.0"},
            {"method", "com.alticast.esg.update"},
            {"params", {
                {"services", serviceFragments},
                {"contents", contentFragments},
                {"schedules", scheduleFragments},
                {"sltServices", sltServices},
            }},
        });
        clear();
    }
};

static void services (const json &req, const Ref<RpcPeer> peer) {
    json res = json::object();

    for (auto bs: model.broadcastStreams) {
        for (auto fs: bs.second->esgFragmentSlides) {
            if (!fs.second || fs.second->type != 1 ||
                    !fs.second->currentFragment) continue;

            stringstream ss;
            fs.second->currentFragment->save(ss);
            res[fs.first] = ss.str();
        }
    }

    peer->sendMessage({
        {"jsonrpc", "2.0"},
        {"result", res},
        {"id", req["id"]}
    });
}

static void query (const json &req, const Ref<RpcPeer> peer) {

    // parse args
    auto params = JsonRef(req)["params"];

    auto jSrvs = params["services"];
    set<string> srvs;
    for (int i = 0; ; i++) {
        auto srv = jSrvs[i];
        if (!srv.isString()) break;
        srvs.insert(srv.asString());
    }
    uint32_t startTime = params["startTime"].toU32(0);
    uint32_t endTime = params["endTime"].toU32(numeric_limits<uint32_t>::max());

    // find all fragments satisfying the condition.
    map<const string, Ref<EsgFragmentSlide> > res;
    for (auto bs: model.broadcastStreams) {
        for (auto fs: bs.second->esgFragmentSlides) {
            if (!fs.second || fs.second->type != 3 ||
                    !fs.second->currentFragment) continue;

            // filter-out by service list
            auto frag = fs.second->currentFragment->document_element();
            string sfid = frag.child("ServiceReference").
                    attribute("idRef").value();
            if (srvs.find(sfid) == srvs.end()) continue;

            bool addme = false;
            for (auto cr: frag.children("ContentReference")) {
                bool addme2 = false;
                for (auto pw: cr.children("PresentationWindow")) {
                    uint32_t st = pw.attribute("startTime").as_ullong(0);
                    uint32_t et = pw.attribute("endTime").as_ullong(0);

                    // NOTE:
                    // Condition is satisfied only when
                    // non-zero time is overlapping.
                    if (st && et) {
                        uint32_t b = max(startTime, st);
                        uint32_t e = min(endTime, et);
                        if (b < e) {
                            addme2 = true;
                            break;
                        }
                    } else if (st) {
                        if (st < endTime) {
                            addme2 = true;
                            break;
                        }
                    } else if (et) {
                        if (et > startTime) {
                            addme2 = true;
                            break;
                        }
                    } else {
                        addme2 = true;
                        break;
                    }
                }
                if (addme2) {
                    string cid = cr.attribute("idRef").value();
                    res[cid] = bs.second->esgFragmentSlides[cid];
                    addme = true;
                }
            }
            if (addme) {
                // Put related fragments
                res[fs.first] = fs.second;
                // NOTE: Excluded service fragments from result
                //res[sfid] = bs.second->esgFragmentSlides[sfid];
            }
        }
    }

    json jres = json::object();
    for (auto r: res) {
        if (!r.second || !r.second->currentFragment) continue;

        stringstream ss;
        r.second->currentFragment->save(ss);
        jres[r.first] = ss.str();
    }

    peer->sendMessage({
        {"jsonrpc", "2.0"},
        {"result", jres},
        {"id", req["id"]}
    });
}

static void subscribe (const json &req, const Ref<RpcPeer> peer) {
    peer->subscribe("alticast:esg");

    peer->sendMessage({
        {"jsonrpc", "2.0"},
        {"result", true},
        {"id", req["id"]}
    });
}

static void unsubscribe (const json &req, const Ref<RpcPeer> peer) {
    peer->unsubscribe("alticast:esg");

    peer->sendMessage({
        {"jsonrpc", "2.0"},
        {"result", true},
        {"id", req["id"]}
    });
}

struct EsgNotificationHandler: RpcNotificationHandler {

    map<RpcPeer*, Ref<EsgNotifier>> notifiers;

    bool subscribe (const std::string &type, const nlohmann::json &opt,
            const Ref<RpcPeer> &peer) override {
        auto &n = notifiers[peer.get()];
        if (n) return true;

        n = new EsgNotifier(peer);
        model.esgFragmentUpdate.addListener(n);
        model.serviceUpdate.addListener(n);
        return true;
    }

    bool unsubscribe (const std::string &type, const Ref<RpcPeer> &peer)
            override {
        auto ni = notifiers.find(peer.get());
        if (ni == notifiers.end()) return false;

        auto &n = ni->second;
        n->dispose();
        notifiers.erase(ni);
        return true;
    }
};

void RpcAlticastEsg_init ()
{
    web.rpcManager.registerHandler("com.alticast.esg.services", services);
    web.rpcManager.registerHandler("com.alticast.esg.query", query);
    web.rpcManager.registerHandler("com.alticast.esg.subscribe", subscribe);
    web.rpcManager.registerHandler("com.alticast.esg.unsubscribe", unsubscribe);

    static EsgNotificationHandler esgNotificationHandler;
    web.rpcManager.registerNotificationHandler("alticast:esg", &esgNotificationHandler);
}
