#include <iostream>

#include "acba.h"
#include "rpc_manager.h"
#include "atsc3_web.h"
#include "atsc3_task.h"
#include "nlohmann/json.hpp"

using namespace std;
using namespace acba;
using namespace atsc3;
using json = nlohmann::json;

static Log l("rpc_atsc3_rmp_subscribe");

struct ServiceNotifier: RefCounter<ServiceNotifier>,Listener<> {

    ServiceNotifier (const Ref<RpcPeer> &p): peer(p) {}
    Ref<RpcPeer> peer;

    void onUpdate () override {

        l.info("onUpdate for ServiceChange");
        // 9.3.3
        json gsid = nullptr;
        if (model.currentService) gsid = model.currentService->globalId;
        json ret = {};
        ret["msgType"] = "serviceChange";
        ret["service"] = gsid;
        peer->sendMessage({
            {"jsonrpc", "2.0"},
            {"method", "org.atsc.notify"},
            {"params", ret }
        });
    }

    void dispose () {
        model.currentServiceUpdate.removeListener(this);
        l.info("ServiceNotifier dispose()");
    }
};

struct ServiceNotificationHandler: RpcNotificationHandler {
    map<RpcPeer*, Ref<ServiceNotifier>> serviceNotifiers;
    bool subscribe (const std::string &type, const nlohmann::json &opt,
            const acba::Ref<RpcPeer> &peer) override {

        auto &n = serviceNotifiers[peer.get()];
        if (n) return true;

        n = new ServiceNotifier(peer);
        model.currentServiceUpdate.addListener(n.get());
        return true;
    }

    bool unsubscribe (const std::string &type, const acba::Ref<RpcPeer> &peer)
            override {
        auto ni = serviceNotifiers.find(peer.get());
        if (ni == serviceNotifiers.end()) return false;

        auto &n = ni->second;
        n->dispose();
        return true;
    }
};

void RpcAtsc3Service_init ()
{
    static ServiceNotificationHandler snh;
    web.rpcManager.registerNotificationHandler("atsc3:serviceChange", &snh);
}
