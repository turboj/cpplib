#include <iostream>

#include "acba.h"
#include "rpc_manager.h"
#include "atsc3_web.h"
#include "atsc3_task.h"
#include "nlohmann/json.hpp"
#include "JsonRef.h"

using namespace std;
using namespace acba;
using namespace atsc3;
using json = nlohmann::json;

static Log l("rpc_atsc3_rmp_subscribe");

struct RmpNotifier: RefCounter<RmpNotifier>, Listener<RMP_MEDIA_EVENT, double> {

    RmpNotifier (const Ref<RpcPeer> &p, RMP_MEDIA_EVENT evt): peer(p), rmpEvent(evt) {};
    Ref<RpcPeer> peer;
    RMP_MEDIA_EVENT rmpEvent;

    void onUpdate (RMP_MEDIA_EVENT evt, double value) override {

		l.info("onUpdate for rmpMediaTimeChangeUpdate");

        if (evt == RMP_MEDIA_TIME_CHANGE) {

            // 9.14.5
            peer->sendMessage({
                {"jsonrpc", "2.0"},
                {"method", "org.atsc.notify"},
                {"params", {
                    {"msgType", "rmpMediaTimeChange"},
                    {"currentTime", ((long)(1000*value))/1000.000}
                }},
            });
        } else if (evt == RMP_PLAY_STATE_CHANGE) {
            // 9.14.6
            peer->sendMessage({
                {"jsonrpc", "2.0"},
                {"method", "org.atsc.notify"},
                {"params", {
                    {"msgType", "rmpPlaybackStateChange"},
                    {"playbackState", task.rmp.rmpPlaybackState}
                }},
            });

        } else if (evt == RMP_PLAY_RATE_CHANGE) {
            // 9.14.7
            peer->sendMessage({
                {"jsonrpc", "2.0"},
                {"method", "org.atsc.notify"},
                {"params", {
                    {"msgType", "rmpPlaybackRateChange"},
                    {"playbackRate", task.rmp.rmpPlaybackState}
                 }},
            });
        }
    }

    int getRmpEventType () {
        return rmpEvent;
    }

    void dispose () {
        if (rmpEvent == RMP_MEDIA_TIME_CHANGE)
            task.rmp.rmpMediaTimeChangeUpdate.removeListener(this);
        else if (rmpEvent == RMP_PLAY_STATE_CHANGE)
            task.rmp.rmpPlaybackStateChangeUpdate.removeListener(this);
        else if (rmpEvent == RMP_PLAY_RATE_CHANGE)
            task.rmp.rmpPlaybackRateChangeUpdate.removeListener(this);
        l.info("RmpNotifier dispose()");
    }
};

void org_atsc_track_selection (const json &req, const Ref<RpcPeer> peer)
{
    int errorCode = -32603;

    int id = -1;
    auto jSelectionId = JsonRef(req)["params"]["selectionId"];
    if (jSelectionId.isNumber()) {
        id = jSelectionId.toI32();
    } else {
        errorCode = -32602;
    }
    if (id != -1) {
        bool result = task.platform->rmpSelectTrack(id);

        if (result) {
            peer->sendMessage({
                {"jsonrpc", "2.0"},
                {"id", req["id"]},
                {"result", {}}
            });
        } else {
            peer->sendMessage({
                {"jsonrpc", "2.0"},
                {"id", req["id"]},
                {"error", {{"code", -10},
                        {"message", "Track can not be selected"}}}
            });
        }
    } else {
        string errMsg;
        if (errorCode == -32602) {
            errMsg = "Invalid method parameter";
        } else {
            errMsg = "Internal error";
        }
        peer->sendMessage({
            {"jsonrpc", "2.0"},
            {"id", req["id"]},
            {"error", {{"code", errorCode},
                    {"message", errMsg}}}
        });
    }

}

struct RmpNotificationHandler: RpcNotificationHandler {
    map<std::pair<RpcPeer*, RMP_MEDIA_EVENT>, Ref<RmpNotifier>> rmpNotifiers;

    bool subscribe (const std::string &type, const nlohmann::json &opt,
            const acba::Ref<RpcPeer> &peer) override {

        RMP_MEDIA_EVENT evt;
        if (type == "atsc3:rmpMediaTimeChange")
            evt = RMP_MEDIA_TIME_CHANGE;
        else if (type == "atsc3:rmpPlaybackStateChange")
            evt = RMP_PLAY_STATE_CHANGE;
        else if (type == "atsc3:rmpPlaybackRateChange")
            evt = RMP_PLAY_RATE_CHANGE;

        l.info("subscribe %s", type.c_str());
        auto &n = rmpNotifiers[{peer.get(), evt}];
        if (n) return true;

        n = new RmpNotifier({peer, evt});
        if (type == "atsc3:rmpMediaTimeChange")
            task.rmp.rmpMediaTimeChangeUpdate.addListener(n.get());
        else if (type == "atsc3:rmpPlaybackStateChange")
            task.rmp.rmpPlaybackStateChangeUpdate.addListener(n.get());
        else if (type == "atsc3:rmpPlaybackRateChange")
            task.rmp.rmpPlaybackRateChangeUpdate.addListener(n.get());

        return true;
    }

    bool unsubscribe (const std::string &type, const acba::Ref<RpcPeer> &peer)
            override {
        RMP_MEDIA_EVENT evt;
        if (type == "atsc3:rmpMediaTimeChange")
            evt = RMP_MEDIA_TIME_CHANGE;
        else if (type == "atsc3:rmpPlaybackStateChange")
            evt = RMP_PLAY_STATE_CHANGE;
        else if (type == "atsc3:rmpPlaybackRateChange")
            evt = RMP_PLAY_RATE_CHANGE;

        l.info("unsubscribe %d", evt);
        auto ni = rmpNotifiers.find({peer.get(), evt});
        if (ni == rmpNotifiers.end()) return false;

        auto &n = ni->second;
        n->dispose();
        rmpNotifiers.erase(ni);
        return true;
    }
};

void RpcAtsc3Rmp_init ()
{
    static RmpNotificationHandler rnh;
    web.rpcManager.registerNotificationHandler("atsc3:rmpMediaTimeChange", &rnh);
    web.rpcManager.registerNotificationHandler("atsc3:rmpPlaybackStateChange",
            &rnh);
    web.rpcManager.registerNotificationHandler("atsc3:rmpPlaybackRateChange",
            &rnh);

    web.rpcManager.registerHandler("org.atsc.track.selection",
            org_atsc_track_selection);
}
