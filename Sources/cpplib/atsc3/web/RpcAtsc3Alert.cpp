#include <sstream>

#include "acba.h"
#include "rpc_manager.h"
#include "atsc3_web.h"
#include "atsc3_task.h"
#include "nlohmann/json.hpp"
#include "JsonRef.h"
#include "pugixml.hpp"
#include "datetime.h"

using namespace std;
using namespace acba;
using namespace atsc3;
using namespace pugi;
using json = nlohmann::json;

static Log l("rpc_atsc3_alert");

static void query (const json &req, const Ref<RpcPeer> peer) {

    auto alertTypes = JsonRef(req)["params"]["alertingTypes"];
    if (!alertTypes.isArray()) {
        peer->sendMessage({
            {"jsonrpc", "2.0"},
            {"id", req["id"]},
            {"error", {
                {"code", -32602}
            }},
        });
        return;
    }

    auto alertList = json::array();
    for (int i = 0;; i++) {
        auto jAt = alertTypes[i];
        if (!jAt.isString()) break;
        auto &at = jAt.asString();

        if (at == "AEAT" && model.activeAeatInfo) {
            auto &ai = *model.activeAeatInfo;

            stringstream ss;
            ai.aeatDocument->save(ss);

            auto fevents = json::array();
            for (auto i: ai.filteredEvents) fevents.push_back(i);

            alertList.push_back(json({
                {"alertingType", "AEAT"},
                {"alertingFragment", ss.str()},
                {"receiveTime", toDatetime(ai.receiveTime)},
                {"filteredEventList", fevents},
            }));
        } else if (at == "OSN" && model.activeOsmnInfo) {
            auto &oi = *model.activeOsmnInfo;

            stringstream ss;
            oi.osmnDocument->save(ss);

            alertList.push_back(json({
                {"alertingType", "OSN"},
                {"alertingFragment", ss.str()},
                {"receiveTime", toDatetime(oi.receiveTime)},
            }));
        }
    }

    peer->sendMessage({
        {"jsonrpc", "2.0"},
        {"result", {
            {"alertList", alertList}
        }},
        {"id", req["id"]}
    });
}

struct AlertNotificationHandler: RpcNotificationHandler {

    AlertNotificationHandler () {
    }

    set<Ref<RpcPeer> > peers;

    bool subscribe (const string &type, const json &opt,
            const Ref<RpcPeer> &peer) override {
        peers.insert(peer);
        return true;
    }

    bool unsubscribe (const string &type, const Ref<RpcPeer> &peer) override {
        peers.erase(peer);
        return true;
    }

    struct AeatInfoListener: Listener<> {
        void onUpdate () override {
            auto *self = container_of(this,
                    &AlertNotificationHandler::aeatInfoListener);

            if (!model.activeAeatInfo) {
                self->notify({
                    {"alertingType", "AEAT"},
                }, true);
                return;
            }

            auto &ai = *model.activeAeatInfo;

            stringstream ss;
            ai.aeatDocument->save(ss);

            auto fevents = json::array();
            for (auto i: ai.filteredEvents) fevents.push_back(i);

            self->notify({
                {"alertingType", "AEAT"},
                {"alertingFragment", ss.str()},
                {"receiveTime", toDatetime(ai.receiveTime)},
                {"filteredEventList", fevents},
            });
        }
    } aeatInfoListener;

    struct OsmnInfoListener: Listener<> {
        void onUpdate () override {
            auto *self = container_of(this,
                    &AlertNotificationHandler::osmnInfoListener);

            if (!model.activeOsmnInfo) {
                self->notify({
                    {"alertingType", "OSN"},
                }, true);
                return;
            }

            auto &oi = *model.activeOsmnInfo;

            stringstream ss;
            oi.osmnDocument->save(ss);

            self->notify({
                {"alertingType", "OSN"},
                {"alertingFragment", ss.str()},
                {"receiveTime", toDatetime(oi.receiveTime)},
            });
        }
    } osmnInfoListener;

    void notify (const json &evt, bool privilegedOnly = false) {
        json msg = {
            {"jsonrpc", "2.0"},
            {"method", "org.atsc.notify"},
            {"params", {
                {"msgType", "alertingChange"},
                {"alertList", json::array({evt})}
            }}
        };

        for (auto p: peers) {
            if (privilegedOnly && !p->privileged) continue;
            p->sendMessage(msg);
        }
    }
};

void RpcAtsc3Alert_init ()
{
    web.rpcManager.registerHandler("org.atsc.query.alerting", query);
    static AlertNotificationHandler anh;
    model.activeAeatInfoUpdate.addListener(&anh.aeatInfoListener);
    model.activeOsmnInfoUpdate.addListener(&anh.osmnInfoListener);

    web.rpcManager.registerNotificationHandler("atsc3:alertingChange", &anh);
}
