#include <arpa/inet.h>
#include <sys/socket.h>
#include <assert.h>
#include <set>
#include <string>
#include <sstream>
#include <algorithm>

#include "acba.h"
#include "atsc3_model.h"
#include "atsc3_task.h"
#include "atsc3_web.h"
#include "rpc_manager.h"
#include "nlohmann/json.hpp"
#include "JsonRef.h"
#include "pugixml.hpp"
#include "datetime.h"

using namespace std;
using namespace acba;
using namespace atsc3;
using namespace pugi;
using json = nlohmann::json;

static Log l("rpc");

namespace {

struct RpcServer: HttpRequestHandler {

    Ref<RpcPeer> peer;

    void handleWebsocketOpen (HttpReactor &r) override {

        l.info("open %p", this);
        for (auto h: r.headers) {
            l.info("    %s: %s", h.first.c_str(), h.second.c_str());
        }
        assert(!peer);

        // get my ip
        sockaddr_in addr;
        socklen_t sz = sizeof(addr);
        if (getsockname(r.getFd(), (sockaddr*)&addr, &sz)) {
            r.abort("getsockname failed.");
            return;
        }
        if (addr.sin_family != AF_INET) {
            r.abort("getsockname returns non AF_INET");
            return;
        }
        uint32_t ip = ntohl(addr.sin_addr.s_addr);

        peer = new RpcPeer(this, &r);

        // Associate websocket peer with application context cache
        for (auto &acci: model.applicationContextCaches) {
            auto &acc = acci.second;
            if (acc->hostIp == ip) {
                l.info("Websocket connection from %s", acc->id.c_str());
                peer->applicationContextCache = acc;
                break;
            }
        }

        // try to authenticate the connection
        const char *q = strstr(r.requestQuery.c_str(), "privateId=");
        if (!q) return;
        string id = string(q).substr(sizeof("privateId=") - 1, 256);
        auto &sww = task.applicationManager.systemWebWindow;
        if (sww && sww->privateId == id) {
            peer->privileged = true;
            l.info("Established privileged rpc connection %s", id.c_str());
        }
    }

    void handleWebsocketTextMessage (HttpReactor &r) override {
        l.info("text %p", this);
        json req = json::parse({r.contentBuffer.begin(), r.contentBuffer.end()},
                nullptr, false);
        if (req.is_discarded()) {
            string str((char*)r.contentBuffer.begin(), r.contentBuffer.size());
            l.error("Invalid json format %s", str.c_str());
            r.contentBuffer.dump();
            return; // ignore request
        }
        l.info("msg: %s", req.dump(4).c_str());

        JsonRef jref = req;

        // TODO: send error
        if (jref["jsonrpc"].asString() != "2.0") return;
        if (!jref["method"].isString()) return;

        auto &m = jref["method"].asString();

        auto rhi = web.rpcManager.rpcHandlers.find(m);
        if (rhi == web.rpcManager.rpcHandlers.end()) {
            l.warn("couldn't find rpc handler for %s", m.c_str());
            peer->sendMessage({
                {"jsonrpc", "2.0"},
                {"error", {
                    {"code", /* Method not found */ -32601},
                    {"message", "Unknown method"}
                }},
                {"id", req["id"]}
            });
            return;
        }

        rhi->second(req, peer);
    }

    void handleWebsocketClose (HttpReactor &r) override {
        l.info("close %p", &r);
        if (!peer) return;

        peer->dispose();
        peer = nullptr;
    }
};

}

void RpcAlticast_init ();
void RpcAlticastEsg_init ();
void RpcAlticastTune_init ();
void RpcAtsc3_init ();
void RpcAtsc3Subscribe_init ();
void RpcAtsc3Alert_init ();
void RpcAtsc3Rmp_init();
void RpcAtsc3Config_init();
void RpcAtsc3Service_init();
void RpcAtsc3Drm_init();
void RpcAtsc3EventStream_init();
void RpcAtsc3Esg_init ();
void RpcAtsc3Cache_init ();
void RpcAtsc3Signaling_init ();
void RpcAtsc3Cancel_init();

void RpcManager::init ()
{
    RpcAlticast_init();
    RpcAlticastEsg_init();
    RpcAlticastTune_init();
    RpcAtsc3_init();
    RpcAtsc3Subscribe_init();
    RpcAtsc3Alert_init();
    RpcAtsc3Rmp_init();
    RpcAtsc3Config_init();
    RpcAtsc3Service_init();
    RpcAtsc3Drm_init();
    RpcAtsc3EventStream_init();
    RpcAtsc3Esg_init();
    RpcAtsc3Cache_init();
    RpcAtsc3Signaling_init();
    RpcAtsc3Cancel_init();
}

HttpRequestHandler *RpcManager::acceptHttpRequest (const string &basePath,
        HttpReactor *r)
{
    if (!r->websocketVersion) return 0;
    return new RpcServer();
}

void RpcManager::registerHandler (const string &name, RpcHandler h)
{
    // make sure handlers are not conflicting.
    auto hi = rpcHandlers.find(name);
    assert(hi == rpcHandlers.end());

    l.info("registered handler for method %s", name.c_str());
    rpcHandlers[name] = h;
}

void RpcManager::registerNotificationHandler (const string &name,
        RpcNotificationHandler *h)
{
    // make sure handlers are not conflicting.
    auto hi = notificationHandlers.find(name);
    assert(hi == notificationHandlers.end());

    l.info("registered notification handler for message type %s", name.c_str());
    notificationHandlers[name] = h;
}
