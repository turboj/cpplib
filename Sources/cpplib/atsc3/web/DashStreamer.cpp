#include <set>
#include <string>
#include <sstream>

#include "acba.h"
#include "atsc3_model.h"
#include "atsc3_task.h"
#include "dash_streamer.h"

#define FILE_WAIT_TIMEOUT 3

using namespace std;
using namespace acba;
using namespace atsc3;

static Log l("dash_streamer");

struct DashHandler: HttpRequestHandler, Reactor,
        Listener<const Ref<RouteFile>&> {

    DashHandler (): httpReactor(nullptr) {}

    void handleHttpRequest (HttpReactor &r) override {
        int bsid = -1;
        int sid = -1;

        const char *path = r.requestHandlerPath.c_str();
        l.info("request %s", path);

        // parse bsid/sid
        if (sscanf(path, "/%d/%d/", &bsid, &sid) != 2) {
            r.sendResponse(404, {"Content-Type: text/plain"}, "Invalid Path");
            return;
        }
        char path2[128];
        int len2 = sprintf(path2, "/%d/%d/", bsid, sid);
        if (strncmp(path, path2, len2) != 0) {
            r.sendResponse(404, {"Content-Type: text/plain"}, "Invalid Path2");
            return;
        }
        path += len2;

        // locate service object
        Service *srv = nullptr;
        auto bs = model.broadcastStreams.find(bsid);
        if (bs != model.broadcastStreams.end()) {
            for (auto &sg: bs->second->serviceGroups) {
                auto s = sg.second->services.find(sid);
                if (s != sg.second->services.end()) {
                    srv = s->second.get();
                }
            }
        }

        // support mmt service only.
        if (!srv || srv->protocol != 1) {
            r.sendResponse(404, {"Content-Type: text/plain"},
                    "Non-existant path");
            return;
        }
        RouteService *rsrv = static_cast<RouteService*>(srv);

        // Send MPD if path matches
        if (strcmp(path, "_mpd.xml") == 0) {
            if (!rsrv->resolvedMpdFile) {
                r.sendResponse(404, {"Content-Type: text/plain"}, "No mpd file");
                return;
            }

            auto &mf = rsrv->resolvedMpdFile;
            auto &b = mf->content;
            r.sendHead(200, {string("Content-Type: ") + mf->type}, b.size());
            r.sendBody(b.begin(), b.size());
            r.endResponse();
            return;
        }

        httpReactor = &r;
        routeFilePath = path;
        routeService = rsrv;

        auto fi = rsrv->files.find(path);
        if (fi == rsrv->files.end()) {
            rsrv->fileUpdate.addListener(this);
            task.eventDriver.setTimeout(FILE_WAIT_TIMEOUT * 1000000, this);
        } else {
            task.eventDriver.setTimeout(0, this);
        }
    }

    void onUpdate (const Ref<RouteFile>& rf) override {
        if (!rf || rf->path != routeFilePath) return;
        task.eventDriver.setTimeout(0, this);
    }

    void processEvent (int evt) override {
        routeService->fileUpdate.removeListener(this);

        auto rfi = routeService->files.find(routeFilePath);
        if (rfi == routeService->files.end()) {
            l.warn("sending 404 error for %s", routeFilePath.c_str());
            httpReactor->sendResponse(404, {"Content-Type: text/plain"}, "");
            return;
        }
        auto &rf = rfi->second;

        // good to send!
        l.info("sending route file %s(%u.%u) from bsid:%d sid:%d",
                routeFilePath.c_str(), rf->tsi, rf->toi, routeService->bsid,
                routeService->id);
        vector<string> headers;
        if (rf->type.empty()) {
            headers.push_back("Content-Type: application/octet-stream");
        } else {
            headers.push_back(string("Content-Type: ") + rf->type);
        }
        Buffer &b = rf->content;
        httpReactor->sendHead(200, headers, b.size());
        httpReactor->sendBody(b.begin(), b.size());
        httpReactor->endResponse();
    }

    HttpReactor *httpReactor;
    string routeFilePath;
    Ref<RouteService> routeService;
};

HttpRequestHandler *DashStreamer::acceptHttpRequest (const string &basePath,
        HttpReactor *r)
{
    if (r->websocketVersion) return 0;
    return new DashHandler();
}
