#ifndef ATSC3_DASH_STREAMER_H
#define ATSC3_DASH_STREAMER_H

#include <sstream>

#include "acba.h"
#include "http_server.h"
#include "atsc3_model.h"

namespace atsc3 {

struct DashStreamer: acba::HttpRequestAcceptor {
    acba::HttpRequestHandler *acceptHttpRequest (const std::string &basePath,
            acba::HttpReactor *r) override;
};

}
#endif
