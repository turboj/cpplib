#include "acba.h"
#include "rpc_manager.h"
#include "atsc3_web.h"
#include "atsc3_task.h"
#include "nlohmann/json.hpp"
#include "JsonRef.h"
#include "pugixml.hpp"
#include "datetime.h"

using namespace std;
using namespace acba;
using namespace atsc3;
using namespace pugi;
using json = nlohmann::json;

static Log l("rpc_atsc3");

typedef struct RmpStateNotifier RmpStateNotifier;
static map<RpcPeer*, Ref<RmpStateNotifier>> rmpStateNotifiers;
struct RmpStateNotifier: RefCounter<RmpStateNotifier>, Listener<RMP_MEDIA_EVENT, double> {
    RmpStateNotifier (const Ref<RpcPeer> &p, int id): peer(p), rpcId(id) {};
    Ref<RpcPeer> peer;
    int rpcId;

    void onUpdate (RMP_MEDIA_EVENT evt, double value) override {
        l.info("onUpdate for rmpPlaybackStateChangeUpdate");
        if (evt == RMP_PLAY_STATE_CHANGE) {
            int errorCode = 0;
            string msg;
            if (task.rmp.rmpPlaybackState == RMP_PLAY_STATE_PLAYING) {
                // nothing to do
            } else if (task.rmp.rmpPlaybackState == RMP_PLAY_STATE_FAILED) {
                errorCode = -12;
                msg = "The content cannot be played";
            } else
                return;

            if (errorCode == 0) {
                peer->sendMessage({
                    {"jsonrpc", "2.0"},
                    {"result", {}},
                    {"id", rpcId}
                });
            } else {
               peer->sendMessage({
                    {"jsonrpc", "2.0"},
                    {"error", {{"code", errorCode}, {"message", msg}}},
                    {"id", rpcId}
                });
            }
            auto ni = rmpStateNotifiers.find(peer.get());
            if (ni == rmpStateNotifiers.end()) return;
            auto &n = ni->second;
            n->dispose();
            rmpStateNotifiers.erase(ni);
        }
    }
    void dispose() {
        task.rmp.rmpPlaybackStateChangeUpdate.removeListener(this);
    }
};

static void org_atsc_request_keys (const json &req, const Ref<RpcPeer> peer)
{
    set<string> keys;
    auto jKeys = JsonRef(req)["params"]["keys"];
    for (int i = 0; ; i++) {
        auto k = jKeys[i];
        if (!k.isString()) break;
        keys.insert(k.asString());
    }

    auto &at = task.applicationManager.applicationTask;
    if (at && at->webWindow) {
        at->webWindow->bindKeys(keys);
    } else {
        l.warn("Application is not running");
        keys.clear();
    }

    json accepted = json::array();
    for (auto k: keys) accepted.push_back(k);

    peer->sendMessage({
        {"jsonrpc", "2.0"},
        {"result", {
            {"accepted", accepted}
        }},
        {"id", req["id"]}
    });
}

static void org_atsc_relinquish_keys (const json &req, const Ref<RpcPeer> peer)
{
    auto &am = task.applicationManager;

    set<string> keys;
    auto jKeys = JsonRef(req)["params"]["keys"];
    for (int i = 0; ; i++) {
        auto k = jKeys[i];
        if (!k.isString()) break;
        keys.insert(k.asString());
    }

    // If no keys are specified, unbind all keys
    if (keys.size() == 0) {
        for (auto kbi: am.keyBindings) {
            auto &kb = kbi.second;
            if (kb.applicationBound) {
                keys.insert(kb.name);
            }
        }
    }

    auto &at = am.applicationTask;
    if (at && at->webWindow) {
        at->webWindow->unbindKeys(keys);
    } else {
        l.warn("Application is not running");
    }

    peer->sendMessage({
        {"jsonrpc", "2.0"},
        {"result", {}},
        {"id", req["id"]}
    });
}

static void org_atsc_query_ratingLevel (const json &req,
        const Ref<RpcPeer> peer)
{

    string rating;
    model.preference.get("rating", rating);

    string contentRating;
    task.ratingHandler.retrieveContentAdvisoryRating(
        model.currentService.get(), contentRating);

    peer->sendMessage({
        {"jsonrpc", "2.0"},
        {"id", req["id"]},
        {"result", {
            {"rating", rating},
            {"contentRating", contentRating},
            {"blocked", task.ratingHandler.blocked}
        }}
    });
}

static void org_atsc_query_cc (const json &req, const Ref<RpcPeer> peer)
{
    string ccEnabled;
    model.preference.get("ccEnabled", ccEnabled);
    peer->sendMessage({
        {"jsonrpc", "2.0"},
        {"id", req["id"]},
        {"result", {{"ccEnabled", ccEnabled=="true"?true:false}}}
    });
}

static void org_atsc_query_service (const json &req, const Ref<RpcPeer> peer)
{

    json gsid = nullptr;
    if (model.currentService) gsid = model.currentService->globalId;
    peer->sendMessage({
        {"jsonrpc", "2.0"},
        {"id", req["id"]},
        {"result", {{"service", gsid}}}
    });
}

static void org_atsc_query_languages (const json &req, const Ref<RpcPeer> peer)
{
    string audio;
    model.preference.get("preferredAudioLang", audio);
    string ui;
    model.preference.get("preferredUiLang", ui);
    string cc;
    model.preference.get("preferredCaptionSubtitleLang", cc);

    peer->sendMessage({
        {"jsonrpc", "2.0"},
        {"id", req["id"]},
        {"result", {
            {"preferredAudioLang", audio},
            {"preferredUiLang", ui},
            {"preferredCaptionSubtitleLang", cc}
        }}
    });
}

static void org_atsc_query_captionDisplay (const json &req,
        const Ref<RpcPeer> peer)
{
    string caption;
    model.preference.get("captionDisplay", caption);

    json ret = json::parse(caption);
    ret["msgType"] = "captionDisplayPrefs";
    if (caption.empty()) {
        peer->sendMessage({
            {"jsonrpc", "2.0"},
            {"id", req["id"]},
            {"result", {}}
        });
    } else {
        peer->sendMessage({
            {"jsonrpc", "2.0"},
            {"id", req["id"]},
            {"result", ret}
        });
    }
}

static void org_atsc_query_audioAccessibility (const json &req,
        const Ref<RpcPeer> peer)
{
    json audio = {};
    string videoDesc;
    model.preference.get("videoDescriptionService", videoDesc);
    string audioEI;
    model.preference.get("audioEIService", audioEI);

    audio["videoDescriptionService"] = json::parse(videoDesc);
    audio["audioEIService"] = json::parse(audioEI);
    peer->sendMessage({
        {"jsonrpc", "2.0"},
        {"id", req["id"]},
        {"result", audio}
    });
}

static void org_atsc_query_MPDUrl (const json &req, const Ref<RpcPeer> peer)
{
    if (model.currentService && model.currentService->protocol == 1) {
        auto rsrv = static_cast<RouteService*>(model.currentService.get());
        if (rsrv->resolvedMpdFile) {
            char url[256];
            sprintf(url, "http://localhost:8080/dash/%d/%d/_mpd.xml",
                rsrv->bsid, rsrv->id);

            peer->sendMessage({
                {"jsonrpc", "2.0"},
                {"id", req["id"]},
                {"result", {{"MPDUrl", url}}}
            });
            return;
        }
    }

    peer->sendMessage({
        {"jsonrpc", "2.0"},
        {"id", req["id"]},
        {"error", {{"code", -4},
                    {"message", "Content Not Found"}}}
    });
}

static void org_atsc_acquire_service (const json &req, const Ref<RpcPeer> peer)
{
    int errorCode = 0;

    Service *srv = nullptr;
    string strSvc;

    auto jSvcToAcquire = JsonRef(req)["params"]["svcToAcquire"];
    if (!jSvcToAcquire.isString()) {
        errorCode = -32602;
    } else {
        strSvc = jSvcToAcquire.asString();
    }

    if (!strSvc.empty()) {
        for (auto bs: model.broadcastStreams) {
            for (auto sg: bs.second->serviceGroups) {
                for (auto s: sg.second->services) {
                    if (s.second->globalId == strSvc) srv = s.second;
                }
            }
        }
        if (srv == nullptr) {
            errorCode = -6;
        } else {
            task.rmp.setScalePosition(100, 0, 0);
            model.selectService(srv);
            peer->sendMessage({
                {"jsonrpc", "2.0"},
                {"id", req["id"]},
                {"result", !!srv}
            });
        }
    }
    if (errorCode != 0) {
        string errMsg;
        if (errorCode == -6)
            errMsg = "Service not found";
        else if (errorCode == -7)
            errMsg = "Service not authorized"; // TODO find the case
        else if (errorCode == -32602)
            errMsg = "Invalid params";
        peer->sendMessage({
            {"jsonrpc", "2.0"},
            {"id", req["id"]},
            {"error", {{"code", errorCode},
                        {"message", errMsg}}}
        });
    }
}

static void org_atsc_scale_position (const json &req, const Ref<RpcPeer> peer)
{
    auto params = JsonRef(req)["params"];
    if (!params.isObject()) {
        peer->sendMessage({
            {"jsonrpc", "2.0"},
            {"id", req["id"]},
            {"error",{{"code", -8},
                        {"message", "Video scaling/position failed"}}}
        });
        return;
    }

    auto scaleFactor = params["scaleFactor"].toF64(100.0);
    auto xPos = params["xPos"].toF64();
    auto yPos = params["yPos"].toF64();

    // TODO error handling
    task.rmp.setScalePosition(scaleFactor, xPos, yPos);

    peer->sendMessage({
        {"jsonrpc", "2.0"},
        {"id", req["id"]},
        {"result",{}}
    });
}

static void org_atsc_setRMPURL (const json &req, const Ref<RpcPeer> peer)
{
    int errorCode = 0;

    auto params = JsonRef(req)["params"];
    if (!params.isObject()) errorCode = -32602;

    auto oper = params["operation"];
    string strOper;
    if (oper.isString()) {
        strOper = oper.asString();
    } else {
        errorCode = -32602;
    }

    if (strOper == "startRmp") {
        auto jrmpurl = params["rmpurl"];
        if (jrmpurl.isString()) {

            auto &n = rmpStateNotifiers[peer.get()];
            if (!n) {
                n = new RmpStateNotifier(peer, req["id"]);
                task.rmp.rmpPlaybackStateChangeUpdate.addListener(n.get());
            }
            auto &rmpurl = jrmpurl.asString();
            double syncTime = params["rmpSyncTime"].toF64();

            // play MPD at the end of the current presentation
            if (task.rmp.isSignaling == false && syncTime == -1.0) {
                task.rmp.nextContentByApp = rmpurl;
                task.rmp.syncTime = 0;
            }
            // play MPD when the presentation time reaches rmpSyncTime
            else if (task.rmp.isSignaling == true && syncTime > 0 ) {
                atsc3_RMPMediaTime rmpTime;
                task.rmp.getRMPMediaTime(&rmpTime);

                if (rmpTime.currentTime > syncTime) {
                    l.info("rmpSyncTime cannot be achieved!!!");
                    // error code
                    errorCode = -19;
                } else {
                    task.rmp.syncTime = syncTime;
                    task.rmp.nextContentByApp = rmpurl;
                }
            }
            // play MPD immediately
            else if (syncTime == 0){
                task.rmp.nextContentByApp = "";
                task.rmp.syncTime = 0;
                task.rmp.startRmp(&rmpurl);
            }
        } else {
            errorCode = -32602;
        }
        if (errorCode == 0) {
            // In case of normal, the response message will be sent from RmpStateNotifier created above.
            return;
        }

    } else if (strOper == "stopRmp") {

        double syncTime = params["rmpSyncTime"].toF64();

        if (task.rmp.started == false) {
             peer->sendMessage({
                {"jsonrpc", "2.0"},
                {"result", {}},
                {"id", req["id"]}
            });
            return;
        }
        // stop current playing immediately
        if (syncTime == 0.0) {
            task.rmp.nextContentByApp = "";
            task.rmp.syncTime = 0;
            task.rmp.stopRmp();
        }
        // stop current playing when the presentation time reaches rmpSyncTime
        else if ( syncTime > 0 ) {
            task.rmp.nextContentByApp = "";
            task.rmp.syncTime = syncTime;
        }

    } else if (strOper == "resumeService") {
        double syncTime = params["rmpSyncTime"].toF64();

        // play Service-level signaling immediately
        if (syncTime == 0.0) {
            task.rmp.syncTime = 0;
            task.rmp.nextContentByApp = "";
            task.rmp.resumeService();
        }
        // play service-level signaling when the presentation time reaches rmpSyncTime
        else if (task.rmp.isSignaling == false && syncTime > 0) {
            task.rmp.nextContentByApp = "";
            task.rmp.syncTime = syncTime;
        }
    }

    /**
     *  ERROR
     *  -32602: Invalid params
     *  -32603: Internal error
     *  -11: The indicated MPD cannot be accessed
     *  -12: The content cannot be played
     *  -13: The requested MPD Anchor cannot be reached
     *  -19: The synchronization specified by rmpSyncTime cannot be achieved
     **/
    if (errorCode != 0) {
        string errMsg = "";
        if (errorCode == -32602)
            errMsg = "Invalid params";
        else if (errorCode == -32603)
            errMsg = "Internal error";

        peer->sendMessage({
            {"jsonrpc", "2.0"},
            {"id", req["id"]},
            {"error", {{"code", errorCode}, {"message", errMsg}}}
        });
    } else {
        peer->sendMessage({
            {"jsonrpc", "2.0"},
            {"result", {}},
            {"id", req["id"]}
        });
    }
}

static void org_atsc_rmpMediaTime (const json &req, const Ref<RpcPeer> peer)
{
    bool bSuccess = false;
    atsc3_RMPMediaTime _mediaTime;
    task.rmp.getRMPMediaTime(&_mediaTime);

    if (_mediaTime.currentTime != -1)
        bSuccess = true;

    json res = json::object();
    res["currentTime"] = ((long)(1000*_mediaTime.currentTime))/1000.000;
    if (_mediaTime.startDateTimeMs != 0)
        res["startDate"] = toDatetime(_mediaTime.startDateTimeMs/1000.000);

    if (bSuccess) {
        peer->sendMessage({
            {"jsonrpc", "2.0"},
            {"id", req["id"]},
            {"result", res}
        });
    } else {
        peer->sendMessage({
            {"jsonrpc", "2.0"},
            {"id", req["id"]},
            {"error", {{"code", -1}, {"message", "error"}}}
        });
    }

}

static void org_atsc_rmpWallClockTime (const json &req, const Ref<RpcPeer> peer)
{
    double now = currentUnixTime();
    json res = json::object();
    res["wallClock"] = toDatetime(now);
    peer->sendMessage({
        {"jsonrpc", "2.0"},
        {"id", req["id"]},
        {"result", res}
    });
}

static void org_atsc_rmpPlaybackState (const json &req, const Ref<RpcPeer> peer)
{
    peer->sendMessage({
        {"jsonrpc", "2.0"},
        {"id", req["id"]},
        {"result", {
            {"playbackState", task.rmp.rmpPlaybackState}}
        }
    });
}

static void org_atsc_rmpPlaybackRate (const json &req, const Ref<RpcPeer> peer)
{
    bool bSuccess = false;
    double playbackRate = task.rmp.getPlaybackRate();

    if (playbackRate != -1)
        bSuccess = true;

    if (bSuccess) {
        peer->sendMessage({
            {"jsonrpc", "2.0"},
            {"id", req["id"]},
            {"result", {
                {"playbackRate", playbackRate}}
            }
        });
    } else {
        peer->sendMessage({
            {"jsonrpc", "2.0"},
            {"id", req["id"]},
            {"error", {{"code", -1}, {"message", "error"}}}
        });
    }
}

static void org_atsc_query_baseURI (const json &req, const Ref<RpcPeer> peer)
{
    if (task.applicationManager.applicationTask &&
        task.applicationManager.applicationTask->contextCache &&
        !task.applicationManager.applicationTask->contextCache->baseUri.empty()) {
        peer->sendMessage({
            {"jsonrpc", "2.0"},
            {"id", req["id"]},
            {"result", {{"baseURI",
                task.applicationManager.applicationTask->contextCache->baseUri}}
            }
        });
    } else {
       peer->sendMessage({
            {"jsonrpc", "2.0"},
            {"id", req["id"]},
            {"error", {{"code", -4}, {"message", "No BaseURI"}}}
        });
    }
}

static void org_atsc_query_deviceInfo (const json &req, const Ref<RpcPeer> peer)
{
    json res = json::object();
    res["deviceMake"] = task.platform->deviceInfo.deviceMake;
    res["deviceModel"] = task.platform->deviceInfo.deviceModel;
    res["deviceId"] = task.platform->deviceInfo.deviceId;
    res["advertisingId"] = task.platform->deviceInfo.advertisingId;

    json input = json::object();
    for (auto kbp: task.applicationManager.keyBindings) {
        auto &kb = kbp.second;
        if (kb.flags & atsc3_KEYFLAG_SYSTEM_ONLY) continue;
        if (kb.infos.size() != 1) continue;
        auto &info0 = *kb.infos[0];
        if (info0.code < 0) continue;
        input[kb.name] = info0.code;
    }
    res["deviceInput"] = input;

    json info = json::object();
    // TODO: implement
    res["deviceInfo"] = info;

    peer->sendMessage({
        {"jsonrpc", "2.0"},
        {"id", req["id"]},
        {"result", res}
    });
}

static void org_atsc_audioVolume (const json &req, const Ref<RpcPeer> peer)
{
    int errorCode = 0;

    double res = 0;
    auto params = JsonRef(req)["params"];
    if (!params.isObject()) errorCode = -32602;

    auto vol = params["audioVolume"];
    double nVol = vol.isNumber()?vol.toF64():-1.0;
    if (nVol <= 1.0 && nVol >= 0.0) {
        res = task.platform->rmpAudioVolume(nVol);
    } else {
        errorCode = -32602;
    }

    if (errorCode == 0) {
        peer->sendMessage({
            {"jsonrpc", "2.0"},
            {"id", req["id"]},
            {"result", {{"audioVolume", res}}}
        });
    } else {
        peer->sendMessage({
            {"jsonrpc", "2.0"},
            {"id", req["id"]},
            {"error", {{"code", errorCode}, {"message", "Invalid Params"}}}
        });
    }
}

struct ContentChangeNotifier: RpcNotificationHandler, Listener<>,
        Listener<const Ref<Package>&> {

    set<RpcPeer*> peers;

    Ref<ApplicationTask> currentApplication;

    bool subscribe (const std::string &type, const json &opt,
            const acba::Ref<RpcPeer> &peer) override {
        peers.insert(peer.get());
        return true;
    }

    bool unsubscribe (const std::string &type, const acba::Ref<RpcPeer> &peer)
            override {
        peers.erase(peer.get());
        return true;
    }

    void onUpdate () override {
        auto &amapp = task.applicationManager.applicationTask;

        if (amapp == currentApplication) return;

        if (currentApplication) {
            currentApplication->contextCache->packageUpdate.removeListener(
                    this);
        }

        currentApplication = amapp;

        if (amapp) amapp->contextCache->packageUpdate.addListener(this);
    }

    void onUpdate (const Ref<Package> &pkg) override {
        json pkgs = json::array();
        pkgs.push_back(pkg->origin->path);

        json msg = {
            {"jsonrpc", "2.0"},
            {"method", "org.atsc.notify"},
            {"params", {
                {"msgType", "contentChange"},
                {"packageList", pkgs}
            }},
        };

        for (auto &p: peers) p->sendMessage(msg);
    }
};

struct MPDChangeNotifier: RefCounter<MPDChangeNotifier>, Listener<>
{
    MPDChangeNotifier (const Ref<RpcPeer> &p, RouteService *rs): peer(p), routeService(rs) {};

    Ref<RpcPeer> peer;
    RouteService *routeService;

    void onUpdate () override {
        json ret = {};
        ret["msgType"] = "MPDChange";
        peer->sendMessage({
            {"jsonrpc", "2.0"},
            {"method", "org.atsc.notify"},
            {"params", ret }
        });
    }

    void dispose () {
        if (routeService)
            routeService->resolvedMpdFileUpdate.removeListener(this);
    }
};

struct MPDChangeNotifierHandler: RpcNotificationHandler
{
    map<RpcPeer*, Ref<MPDChangeNotifier>> mpdChangeNotifiers;

    bool subscribe (const std::string &type, const nlohmann::json &opt,
            const acba::Ref<RpcPeer> &peer) override {

        auto &n = mpdChangeNotifiers[peer.get()];
        if (n) return true;

        auto rsrv = static_cast<RouteService*>(model.currentService.get());
        n = new MPDChangeNotifier(peer, rsrv);
        rsrv->resolvedMpdFileUpdate.addListener(n);
        return true;
    }

    bool unsubscribe (const std::string &type, const acba::Ref<RpcPeer> &peer)
            override {

        auto ni = mpdChangeNotifiers.find(peer.get());
        if (ni == mpdChangeNotifiers.end()) return false;

        auto &n = ni->second;
        n->dispose();
        mpdChangeNotifiers.erase(ni);
        return true;
    }
};

void RpcAtsc3_init ()
{
    web.rpcManager.registerHandler("org.atsc.request.keys",
            org_atsc_request_keys);
    web.rpcManager.registerHandler("org.atsc.relinquish.keys",
            org_atsc_relinquish_keys);
    web.rpcManager.registerHandler("org.atsc.query.deviceInfo", org_atsc_query_deviceInfo);

    static ContentChangeNotifier ccn;
    task.applicationManager.applicationTaskUpdate.addListener(&ccn);

    web.rpcManager.registerNotificationHandler("atsc3:contentChange", &ccn);
    web.rpcManager.registerHandler("org.atsc.query.ratingLevel",
            org_atsc_query_ratingLevel); //9.2.1
    web.rpcManager.registerHandler("org.atsc.query.cc",
            org_atsc_query_cc); // 9.2.2
    web.rpcManager.registerHandler("org.atsc.query.service",
            org_atsc_query_service); // 9.2.3
    web.rpcManager.registerHandler("org.atsc.query.languages",
            org_atsc_query_languages); // 9.2.4
    web.rpcManager.registerHandler("org.atsc.query.captionDisplay",
            org_atsc_query_captionDisplay); // 9.2.5
    web.rpcManager.registerHandler("org.atsc.query.audioAccessibilityPref",
            org_atsc_query_audioAccessibility); // 9.2.6
    web.rpcManager.registerHandler("org.atsc.query.MPDUrl",
            org_atsc_query_MPDUrl); // 9.2.7
    web.rpcManager.registerHandler("org.atsc.query.baseURI",
            org_atsc_query_baseURI); // 9.2.8

    web.rpcManager.registerHandler("org.atsc.acquire.service",
            org_atsc_acquire_service); // 9.7.1
    web.rpcManager.registerHandler("org.atsc.scale-position",
            org_atsc_scale_position); // 9.7.2
    web.rpcManager.registerHandler("org.atsc.setRMPURL",
            org_atsc_setRMPURL); // 9.7.3
    web.rpcManager.registerHandler("org.atsc.query.rmpMediaTime",
            org_atsc_rmpMediaTime); // 9.14.1
    web.rpcManager.registerHandler("org.atsc.query.rmpWallClockTime",
            org_atsc_rmpWallClockTime); // 9.14.2
    web.rpcManager.registerHandler("org.atsc.query.rmpPlaybackState",
            org_atsc_rmpPlaybackState); // 9.14.3
    web.rpcManager.registerHandler("org.atsc.query.rmpPlaybackRate",
            org_atsc_rmpPlaybackRate); // 9.14.4

    web.rpcManager.registerHandler("org.atsc.audioVolume",
            org_atsc_audioVolume); // 9.7.4
    static MPDChangeNotifierHandler mpdcnh;
    web.rpcManager.registerNotificationHandler("atsc3:MPDChange", &mpdcnh);
}
