#include <set>
#include <string>
#include <sstream>

#include "acba.h"
#include "atsc3_model.h"
#include "atsc3_task.h"
#include "file_server.h"

using namespace std;
using namespace acba;
using namespace atsc3;

static Log l("file_server");

struct FileHandler: HttpRequestHandler {

    void handleHttpRequest (HttpReactor &r) override {
        int bsid = -1;
        int sid = -1;

        const char *path = r.requestHandlerPath.c_str();
        l.info("request %s", path);

        // parse bsid/sid
        if (sscanf(path, "/%d/%d/", &bsid, &sid) != 2) {
            r.sendResponse(404, {"Content-Type: text/plain"}, "Invalid Path");
            return;
        }
        char path2[128];
        int len2 = sprintf(path2, "/%d/%d/", bsid, sid);
        if (strncmp(path, path2, len2) != 0) {
            r.sendResponse(404, {"Content-Type: text/plain"}, "Invalid Path2");
            return;
        }
        path += len2;

        // locate service object
        Service *srv = nullptr;
        auto bs = model.broadcastStreams.find(bsid);
        if (bs != model.broadcastStreams.end()) {
            for (auto sg: bs->second->serviceGroups) {
                auto s = sg.second->services.find(sid);
                if (s != sg.second->services.end()) {
                    srv = s->second.get();
                }
            }
        }

        // TODO: support mmt service
        if (!srv || srv->protocol != 1) {
            r.sendResponse(404, {"Content-Type: text/plain"},
                    "Non-existant path");
            return;
        }
        auto rsrv = static_cast<RouteService*>(srv);

        // send list of files for empty path
        if (!path[0]) {
            stringstream ss;
            for (auto s: rsrv->slsFiles) {
                ss << s.first << "\n";
            }
            for (auto f: rsrv->files) ss << f.first << "\n";
            r.sendResponse(200, {"Content-Type: text/plain"}, ss.str());
            return;
        }

        // firstly, check if matching sls file exists
        auto slsFile = rsrv->slsFiles.find(path);
        if (slsFile != rsrv->slsFiles.end()) {
            l.info("sending sls file %s", path);
            File &f = *slsFile->second;
            Buffer &b = f.content;

            // construct http headers
            vector<string> headers;
            if (f.type.empty()) {
                headers.push_back("Content-Type: text/plain");
            } else {
                headers.push_back(string("Content-Type: ") + f.type);
            }

            r.sendHead(200, headers, b.size());
            r.sendBody(b.begin(), b.size());
            r.endResponse();
            return;
        }

        // otherwise, assume that path locates route object file.
        auto fi = rsrv->files.find(path);
        if (fi == rsrv->files.end()) {
            r.sendResponse(404, {"Content-Type: text/plain"}, "File not found");
            return;
        }
        auto &rf = fi->second;
        l.info("sending route file %s(%u.%u) from bsid:%d sid:%d", path,
                rf->tsi, rf->toi, rsrv->bsid, rsrv->id);
        vector<string> headers;
        if (rf->type.empty()) {
            headers.push_back("Content-Type: application/octet-stream");
        } else {
            headers.push_back(string("Content-Type: ") + rf->type);
        }
        Buffer &b = rf->content;
        r.sendHead(200, headers, b.size());
        r.sendBody(b.begin(), b.size());
        r.endResponse();
    }
};

HttpRequestHandler *FileServer::acceptHttpRequest (const string &basePath,
        HttpReactor *r)
{
    if (r->websocketVersion) return 0;
    return new FileHandler();
}
