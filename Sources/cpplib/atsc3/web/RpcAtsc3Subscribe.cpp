#include "acba.h"
#include "rpc_manager.h"
#include "atsc3_web.h"
#include "atsc3_task.h"
#include "nlohmann/json.hpp"
#include "JsonRef.h"

using namespace std;
using namespace acba;
using namespace atsc3;
using json = nlohmann::json;

static Log l("rpc_atsc3_subscribe");

static void org_atsc_subscribe (const json &req, const Ref<RpcPeer> peer)
{
    auto msgTypeArray = JsonRef(req)["params"]["msgType"];
    if (!msgTypeArray.isArray()) {
        peer->sendMessage({
            {"jsonrpc", "2.0"},
            {"id", req["id"]},
            {"error", {
                {"code", -32602}
            }},
        });
        return;
    }

    set<string> mtypesToSubscribe;
    for (int i = 0;; i++) {
        auto mt = msgTypeArray[i];
        if (!mt.isString()) {
            l.warn("msgType contains non-string item");
            break;
        }

        auto &s = mt.asString();
        if (s == "All") {
            for (auto nhi: web.rpcManager.notificationHandlers) {
                auto &k = nhi.first;
                if (k.compare(0, 6, "atsc3:") != 0) continue;
                mtypesToSubscribe.insert(k.substr(6));
            }
        } else {
            mtypesToSubscribe.insert(s);
        }
    }

    auto mtypesSubscribed = json::array();
    for (auto mt: mtypesToSubscribe) {
        string k = string("atsc3:") + mt;
        if (peer->subscribe(k)) mtypesSubscribed.push_back(mt);
    }

    peer->sendMessage({
        {"jsonrpc", "2.0"},
        {"id", req["id"]},
        {"result", {
            {"msgType", mtypesSubscribed}
        }}
    });
}

static void org_atsc_unsubscribe (const json &req,
                                 const Ref<RpcPeer> peer)
{
    auto msgTypeArray = JsonRef(req)["params"]["msgType"];
    if (!msgTypeArray.isArray()) {
        peer->sendMessage({
            {"jsonrpc", "2.0"},
            {"id", req["id"]},
            {"error", {
                {"code", -32602}
            }},
        });
        return;
    }

    set<string> mtypesToUnsubscribe;
    for (int i = 0; ; i++) {
        auto mt = msgTypeArray[i];
        if (!mt.isString()) {
            l.warn("msgType contains non-string item");
            break;
        }

        auto &s = mt.asString();

        if (s == "All") {
            for (auto k: peer->subscriptions) {
                if (k.compare(0, 6, "atsc3:") != 0) continue;
                mtypesToUnsubscribe.insert(k.substr(6));
            }
        } else {
            mtypesToUnsubscribe.insert(s);
        }
    }

    auto mtypesUnsubscribed = json::array();
    for (auto mt: mtypesToUnsubscribe) {
        string k = string("atsc3:") + mt;
        if (peer->unsubscribe(k)) mtypesUnsubscribed.push_back(mt);
    }

    peer->sendMessage({
        {"jsonrpc", "2.0"},
        {"id", req["id"]},
        {"result", {
            {"msgType", mtypesUnsubscribed}
        }}
    });
}

void RpcAtsc3Subscribe_init ()
{
    web.rpcManager.registerHandler("org.atsc.subscribe", org_atsc_subscribe); // 9.7.5.1
    web.rpcManager.registerHandler("org.atsc.unsubscribe", org_atsc_unsubscribe); // 9.7.5.2
}
