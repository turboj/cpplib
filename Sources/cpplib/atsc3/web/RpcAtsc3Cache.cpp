#include <sys/stat.h>
#include <sys/types.h>

#include "acba.h"
#include "rpc_manager.h"
#include "atsc3_web.h"
#include "atsc3_task.h"
#include "atsc3_model.h"
#include "nlohmann/json.hpp"
#include "JsonRef.h"
#include "dnld.h"

using namespace std;
using namespace acba;
using namespace atsc3;
using json = nlohmann::json;

static Log l("rpc_atsc3_cache");

struct CacheProcessor: RefCounter<CacheProcessor> {

    int req_id;
    Ref<RpcPeer> peer;

    struct MpdDownloader: HttpDownloadCompletionHandler, acba::Reactor {

        string sourceUrl;
        string targetUrl;
        string url;
        Ref<CacheProcessor> cachePs;
        Ref<ApplicationContextCache> ctxCache;
        Ref<Download> dnld;
        Ref<File> mpd;
        Log l;

        MpdDownloader(const string &s_url, const string &t_url,
                const string &f_url, const Ref<CacheProcessor> &p,
                const Ref<ApplicationContextCache> &ctx) : sourceUrl(s_url),
                targetUrl(t_url), url(f_url), cachePs(p), ctxCache(ctx),
                l("mpdDl", "["+f_url+"]") {}

        void start() {
            mpd = new File();
            HttpDownloadRequest req;
            req.url = sourceUrl+url;
            req.bodyBuffer = &(mpd->content);
            req.completionHandler = this;
            l.info("startDownload : %s", req.url.c_str());
            dnld = task.downloadManager.download(req);
        }

        void abort() {
            if (dnld) dnld->abort();
        }

        void onCompletion(HttpDownloadResponse &resp) override {
            l.info("status:%d", resp.status());
            if (resp.status() != 200 ){
                cachePs->sendMessage(-18);
                return;
            }

            int len = resp.bodyLength();
            l.info("length:%d", len);
            assert(len == mpd->content.size());
            ref();
            task.eventDriver.setTimeout(0, this);
        }

        void onError(int ec) override {
            l.error("Failed to download! ec:%d", ec);
            cachePs->sendMessage(-18);
        }

        void processEvent(int evt) override {
            l.info("processEvent called");
            if (!ctxCache) return;
            string dst = targetUrl + url;
            l.info("processEvent : store to %s", dst.c_str());
            ctxCache->files[dst] = mpd;

            CacheRequestInfo *reqInfo = new CacheRequestInfo(sourceUrl, targetUrl,
                    url, true);
            cachePs->sendMessage(task.applicationManager.processCache(*reqInfo));
            deref();
        }
    };

    void sendMessage(int result) {
        // 0=not cached, 1=cached, otherwise error

        l.info("org.atsc.CacheRequest(DASH) : sendMessage(req_id=%d)", req_id);
        json cached = true;
        switch (result) {

        case 0 :
            cached = false;
        case 1 :
            peer->sendMessage({
                {"jsonrpc", "2.0"},
                {"result", {
                    {"cached", cached},
                }},
                {"id", req_id}
            });
            break;

        case -15 :
            peer->sendMessage({
                {"jsonrpc", "2.0"},
                {"error", {
                    {"code", result},
                    {"message", "invalid sourceURL or targetURL"}
                }},
                {"id", req_id}
            });
            break;

        case -16 :
            peer->sendMessage({
                {"jsonrpc", "2.0"},
                {"error", {
                    {"code", result},
                    {"message", "invalid URLs"}
                }},
                {"id", req_id}
            });
            break;

        case -17 :
            peer->sendMessage({
                {"jsonrpc", "2.0"},
                {"error", {
                    {"code", result},
                    {"message", "invalid DASH Period"}
                }},
                {"id", req_id}
            });
            break;

        case -18 :
            peer->sendMessage({
                {"jsonrpc", "2.0"},
                {"error", {
                    {"code", result},
                    {"message", "MPD not found"}
                }},
                {"id", req_id}
            });
            break;

        defult :
            peer->sendMessage({
                {"jsonrpc", "2.0"},
                {"error", {{"code", result}}},
                {"id", req_id}
            });
            break;
        }

    }

    void process_cache(const json &req, const Ref<RpcPeer> peer) {
//sendMessage 0(not cached), 1(cached), -15(sourceURL/targetURL error), or -16(URLs error)
        req_id = req["id"];
        this->peer = peer;

        auto params = JsonRef(req)["params"];
        if (!params.isObject()) {
            l.warn("org.atsc.CacheRequest: params not structured");
            sendMessage(-15);
            return;
        }

        string targetURL;
        auto jTargetUrl = params["targetURL"];
        if (jTargetUrl.isString()) {
            targetURL = jTargetUrl.asString();
            if (targetURL.size() > 0 && targetURL.back() != '/')
                targetURL.push_back('/');
        }
        l.info("org.atsc.CacheRequest : targetURL=%s", targetURL.c_str());

        auto jUrls = params["URLs"];
        if (!jUrls.isArray()) {
            l.warn("org.atsc.CacheRequest : can't get URLs");
            sendMessage(-16);
            return;
        }
        vector<string> flist;
        for (int i = 0; i < jUrls.length(); i++) {
            auto jUrl = jUrls[i];
            if (!jUrl.isString()) {
                l.warn("org.atsc.CacheRequest : URLs parse error");
                sendMessage(-16);
                return;
            }
            flist.push_back(jUrl.asString());
        }

        string sourceURL;
        auto jSourceUrl = params["sourceURL"];
        if (jSourceUrl.isString()) { // download
            sourceURL = jSourceUrl.asString();
            if (sourceURL.back() != '/') sourceURL.push_back('/');
        }
        l.info("org.atsc.CacheRequest : sourceURL=%s", sourceURL.c_str());

        CacheRequestInfo *reqInfo = new CacheRequestInfo(sourceURL, targetURL,
                flist);
        sendMessage(task.applicationManager.processCache(*reqInfo));
    }

    void process_cacheDash(const json &req, const Ref<RpcPeer> peer) {
//sendMessage 0(not cached), 1(cached), -15(sourceURL/targetURL error), -17(Period error), or -18(MPD not found)
        req_id = req["id"];
        this->peer = peer;

        auto params = JsonRef(req)["params"];
        if (!params.isObject()) {
            l.warn("org.atsc.CacheRequestDASH : params not structured");
            sendMessage(-15);
            return;
        }

        string targetURL;
        auto jTargetUrl = params["targetURL"];
        if (jTargetUrl.isString()) {
            targetURL = jTargetUrl.asString();
            if (targetURL.back() != '/') targetURL.push_back('/');
        }
        l.info("org.atsc.CacheRequestDASH : targetURL=%s", targetURL.c_str());

        string sourceURL;
        auto jSourceUrl = params["sourceURL"];
        if (jSourceUrl.isString()) {
            sourceURL = jSourceUrl.asString();
            if (sourceURL.back() != '/') sourceURL.push_back('/');
        }
        l.info("org.atsc.CacheRequestDASH : sourceURL=%s", sourceURL.c_str());

        // parse media file list from Period or MPD
        auto period = params["Period"];
        auto jMpdFileName = params["mpdFileName"];
        if (period.isString()) {
            CacheRequestInfo *reqInfo = new CacheRequestInfo(sourceURL,
                    targetURL, period.asString(), false);
            sendMessage(task.applicationManager.processCache(*reqInfo));
        } else if (jMpdFileName.isString()) {
            auto &mpdPath = jMpdFileName.asString();
            if (sourceURL == "") {
                CacheRequestInfo *reqInfo = new CacheRequestInfo(sourceURL,
                    targetURL, mpdPath, true);
                sendMessage(task.applicationManager.processCache(*reqInfo));
            } else {
                if (!task.applicationManager.applicationTask
                        || !task.applicationManager.applicationTask->contextCache) {
                    l.warn("org.atsc.CacheRequestDASH : App cache ctx not avail");
                    sendMessage(0);
                    return;
                }
                // continue after downloading MPD
                Ref<MpdDownloader> md = new MpdDownloader(sourceURL,
                        targetURL, mpdPath, this,
                        task.applicationManager.applicationTask->contextCache);
                md->start();
                return;
            }
        } else {
            l.warn("org.atsc.CacheRequestDASH : no Period/MPD avail");
            sendMessage(-17);
            return;
        }
    }
};

static void request_cache (const json &req, const Ref<RpcPeer> peer) {

    l.info("org.atsc.CacheRequest called");

    Ref<CacheProcessor> proc = new CacheProcessor();
    proc->process_cache(req, peer);
}

static void request_cacheDash(const json &req, const Ref<RpcPeer> peer) {
    l.info("org.atsc.CacheRequestDASH called");
    Ref<CacheProcessor> proc = new CacheProcessor();
    proc->process_cacheDash(req, peer);
}

void RpcAtsc3Cache_init ()
{
    web.rpcManager.registerHandler("org.atsc.CacheRequest", request_cache);
    web.rpcManager.registerHandler("org.atsc.CacheRequestDASH", request_cacheDash);
}
