#ifndef ATSC3_RPC_MANAGER_H
#define ATSC3_RPC_MANAGER_H

#include <map>
#include <set>
#include <string>
#include <functional>

#include "http_server.h"
#include "nlohmann/json.hpp"
#include "EventDriver.h"
#include "atsc3_model.h"

namespace atsc3 {

struct RpcNotificationHandler;

struct RpcPeer: acba::RefCounter<RpcPeer> {

    RpcPeer (acba::HttpRequestHandler *h, acba::HttpReactor *r):
            webSocketHandler(h), webSocket(r), privileged(false) {}
    virtual ~RpcPeer () {}

    bool privileged;
    acba::Ref<acba::HttpRequestHandler> webSocketHandler;
    acba::HttpReactor *webSocket;
    acba::Update<RpcPeer&> webSocketUpdate;
    acba::Ref<ApplicationContextCache> applicationContextCache;

    bool sendMessage (const nlohmann::json &msg);

    std::set<std::string> subscriptions;
    // Returns true on success
    bool subscribe (const std::string &evt, const nlohmann::json &opt = nullptr);
    // Returns true if fully unsubscribed.
    bool unsubscribe (const std::string &evt,
            const nlohmann::json &opt = nullptr);

    enum REQUEST_STATUS {
        REQUEST_CANCELABLE,
        REQUEST_UNCANCELABLE,
        REQUEST_CANCELED
    };

    std::map<int, REQUEST_STATUS> requests;

    // Register a on-going request by id.
    // If the request already exist, it returns false.
    // If the request is set cancelable, it can be canceled via cancelRequest().
    // A request registered with this method must be removed by removeRequest().
    bool addRequest (int reqId, bool cancelable = false);

    // Check if given request is still valid (not canceled).
    // Otherwise return false.
    // If the request was previously cancelable and the given @cancelable
    // false, the request will be set uncancelable.
    // If the request was previously uncancelable, the argument @cancelable
    // is ignored. (No need to call this method either anyway).
    bool continueRequest (int reqId, bool cancelable = false);

    // Set the status of the given request as canceled if eligible.
    // Returns true if the request was successfully conceled or alreay cancled.
    bool cancelRequest (int reqId);

    // Remove previously registerd request.
    // Returns true if the given request was valid and not yet canceled.
    bool removeRequest (int reqId);

    // Retrieve all request ids currently added.
    void getRequests (std::set<int> &reqs);

    // Check if given request is known.
    bool isKnownRequest (int reqId);

    void dispose ();
};

typedef std::function<void(const nlohmann::json&, const acba::Ref<RpcPeer>)> RpcHandler;

struct RpcNotificationHandler {
    // Returns true on success
    virtual bool subscribe (const std::string &type, const nlohmann::json &opt,
            const acba::Ref<RpcPeer> &peer) {
        // default to simpler version
        return subscribe(type, peer);
    }
    virtual bool subscribe (const std::string &type,
            const acba::Ref<RpcPeer> &peer) {
        return false;
    }

    // Returns true if fully unsubscribed.
    virtual bool unsubscribe (const std::string &type,
            const nlohmann::json &opt, const acba::Ref<RpcPeer> &peer) {
        // default to simpler version
        return unsubscribe(type, peer);
    }
    virtual bool unsubscribe (const std::string &type,
            const acba::Ref<RpcPeer> &peer) {
        return false;
    }
};

struct RpcManager: acba::HttpRequestAcceptor {
    void init ();
    acba::HttpRequestHandler *acceptHttpRequest (const std::string &basePath,
            acba::HttpReactor *r) override;
    void registerHandler (const std::string &name, RpcHandler h);
    void registerNotificationHandler (const std::string &name,
            RpcNotificationHandler *h);

    std::map<const std::string, RpcHandler> rpcHandlers;
    std::map<const std::string, RpcNotificationHandler*> notificationHandlers;
};

}
#endif
