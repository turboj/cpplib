#include <assert.h>
#include <set>
#include <string>
#include <sstream>
#include <algorithm>

#include "acba.h"
#include "atsc3_web.h"
#include "rpc_manager.h"
#include "nlohmann/json.hpp"

using namespace std;
using namespace acba;
using namespace atsc3;
using json = nlohmann::json;

static Log l("rpcpeer");

bool RpcPeer::sendMessage (const json &msg) {
    if (!webSocket) return false;

    try {
        webSocket->sendFrame(msg.dump(4));
    } catch (...) {
        l.error("Failed to deserialize json message (not utf-8 encoded)");
        return false;
    }
    return true;
}

bool RpcPeer::subscribe (const string &evt, const json &opt)
{
    auto &nhs = web.rpcManager.notificationHandlers;
    auto nhi = nhs.find(evt);
    if (nhi == nhs.end()) {
        l.warn("Unknown notificaton type %s", evt.c_str());
        return false;
    }

    auto &nh = nhi->second;
    auto ok = nh->subscribe(evt, opt, this);
    if (ok) subscriptions.insert(evt);
    return ok;
}

bool RpcPeer::unsubscribe (const string &evt, const json &opt)
{
    if (subscriptions.find(evt) == subscriptions.end()) {
        l.info("Never subscribed for %s", evt.c_str());
        return false;
    }

    auto &nhs = web.rpcManager.notificationHandlers;
    auto nhi = nhs.find(evt);
    assert(nhi != nhs.end());
    auto &nh = nhi->second;

    auto ok = nh->unsubscribe(evt, opt, this);
    if (ok) subscriptions.erase(evt);
    return ok;
}

bool RpcPeer::addRequest (int reqId, bool cancelable)
{
    auto ri = requests.find(reqId);
    if (ri != requests.end()) {
        l.warn("Conflicting long request id:%d", reqId);
        return false;
    }

    requests[reqId] = cancelable ? REQUEST_CANCELABLE : REQUEST_UNCANCELABLE;
    return true;
}

bool RpcPeer::continueRequest (int reqId, bool cancelable)
{
    auto ri = requests.find(reqId);
    if (ri == requests.end()) {
        l.warn("continueRequest() got unknown reqest id:%d", reqId);
        return false;
    }
    auto &st = ri->second;

    if (st == REQUEST_CANCELABLE && !cancelable) st = REQUEST_UNCANCELABLE;
    return st != REQUEST_CANCELED;
}

bool RpcPeer::cancelRequest (int reqId)
{
    auto ri = requests.find(reqId);
    if (ri == requests.end()) {
        l.warn("cancelRequest() got unknown request id:%d", reqId);
        return false;
    }
    auto &st = ri->second;

    if (st == REQUEST_CANCELABLE) st = REQUEST_CANCELED;
    return st == REQUEST_CANCELED;
}

bool RpcPeer::removeRequest (int reqId)
{
    auto ri = requests.find(reqId);
    if (ri == requests.end()) {
        l.warn("removeRequest() got unknown request id:%d", reqId);
        return false;
    }
    auto st = ri->second;
    requests.erase(ri);

    return st != REQUEST_CANCELED;
}

void RpcPeer::getRequests (set<int> &reqs)
{
    for (auto ri: requests) reqs.insert(ri.first);
}

bool RpcPeer::isKnownRequest (int reqId)
{
    return requests.count(reqId) == 1;
}

void RpcPeer::dispose ()
{
    if (!webSocket) return;

    // flush subscriptions
    auto ss = subscriptions;
    for (auto s: ss) {
        l.info("Unsubscribing %s on disconnect", s.c_str());
        unsubscribe(s);
    }
    assert(subscriptions.empty());

    // try to cancel remaining out-standing requests.
    for (auto &ri: requests) cancelRequest(ri.first);

    webSocket = nullptr;
    webSocketUpdate.notify(*this);
}
