#include <iostream>

#include "acba.h"
#include "rpc_manager.h"
#include "atsc3_web.h"
#include "atsc3_task.h"
#include "nlohmann/json.hpp"
#include "JsonRef.h"

using namespace std;
using namespace acba;
using namespace atsc3;
using json = nlohmann::json;

static Log l("rpc_atsc3_drm");

struct DrmNotifier: RefCounter<DrmNotifier>,Listener<string, json&> {
    DrmNotifier (const Ref<RpcPeer> &p): peer(p) {}
    Ref<RpcPeer> peer;

    void onUpdate (string systemId, json& message) override {

        l.info("onUpdate for DrmNotifier");

        json ret = {};
        ret["msgType"] = "DRM";
        ret["systemId"] = systemId;
        ret["message"] = message;

        peer->sendMessage({
            {"jsonrpc", "2.0"},
            {"method", "org.atsc.notify"},
            {"params", ret }
        });
    }

    void dispose() {
        task.rmp.drmInfoUpdate.removeListener(this);
        l.info("DrmNotifier dispose()");
    }
};
struct DrmNotifierHandler: RpcNotificationHandler {
    map<RpcPeer*, Ref<DrmNotifier>> drmNotifiers;
    bool subscribe (const std::string &type, const nlohmann::json &opt,
            const acba::Ref<RpcPeer> &peer) override {

        auto &n = drmNotifiers[peer.get()];
        if (n) return true;

        n = new DrmNotifier(peer);
        task.rmp.drmInfoUpdate.addListener(n.get());
        return true;
    }

    bool unsubscribe (const std::string &type, const acba::Ref<RpcPeer> &peer)
            override {
        auto ni = drmNotifiers.find(peer.get());
        if (ni == drmNotifiers.end()) return false;

        auto &n = ni->second;
        n->dispose();
        return true;
    }
};

static void org_atsc_drmOperation (const json &req, const Ref<RpcPeer> peer)
{
    int errorCode = 0;
    auto params = JsonRef(req)["params"];
    if (!params.isObject()) {
        errorCode = -32602;
    }
    auto systemId = params["systemId"];
    string strSystemId;
    if (systemId.isString()) {
        strSystemId = systemId.asString();
    } else {
        errorCode = -32602;
    }

    if ("urn:uuid:edef8ba9-79d6-4ace-a3c8-27dcd51d21ed" != strSystemId &&
        "urn:uuid:9a04f079-9840-4286-ab92-e65be0885f95" != strSystemId &&
        "urn:uuid:1077efec-c0b2-4d02-ace3-3c1e52e2fb4b" != strSystemId) {
        errorCode = -14;
    }

    if (errorCode != 0) {
        l.warn("DRM Operation error ");
        string errMsg = "";
        if (errorCode == -32602)
            errMsg = "Invalid params";
        else if (errorCode == -32603)
            errMsg = "Internal error";
        else if (errorCode == -14)
            errMsg = "The specified content protection system is not supported by the Receiver";

        peer->sendMessage({
            {"jsonrpc", "2.0"},
            {"id", req["id"]},
            {"error", {{"code", errorCode}, {"message", errMsg}}}
        });

    } else {
        // TODO various cases for drmOperation
        // Currently support only exchange License from Broadcast App.
        // json operation = params["message"]["operation"];
        string message = params["message"].asString();
        //if (operation.is_null()) {
            task.rmp.receivedLicenseResponse(strSystemId.c_str(),
                    message.c_str());
            peer->sendMessage({
                {"jsonrpc", "2.0"},
                {"result", {}},
                {"id", req["id"]}
            });
        /*} else {
           peer->sendMessage({
                {"jsonrpc", "2.0"},
                {"error", {{"code", 32601}, {"message", "The operation is not available."}}},
                {"id", req["id"]}
            });
        }
        */
    }
}

void RpcAtsc3Drm_init ()
{
    web.rpcManager.registerHandler("org.atsc.drmOperation",
        org_atsc_drmOperation);

    static DrmNotifierHandler dnh;
    web.rpcManager.registerNotificationHandler("atsc3:DRM", &dnh);

}
