# Pseudo target including all atsc3 modules
$(call begin-target, atsc3, staticlib)
    LOCAL_SRCS := $(wildcard $(LOCAL_PATH)/*.cpp)
    LOCAL_IMPORTS := atsc3_model-staticlib atsc3_task-staticlib
    LOCAL_IMPORTS += atsc3_web-staticlib atsc3_platform_interface

    EXPORT_IMPORTS := $(LOCAL_IMPORTS)
$(end-target)
