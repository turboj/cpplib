#include "atsc3_model.h"
#include "atsc3_task.h"
#include "atsc3_platform.h"
#include "atsc3_web.h"
#include "ip_reader.h"
#include "acba.h"
#include "cd_manager.h"

// Some test application uses wrong websocket path
// not having atscCmd at the end. Accept reluctantly to proceed the tests.
#define ALLOW_WEBSOCKET_WITHOUT_ATSCMD 1

using namespace acba;

static Log l("init");

bool atsc3::init (int &argc, char **&argv, const atsc3_Platform *p)
{
    l.info("Wiring components...");
    task.platform = p;
    task.tuneManager.init();
    model.knownStreamLocationsUpdate.addListener(&task.tuneManager.discovery);
    model.currentServiceUpdate.addListener(&task.applicationManager.selection);
    model.currentServiceUpdate.addListener(&task.serviceManager.selector);
    model.currentServiceUpdate.addListener(&task.serviceManager.alertFlusher);
    model.currentServiceUpdate.addListener(
            &task.ratingHandler.serviceChangeListener);
    model.esgFragmentUpdate.addListener(
            &task.ratingHandler.esgUpdateListener);
    model.config.update.addListener(&model.preference);
    model.preference.update.addListener(
            &task.ratingHandler.preferenceChangeListener);
    model.backgroundServicesUpdate.addListener(&task.serviceManager.selector);
    model.serviceUpdate.addListener(&task.serviceManager.selector);
    task.rmp.rmpPeriodChanged.addListener(&task.ratingHandler.rmpPeriodChangeListener);
    model.preference.update.addListener(&task.rmp.prefUpdateListener);

    task.eventDriver.setTimeout(0, &task.serviceManager.trimmer);
    task.tuneManager.join(0, ip2u("224.0.23.60"), 4937,
            &task.serviceManager.llsReader);

    l.info("Setting web server...");
    web.httpServer.start(8080, &task.eventDriver);
    web.rpcManager.init();
    web.httpServer.bind("/atscCmd", &web.rpcManager);
#if ALLOW_WEBSOCKET_WITHOUT_ATSCMD
    web.httpServer.bind("", &web.rpcManager);
#endif
    web.httpServer.bind("/file", &web.fileServer);
    web.httpServer.bind("/dash", &web.dashStreamer);
    web.httpServer.bind("/mpu", &web.mpuStreamer); // fix path
    web.httpServer.bind("/app", &web.appServer);

    web.httpServer.bind(CD_WEB_CONTEXT_ROOT, &web.cdServer);
    web.httpServer.bind(ATSC_CD, &web.cdServer);
    web.httpServer.bind(CD_REMOTE_WS_ENDPOINT, &web.cdServer);
    web.httpServer.bind(CD_LOCAL_WS_ENDPOINT, &web.cdServer);

    l.info("ApplicationManager.init()");
    task.applicationManager.init();
    task.downloadManager.start();

    CdManager::getInstance()->start();

    task.rmp.initPreference();
    l.info("done");
    return true;
}
