$(call begin-target, atsc3_model, staticlib)
    LOCAL_SRCS := $(wildcard $(LOCAL_PATH)/*.cpp)
    LOCAL_IMPORTS := readstream-staticlib pugixml-staticlib

    EXPORT_CXXFLAGS := -I$(LOCAL_PATH)
    EXPORT_IMPORTS := $(LOCAL_IMPORTS)
$(end-target)
