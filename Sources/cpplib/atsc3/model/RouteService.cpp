#include <sys/stat.h>
#include <sys/types.h>

#include <string>
#include <sstream>

#include "atsc3_model.h"
#include "acba.h"
#include "path_util.h"

using namespace std;
using namespace acba;
using namespace atsc3;

static Log l("rsrv");

void RouteService::dump ()
{
    l.info("Service id:%d globalId:%s ver:%d proto:%d cat:%d num_sessions:%d",
            id, globalId.c_str(), version, protocol, category, sessions.size());
    for (auto &lang: languages) {
        l.info("    lang: %s", lang.c_str());
    }
    for (auto &n: names) {
        l.info("    name(%s): %s", n.first.c_str(), n.second.c_str());
    }

    for (auto &f: slsFiles) {
        stringstream ss;
        ss << model.storageRoot << "/dump/sls-";
        if (globalId.empty()) {
            ss << bsid << '.' << id;
        } else {
            ss << escapeSlash(globalId);
        }
        ss << '/' << f.first;
        string path = ss.str();

        makeBaseDir(path.c_str());
        FILE *fp = fopen(path.c_str(), "w");
        if (fp) {
            Buffer &b = f.second->content;
            fwrite(b.begin(), 1, b.size(), fp);
            fclose(fp);
        }

        l.info("    sls %s", f.first.c_str());
    }

    for (auto &f: files) {
        stringstream ss;
        ss << model.storageRoot << "/dump/route-" << bsid << '.' << id;
        ss << '/' << f.first;
        string path = ss.str();

        makeBaseDir(path.c_str());
        FILE *fp = fopen(path.c_str(), "w");
        if (fp) {
            Buffer &b = f.second->content;
            fwrite(b.begin(), 1, b.size(), fp);
            fclose(fp);
        }

        l.info("    file %s", f.first.c_str());
    }

    for (auto &rs: sessions) rs->dump();
}
