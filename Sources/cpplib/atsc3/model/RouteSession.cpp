#include <sys/stat.h>
#include <sys/types.h>

#include <sstream>

#include "ip_reader.h"
#include "udp_reader.h"
#include "atsc3_model.h"
#include "mime_reader.h"
#include "datetime.h"
#include "pugixml.hpp"
#include "acba.h"

using namespace std;
using namespace acba;
using namespace pugi;
using namespace atsc3;

static Log l("rs");

void RouteSession::dump ()
{
    l.info("RouteSession srcip:%d.%d.%d.%d dstip:%d.%d.%d.%d dstport:%d",
            (sourceIp >> 24) & 0xff,
            (sourceIp >> 16) & 0xff,
            (sourceIp >> 8) & 0xff,
            (sourceIp >> 0) & 0xff,
            (destinationIp >> 24) & 0xff,
            (destinationIp >> 16) & 0xff,
            (destinationIp >> 8) & 0xff,
            (destinationIp >> 0) & 0xff,
            destinationPort);
}

const char *RouteSession::findRepresentationId (uint32_t tsi)
{
    // Find LS corresponding to tsi (se.first)
    for (auto ls: xmlRs.children("LS")) {
        if (ls.attribute("tsi").as_int() != tsi) continue;

        const char *repId = ls.child("SrcFlow").child("ContentInfo").
                child("MediaInfo").attribute("repId").value();
        if (!repId[0]) {
            l.info("Couldn't find representation id from LS element");
            return 0;
        } else {
            return repId;
        }
    }

    l.info("Couldn't find LS element matching with given tsi:%d", tsi);
    return 0;
}
