#include <unistd.h>
#include <sys/stat.h>
#include <sys/types.h>

#include <string>
#include <sstream>

#include "atsc3_model.h"
#include "acba.h"

#include "path_util.h"

using namespace std;
using namespace acba;
using namespace atsc3;

static Log l("msrv");

void MmtpService::dump ()
{
    l.info("MmtpService id:%d globalId:%s ver:%d proto:%d cat: %d "
            "num_sessions:%d", id, globalId.c_str(), version, protocol,
            category, sessions.size());

    for (auto msg: messages) {
        stringstream ss;
        ss << model.storageRoot << "/dump/sls-" << escapeSlash(globalId);
        ss << "/message:" << (msg.first >> 8) << '-' << (msg.first & 0xff);
        string path = ss.str();

        makeBaseDir(path.c_str());
        FILE *fp = fopen(path.c_str(), "w");
        if (fp) {
            Buffer &b = msg.second->content;
            fwrite(b.begin(), 1, b.size(), fp);
            fclose(fp);
        }

        l.info("    sls message %04x", msg.first);
    }

    for (auto msg: ma3Messages) {
        stringstream ss;
        ss << model.storageRoot << "/dump/sls-" << escapeSlash(globalId);
        ss << "/ma3:" << msg.first;
        string path = ss.str();

        makeBaseDir(path.c_str());
        FILE *fp = fopen(path.c_str(), "w");
        if (fp) {
            Buffer &b = msg.second->content;
            fwrite(b.begin(), 1, b.size(), fp);
            fclose(fp);
        }

        l.info("    sls ma3 message %u", msg.first);
    }

    if (currentPackage) {
        l.info("PACKAGE %s lastMpTabldId:%d mpTableVersion:%d numAssets:%d",
                currentPackage->id.c_str(),
                currentPackage->lastMpTableId,
                currentPackage->mpTableVersion,
                currentPackage->assets.size());
        for (auto asset: currentPackage->assets) {
            l.info("    ASSET %s type:%s isDefault:%d packetId:%u",
                    asset.first.c_str(),
                    asset.second->type.c_str(),
                    asset.second->isDefault,
                    asset.second->packetId);
            for (auto ts: asset.second->timestamps) {
                l.info("        TIMESTAMP %u -> %llu", ts.first, ts.second);
            }
        }
    }

    for (auto ms: sessions) {
        ms->dump();
    }

    for (auto mi: mpus) {
        auto &mpu = mi.second;

        stringstream ss;
        ss << model.storageRoot << "/dump/mmtp-" << bsid << ':' << id;
        ss << '/' << mpu->packetId << '.' << mpu->sequenceNumber;
        if (mpu->flags != Mpu::FLAGS_ALL) ss << "-partial";
        string path = ss.str();

        makeBaseDir(path.c_str());
        FILE *fp = fopen(path.c_str(), "w");
        if (fp) {
            auto b = &mpu->mpuMetadata;
            fwrite(b->begin(), 1, b->size(), fp);
            b = &mpu->movieFragmentMetadata;
            fwrite(b->begin(), 1, b->size(), fp);
            b = &mpu->mediaData;
            fwrite(b->begin(), 1, b->size(), fp);
            fclose(fp);

            // Add zeros in place of MMTHSample track
            if (mpu->flags == Mpu::FLAGS_ALL) {
                ::truncate(path.c_str(), mpu->mpuMetadata.size() +
                        mpu->movieFragmentMetadata.size() + mpu->mdatSize);
            }
        }
    }
}

int MmtpService::getDefaultVideoPacketId () {
    if (!currentPackage) return -1;

    int pktId = -1;
    for (auto asseti: currentPackage->assets) {
        static vector<string> vcodecs = {
                "avc1", "avc2", "avc3", "avc4", "hev1", "hev2", "hvc1",
                "hvc2", "hvt1", "lhe1", "lht1", "lhv1" , "mp4v"};
        Asset &asset = *asseti.second;
        auto i = find(vcodecs.begin(), vcodecs.end(), asset.type);
        if (i != vcodecs.end() && asset.packetId != -1) {
            if (pktId == -1 || asset.isDefault) {
                pktId = asset.packetId;
            }
        }
    }

    return pktId;
}

int MmtpService::getDefaultAudioPacketId () {
    if (!currentPackage) return -1;

    int pktId = -1;
    for (auto asseti: currentPackage->assets) {
        static vector<string> acodecs = {
            "ac-3", "ac-4", "alac", "alaw", "dra1", "mp4a"
            , "ec-3", "m4ae", "mha1", "mha2", "mhm1", "mhm2"
        };
        Asset &asset = *asseti.second;
        auto i = find(acodecs.begin(), acodecs.end(), asset.type);
        if (i != acodecs.end() && asset.packetId != -1) {
            if (pktId == -1 || asset.isDefault) {
                pktId = asset.packetId;
            }
        }
    }

    return pktId;
}

int MmtpService::getDefaultTextPacketId () {
    if (!currentPackage) return -1;

    int pktId = -1;
    for (auto asseti: currentPackage->assets) {
        static vector<string> acodecs = {
            "stpp"
        };
        Asset &asset = *asseti.second;
        auto i = find(acodecs.begin(), acodecs.end(), asset.type);
        if (i != acodecs.end() && asset.packetId != -1) {
            if (pktId == -1 || asset.isDefault) {
                pktId = asset.packetId;
            }
        }
    }

    return pktId;
}
