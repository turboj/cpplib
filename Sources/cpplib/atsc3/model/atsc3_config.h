#ifndef ATSC3_CONFIG_H
#define ATSC3_CONFIG_H

#include <string>

#include "acba.h"

using namespace std;

namespace atsc3 {

struct Config {

    // 0: success
    int get (const string &key, string &value);

    // 0: success
    int set (const string &key, const string &value);

    int remove (const string &key);

    acba::Update<const string&> update;
};

struct Preference: acba::Listener<const string&> {
    // provide default
    // use different key name space
    // may get default values from platform (WISHLIST)
    // use Settings for overriding the default value.
    // may limit available keys.

    /**
     * Available keys are:
     * "preferredAudioLang", "preferredUiLang", "preferredCaptionSubtitleLang",
     * "rating", "videoDescriptionService", "audioEIService",
     * "captionDisplay",  "ccEnabled"
     *
     * returns 0 on success
     */
    int get (const string &key, string &value);

    // 0: success
    int set (const string &key, const string &value);

    acba::Update<const string&> update;

    // Send update notification for preference values
    void onUpdate (const string &) override;
};

}
#endif
