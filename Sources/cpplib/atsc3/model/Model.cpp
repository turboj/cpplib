#include "atsc3_model.h"
#include "acba.h"

using namespace atsc3;
using namespace acba;

atsc3::Model atsc3::model;

static Log l("model");

static const char *bar = "================================================================================\n";

void Model::dump ()
{
    l.info("");
    l.info("%s", bar);
    l.info("== DUMP KNOWN STREAMS");
    l.info("%s", bar);
    for (auto loc: knownStreamLocations) {
        l.info("Stream %s", loc.c_str());
    }

    l.info("");
    l.info("%s", bar);
    l.info("== DUMP BROADCAST STREAMS");
    l.info("%s", bar);
    for (auto bs: broadcastStreams) {
        bs.second->dump();
    }

    l.info("");
    l.info("%s", bar);
    l.info("== DUMP APPLICATION CONTEXT CACHES");
    l.info("%s", bar);
    for (auto acc: applicationContextCaches) {
        acc.second->dump();
    }

    l.info("");
    l.info("%s", bar);
    l.info("== ETc.");
    l.info("%s", bar);
    if (currentService) {
        l.info("Currently Selected Service:%s",
                currentService->xml.attribute("shortServiceName").value());
    } else {
        l.info("Currently no selected service");
    }
}

bool Model::selectService (Service *srv)
{
    if (model.currentService.get() == srv) return true;

    if (srv) {
        // TODO: check validity of the given service
        l.info("Select service %s (%d/%d)", srv->globalId.c_str(),
                srv->bsid, srv->id);
    } else {
        l.info("Unselect service %s (%d/%d)",
                model.currentService->globalId.c_str(),
                model.currentService->bsid,
                model.currentService->id);
    }
    model.currentService = srv;
    model.currentServiceUpdate.notify();
    return true;
}
