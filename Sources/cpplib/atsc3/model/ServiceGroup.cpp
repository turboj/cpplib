#include <sys/stat.h>

#include <sstream>

#include "pugixml.hpp"
#include "atsc3_model.h"
#include "ip_reader.h"
#include "gzip_reader.h"
#include "acba.h"
#include "path_util.h"

using namespace std;
using namespace atsc3;
using namespace acba;
using namespace pugi;

static Log l("srvgrp");

void ServiceGroup::dump ()
{
    l.info("Service Group group_id:%d num_services:%d", id, services.size());

    for (auto s: services) {
        s.second->dump();
    }

    static const map<int, const char*> tblnames {
        {1, "slt"},
        {2, "rrt"},
        {3, "systemtime"},
        {4, "aeat"},
        {5, "onscreenmessagenotification"},
        {6, "security"},
        {254, "signedmultitable"},
        {255, "userdefined"}
    };

    for (auto lfi: llsFiles) {
        auto &lf = lfi.second;

        // unzip
        Buffer *buf = &lf->content;
        GzipReader gr;
        if (lf->attributes["compressed"].asBool()) {
            if (gr.read(buf->begin(), buf->size(), nullptr) != buf->size()) {
                l.warn("gunzip failed");
                continue;
            }
            buf = &gr.outBuffer;
        }

        stringstream ss;
        ss << model.storageRoot << "/dump/lls-" << bsid << '.' << id;
        auto tbli = tblnames.find(lfi.first);
        if (tbli == tblnames.end())  {
            ss << '/' << lfi.first;
        } else {
            ss << '/' << tbli->second;
        }
        ss << '-' << lf->attributes["version"].asI32();
        string path = ss.str();

        makeBaseDir(path.c_str());
        FILE *fp = fopen(path.c_str(), "w");
        if (fp) {
            fwrite(buf->begin(), 1, buf->size(), fp);
            fclose(fp);
        }
    }
}
