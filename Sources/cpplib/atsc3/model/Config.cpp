#include <stdlib.h>
#include <sys/stat.h>

#include <string>
#include <map>

#include "acba.h"
#include "atsc3_model.h"
#include "atsc3_config.h"

using namespace std;
using namespace acba;
using namespace atsc3;

static Log l("config");

int Config::get (const string &key, string &value)
{
    string path = model.storageRoot + "/config";
    mkdir(path.c_str(), 0755);
    path += "/" + key;

    FILE *f = fopen(path.c_str(), "r");
    if (!f) {
        l.debug("Failed to open %s for reading", path.c_str());
        return -1;
    }

    // read value
    Buffer buf;
    while (true) {
        buf.ensureMargin(512);
        auto sz = fread(buf.end(), 1, buf.margin(), f);
        if (sz == 0) break;
        buf.grow(sz);
    }
    buf.writeI8(0); // add trailing null char for safety.

    fclose(f);

    value = (char*)buf.begin();
    return 0;
}

int Config::set (const string &key, const string &value)
{
    string path = model.storageRoot + "/config";
    mkdir(path.c_str(), 0755);
    path += "/" + key;

    FILE *f = fopen(path.c_str(), "w");
    if (!f) {
        l.warn("Failed to open %s for writing", path.c_str());
        return -1;
    }

    if (fputs(value.c_str(), f) < 0) {
        l.warn("Failed to write config value %s as %s", key.c_str(),
                value.c_str());
        fclose(f);
        ::remove(path.c_str());
        update.notify(key);
        return -1;
    }

    fclose(f);
    update.notify(key);

    return 0;
}

int Config::remove (const string &key)
{
    string path = model.storageRoot + "/config/" + key;

    if (remove(path.c_str())) return -1;

    update.notify(key);
    return 0;
}
