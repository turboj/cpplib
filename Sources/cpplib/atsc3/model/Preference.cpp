#include <sys/stat.h>

#include <functional>
#include <string>
#include <map>
#include <set>

#include "acba.h"
#include "EventDriver.h"
#include "atsc3_model.h"
#include "atsc3_config.h"
//#include "nlohmann/json.hpp"

using namespace std;
using namespace acba;
using namespace atsc3;
//using json = nlohmann::json;

static Log l("preference");

struct PreferenceHandler {
    string defaultValue;
    function<bool(const string&)> checkValid;
};

static bool isLanguage (const string& s) {
    static set<string> langs = {
        "en", "kr", "fr"
        // TODO: fill in
    };

    return (langs.find(s) != langs.end());
}

static bool isBoolean (const string& s) {
    if (s == "true" || s == "false") return true;
    return false;
}

static bool isNonEmpty (const string& s) {
    return !s.empty();
}

static map<const string, PreferenceHandler> preferenceHandlers = {
    {"preferredAudioLang", {"en", isLanguage }},
    {"preferredUiLang", {"en", isLanguage}},
    {"preferredCaptionSubtitleLang", {"en", isLanguage}},
    {"rating", {
        "1,'TV-MA-D-L', {0 'TV-MA'}{1 'D'}{2 'L'},2,'R(en)/R(fr)',{0 'R'}{1 'R'}",
        isNonEmpty, // TODO
    }},
    {"videoDescriptionService", {
        "{\"enabled\":true, \"language\":\"en\"}",
        isNonEmpty, // TODO
    }},
    {"audioEIService", {
        "{\"enabled\":true, \"language\":\"en\"}",
        isNonEmpty, // TODO: seperate?
    }},
    {"captionDisplay", {
        "{\"cta708\":{\"characterColor\": \"#FFFFFF\","
        "\"characterOpacity\": 1.0,"
        "\"characterSize\": 40,"
        /**
         * FontStyle shall be as specified in CTA-708
         * "Default", "MonospacedSerifs", "ProportionalSerifs", "MonospacedNoSerifs",
         * "ProportionalNoSerifs", "Casual", "Cursive", "SmallCaps"
         */
        "\"fontStyle\": \"Default\","
        "\"backgroundColor\": \"#000000\","
        "\"backgroundOpacity\": 0.6,"
        //"None", "Raised", "Depressed", "Uniform", "LeftDropShadow", "RightDropShadow"
        "\"characterEdge\": \"None\","
        "\"characterEdgeColor\": \"#FFFFFF\","
        "\"windowColor\": \"#000000\","
        "\"windowOpacity\": 0.0"
        "}}",
        isNonEmpty,
    }},
    {"ccEnabled", {"true", isBoolean}}
};

int Preference::get (const string &key, string &value)
{
    // get handler
    auto phi = preferenceHandlers.find(key);
    if (phi == preferenceHandlers.end()) return -1; // not a preference key
    auto &ph = phi->second;

    // read value
    string val;
    int rc = model.config.get("atsc3."+key, val);

    if (!rc && ph.checkValid(val)) {
        value = val;
    } else {
        value = ph.defaultValue;
    }

    return 0;
}

int Preference::set (const string &key, const string &value)
{
    // get handler
    auto phi = preferenceHandlers.find(key);
    if (phi == preferenceHandlers.end()) return -1; // not a preference key
    auto &ph = phi->second;

    if (!ph.checkValid(value)) {
        l.warn("Ignored invalid preference setting %s = '%s'",
                key.c_str(), value.c_str());
        return -1;
    }

    return model.config.set("atsc3."+key, value);
}

void Preference::onUpdate (const string &key)
{
    if (strncmp(key.c_str(), "atsc3.", 6) != 0) return;

    auto phi = preferenceHandlers.find(key.c_str() + 6);
    if (phi == preferenceHandlers.end()) return;

    update.notify(key.c_str() + 6);
}
