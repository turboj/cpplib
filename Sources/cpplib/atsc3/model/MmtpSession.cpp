#include <sys/stat.h>
#include <sys/types.h>

#include "atsc3_model.h"
#include "acba.h"

using namespace acba;
using namespace atsc3;

static Log l("ms");

void MmtpSession::dump ()
{
    l.info("MmtpSession dstip:%d.%d.%d.%d dstport:%d",
            (destinationIp >> 24) & 0xff,
            (destinationIp >> 16) & 0xff,
            (destinationIp >> 8) & 0xff,
            (destinationIp >> 0) & 0xff,
            destinationPort);
}
