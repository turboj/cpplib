#ifndef ATSC3_MODEL_H
#define ATSC3_MODEL_H

#include <vector>
#include <map>
#include <set>
#include <memory>
#include <stdint.h>
#include <string>
#include <limits>

#include "stream_reader.h"
#include "udp_reader.h"
#include "pugixml.hpp"
#include "acba.h"
#include "atsc3_config.h"

namespace atsc3 {

typedef acba::RefCounted<pugi::xml_document> XmlDocument;

struct Service: acba::RefCounter<Service> {

    Service (): bsid(-1), sgid(-1), id(-1), version(-1),
            protocol(-1), category(-1), active(true) {}

    virtual void dump () {}

    int bsid;
    int sgid;
    std::string globalId;

    int id;
    int version;
    int protocol;
    int category;
    bool active;

    std::set<std::string> languages;
    std::map<std::string, std::string> names;

    acba::Ref<XmlDocument> sltDocument; // holds actual mem located by xml
    // attrs: globalServiceID, majorChannelNo, minorChannelNo,
    // serviceCategory, serviceId, shortServiceName, sltSvcSeqNum
    pugi::xml_node xml;

    acba::Update<Service&> update;
};

struct File: acba::RefCounter<File> {
    acba::Buffer content;
    std::string type;
    typedef std::map<const std::string, acba::Variant> Attributes;
    Attributes attributes;
};

struct RouteFile: File {
    RouteFile (uint32_t s, uint32_t o): tsi(s), toi(o), codePoint(-1),
            updateTime(0.0),
            expireTime(std::numeric_limits<double>::infinity()) {}
    uint32_t tsi, toi;
    int codePoint;
    std::string path;
    double updateTime;
    double expireTime;

    enum FORMAT {
        UNKNOWN = -1,
        RESERVED = 0,
        FILE_MODE,
        ENTITY_MODE,
        UNSIGNED_PACKAGE_MODE,
        SIGNED_PACKAGE_MODE
    };

    FORMAT getFormat () {
        auto fi = attributes.find("formatId");
        if (fi == attributes.end()) return UNKNOWN;
        return FORMAT(fi->second.asI32());
    }
};

struct RouteSession: acba::RefCounter<RouteSession> {

    RouteSession (uint32_t srcIp, uint32_t dstIp, uint32_t dstPort):
            sourceIp(srcIp), destinationIp(dstIp), destinationPort(dstPort) {
    }

    const char *findRepresentationId (uint32_t tsi);
    void dump ();

    const uint32_t sourceIp, destinationIp; // host endian
    const uint16_t destinationPort;
    pugi::xml_node xmlRs;
    acba::Update<> xmlRsUpdate;
};

struct RouteService: Service {

    void dump () override;

    acba::Ref<XmlDocument> heldDocument;
    acba::Update<> heldDocumentUpdate;

    acba::Ref<XmlDocument> usdDocument;
    acba::Update<> usdDocumentUpdate;

    acba::Ref<XmlDocument> stsidDocument;

    acba::Ref<XmlDocument> mpdDocument; // may be forged from original mpd in slsFiles
    acba::Update<> mpdDocumentUpdate;

    // TODO: Consider moving to task
    acba::Ref<File> resolvedMpdFile; // xlink-resolved version
    acba::Update<> resolvedMpdFileUpdate;

    acba::Ref<RouteSession> mainSession;
    std::vector<acba::Ref<RouteSession> > sessions;

    std::map<const std::string, acba::Ref<RouteFile> > files;
    acba::Update<const acba::Ref<RouteFile>&> fileUpdate;

    // slsFileUpdate is intentionally absent.
    // Use specific update variables above instead.
    std::map<const std::string, acba::Ref<File> > slsFiles;
};

struct Mpu: acba::RefCounter<Mpu> {
    Mpu (uint16_t pktid, uint32_t seqnum): packetId(pktid),
            sequenceNumber(seqnum), presentationTime(0),
            expireTime(std::numeric_limits<double>::infinity()),
            mdatSize(0), receivedMdatSize(0), flags(0) {}
    uint16_t packetId;
    uint32_t sequenceNumber;
    double presentationTime; // epoc time derived from asset timestamp desc.
    double expireTime;

    acba::Buffer mpuMetadata;
    acba::Buffer movieFragmentMetadata;
    acba::Buffer mediaData; // excludes MMTHSample track.
    uint32_t mdatSize; // read from 'mdat'. includes MMTHSample track.
    uint32_t receivedMdatSize; // includes MMTHSample track.

    static const int FLAG_MPU_PTS = 1 << 0;
    static const int FLAG_MPU_METADATA = 1 << 1;
    static const int FLAG_MOVIE_FRAGMENT_METADATA = 1 << 2;
    static const int FLAG_FULL_MEDIA_DATA = 1 << 3;
    static const int FLAGS_ALL = (1 << 4) - 1;
    int flags;
};

struct MmtpSession: acba::RefCounter<MmtpSession> {
    MmtpSession (uint32_t dstIp, uint32_t dstPort):
            destinationIp(dstIp), destinationPort(dstPort) {
    }

    void dump ();

    const uint32_t destinationIp; // host endian
    const uint16_t destinationPort;
};

struct MmtMessage: acba::RefCounter<MmtMessage> {

    MmtMessage (uint16_t _id, uint8_t ver): id(_id), version(ver) {}

    uint16_t id;
    uint8_t version;
    acba::Buffer content;
    typedef std::map<const std::string, acba::Variant> Attributes;
    Attributes attributes;
};

struct Asset: acba::RefCounter<Asset> {

    Asset (): isDefault(false), packetId(-1) {}

    std::string id;
    std::string type;
    bool isDefault; // default_asset_flag
    int packetId; // -1 or uint16_t
    std::map<uint32_t, uint64_t> timestamps; // mpuSeqNum, mpuPts
};

struct MmtPackage: acba::RefCounter<MmtPackage> {

    MmtPackage (): lastMpTableId(-1), mpTableVersion(-1), lastPiTableId(-1),
            piTableVersion(-1) {}

    std::string id;

    int lastMpTableId;
    int mpTableVersion;

    int lastPiTableId;
    int piTableVersion;

    std::map<std::string, acba::Ref<Asset> > assets; // by asset id
};

struct MmtpService: Service {

    void dump () override;
    int getDefaultVideoPacketId ();
    int getDefaultAudioPacketId ();
    int getDefaultTextPacketId ();

    std::vector<acba::Ref<MmtpSession> > sessions;
    std::map<uint16_t, acba::Ref<MmtMessage> > messages;
    acba::Ref<MmtPackage> currentPackage;

    // Compressed message is decompressed before putting in.
    std::map<uint16_t, acba::Ref<File> > ma3Messages;

    std::map<const std::pair<uint16_t, uint32_t>, acba::Ref<Mpu> > mpus;
    // (mpu, event, void *param)
    // event 0 -> Received ft 0
    // event 1 -> Received ft 1
    // event 2 -> Received ft 2. param locates to MMTHSample
    // event 3 -> Missing ft 2. param locates to uint32_t of missing bytes.
    // event 4 -> reserved for MPUStreamer delivering MPU presentation time.
    acba::Update<const acba::Ref<Mpu>&, uint8_t, const void*> mpuUpdate;
};

struct ServiceGroup: acba::RefCounter<ServiceGroup> {

    ServiceGroup (int gid = -1, int _bsid = -1): id(gid), bsid(_bsid) {}

    void dump ();

    int id; // group_id
    int bsid;

    std::map<int, acba::Ref<File> > llsFiles;

    std::map<int, acba::Ref<Service> > services; // sid -> Service

    acba::Update<> update;
};

struct EsgFragmentSlide: acba::RefCounter<EsgFragmentSlide> {
    EsgFragmentSlide (byte_t t, int tid): type(t), transportId(tid),
            checkTime(std::numeric_limits<double>::infinity()),
            currentVersion(0), nextVersion(0) {}
    byte_t type;
    int transportId;
    double checkTime;
    uint32_t currentVersion; // valid only when currentFragment is non zero
    uint32_t nextVersion; // valid only when nextFragment is non zero
    acba::Ref<XmlDocument> currentFragment;
    acba::Ref<XmlDocument> nextFragment;
    std::string applicationContextIds;
};

struct BroadcastStream: acba::RefCounter<BroadcastStream> {

    BroadcastStream (int bsid = -1): id(bsid) {}

    void dump ();

    int id; // bsid
    std::map<int, acba::Ref<ServiceGroup> > serviceGroups;
    std::string location;
    // tblId, grpId, llsFile
    acba::Update<uint8_t, uint8_t, const acba::Ref<File> > llsUpdate;

    std::map<const std::string, acba::Ref<XmlDocument> > sgdds;
    typedef acba::Ref<EsgFragmentSlide> EsgFragmentSlideRef;
    std::map<int, EsgFragmentSlideRef> esgFragmentSlideByTransportId;
    std::map<const std::string, EsgFragmentSlideRef> esgFragmentSlides;
};

struct Package: acba::RefCounter<Package> {
    Package (const std::string &_id): id(_id) {}
    std::string id;
    acba::Ref<RouteFile> origin;
    std::map<const std::string, acba::Ref<File> > files;
};

struct ApplicationContextCache: acba::RefCounter<ApplicationContextCache> {

    void dump();

    ApplicationContextCache (const std::string &_id): id(_id), hostIp(0) {}

    std::string id;
    std::string baseUri;
    uint32_t hostIp; // host ip of baseUri, native endian

    std::map<const std::string, acba::Ref<Package> > packages;
    acba::Update<const acba::Ref<Package>&> packageUpdate;
    std::map<const std::string, acba::Ref<File> > files;

    bool addPackage (const acba::Ref<Package> &pkg);
};

struct AeatInfo: acba::RefCounter<AeatInfo> {

    AeatInfo(int _bsid=-1, int _sgid=-1): receiveTime(0.0), bsid(_bsid), sgid(_sgid) {}
    double receiveTime;
    std::set<std::string> filteredEvents;
    acba::Ref<XmlDocument> aeatDocument;
    int bsid;
    int sgid;
};

struct OsmnInfo: acba::RefCounter<OsmnInfo> {

    OsmnInfo(int _bsid=-1, int _sgid=-1): receiveTime(0.0), bsid(_bsid), sgid(_sgid) {}
    double receiveTime;
    acba::Ref<XmlDocument> osmnDocument;
    int bsid;
    int sgid;
};

enum ESG_FRAGMENT_EVENT {
    ESG_FRAGMENT_ADDED,
    ESG_FRAGMENT_UPDATED,
    ESG_FRAGMENT_REMOVED,
};

struct Model {

    Model (): storageRoot(".") {}

    ////////////////////////////////////////////////////////////////////////////
    // Network Management

    void addKnownStreamLocation (const std::string &s) {
        knownStreamLocations.push_back(s);
        knownStreamLocationsUpdate.notify();
    }

    std::vector<std::string> knownStreamLocations;
    acba::Update<> knownStreamLocationsUpdate;

    ////////////////////////////////////////////////////////////////////////////
    // Service Management

    std::map<int, acba::Ref<BroadcastStream> > broadcastStreams;
    std::map<const std::string, acba::Ref<BroadcastStream> >
            broadcastStreamByLocation;
    acba::Update<const acba::Ref<Service>&> serviceUpdate;

    bool selectService (Service *srv);
    acba::Ref<Service> currentService;
    acba::Update<> currentServiceUpdate;

    std::set<acba::Ref<Service> > backgroundServices;
    acba::Update<> backgroundServicesUpdate;

    ////////////////////////////////////////////////////////////////////////////
    // Application Management

    // key: app context id
    std::map<const std::string, acba::Ref<ApplicationContextCache> >
            applicationContextCaches;

    ////////////////////////////////////////////////////////////////////////////
    // ESG

    // fragments are managed within BroadcastStream instance.
    acba::Update<ESG_FRAGMENT_EVENT, const std::string&,
            const acba::Ref<EsgFragmentSlide>&,
            const acba::Ref<BroadcastStream>&> esgFragmentUpdate;

    ////////////////////////////////////////////////////////////////////////////
    // AEA

    acba::Ref<AeatInfo> activeAeatInfo;
    acba::Update<> activeAeatInfoUpdate;

    ////////////////////////////////////////////////////////////////////////////
    // OnScreenMessageNotification

    acba::Ref<OsmnInfo> activeOsmnInfo;
    acba::Update<> activeOsmnInfoUpdate;

    ////////////////////////////////////////////////////////////////////////////
    // Etc.

    std::string storageRoot;
    Config config; // Do not access here for accessing preference
    Preference preference;

    void dump ();
};

extern Model model;

}
#endif
