#include <sstream>
#include "atsc3_model.h"
#include "acba.h"

using namespace std;
using namespace atsc3;
using namespace acba;

static Log l("bs");

void BroadcastStream::dump ()
{
    l.info("Broadcast Stream %s bsid:%d num_groups:%d", location.c_str(),
            id, serviceGroups.size());
    for (auto sg: serviceGroups) sg.second->dump();

    for (auto fsi: esgFragmentSlides) {
        auto &fs = fsi.second;

        stringstream ss;
        ss << "current:";
        if (fs->currentFragment) {
            ss << "{";
            ss << "ver:" << fs->currentVersion;
            auto root = fs->currentFragment->document_element();
            ss << " window:";
            ss << root.attribute("validFrom").value();
            ss << "-";
            ss << root.attribute("validTo").value();
            ss << "}";
        } else {
            ss << "null";
        }

        ss << " next:";
        if (fs->nextFragment) {
            ss << "{";
            ss << "ver:" << fs->nextVersion;
            auto root = fs->nextFragment->document_element();
            ss << " window:";
            ss << root.attribute("validFrom").value();
            ss << "-";
            ss << root.attribute("validTo").value();
            ss << "}";
        } else {
            ss << "null";
        }

        l.info("FRAGMENT %u %s %s", fs->type, fsi.first.c_str(),
                ss.str().c_str());
    }
}
