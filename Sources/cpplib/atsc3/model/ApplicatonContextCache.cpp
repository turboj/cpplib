#include <sys/stat.h>
#include <sys/types.h>
#include <sstream>
#include <string>

#include "atsc3_model.h"
#include "acba.h"
#include "path_util.h"

using namespace std;
using namespace atsc3;
using namespace acba;

static Log l("appctx");

void ApplicationContextCache::dump ()
{
    l.info("ApplicatoinContextCache %s", id.c_str());
    for (auto f: files) {
        Buffer &buf = f.second->content;
        l.info("    file %s size:%d", f.first.c_str(), buf.size());

        stringstream ss;
        ss << model.storageRoot << "/dump/appctx-" << id << '/' << f.first;
        string path = ss.str();
        makeBaseDir(path.c_str());
        FILE *fp = fopen(path.c_str(), "w");
        if (fp) {
            Buffer &buf = f.second->content;
            fwrite(buf.begin(), 1, buf.size(), fp);
            fclose(fp);
        }
    }
}

bool ApplicationContextCache::addPackage (const acba::Ref<Package> &pkg)
{
    l.info("adding package %s into %s", pkg->id.c_str(), id.c_str());

    packages[pkg->id] = pkg;
    for (auto f: pkg->files) {
        files[f.first] = f.second;
        l.info("    file %s", f.first.c_str(), pkg->id.c_str());
    }

    packageUpdate.notify(pkg);
    return true;
}
