#if USE_TUNER_CLIENT

#include <string>

#include "stream_reader.h"
#include "pcap_reader.h"
#include "ip_reader.h"
#include "udp_reader.h"
#include "atsc3_task.h"
#include "tune_manager.h"
#include "EventDriver.h"
#include "acba.h"
#include "acba_crc32.h"

#include "tuner.h"
#include "tuner_client.h"

using namespace std;
using namespace acba;
using namespace atsc3;
using namespace tuner;

static Log l("vatsc3tune");

struct VirtualAtsc3Tune: Tune {

    TuneContext tuneContext;
    IpReader ipReader;
    UdpReader udpReader;
    StreamReaderRef consumer;
    int lmtCrc32;

    Ref<TuneProxy> proxy;

    Log l;

    VirtualAtsc3Tune (const string &loc, const StreamReaderRef &cons):
            Tune(loc), l("vatsc3tune", loc+" "), ipReader(&udpReader),
            udpReader(cons.get()), lmtCrc32(0), consumer(cons) {
        tuneContext.tune = this;
    }

    bool start () {
        using namespace placeholders;
        proxy = client().tune(location, bind(&VirtualAtsc3Tune::onTunerCallback,
                this, _1, _2, _3));
        if (!proxy) return false;

        ref(); // corresponding deref() occurrs on CALLBACK_END
        return true;
    }

    void stop () {
        proxy->dispose();
        proxy = nullptr;
    }

    void setRoutingTable (const acba::Ref<RoutingTable> &rt) override {
        vector<MulticastAddress> addrs;
        for (auto &rei: rt->entries) {
            MulticastAddress addr;
            addr.sourceIp = rei.second.sourceIp;
            addr.sourcePort = 0;
            addr.destinationIp = rei.first >> 32;
            addr.destinationPort = (rei.first >> 16) & 0xffff;
            addrs.push_back(addr);
        }

        proxy->setMulticasts(addrs.data(), addrs.size());
    }

    void onTunerCallback (CALLBACK_TYPE type, const byte_t *data, int sz) {
        l.trace("onTunerCallback %d", type);

        switch (type) {
        case CALLBACK_TUNER_LOCKED:
            {
                ScopedLock sl(mutex);
                if (locked) break;
                locked = true;
                update.notify(EVENT_TUNER_LOCKED);
            }
            break;

        case CALLBACK_TUNER_UNLOCKED:
            {
                ScopedLock sl(mutex);
                if (!locked) break;
                locked = false;
                update.notify(EVENT_TUNER_UNLOCKED);
            }
            break;

        case CALLBACK_IP:
            ipReader.read(data, sz, &tuneContext);
            break;

        case CALLBACK_LMT:
            parseLmt(data, sz);
            break;

        case CALLBACK_END:
            l.warn("callback end.");
            deref();
            break;

        default:
            l.warn("Unsupported tuner callback %d", type);
            break;
        }
    }

#define SAFEREAD_LOG(args...) l.warn("expected " args)
#define SAFEREAD_RETURN return

    void parseLmt (const byte_t *data, int sz) {
        int h = (int)acba_crc32(0, data, sz);
        if (lmtCrc32 == h) {
            l.info("Ignored the same LMT sz:%d", sz);
            return;
        }
        lmtCrc32 = h;

        l.info("Got new LMT sz:%d", sz);

        const byte_t *d = data;
        const byte_t *e = data + sz;

        byte_t nplpm1 = SAFEREAD_U8(d, e, "num_PLPs_minus1") >> 2;
        l.info("    number of plps:%u", nplpm1 + 1);
        for (int i = 0; i <= nplpm1; i++) {
            byte_t plpid = SAFEREAD_U8(d, e, "PLP_ID") >> 2;
            byte_t nmcast = SAFEREAD_U8(d, e, "num_multicasts");
            l.info("    plp-%u has %u multicast(s)", plpid, nmcast);
            for (int j = 0; j < nmcast; j++) {
                uint32_t srcip = SAFEREAD_U32(d, e, "src_IP_add");
                uint32_t dstip = SAFEREAD_U32(d, e, "dst_IP_add");
                uint32_t srcport = SAFEREAD_U16(d, e, "src_IP_add");
                uint32_t dstport = SAFEREAD_U16(d, e, "dst_IP_add");
                byte_t flags = SAFEREAD_U8(d, e, "flags");
                if (flags & 0x80) SAFEREAD_SKIP(d, e, 1, "SID");
                if (flags & 0x40) SAFEREAD_SKIP(d, e, 1, "context_id");
                l.info("        %08x:%u -> %08x:%u", srcip, srcport, dstip,
                        dstport);
            }
        }
    }
};

struct VirtualAtsc3TuneProvider: TuneProvider {

    VirtualAtsc3TuneProvider (): TuneProvider("atsc3") {}

    Tune *create (const std::string &loc, const StreamReaderRef &cons)
            override {
        auto vat = new VirtualAtsc3Tune(loc, cons);
        if (!vat->start()) {
            delete vat;
            return nullptr;
        }

        return vat;
    }

    void destroy (const acba::Ref<Tune> &t) override {
        auto at = static_cast<VirtualAtsc3Tune*>(t.get());
        at->stop();
    }
};

void VirtualAtsc3TuneProvider_init ()
{
    task.tuneManager.registerTuneProvider(new VirtualAtsc3TuneProvider());
}

#endif
