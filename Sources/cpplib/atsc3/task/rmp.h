#ifndef ATSC3_RMP_H
#define ATSC3_RMP_H

#include <string>

#include "acba.h"
#include "service_manager.h"
#include "nlohmann/json.hpp"

namespace atsc3 {

// All play state are defined in A344 9.14.3
enum RMP_PLAY_STATE {
    RMP_PLAY_STATE_INIT = -1,
    RMP_PLAY_STATE_PLAYING = 0,
    RMP_PLAY_STATE_FAILED = 1, // paused for any reason(currently for only errors, not supported user input)
    RMP_PLAY_STATE_ENDED = 2,
    RMP_PLAY_STATE_DRM = 3
};
enum RMP_MEDIA_EVENT {
    RMP_MEDIA_TIME_CHANGE,
    RMP_PLAY_STATE_CHANGE,
    RMP_PLAY_RATE_CHANGE
};

struct Rmp: acba::Reactor, acba::Listener<>, acba::Listener<RMP_MEDIA_EVENT, double> {
    Rmp (): started(false), rmpPlaybackState(RMP_PLAY_STATE_INIT), isSignaling(false),
        nextContentByApp(""), syncTime(0.0), drmSchemeId("") {}

    void onUpdate () override;
    void tryStart ();
    void play (ServiceTask *stask);
    void stop ();

    void processEvent (int evt) override;
    void onUpdate (RMP_MEDIA_EVENT evt, double value) override;

    void startRmp (const std::string *url);
    void stopRmp ();
    void resumeService ();
    void getRMPMediaTime (atsc3_RMPMediaTime *rmpMediaTime);
    int getPlaybackState ();
    double getPlaybackRate ();
    void setScalePosition (double scaleFactor, double xPos, double yPos);
    void initPreference ();
    const char* findDRMSchemeId ();
    void receivedLicenseResponse (const char *systemId, const char *message);
    void cacheLicense (const char *systemId, unsigned char* buf, int size);

    acba::Ref<ServiceTask> serviceTask; // non-null only if started.
    bool started;
    int rmpPlaybackState;

    bool isSignaling; // for startRmp by broadcast App.
    std::string nextContentByApp;
    double syncTime;
    std::string drmSchemeId;

    struct PreferenceUpdateListener: acba::Listener<const std::string&> {
        void onUpdate (const std::string &key) override;
    } prefUpdateListener;

    ///////////////////////////////////////////////////////////////////////////
    // State Update

    acba::Update<RMP_MEDIA_EVENT, double> rmpMediaTimeChangeUpdate;
    acba::Update<RMP_MEDIA_EVENT, double> rmpPlaybackStateChangeUpdate;
    acba::Update<RMP_MEDIA_EVENT, double> rmpPlaybackRateChangeUpdate;
    acba::Update<const std::string&> rmpPeriodChanged;

    acba::Update<bool> captionStateUpdate;

    ///////////////////////////////////////////////////////////////////////////
    // EventStream

    acba::Update<const std::string&, double, const std::string&,
            double, double, const std::string&, double> eventStreamEvent;

    void setEventMessageFromPeriod (const std::string& _schemeIdUri, double _id,
            const std::string& _value, double _presentationTimeUs,
            double _durationMs, const std::string& _data, double _mediaStartMs);
    void checkStoredEventMessage (const std::string& _schemeIdUri,
            const std::string& _value);

    //////////////////////////////////////////////////////////////////////////
    // DRM

    acba::Update<std::string, nlohmann::json&> drmInfoUpdate;
};

}
#endif
