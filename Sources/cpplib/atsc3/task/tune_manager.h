#ifndef TUNE_MANAGER_H
#define TUNE_MANAGER_H

#include <pthread.h>

#include <string>
#include <vector>

#include "atsc3_model.h"
#include "atsc3_platform.h"
#include "acba.h"
#include "EventDriver.h"
#include "stream_reader.h"

namespace atsc3 {

/**
 * Holds every multicast join used by the receiver.
 * Once constructed, this instance is treated as immutable.
 */
struct RoutingTable: acba::RefCounter<RoutingTable> {

    RoutingTable () {}
    RoutingTable (const acba::Ref<RoutingTable> &other):
            entries(other->entries) {}

    struct Entry {
        uint32_t sourceIp;
        StreamReaderRef streamReader;
    };

    // key - dstip(32) + port(16) + zero(16)
    std::map<uint64_t, Entry> entries;
};

struct Tune: acba::RefCounter<Tune> {

    Tune (const std::string &loc): location(loc), locked(false),
            mutex(PTHREAD_MUTEX_INITIALIZER) {}

    std::string location;

    enum EVENT {
        EVENT_TUNER_LOCKED,
        EVENT_TUNER_UNLOCKED,
        EVENT_SERVICE_LIST_RECEIVED,
    };

    // (groupId, tableId) -> version
    std::map<uint16_t, uint8_t> llsVersions;

    bool isLocked () {
        acba::ScopedLock sl(mutex);
        return locked;
    }

    void addListener (acba::Listener<EVENT> *l);
    void removeListener (acba::Listener<EVENT> *l);

    virtual void setRoutingTable (const acba::Ref<RoutingTable> &rt) {}

protected:
    pthread_mutex_t mutex;
    acba::Update<EVENT> update;
    bool locked;
    void notify (EVENT e);

    friend struct ServiceManager;
};

struct TuneConsumer: RefCountedStreamReader {
    TuneConsumer (const acba::Ref<RoutingTable> &rt): routingTable(rt),
            nextRoutingTable(rt) {}

    int read (const byte_t *data, int sz, Chain *ctx) override;
    void setRoutingTable (const acba::Ref<RoutingTable> &rt);

private:
    // Currently effective routing table, used from tuner thread.
    acba::Ref<RoutingTable> routingTable;

    // Assigned by main thread, to be updated to routingTable from tuner thread.
    acba::Ref<RoutingTable> nextRoutingTable;
};

struct TuneProvider: acba::RefCounter<TuneProvider> {
    TuneProvider (const std::string &t): type(t) {}
    std::string type;
    virtual Tune *create (const std::string &loc, const StreamReaderRef &cons) = 0;
    virtual void destroy (const acba::Ref<Tune> &t) = 0;
};

struct TuneContext: ChainNode<TuneContext> {
    Tune *tune;
};

struct TuneManager {

    TuneManager (): routingTable(new RoutingTable()) {}

    int join (uint32_t srcIp, uint32_t dstIp, uint16_t port, const StreamReaderRef &r);
    int leave (uint32_t srcIp, uint32_t dstIp, uint16_t port);
    void updateRoutingTable (const acba::Ref<RoutingTable> &rt);

    acba::Ref<RoutingTable> routingTable;

    struct Discovery: acba::Listener<>, acba::Reactor {
        void onUpdate () override;
        void processEvent (int events) override;
    } discovery;

    std::vector<acba::Ref<TuneProvider> > tuneProviders;

    struct TuneHolder {
        TuneHolder (): useCount(0) {}
        acba::Ref<Tune> tune;
        acba::Ref<TuneProvider> provider;
        acba::Ref<TuneConsumer> consumer;
        int useCount;
    };
    std::map<const std::string, TuneHolder> tunes;

    void init ();
    void registerTuneProvider (const acba::Ref<TuneProvider> &prov);

    Tune *requestTune (const std::string &loc);
    void releaseTune (const acba::Ref<Tune> &t);
};

}
#endif
