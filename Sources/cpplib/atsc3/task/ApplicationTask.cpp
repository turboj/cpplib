#include <sstream>

#include "atsc3_model.h"
#include "atsc3_task.h"
#include "mime_reader.h"
#include "application_manager.h"
#include "pugixml.hpp"
#include "acba.h"
#include "datetime.h"

using namespace std;
using namespace acba;
using namespace pugi;
using namespace atsc3;

#define REV "20190502"

static Log l("app");

void ApplicationTask::start ()
{
    l.info("Starting application... context:%s%s%s url:%s",
            contextCache->id.c_str(),
            entryPackage.empty() ? "" : " entryPackage:",
            entryPackage.empty() ? "" : entryPackage.c_str(),
            url.c_str());

    tryCreateWebWindow();
}

void ApplicationTask::stop ()
{
    l.info("Stopping application context:%s", contextCache->id.c_str());

    if (webWindow) {
        webWindow->close();
        webWindow = nullptr;

        if (serviceUsage) {
            serviceUsage->stopApplication(id.c_str(), currentUnixTime());
        }
    }
}

struct PackageExtractor: StreamReader {

    int read (const byte_t *data, int sz, Chain *pctx) {
        auto mctx = pctx->as<MimeContext>();
        assert(mctx);

        string type;
        string location;

        for (auto i: mctx->headers) {
            if (strcmp(i.first.c_str(), "content-type") == 0) {
                type = mime_value(i.second.c_str());
                transform(type.begin(), type.end(), type.begin(), ::tolower);
            } else if (strcmp(i.first.c_str(), "content-location") == 0) {
                location = mime_value(i.second.c_str());
            }
        }


        if (strncmp(type.c_str(), "multipart/", 10) == 0) {
            // recurse into multipart content
            MultipartReader mr(this);
            mr.read(data, sz, pctx);
            return sz;
        }

        if (type == "application/mbms-envelope+xml") {
            xml_document doc;
            xml_parse_result res = doc.load_buffer(data, sz);
            if (!res) {
                l.warn("Failed to parse metadata envelope! %s\n",
                        res.description());
            }

            for (auto i: doc.child("metadataEnvelope").children("item")) {
                const char *loc = i.attribute("metadataURI").value();
                int ver = i.attribute("version").as_int(-1);
                const char *type = i.attribute("contentType").value();

                Ref<File> &f = package->files[loc];
                if (!f) f = new File();
                f->type = type;
            }
        }

        if (location.empty()) {
            l.warn("Package contains a file without Content-Location. type:%s",
                    type.c_str());
            return sz;
        }

        Ref<File> &f = package->files[location];
        if (!f) {
            f = new File();
            f->type = type;
        }
        f->content.writeBytes(data, sz);

        return sz;
    }

    void extract (RouteFile &rf) {
        package = new Package(rf.path);
        MimeReader mr(this);
        mr.read(rf.content.begin(), rf.content.size(), NULL);

        package->origin = &rf;
    }

    Ref<Package> package;
};

void ApplicationTask::tryAddPackage (const Ref<RouteFile> &f)
{
    // Check if given file is eligible for the package.
    if (!f) return;
    auto fmt = f->getFormat();
    if (fmt != RouteFile::UNSIGNED_PACKAGE_MODE &&
            fmt != RouteFile::SIGNED_PACKAGE_MODE) {
        return;
    }

    // Check if the package belongs to current app context cache
    string ids = f->attributes["appContextIdList"].asString();
    if (ids.find(" "+contextCache->id+" ") == string::npos) {
        if (f->path == entryPackage) {
            l.warn("Dropped could-be Entry package "
                    "which has no valid appContextIdList.");
        }
        return;
    }

    // TODO: check integrity of signed package


    // Check if package update is necessary.
    auto &pkgs = contextCache->packages;
    auto pkgi = pkgs.find(f->path);
    if (pkgi != pkgs.end()) {
        auto &pkg = pkgi->second;
        if (pkg->origin == f) return;
    }

    // Load package into context cache
    PackageExtractor pe;
    pe.extract(*f);
    if (!pe.package) {
        l.error("Failed to extract package file:%s", f->path.c_str());
        return;
    }
    contextCache->addPackage(pe.package);

    tryCreateWebWindow();
}

void ApplicationTask::tryCreateWebWindow ()
{
    if (webWindow) return;

    // Check if entry package exist
    if (!entryPackage.empty()) {
        auto pkgi = contextCache->packages.find(entryPackage);
        if (pkgi == contextCache->packages.end()) return;

        // Also check if entry page exist
        auto &pkg = pkgi->second;
        auto fi = pkg->files.find(url);
        if (fi == pkg->files.end()) {
            l.warn("Received entry package, but entry page is not there");
            return;
        }
    }

    // Create new web window
    stringstream ss;
    if (entryPackage.empty()) {
        ss << url;
    } else {
        ss << contextCache->baseUri << '/' << url;
    }
    char wsUrl[128];
    sprintf(wsUrl, "?wsURL=ws://%d.%d.%d.%d:8080",
            contextCache->hostIp >> 24,
            (contextCache->hostIp >> 16) & 0xff,
            (contextCache->hostIp >> 8) & 0xff,
            contextCache->hostIp & 0xff);
    ss << wsUrl;
    ss << "&rev=" REV;
    l.info("Create web window %s", ss.str().c_str());
    webWindow = new WebWindow();
    webWindow->open(ss.str(), 1);

    // Service Usage
    if (serviceUsage) {
        serviceUsage->startApplication(id.c_str(), currentUnixTime(),
                "2" /* downloaded and auto-launched */, "");
    }
}
