#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>

#include <algorithm>
#include <string>
#include <sstream>

#include "ip_reader.h"
#include "mime_reader.h"
#include "atsc3_model.h"
#include "pcap_reader.h"
#include "gzip_reader.h"
#include "datetime.h"
#include "pugixml.hpp"
#include "service_manager.h"
#include "application_manager.h"
#include "usage_manager.h"
#include "atsc3_model.h"
#include "atsc3_task.h"
#include "acba.h"
#include "mpd_parser.h"

#define FORGE_LIVE_MPD_ALWAYS 1

#define FORGE_MPD_WITH_AVONLY 1

using namespace std;
using namespace atsc3;
using namespace acba;
using namespace pugi;
using namespace dash;

static Log l("rsrv");

void RouteServiceTask::start ()
{
    RouteService *rsrv = static_cast<RouteService*>(service.get());

    switch (rsrv->category) {
    case 1: // Linear A/V service
    case 2: // Linear audio only service
        usage = UsageManager::createServiceUsage("<unknown_country>",
                to_string(rsrv->bsid).c_str(), to_string(rsrv->id).c_str(),
                rsrv->globalId.c_str(), to_string(rsrv->category).c_str(),
                currentUnixTime(), "0", "<unknown_type>", "<unknown_cid>");
        if (!usage) l.warn("Failed to get service usage");
        break;
    }

    for (auto rs: rsrv->sessions) {
        RouteSessionTask *rst = new RouteSessionTask(rs, rsrv);
        rs->xmlRsUpdate.addListener(&rst->fdtHandler);
        rst->start();

        sessionTasks.push_back(rst);
    }

    rsrv->resolvedMpdFileUpdate.addListener(&dashHandler);
    rsrv->fileUpdate.addListener(&dashHandler);
    rsrv->fileUpdate.addListener(&slsHandler);
    rsrv->fileUpdate.addListener(&esgProcessor);

    // HACK to include hand-made HELD
    char fn[512];
    sprintf(fn, "%s/held-%s.xml", model.storageRoot.c_str(),
            rsrv->globalId.c_str());
    for (char *c = fn + model.storageRoot.size() + 1; *c; c++) {
        if (*c == '/') *c = '_';
    }
    FILE *fp = fopen(fn, "r");
    if (fp) {
        l.warn("Loading custom HELD for %s", fn);
        auto &sf = rsrv->slsFiles["_held.xml"];
        sf = new File();
        sf->type = "application/atsc-held+xml";
        auto &buf = sf->content;

        while (true) {
            buf.ensureMargin(4096);
            size_t sz = fread(buf.end(), 1, buf.margin(), fp);
            if (sz <= 0) break;
            buf.grow(sz);
        }

        processHeld(buf.begin(), buf.size());
    }
}

void RouteServiceTask::stop ()
{
    RouteService *rsrv = static_cast<RouteService*>(service.get());

    rsrv->resolvedMpdFileUpdate.removeListener(&dashHandler);
    rsrv->fileUpdate.removeListener(&dashHandler);
    rsrv->fileUpdate.removeListener(&slsHandler);
    rsrv->fileUpdate.removeListener(&esgProcessor);

    for (auto st: sessionTasks) {
        st->stop();

        st->session->xmlRsUpdate.removeListener(&st->fdtHandler);
    }
    sessionTasks.clear();
    task.eventDriver.unsetTimeout(&dashHandler);
    if (task.rmp.serviceTask.get() == this) task.rmp.stop();

    if (usage) {
        usage->setStopTime(currentUnixTime());
        usage = nullptr;
    }

    // TODO: Consider moving to task
    rsrv->resolvedMpdFile = nullptr;
}

struct SlsContext: ChainNode<SlsContext> {
    map<string, uint32_t> versions; // version is set zero if not available.
};

void RouteServiceTask::SlsHandler::onUpdate (const acba::Ref<RouteFile> &rf)
{
    if (!rf || rf->tsi != 0) return;
    assert(rf->toi != 0);

    SlsContext ctx;
    MimeReader mr(this);
    auto &buf = rf->content;
    mr.read(buf.begin(), buf.size(), &ctx);

    // FIXME: Ad-Hoc work-around to notify when held is absent.
    auto self = container_of(this, &RouteServiceTask::slsHandler);
    RouteService *rsrv = static_cast<RouteService*>(self->service.get());
    if (!rsrv->heldDocument) rsrv->heldDocumentUpdate.notify();
}

// TODO: metadatEnvelop.item contains contentLength, validUntil
// TODO: boundaryHeader contains ATSC-HTTP-Attributes
int RouteServiceTask::SlsHandler::read (const byte_t *data, int sz, Chain *pctx)
{
    auto self = container_of(this, &RouteServiceTask::slsHandler);
    RouteService *rsrv = static_cast<RouteService*>(self->service.get());
    MimeContext *mctx = pctx->as<MimeContext>();

    string type;
    string location;

    for (auto i: mctx->headers) {
        l.debug("mime header %s: %s", i.first.c_str(), i.second.c_str());
        if (strcmp(i.first.c_str(), "content-type") == 0) {
            type = mime_value(i.second.c_str());
            transform(type.begin(), type.end(), type.begin(), ::tolower);
        } else if (strcmp(i.first.c_str(), "content-location") == 0) {
            location = mime_value(i.second.c_str());
        }
    }

    l.info("got sls type %s loc:%s", type.c_str(), location.c_str());

    if (location.length() > 0) {
        l.info("FILE %s\n", location.c_str());

        if (type == "application/atsc-held+xml") {
            // HACK to ignore stream held if custom one already exist
            char fn[512];
            sprintf(fn, "%s/held-%s.xml", model.storageRoot.c_str(),
                    rsrv->globalId.c_str());
            for (char *c = fn + model.storageRoot.size() + 1; *c; c++) {
                if (*c == '/') *c = '_';
            }
            struct stat st;
            if (::stat(fn, &st) == 0) {
                l.info("Dropped held from stream in favor of custom version");
                return sz;
            }
        }

        auto &sf = rsrv->slsFiles[location];

        // determine version of this signal
        auto sctx = pctx->as<SlsContext>();
        auto vi = sctx->versions.find(location);
        uint32_t ver = 0;
        if (vi != sctx->versions.end()) {
            ver = vi->second;
        } else {
            l.warn("Couldn't determine version of %s", type.c_str());
        }

        if (sf && ver && sf->attributes["version"].asI64() == ver) {
            l.info("Ignored identical version %lu of %s", ver, type.c_str());
            return sz;
        }

        sf = new File();
        sf->type = type;
        sf->attributes["version"] = (int64_t)ver;
        sf->content.writeBytes(data, sz);

        if (type == "application/dash+xml") {
            // HACK to override MPD
            char fn[512];
            sprintf(fn, "%s/mpd-%s.xml", model.storageRoot.c_str(),
                    rsrv->globalId.c_str());
            for (char *c = fn + model.storageRoot.size() + 1; *c; c++) {
                if (*c == '/') *c = '_';
            }
            FILE *fp = fopen(fn, "r");
            if (fp) {
                l.warn("Loading custom MPD %s", fn);
                auto &buf = sf->content;
                buf.clear();
                while (true) {
                    buf.ensureMargin(4096);
                    size_t sz = fread(buf.end(), 1, buf.margin(), fp);
                    if (sz <= 0) break;
                    buf.grow(sz);
                }
            }
        }
    }

    if (strncmp(type.c_str(), "multipart/", 10) == 0) {
        // recurse into multipart content
        MultipartReader mr(this);
        mr.read(data, sz, pctx);
    } else if (type == "application/mbms-envelope+xml") {
        xml_document mbms;
        auto res = mbms.load_buffer(data, sz);
        if (!res) {
            l.warn("Failed to parse MBMS. %s", res.description());
            return sz;
        }

        auto sctx = pctx->as<SlsContext>();
        for (auto item: mbms.document_element().children("item")) {
            sctx->versions[item.attribute("metadataURI").value()] =
                    item.attribute("version").as_int(/*not avail*/0);
        }
    } else if (type == "application/route-s-tsid+xml") {
        rsrv->stsidDocument = new XmlDocument;
        xml_parse_result res = rsrv->stsidDocument->load_buffer(data, sz);
        if (!res) {
            l.info("Failed to parse S-TSID! %s\n", res.description());
            return sz;
        }

        RouteSession *mrs = rsrv->mainSession;
        set<RouteSession*> rsset; // for checking duplicated RS
        for (xml_node rsXml:
                rsrv->stsidDocument->child("S-TSID").children("RS")) {
            xml_attribute sipAttr = rsXml.attribute("sIpAddr");
            xml_attribute dipAttr = rsXml.attribute("dIpAddr");
            xml_attribute dportAttr = rsXml.attribute("dPort");
            uint32_t sip = sipAttr? ip2u(sipAttr.value()): mrs->sourceIp;
            uint32_t dip = dipAttr? ip2u(dipAttr.value()): mrs->destinationIp;
            uint16_t dport = dportAttr? atoi(dportAttr.value()):
                    mrs->destinationPort;

            RouteSession *rs = 0;
            for (auto rsi: rsrv->sessions) {
                if (rsi->sourceIp == sip && rsi->destinationIp == dip &&
                        rsi->destinationPort == dport) {
                    rs = rsi;
                    break;
                }
            }
            if (!rs) {
                rs = new RouteSession(sip, dip, dport);
                rsrv->sessions.push_back(rs);

                // start route session task for newly discovered RS
                RouteSessionTask *rst = new RouteSessionTask(rs, rsrv);
                rs->xmlRsUpdate.addListener(&rst->fdtHandler);
                rst->start();

                self->sessionTasks.push_back(rst);
            }

            if (rsset.find(rs) == rsset.end()) {
                rs->xmlRs = rsXml;
                rs->xmlRsUpdate.notify();
            } else {
                // Multiple RS with the same address was detected.
                // merge into xmlRs
                for (auto ls: rsXml.children("LS")) {
                    rs->xmlRs.append_copy(ls);
                }
                rs->xmlRsUpdate.notify();
            }
            rsset.insert(rs);
        }
    } else if (type == "application/route-usd+xml") {
        self->processUsd(data, sz);
    } else if (type == "application/atsc-held+xml") {
        self->processHeld(data, sz);
    } else if (type == "application/dash+xml") {
        task.eventDriver.setTimeout(0, &self->dashHandler);
    }
    return sz;
}

void RouteServiceTask::processUsd (const byte_t *data, int sz)
{
    RouteService *rsrv = static_cast<RouteService*>(service.get());

    Ref<XmlDocument> doc = new XmlDocument();
    xml_parse_result res = doc->load_buffer(data, sz);
    if (!res) {
        l.warn("Skipped USD. Parsing failed.");
        return;
    }
    xml_node root = doc->document_element();
    if (strcmp(root.name(), "BundleDescriptionROUTE") != 0) {
        l.error("Expected document element as BundleDescriptionROUTE. got %s",
                root.name());
        return;
    }

    xml_node usd = root.child("UserServiceDescription");
    int sid = usd.attribute("serviceId").as_int(-1);
    if (rsrv->id != sid) {
        l.error("Expected USD@serviceId:%d. Got %d", rsrv->id, sid);
        return;
    }

    bool active = usd.attribute("serviceStatus").as_bool(true);
    if (rsrv->active != active) {
        rsrv->active = active;
        model.serviceUpdate.notify(rsrv);
    }

    rsrv->names.clear();
    for (auto name: usd.children("Name")) {
        auto lang = name.attribute("lang").as_string(nullptr);
        if (!lang) continue;
        rsrv->names[lang] = name.text().get();
    }

    rsrv->languages.clear();
    for (auto lang: usd.children("ServiceLanguage")) {
        rsrv->languages.insert(lang.text().get());
    }

    // TODO: Integrate with FileServer
    for (auto dm: usd.children("DeliveryMethod")) {
        for (auto bas: dm.children("BroadcastAppService")) {
            for (auto bp: bas.children("BasePattern")) {
                l.debug("BroadcastAppService basePattern:%s", bp.text().get());
            }
        }
        for (auto uas: dm.children("UnicastAppService")) {
            for (auto bp: uas.children("BasePattern")) {
                l.error("TOOD: UnicastAppService basePattern:%s",
                        bp.text().get());
            }
        }
    }

    rsrv->usdDocument = doc;
    rsrv->usdDocumentUpdate.notify();
}

void RouteServiceTask::processHeld (const byte_t *data, int sz)
{
    RouteService *rsrv = static_cast<RouteService*>(service.get());

    Ref<XmlDocument> doc = new XmlDocument();
    xml_parse_result res = doc->load_buffer(data, sz);
    if (!res) {
        l.warn("Skipped HELD. Parsing failed.");
        return;
    }

    rsrv->heldDocument = doc;
    rsrv->heldDocumentUpdate.notify();
}

void RouteServiceTask::DashHandler::onUpdate ()
{
    auto *self = container_of(this, &RouteServiceTask::dashHandler);
    if (self->playReady) return;
    auto rsrv = static_cast<RouteService*>(self->service.get());
    if (!rsrv->resolvedMpdFile) return; // unlikely.

    l.info("Ready to Play");
    self->playReady = true;
    self->playReadyUpdate.notify();
}

void RouteServiceTask::DashHandler::onUpdate (const acba::Ref<RouteFile> &rf)
{
    if (!rf) return;

    RouteServiceTask *self = container_of(this, &RouteServiceTask::dashHandler);

    // receiveStartTime is determined later
    segments[rf->tsi].last = rf;

    if (!self->playReady) task.eventDriver.setTimeout(0, this);
}

void RouteServiceTask::DashHandler::processEvent (int evt)
{
    RouteServiceTask *self = container_of(this, &RouteServiceTask::dashHandler);
    RouteService *rsrv = static_cast<RouteService*>(self->service.get());

    // Check if mpd is available and elgible for updating
    Ref<File> mpd;
    for (auto i: rsrv->slsFiles) {
        if (i.second->type == "application/dash+xml") {
            mpd = i.second;
            break;
        }
    }
    if (!mpd) {
        l.info("Waiting for MPD...");
        return;
    }
    if (lastMpd && mpd) {
        if (lastMpd == mpd) return;
        if (lastMpd->content == mpd->content) {
            lastMpd = mpd;
            return;
        }
    }

    // Parse mpd.
    Ref<XmlDocument> doc = new XmlDocument();
    xml_parse_result res = doc->load_buffer(mpd->content.begin(),
            mpd->content.size());
    if (!res) {
        l.error("Failed to parse mpd xml");
        return;
    }

    // If broadcast stream is not real-time, tweak mpd file for media playback
    if (FORGE_LIVE_MPD_ALWAYS || model.broadcastStreams[rsrv->bsid]->
                location.compare(0, 5, "pcap:") == 0) {
        // if initial mpd.
        if (!lastMpd && !forgeLiveMpd(doc)) {
            l.info("Need more segments to determine time offset...");
            return;
        }

        assert(!isnan(offset));

        xml_attribute astAttr = doc->child("MPD").
                attribute("availabilityStartTime");
        l.info("Offset mpd@availabilityStartTime %s by %.2lf (%.2lfdays)",
                astAttr.value(), offset, offset / 3600.0 / 24.0);
        double ast = parseDatetime(astAttr.value());
        astAttr.set_value(toDatetime(ast + offset).c_str());
    }

    lastMpd = mpd;
    rsrv->mpdDocument = doc;
    rsrv->mpdDocumentUpdate.notify();

    // TODO: Resolve xlink
    {
        xml_document resolved;
        resolved.reset(*doc);

        // resolve here

        stringstream ss;
        resolved.save(ss);
        auto &rmf = rsrv->resolvedMpdFile;
        rmf = new File();
        rmf->type = "application/dash+xml";
        string s(ss.str());
        rmf->content.writeBytes((const uint8_t*)s.c_str(), s.size());
        rsrv->resolvedMpdFileUpdate.notify();
    }
}

static xml_node findSegmentTemplate (const char *repId, xml_node mpd)
{
    for (auto as: mpd.select_nodes("//AdaptationSet")) {
        xml_node segtemp = as.node().child("SegmentTemplate");
        for (auto rep: as.node().children("Representation")) {
            if (strcmp(rep.attribute("id").value(), repId) != 0) continue;

            xml_node segtemp2 = rep.child("SegmentTemplate");
            return segtemp2 ? segtemp2 : segtemp;
        }
    }

    return xml_node();
}

static xml_node findParent (const char *par, xml_node n)
{
    while (!n || strcmp(n.name(), par) != 0) n = n.parent();
    return n;
}

bool RouteServiceTask::DashHandler::forgeLiveMpd (const Ref<XmlDocument> &doc)
{
    Log l("forgeLiveMpd");

    MpdInfo mi;

    if (mi.read(*doc)) {
        l.warn("Failed to parse mpd");
        return false;
    }
    if (mi.periods.size() == 0) {
        l.warn("No period");
        return false;
    }

    // Try to deduce coherent availability start time
    for (auto &pi: mi.periods) {
        auto dast = deduceAst(pi);
        if (isnan(dast) || dast == 0.0) continue;

        // on success.
        xml_node mpd = doc->child("MPD");
        xml_attribute astAttr = mpd.attribute("availabilityStartTime");
        double ast = parseDatetime(astAttr.value());
        offset = dast - ast;
        return true;
    }

    return false;
}

// AST: availability start time
// Deduced AST: (arrival time of a segment) - (segment start time)
// CDAST: Coherent Deduced Availability Start Time
//
// AST guarantee that a segment is available at certain time.
// In other words, at certain point of time,
// inavailability of a segment means that the AST has not yet arrived,
//
// Thus the deduced AST should be bigger than arriaval time of a segment.
// And the final CDAST should be bigger than every deduced AST of segment
// in each component.
//
// Once CDAST was determined, we may still have to defer playback start until
// all component segments are aligned on time. this may happen because
// we cannot receive all segments ahead of service selection while the AST
// definition allows that.
double RouteServiceTask::DashHandler::deduceAst (PeriodInfo &pi)
{
    RouteServiceTask *self = container_of(this, &RouteServiceTask::dashHandler);
    RouteService *rsrv = static_cast<RouteService*>(self->service.get());

    double cdast = 0.0;
    vector<double> nrsts; // normalized ReceiveStartTimes (-dast)

    for (auto rs: rsrv->sessions) {
        for (xml_node ls: rs->xmlRs.children("LS")) {
            uint32_t tsi = ls.attribute("tsi").as_uint();
            auto repId = ls.child("SrcFlow").child("ContentInfo").
                    child("MediaInfo").attribute("repId").as_string(nullptr);
            if (!repId) {
                l.info("No corresponding representation id found for tsi:%u",
                        tsi);
                continue;
            }

            RepresentationInfo *ri = nullptr;
            for (auto &asi: pi.adaptationSets) {
                for (auto &repi: asi.representations) {
                    if (repi.id == repId) {
                        ri = &repi;
                        break;
                    }
                }
            }
            if (!ri) {
                l.warn("No representation for %s found", repId);
                continue;
            }

#if FORGE_MPD_WITH_AVONLY
            if (ri->mimeType.rfind("video/", 0) != 0 &&
                    ri->mimeType.rfind("audio/", 0) != 0) {
                continue;
            }
#endif

            auto segi = segments.find(tsi);
            if (segi == segments.end()) {
                l.warn("No segment from tsi:%u mimeType:%s", tsi,
                        ri->mimeType.c_str());
                return NAN;
            }
            auto &seg = segi->second;

            if (ri->segmentType ==
                    RepresentationInfo::SEGMENT_TEMPLATE_BY_NUMBER ||
                    ri->segmentType ==
                    RepresentationInfo::SEGMENT_TEMPLATE_BY_TIME) {
                auto st = ri->getSegmentStartTime(seg.last->path);
                if (isnan(st)) return NAN;

                // convert to media time
                st += pi.start;
                if (st < pi.start || st >= pi.end) return NAN;

                // TODO: Consider seg.availabilityTimeOffset?
                auto dast = seg.last->updateTime - st;

                // gather receiveStartTimes
                if (isnan(seg.receiveStartTime)) {
                    seg.receiveStartTime = seg.last->updateTime;
                }
                nrsts.push_back(seg.receiveStartTime - dast);

                l.info("DAST %f for %s. segmentStartTime:%f", dast,
                        seg.last->path.c_str(), st);
                if (dast > cdast) cdast = dast;
            } else {
                l.warn("Unknown segment type %d of %s", ri->segmentType,
                        seg.last->path.c_str());
            }
        }
    }

    // make sure that current time passed every normalized
    // receive start times, otherwise, not ready to start yet.
    if (cdast != 0.0) {
        auto now = currentUnixTime();
        double remain = 0.0;
        for (auto &nrst: nrsts) remain = max(remain, nrst + cdast - now);
        if (remain > 0.0) {
            l.info("Wait %f secs to ensure all segments are on time.", remain);
            task.eventDriver.setTimeout(remain * 1000000, this);
            return NAN;
        }
    }

    return cdast;
}

void RouteServiceTask::EsgProcessor::onUpdate (const Ref<RouteFile> &rf)
{
    if (!rf) return;
    if (rf->type == "application/vnd.oma.bcast.sgdd+xml") {
        processSgdd(rf);
    } else if (rf->type == "application/vnd.oma.bcast.sgdu") {
        processSgdu(rf);
    }
}

void RouteServiceTask::EsgProcessor::processSgdd (const Ref<RouteFile> &rf)
{
    RouteServiceTask *self = container_of(this, &RouteServiceTask::esgProcessor);
    RouteService *rsrv = static_cast<RouteService*>(self->service.get());

    GzipReader gr;
    byte_t *data;
    int len;

    auto tenc = rf->attributes.find("Content-Encoding");
    if (tenc != rf->attributes.end() && tenc->second == "gzip") {
        if (gr.read(rf->content.begin(), rf->content.size(), nullptr)
                != rf->content.size()) {
            l.error("gunzip failed for %s", rf->path.c_str());
            return;
        }
        data = gr.outBuffer.begin();
        len = gr.outBuffer.size();
    } else {
        data = rf->content.begin();
        len = rf->content.size();
    }

    Ref<XmlDocument> doc = new XmlDocument();
    xml_parse_result res = doc->load_buffer(data, len);
    if (!res) {
        l.error("Failed to parse sgdd xml");
        return;
    }

    auto root = doc->child("ServiceGuideDeliveryDescriptor");
    auto idAttr = root.attribute("id");
    if (!idAttr) {
        l.error("Invalid sgdd %s", rf->path.c_str());
        return;
    }
    auto ver = root.attribute("version");

    l.info("Adding sgdd %s version:%s", idAttr.value(), ver.value());
    BroadcastStream *bs = model.broadcastStreams[rsrv->bsid].get();
    bs->sgdds[idAttr.value()] = doc;
}

void RouteServiceTask::EsgProcessor::processSgdu (const Ref<RouteFile> &rf)
{
#define READ_U32(x) (((x)[0] << 24) + ((x)[1] << 16) + ((x)[2] << 8) + (x)[3]);
    RouteServiceTask *self = container_of(this, &RouteServiceTask::esgProcessor);
    RouteService *rsrv = static_cast<RouteService*>(self->service.get());
    BroadcastStream *bs = model.broadcastStreams[rsrv->bsid].get();

    GzipReader gr;
    byte_t *data;
    int len;

    auto tenc = rf->attributes.find("Content-Encoding");
    if (tenc != rf->attributes.end() && tenc->second == "gzip") {
        if (gr.read(rf->content.begin(), rf->content.size(), nullptr)
                != rf->content.size()) {
            l.error("gunzip failed for %s", rf->path.c_str());
            return;
        }
        data = gr.outBuffer.begin();
        len = gr.outBuffer.size();
    } else {
        data = rf->content.begin();
        len = rf->content.size();
    }

    // start parsing
    const byte_t *end = data + len;

    // read sgdu header
    if (data + 9 > end) {
        l.error("Insufficient sgdu head size:%d", len);
        return;
    }
    int extOffset = READ_U32(data);
    int nFrag = (data[6] << 16) + (data[7] << 8) + data[8];
    data += 9; // data[4..5] is reserved.

    // read fragments
    const byte_t *payload = data + nFrag * 12;
    if (payload > end) {
        l.error("Insufficient sgdu fragment head size:%d", len);
        return;
    }
    for (int i = 0; i < nFrag; i++, data += 12) {
        int transId = READ_U32(data);
        uint32_t ver = READ_U32(data + 4);
        int off = READ_U32(data + 8);
        int noff; // next offset
        if (i == nFrag - 1) {
            if (extOffset) {
                noff = extOffset;
            } else {
                noff = end - payload;
            }
        } else {
            noff = READ_U32(data + 12 + 8);
        }
        if (off + 2 > noff || payload + noff > end) {
            l.error("Invalid offset in sgdu fragment head. offset:%d next:%d",
                    off, noff);
            continue;
        }

        // read fragmentEncoding & fragmentType
        byte_t enc = payload[off];
        if (enc != 0) {
            l.error("Expected xml fragment. fragmentEncoding:%d at %d", enc, i);
            continue;
        }
        byte_t type = payload[off + 1];
        off += 2;

        // process fragment
        string ids = rf->attributes["appContextIdList"].asString();
        processFragment(type, transId, ver, payload + off, noff - off, ids);
    }
}

void RouteServiceTask::EsgProcessor::processFragment (byte_t type, int tid,
        uint32_t ver, const byte_t *data, int sz, const string &ids)
{
    RouteServiceTask *self = container_of(this, &RouteServiceTask::esgProcessor);
    RouteService *rsrv = static_cast<RouteService*>(self->service.get());
    BroadcastStream *bs = model.broadcastStreams[rsrv->bsid].get();

    // Search for corresponding fragment slide.
    // skip processing it if already received fragment
    auto &fs = bs->esgFragmentSlideByTransportId[tid];
    if (fs) {
        if ((fs->currentFragment && int(ver - fs->currentVersion) <= 0) ||
                (fs->nextFragment && int(ver - fs->nextVersion) <= 0)) {
            l.info("Dropped out-dated or existing fragment type:%u tid:%d "
                    "ver:%u", type, tid, ver);
            return;
        }
    } else {
        fs = new EsgFragmentSlide(type, tid);
        fs->applicationContextIds = ids;
    }

    // Parse fragment xml
    Ref<XmlDocument> doc = new XmlDocument();
    xml_parse_result res = doc->load_buffer(data, sz);
    if (!res) {
        Buffer buf;
        buf.writeBytes(data, sz);
        buf.writeI8(0);
        l.warn("Failed to parse xml fragment: %s", buf.begin());
        // TODO: Empty fragment slide still remains
        return;
    }

    // check root element.
    xml_node root = doc->document_element();
    const char *rootName = 0;
    switch (type) {
    case 1: // service fragment
        rootName = "Service";
        break;
    case 2: // content fragment
        rootName = "Content";
        break;
    case 3: // schedule fragment
        rootName = "Schedule";
        break;
    }
    if (!rootName || strcmp(root.name(), rootName) != 0 || fs->type != type) {
        l.warn("Detected fragment type inconsistency. expectedType:%u "
                "receivedType:%u receivedXml:%s", fs->type, type, root.name());
        return;
    }

    // check id
    const char *id = root.attribute("id").as_string(0);
    if (!id) {
        l.warn("Fragment doesn't have attribute id");
        return;
    }
    bs->esgFragmentSlides[id] = fs;

    // check version
    xml_attribute verAttr = root.attribute("version");
    if (!verAttr || verAttr.as_uint() != ver) {
        l.warn("Fragment has inconsistent version info. expected:%u attr:%s",
                ver, verAttr.value());
        return;
    }

    // check validity
    double now = currentUnixTime();
    bool valid = true;

    // TODO: Need to handle validFrom/To from SGDD.
    const char *validFrom = root.attribute("validFrom").as_string(0);
    const char *validTo = root.attribute("validTo").as_string(0);
    double vf;
    if (validFrom) {
        vf = parseDatetime(validFrom);
        if (now < vf) valid = false;
    }
    if (validTo) {
        double vt = parseDatetime(validTo);
        if (now > vf) {
            valid = false;
            l.warn("Dropped out-dated(validTo) %s fragment %s", rootName, id);
            return;
        }
    }

    // no next or newer than next
    if (valid) {
        ESG_FRAGMENT_EVENT evt = ESG_FRAGMENT_UPDATED;
        if (!fs->currentFragment) evt = ESG_FRAGMENT_ADDED;
        fs->currentFragment = doc;
        fs->currentVersion = ver;
        fs->checkTime = std::numeric_limits<double>::infinity();
        fs->nextFragment = nullptr;
        l.info("Update fragment %s ver:%u", id, ver);
        model.esgFragmentUpdate.notify(evt, id, fs, bs);
    } else {
        fs->nextFragment = doc;
        assert(validFrom); // valid must be false because of validFrom
        fs->checkTime = vf;
        l.info("Prepare fragment %s ver:%u", id, ver);
    }
}
