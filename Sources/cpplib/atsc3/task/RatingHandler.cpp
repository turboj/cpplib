#include <sstream>
#include <map>

#include "atsc3_task.h"
#include "datetime.h"
#include "rating_handler.h"
#include "pugixml.hpp"
#include "acba.h"

using namespace std;
using namespace acba;
using namespace atsc3;
using namespace pugi;

static Log l("ratingHdl");

static Ref<XmlDocument> _findFragments (const Ref<Service> &srv)
{
    for (auto bs: model.broadcastStreams) {
        for (auto fs: bs.second->esgFragmentSlides) {
            if (!fs.second || fs.second->type != 1 ||
                    !fs.second->currentFragment) continue;

            XmlDocument *xml = fs.second->currentFragment.get();
            xml_node s = xml->child("Service");

            if (!s)
                continue;
            const char* globalServiceID =
                    s.attribute("globalServiceID").as_string("");

            if (strcmp(srv->globalId.c_str(), globalServiceID) == 0) {
                return fs.second->currentFragment;
            }
        }
    }
    return nullptr;
}
static int64_t _getNextScheduleTime (const Ref<Service> &srv)
{
    Ref<XmlDocument> serviceFragment = _findFragments(srv);

    if (!serviceFragment)
        return 0;

    int64_t dur = 0;
    string id =  serviceFragment->child("Service").attribute("id").value();
    int64_t now = currentUnixTime() + 2208988800; //adjust to NTP

    // find ScheduleFragment
    for (auto bs: model.broadcastStreams) {
        for (auto fs: bs.second->esgFragmentSlides) {
            if (!fs.second || fs.second->type != 3 ||
                    !fs.second->currentFragment) continue;

            XmlDocument *xml = fs.second->currentFragment.get();
            xml_node sch = xml->child("Schedule");
            if (!sch)
                continue;
            if (sch.child("ServiceReference")) {
                string sfid = sch.child("ServiceReference").attribute("idRef").value();
                if (strcmp(id.c_str(), sfid.c_str()) == 0) {

                    for (auto cr: sch.children("ContentReference")) {
                        for (auto pw: cr.children("PresentationWindow")) {
                            int64_t start = pw.attribute("startTime").as_ullong(0);
                            int64_t end = pw.attribute("endTime").as_ullong(0);

                            if (start <= now && end > now) {
                                string cid = cr.attribute("idRef").value();
                                dur = end - now;
                                break;
                            }
                        }
                    }
                    break;
                }
            }
        }
    }

    return dur;
}
static Ref<XmlDocument> _findContentFragment (const Ref<XmlDocument> &serviceFragment)
{
    string id =  serviceFragment->child("Service").attribute("id").value();
    int64_t now = currentUnixTime() + 2208988800; //adjust to NTP

    // find ScheduleFragment
    for (auto bs: model.broadcastStreams) {
        for (auto fs: bs.second->esgFragmentSlides) {
            if (!fs.second || fs.second->type != 3 ||
                    !fs.second->currentFragment) continue;

            XmlDocument *xml = fs.second->currentFragment.get();
            xml_node sch = xml->child("Schedule");
            if (!sch)
                continue;
            if (sch.child("ServiceReference")) {
                string sfid = sch.child("ServiceReference").attribute("idRef").value();
                if (strcmp(id.c_str(), sfid.c_str()) == 0) {
                    for (auto cr: sch.children("ContentReference")) {
                        for (auto pw: cr.children("PresentationWindow")) {
                            int64_t start = pw.attribute("startTime").as_ullong(0);
                            int64_t end = pw.attribute("endTime").as_ullong(0);

                            if (start <= now && end > now) {
                                string cid = cr.attribute("idRef").value();
                                return bs.second->esgFragmentSlides[cid]->currentFragment;
                            }
                        }
                    }
                }
            }
        }
    }

    return nullptr;
}

static void _findAdvisoryRatings (vector<std::string> &ratings,
        const Ref<XmlDocument> &serviceFrgmnt, const Ref<XmlDocument> &contentFrgmnt)
{
    xml_node node;
    bool foundCA = false;

    if (contentFrgmnt) {
        node = contentFrgmnt->child("Content");
        for (xml_node caRatings: node.children("ContentAdvisoryRatings")) {
            foundCA = true;
            break;
        }
    }

    if (!foundCA && serviceFrgmnt) {
        node = serviceFrgmnt->child("Service");
    } else if (!foundCA){
        return;
    }

    if (contentFrgmnt) {
        stringstream ss;
        for (xml_node caRatings: node.children("ContentAdvisoryRatings")) {
            /**
             * "$RegionIdentifier, '$RatingDescription', {$RatingDimension
             *  '$RatingValueString'}{..}"
             */
            const string regionId = caRatings.child("RegionIdentifier").text().get();
            if (!regionId.empty()) ss << regionId;
            else ss << "1";

            const string rDesc = caRatings.child("RatingDescription").text().get();
            ss << ", '" << rDesc << "', ";
            for (auto rDimV: caRatings.children("RatingDimVal")) {
                const string rDimension = rDimV.child("RatingDimension").text().get();
                if (!rDimension.empty()) ss << "{" << rDimension;
                else ss << "{" << "0";
                const string rValueStr = rDimV.child("RatingValueString").text().get();
                ss << " '" << rValueStr << "'}";
            }

            ratings.push_back(ss.str());
            l.info("Service ContentAdvisoryRatings %s", ss.str().c_str());
        }
    }
}

static map<std::string, int> ageMap = {
    // region 1
    {"TV-Y", 0},
    {"TV-Y7", 7},
    {"TV-G", 8},
    {"TV-PG", 10},
    {"TV-14", 14},
    {"TV-MA", 17},
    // region 2
    {"G", 0},
    {"PG", 7},
    {"13+", 13},
    {"14+", 14},
    {"14A", 14},
    {"16+", 16},
    {"18+", 18},
    {"R", 19},
    {"A", 19},
};
static int _getAgeOfRating (const std::string &region, const std::string &str)
{
    if (strcmp(region.c_str(), "1") == 0 || strcmp(region.c_str(), "2") == 0) {
        auto age = ageMap.find(str);
        if (age == ageMap.end()) return 0;
        return age->second;
    } else
        return 0;
}

static void trim (std::string& str)
{
    str.erase(0, str.find_first_not_of(' '));       //prefixing spaces
    str.erase(str.find_last_not_of(' ')+1);         //surfixing spaces
}

static void _getRatingFromFormattedString(string& ratingStr,
                            string& ratingR1Value,
                            string& ratingR2Value) {
    //e.g.  " 1,'TV-PG',{0 'TV-PG'},2,'PG(en)/Pour tous(fr)',{0 'PG'}{1 'Pour tous'}"
    string dimension;
    string region;
    string str;
    int lf;
    while (!ratingStr.empty()) {
        lf = ratingStr.find("\n");
        if (lf == -1) str = ratingStr;
        else {
            str = ratingStr.substr(0, lf);
            if (str.empty())
                break;
            ratingStr = ratingStr.substr(lf+1);
        }

        trim(str);
        region = str.substr(0,1);
        for (int f = str.find("{"); f>-1; ) {
            bool parsedRegion = false;
            str = str.substr(f+1);
            trim(str);
            dimension = str.substr(0, 1);
            if (region == "1" && dimension == "0") {
                //l.info("region %s dimension %s", region.c_str(), dimension.c_str());
                ratingR1Value = str.substr(str.find("'")+1);
                ratingR1Value = ratingR1Value.substr(0, ratingR1Value.find("'"));
                parsedRegion = true;
            } else if (region == "2" && dimension == "0") {
                ratingR2Value = str.substr(str.find("'")+1);
                ratingR2Value = ratingR2Value.substr(0, ratingR2Value.find("'"));
                parsedRegion = true;
            }
            if (parsedRegion) {
                f = str.find(","); // find next region
                if (f>-1) {
                    str = str.substr(f+1); // 2,'PG.....',{...}
                    trim(str);
                    region = str.substr(0,1);
                } else
                    break;
            }
            f = str.find("{");
        }
        if (lf == -1)
            break;
    }
}

/**
 * return true if contentAdvisory rating is higher then system preferrence or false
 *
 * Rating Region: 1-US, 2-Canada
 *
 * Region 1
 * Rating Dimension: classify a content labels
 *  0: Age
 *  1: Suggestive dialogue(usually means talks about sex)
 *  2: Coarse or crude language
 *  3: Sexual situations
 *  4: Violence
 *  5??: Fantasy violence(children's programming only)
 * ['TV-Y'(all), 'TV-Y7'(7+), 'TV-G'(all), 'TV-PG', 'TV-14'(14+), 'TV-MA'(17+)]
 * e.g.: "1,'TV-PG D-L-S-V',{0 'TV-PG'}{1 'D'}{2 'L'}{3 'S'}{4 'V'}"
 *
 * Region 2
 * Rating Dimension: Canadian English or French rating
 *  0: Canadian English Rating
 *  1: Canadian French Rating
 * Canada['G'(all), 'PG', '14A'(14+), '18A'(18+), 'R'(Restricted), 'A'(Adult)]
 * Quebec['G', '13+', '16+', '18+']
 * e.g.: "2,'13+(fr)/14+(en)',{0 '14+'}{1 '13+'}"
 */
static bool _checkBlockByRating (const string &rating)
{
    string systemCARating;
    model.preference.get("rating", systemCARating);
    if (!rating.empty() && strcmp(systemCARating.c_str(), rating.c_str()) != 0) {
        string str = rating;
        string contentRatingR1Value;
        string contentRatingR2Value;
        _getRatingFromFormattedString(str, contentRatingR1Value,
                contentRatingR2Value);

        l.info("content rating: region 1 rating: %s", contentRatingR1Value.c_str());
        l.info("content rating: region 2 rating: %s", contentRatingR2Value.c_str());

        // read tv rating from system
        bool hasRegion1 = false;
        bool hasRegion2 = false;
        str = systemCARating;
        trim(str);
        string systemRatingR1Value;
        string systemRatingR2Value;
        _getRatingFromFormattedString(str, systemRatingR1Value,
                systemRatingR2Value);

        if (!contentRatingR1Value.empty())
            hasRegion1 = true;
        if (!contentRatingR2Value.empty())
            hasRegion2 = true;

        l.info("system rating: region 1 rating: %s %d", systemRatingR1Value.c_str(),
                _getAgeOfRating("1", systemRatingR1Value));
        l.info("system rating: region 2 rating: %s %d", systemRatingR2Value.c_str(),
                _getAgeOfRating("1", systemRatingR2Value));

        //check block
        if ((hasRegion1 &&
                    (_getAgeOfRating("1", systemRatingR1Value) <
                    _getAgeOfRating("1", contentRatingR1Value))) ||
           (hasRegion2 &&
                    (_getAgeOfRating("2", systemRatingR2Value) <
                    _getAgeOfRating("2", contentRatingR2Value))) ) {

            return true;
        } else if ((hasRegion1 &&
                        (_getAgeOfRating("1", systemRatingR1Value) >=
                        _getAgeOfRating("1", contentRatingR1Value))) ||
                   (hasRegion2 &&
                        (_getAgeOfRating("2", systemRatingR2Value) >=
                        _getAgeOfRating("2", contentRatingR2Value))) ) {

            return false;
        }
    } else if (rating.empty()) {
        l.info(("ContentAdvisoryRating is empty"));
    }
    return false;
}

static void _changeBlockStatusWithRating (const string &rating)
{
    if (task.ratingHandler.blocked &&  !_checkBlockByRating(rating)) {
        l.info(("Service will be unblocked by ContentAdvisoryRating"));
        task.ratingHandler.blocked = false;
        task.ratingHandler.blockedByRatingUpdate.notify(false);
    } else if (!task.ratingHandler.blocked && _checkBlockByRating(rating)) {
        l.info(("Service will be blocked by ContentAdvisoryRating"));
        task.ratingHandler.blocked = true;
        task.ratingHandler.blockedByRatingUpdate.notify(true);
    }
}

void RatingHandler::retrieveContentAdvisoryRating (const Ref<Service> &srv, string &rating)
{
    Ref<XmlDocument> serviceFragment = _findFragments(srv);
    if (serviceFragment) {
        Ref<XmlDocument> contentFrgmnt = _findContentFragment(serviceFragment);
        vector<std::string> ratings;
        _findAdvisoryRatings(ratings, serviceFragment, contentFrgmnt);

        // retrieve contentAdvisoryRating
        // 1. from Content
        // 2. if Service is the current service, search from Service: Route or MMTP
        stringstream ss;
        if (ratings.size() == 0 && model.currentService &&
                strcmp(model.currentService.get()->globalId.c_str(),
                        srv->globalId.c_str()) == 0) {
            //In case of MMTP, get ratings from usbd Route. or from MPD.
            if (model.currentService.get()->protocol == 1) {
                if (!task.ratingHandler.routeMpdRatingOfCurrentService.empty())
                    ss << task.ratingHandler.routeMpdRatingOfCurrentService << '\n';
            } else if (model.currentService.get()->protocol == 2) {
                auto mmtpSrvTask = static_cast<MmtpServiceTask*>
                        (task.serviceManager.currentServiceTask.get());

                for (string rating: mmtpSrvTask->caRatingInUsbd) {
                    ss << rating << '\n';
                }
            }
        } else {
            for (string rt: ratings) {
                ss << rt << '\n';
            }
        }
        // test
        //ss << " 1, 'TV-PG D-L-S-V', {0 'TV-PG'}{1 'D'}{2 'L'}{3 'S'}{4 'V'}";
        rating = ss.str();
        l.info("retrieveContentAdvisoryRating rating %s", rating.c_str());
    }
}
void RatingHandler::ServiceChangeListener::onUpdate ()
{
    task.eventDriver.setTimeout(500, this);
}
void RatingHandler::ServiceChangeListener::processEvent (int evt)
{
    task.ratingHandler.routeMpdRatingOfCurrentService = "";
    if (!model.currentService) {
        l.info("currentService is null");
        return;
    }

    string rating;
    task.ratingHandler.retrieveContentAdvisoryRating(model.currentService, rating);
    _changeBlockStatusWithRating(rating);
    int64_t dur = _getNextScheduleTime(model.currentService);
    if (dur > 0) {
        task.eventDriver.setTimeout(dur*1000000, &task.ratingHandler.esgUpdateListener);
    }
}

void RatingHandler::EsgUpdateListener::onUpdate (
            ESG_FRAGMENT_EVENT evt, const std::string &fid,
            const acba::Ref<EsgFragmentSlide>& fs,
            const acba::Ref<BroadcastStream>& bs)
{
    task.eventDriver.setTimeout(500, this);
}

void RatingHandler::EsgUpdateListener::processEvent (int evt)
{
    l.info("EsgUpdateListener::processEvent");
    if (!model.currentService) {
        l.info("currentService is null");
        return;
    }
    string rating;
    task.ratingHandler.retrieveContentAdvisoryRating(model.currentService, rating);
    _changeBlockStatusWithRating(rating);
    int64_t dur = _getNextScheduleTime(model.currentService);
    if (dur > 0) {
        task.eventDriver.setTimeout(dur*1000000, this);
    }
}

void RatingHandler::PreferenceChangeListener::onUpdate (const string &key)
{
    if (key == "rating") {
        task.eventDriver.setTimeout(500, &task.ratingHandler.esgUpdateListener);
    }
}

static string periodId;

void RatingHandler::RMPPeriodChangeListener::onUpdate (const string &id)
{
    periodId = id;
    task.eventDriver.setTimeout(500, this);
}

void RatingHandler::RMPPeriodChangeListener::processEvent (int evt)
{
    if (task.rmp.isSignaling != true)
        return;

    // read Period from MPD
    if (task.serviceManager.currentServiceTask) {
        auto routeSrvTask = static_cast<RouteServiceTask*>
                    (task.serviceManager.currentServiceTask.get());
        RouteService *rsrv = static_cast<RouteService*>(routeSrvTask->service.get());
        if (!rsrv->resolvedMpdFile) {
           l.error("mpd is not available");
            return;
        }
        acba::Buffer &b = rsrv->resolvedMpdFile->content;

        xml_document doc;
        xml_parse_result res = doc.load_buffer(b.begin(), b.size());
        if (!res) {
            l.error("Failed to parse mpd xml");
            return;
        }

        xml_node mpd = doc.child("MPD");

        // find Period with period ID
        for (auto period: mpd.children("Period")) {
            if (strcmp(period.attribute("id").value(), periodId.c_str()) != 0)
                continue;
            for (auto as: period.children("AdaptationSet")) {
                xml_node rating = as.child("Rating");
                if (rating) {
                    task.ratingHandler.routeMpdRatingOfCurrentService =
                            rating.attribute("value").value();
                    l.info("update routeMpdRating %s",
                            task.ratingHandler.routeMpdRatingOfCurrentService.c_str());
                    string rating;
                    task.ratingHandler.retrieveContentAdvisoryRating(
                            model.currentService, rating);
                    _changeBlockStatusWithRating(rating);
                    break;
                }
            }
        }
    }
}
