#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <fcntl.h>
#include <errno.h>
#include <string.h>
#include <time.h>

#include <string>
#include <algorithm>
#include <map>

#include "stream_reader.h"
#include "pcap_reader.h"
#include "ip_reader.h"
#include "udp_reader.h"
#include "atsc3_task.h"
#include "tune_manager.h"
#include "EventDriver.h"
#include "acba.h"
#include "acba_crc32.h"

using namespace std;
using namespace acba;
using namespace atsc3;

static Log l("atsc3tune");

struct Atsc3Tune: Tune {

    TuneContext tuneContext;
    IpReader ipReader;
    UdpReader udpReader;
    atsc3_Tuner *tuner;
    int lmtCrc32;
    StreamReaderRef consumer;

    Log l;

    Atsc3Tune (atsc3_Tuner *t, const string &loc, const StreamReaderRef &cons):
            Tune(loc), tuner(t), l("atsc3tune", loc+" "), ipReader(&udpReader),
            udpReader(cons.get()), lmtCrc32(0), consumer(cons) {
        tuneContext.tune = this;
    }

    bool start () {
        atsc3_TuneParams params;
        params.frequency = atoi(location.c_str() + 6);
        params.callback = onTunerCallback0;
        params.context = this;

        bool ret = task.platform->startTune(tuner, &params);
        if (ret) ref();

        return ret;
    }

    void stop () {
        task.platform->stopTune(tuner);
    }

    void setRoutingTable (const acba::Ref<RoutingTable> &rt) override {
        vector<atsc3_MulticastAddress> addrs;
        for (auto &rei: rt->entries) {
            atsc3_MulticastAddress addr;
            addr.sourceIp = rei.second.sourceIp;
            addr.sourcePort = 0;
            addr.destinationIp = rei.first >> 32;
            addr.destinationPort = (rei.first >> 16) & 0xffff;
            addrs.push_back(addr);
        }

        task.platform->setMulticasts(tuner, addrs.data(), addrs.size());
    }

    static void onTunerCallback0 (atsc3_CALLBACK_TYPE type, const byte_t *data,
            int sz, void *appData) {
        auto t = (Atsc3Tune*)appData;
        t->onTunerCallback(type, data, sz);
    }

    void onTunerCallback (atsc3_CALLBACK_TYPE type, const byte_t *data, int sz)
            {
        l.trace("onTunerCallback %d", type);

        switch (type) {
        case atsc3_CALLBACK_TUNER_LOCKED:
            {
                ScopedLock sl(mutex);
                if (locked) break;
                locked = true;
                update.notify(EVENT_TUNER_LOCKED);
            }
            break;

        case atsc3_CALLBACK_TUNER_UNLOCKED:
            {
                ScopedLock sl(mutex);
                if (!locked) break;
                locked = false;
                update.notify(EVENT_TUNER_UNLOCKED);
            }
            break;

        case atsc3_CALLBACK_IP:
            //l.info("send ip. %d", sz);
            ipReader.read(data, sz, &tuneContext);
            break;

        case atsc3_CALLBACK_LMT:
            parseLmt(data, sz);
            break;

        case atsc3_CALLBACK_END:
            deref();
            break;

        default:
            l.warn("Unsupported tuner callback %d", type);
            break;
        }
    }

#define SAFEREAD_LOG(args...) l.warn("expected " args)
#define SAFEREAD_RETURN return

    void parseLmt (const byte_t *data, int sz) {
        int h = (int)acba_crc32(0, data, sz);
        if (lmtCrc32 == h) {
            l.info("Ignored the same LMT sz:%d", sz);
            return;
        }
        lmtCrc32 = h;

        l.info("Got new LMT sz:%d", sz);

        const byte_t *d = data;
        const byte_t *e = data + sz;

        byte_t nplpm1 = SAFEREAD_U8(d, e, "num_PLPs_minus1") >> 2;
        l.info("    number of plps:%u", nplpm1 + 1);
        for (int i = 0; i <= nplpm1; i++) {
            byte_t plpid = SAFEREAD_U8(d, e, "PLP_ID") >> 2;
            byte_t nmcast = SAFEREAD_U8(d, e, "num_multicasts");
            l.info("    plp-%u has %u multicast(s)", plpid, nmcast);
            for (int j = 0; j < nmcast; j++) {
                uint32_t srcip = SAFEREAD_U32(d, e, "src_IP_add");
                uint32_t dstip = SAFEREAD_U32(d, e, "dst_IP_add");
                uint32_t srcport = SAFEREAD_U16(d, e, "src_IP_add");
                uint32_t dstport = SAFEREAD_U16(d, e, "dst_IP_add");
                byte_t flags = SAFEREAD_U8(d, e, "flags");
                if (flags & 0x80) SAFEREAD_SKIP(d, e, 1, "SID");
                if (flags & 0x40) SAFEREAD_SKIP(d, e, 1, "context_id");
                l.info("        %08x:%u -> %08x:%u", srcip, srcport, dstip,
                        dstport);
            }
        }
    }
};

struct Atsc3TuneProvider: TuneProvider {

    set<atsc3_Tuner*> tunersInUse;

    Atsc3TuneProvider (): TuneProvider("atsc3") {}

    Tune *create (const std::string &loc, const StreamReaderRef &cons)
            override {

        // update tuner list
        atsc3_Tuner *tuners[10];
        int nTuners = sizeof(tuners) / sizeof(tuners[0]);
        task.platform->getTuners(tuners, &nTuners);

        for (int i = 0; i < nTuners; i++) {
            auto t = tuners[i];
            if (tunersInUse.count(t) == 1) continue;

            // try tune
            auto at = new Atsc3Tune(t, loc, cons);
            if (!at->start()) {
                delete at;
                continue;
            }

            tunersInUse.insert(t);
            return at;
        }

        return nullptr;
    }

    void destroy (const acba::Ref<Tune> &t) override {
        auto at = static_cast<Atsc3Tune*>(t.get());
        assert(tunersInUse.count(at->tuner) == 1);

        at->stop();
        tunersInUse.erase(at->tuner);
    }
};

void Atsc3TuneProvider_init ()
{
    task.tuneManager.registerTuneProvider(new Atsc3TuneProvider());
}
