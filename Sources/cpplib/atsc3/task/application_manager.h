#ifndef APPLICATION_MANAGER_H
#define APPLICATION_MANAGER_H

#include <vector>
#include <map>
#include <string>
#include <set>

#include "atsc3_model.h"
#include "atsc3_platform.h"
#include "acba.h"

namespace atsc3 {

struct WebWindow: acba::RefCounter<WebWindow> {

    WebWindow ();

    void open (const std::string &url, int zOrder = 0);
    void close ();

    void bindKeys (std::set<std::string> &keys);
    void unbindKeys (std::set<std::string> &keys);

    atsc3_WebWindow *peer;
    std::string privateId;
};

// Controls a single application instance.
// This task may span across multiple services if
// they retain the equivalent HELD information.
struct ApplicationTask: acba::RefCounter<ApplicationTask> {

    std::string entryPackage;
    std::string url;
    acba::Ref<ApplicationContextCache> contextCache;

    acba::Ref<WebWindow> webWindow;

    acba::Ref<ServiceUsage> serviceUsage;
    std::string id; // used for usage report

    void start ();
    void stop ();

    // Check if the given route file is valid package eligible for updating
    // current context cache, and then add it into the cache.
    void tryAddPackage (const acba::Ref<RouteFile> &f);

    // In case of broadcast app, check if entry package exists.
    // and then create web window to run web application.
    void tryCreateWebWindow ();
};

struct KeyBinding {
    KeyBinding (): flags(0), applicationBound(false) {}
    std::string name; // either descrete key name or group name
    std::vector<int> indexes;
    std::vector<const atsc3_KeyInfo*> infos;
    int flags; // aggregated flags from atsc3_KeyInfo
    bool applicationBound;
};

enum CACHE_REQ_TYPE {
    CACHE_URL_LIST,
    CACHE_FROM_PERIOD,
    CACHE_FROM_MPD
};

struct CacheRequestInfo {

/*
 * Constructor for cache req with url list(org.atsc.CacheRequest API)
 * src_url : sourceURL param of API or ""(=no download, only cache check)
 * tgt_url : targetURL param of API or ""(=root dir of app cache)
 * urls : URLs param of API in std::vector<std::string>
 */
    CacheRequestInfo(std::string &src_url, std::string &tgt_url,
            std::vector<std::string> &urls) : type(CACHE_URL_LIST),
            sourceUrl(src_url), targetUrl(tgt_url), urlList(urls) {}

/*
 * Constructor for cache req with mpd or period(org.atsc.CacheRequestDASH API)
 * src_url : sourceURL param of API or ""(=no download, only cache check)
 * tgt_url : targetURL param of API or ""(=root dir of app cache)
 * dash : Period param or mpdFileName param of API
 * isMpd : pass true when dash is mpdFileName, false when dash is Period
 */
    CacheRequestInfo(std::string &src_url, std::string &tgt_url,
            std::string dash, bool isMpd) :
            type(isMpd?CACHE_FROM_MPD:CACHE_FROM_PERIOD), sourceUrl(src_url),
            targetUrl(tgt_url), dashSrc(dash) {}

    CACHE_REQ_TYPE type; // cache reques type. set by constructor
    std::string sourceUrl; // sourceURL to download from. set by constructor
    std::string targetUrl; // targetURL to store to. path inside app cache. set by constructor
    std::vector<std::string> urlList; // only valid with CACHE_URL_LIST type
    std::string dashSrc; // only valid with CACHE_FROM_PERIOD or CACHE_FROM_MPD type. Period string or MPD file name
};

struct ApplicationManager {

    acba::Ref<WebWindow> systemWebWindow;

    acba::Ref<RouteService> currentService; // TODO: Mmtp HELD
    pugi::xml_node entryPackageXml;
    acba::Ref<XmlDocument> heldDocument;
    acba::Ref<ApplicationTask> applicationTask;
    acba::Update<> applicationTaskUpdate;

    std::map<const std::string, KeyBinding> keyBindings;
    bool overridesAllKeys;

    ApplicationManager (): overridesAllKeys(false) {}

    void init ();
    void startSystemUi (const std::string &url);
    ApplicationContextCache *getContextCache (const char *cid);
    void setKeyOverride (bool ovr); // Route all keys to system ui if true.
    // Returns 0 if not cached
    // 1 if cached, -15 if sourceURL or targetURL is invalid
    // -16 if URLs is invalid, -17 if DASH Period is invalid,
    // or -18 if MPD is not found
    int processCache(const CacheRequestInfo &reqInfo);

    void processHeld ();
    pugi::xml_node findBestEntryPackage (double now); // aux
    void setTimeout (pugi::xml_attribute validUntil, double now); // aux

    struct Selection: acba::Listener<>, acba::Reactor {
        void onUpdate () override;
        void processEvent (int evt) override;
    } selection;

    struct HeldListener: acba::Listener<>, acba::Reactor {
        void onUpdate () override;
        void processEvent (int evt) override;
    } heldListener;

    struct PackageListener: acba::Listener<const acba::Ref<RouteFile>&> {
        void onUpdate (const acba::Ref<RouteFile> &f) override;
    } packageListener;
};

}
#endif
