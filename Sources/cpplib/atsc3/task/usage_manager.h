#ifndef USAGE_REPORTER_H
#define USAGE_REPORTER_H

#include <string>
#include <list>

#include "nlohmann/json.hpp"
#include "acba.h"

using json = nlohmann::json;
namespace atsc3 {

struct ServiceUsage: acba::RefCounter<ServiceUsage> {

protected:
    ServiceUsage () {}

public:
    virtual ~ServiceUsage () {}

    virtual void setStopTime (double endTime) = 0;

    virtual void startBroadcast (double startTime, float speed,
            double receiverStartTime) = 0;

    virtual void stopBroadcast (double endTime) = 0;

    virtual void startComponent (const char *componentID,
            const char *componentType, const char *componentRole,
            const char *componentName, const char *componentLang,
            double startTime) = 0;

    virtual void stopComponent (const char *componentID, double endTime) = 0;

    virtual void startSourceDeliveryPath (const char *componentID,
            const char *type, double startTime) = 0;

    virtual void stopSourceDeliveryPath (const char *componentID,
            double endTime) = 0;

    virtual void startApplication (const char *appID, double startTime,
            const char *LifeCycle, const char *Tags) = 0;

    virtual void stopApplication (const char *appID, double endTime) = 0;
};

struct UsageManager {

    static void setDeviceInfo (const char *deviceID, const char *deviceModel,
            const char *deviceManufacturer, const char *deviceOS,
            const char *peripheralDevice, const char *latitude,
            const char *longitude, const char *clockSource);

    // Should get instance using acba::Ref<ServiceUsageReport>
    static ServiceUsage *createServiceUsage (const char *country,
            const char *bsid, const char *serviceID,
            const char *globalServiceID, const char *serviceType,
            const double startTime, const char *DestinationDeviceType,
            const char *contentType, const char *contentCid);

};

}
#endif

