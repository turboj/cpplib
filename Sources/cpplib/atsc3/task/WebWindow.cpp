#include <stdlib.h>
#include <stdio.h>

#include <set>
#include <vector>

#include "atsc3_task.h"
#include "application_manager.h"
#include "acba.h"

using namespace std;
using namespace atsc3;
using namespace acba;

static Log l("ww");

WebWindow::WebWindow (): peer(nullptr)
{
    // set privateId
    FILE *fp = fopen("/dev/urandom", "r");
    assert(fp);
    privateId.reserve(256);
    for (int i = 0; i < 256; i += 8) {
        uint32_t j;
        if (fread(&j, sizeof(j), 1, fp) != 1) j = (uint32_t)rand();
        char s[9];
        sprintf(s, "%08x", j);
        privateId += s;
    }
    fclose(fp);
}

void WebWindow::open (const string &url, int zOrder)
{
    auto &am = task.applicationManager;

    if (peer) close();
    peer = task.platform->createWebWindow(url.c_str(), zOrder);

    if (this == am.systemWebWindow) return;

    // Give app default keys to the new window
    vector<int> keys;
    for (auto kbi: am.keyBindings) {
        KeyBinding &kb = kbi.second;
        assert(!kb.applicationBound);
        if (kb.flags & atsc3_KEYFLAG_APPLICATION_DEFAULT) {
            l.info("Granting app default key %s", kb.name.c_str());
            keys.insert(keys.end(), kb.indexes.begin(), kb.indexes.end());
        }
    }
    if (am.overridesAllKeys) return; // abort if keys are overridden
    if (keys.size() > 0) {
        task.platform->routeKeys(keys.data(), keys.size(), peer);
    }
}

void WebWindow::close ()
{
    auto &am = task.applicationManager;

    if (!peer) return;

    // Clean-up key bindings first.
    vector<int> keys;
    for (auto kbi: am.keyBindings) {
        KeyBinding &kb = kbi.second;
        if (kb.flags & atsc3_KEYFLAG_APPLICATION_DEFAULT ||
                kb.applicationBound) {
            l.info("Releasing app default(or still bound) key %s",
                    kb.name.c_str());
            kb.applicationBound = false;
            keys.insert(keys.end(), kb.indexes.begin(), kb.indexes.end());
        }
    }
    if (!am.overridesAllKeys && keys.size() > 0) {
        // give keys back to system web window
        if (am.systemWebWindow && this != am.systemWebWindow) {
            task.platform->routeKeys(keys.data(), keys.size(),
                am.systemWebWindow->peer);
        } else {
            // in case system web window doesn't exist
            task.platform->routeKeys(keys.data(), keys.size(), nullptr);
        }
    }

    task.platform->destroyWebWindow(peer);
    peer = nullptr;
}

void WebWindow::bindKeys (set<string> &keys)
{
    auto &am = task.applicationManager;

    set<string> reqkeys;
    reqkeys.swap(keys);
    vector<int> granted;

    for (auto k: reqkeys) {
        auto kbi = am.keyBindings.find(k);
        if (kbi == am.keyBindings.end()) continue;
        auto &kb = kbi->second;

        if (kb.flags & atsc3_KEYFLAG_SYSTEM_ONLY) continue;
        if (kb.applicationBound) continue;

        l.info("Request key %s", k.c_str());
        kb.applicationBound = true;
        keys.insert(k);
        granted.insert(granted.end(), kb.indexes.begin(), kb.indexes.end());
    }

    if (task.applicationManager.overridesAllKeys) return;
    if (granted.size() > 0) {
        task.platform->routeKeys(granted.data(), granted.size(), peer);
    }
}

void WebWindow::unbindKeys (set<string> &keys)
{
    auto &am = task.applicationManager;

    set<string> reqkeys;
    reqkeys.swap(keys);
    vector<int> granted;

    for (auto k: reqkeys) {
        auto kbi = am.keyBindings.find(k);
        if (kbi == am.keyBindings.end()) continue;
        auto &kb = kbi->second;

        if (!kb.applicationBound) continue;

        l.info("Reliquish key %s", k.c_str());
        kb.applicationBound = false;
        keys.insert(k);
        granted.insert(granted.end(), kb.indexes.begin(), kb.indexes.end());
    }

    if (task.applicationManager.overridesAllKeys) return;
    if (granted.size() > 0) {
        // give keys back to system web window
        if (am.systemWebWindow && this != am.systemWebWindow) {
            task.platform->routeKeys(granted.data(), granted.size(),
                am.systemWebWindow->peer);
        } else {
            // in case system web window doesn't exist
            task.platform->routeKeys(granted.data(), granted.size(), nullptr);
        }
    }
}
