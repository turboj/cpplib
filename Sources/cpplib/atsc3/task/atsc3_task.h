#ifndef ATSC3_TASK_H
#define ATSC3_TASK_H

#include <vector>
#include <string>

#include "acba.h"
#include "tune_manager.h"
#include "service_manager.h"
#include "application_manager.h"
#include "rating_handler.h"
#include "rmp.h"
#include "atsc3_platform.h"
#include "EventDriver.h"
#include "dnld.h"

namespace atsc3 {

bool init (int &argc, char **&argv, const atsc3_Platform *p);

struct Task {
    TuneManager tuneManager;
    ServiceManager serviceManager;
    ApplicationManager applicationManager;
    Rmp rmp;
    acba::EventDriver eventDriver;
    acba::DownloadManager downloadManager;
    RatingHandler ratingHandler;
    const atsc3_Platform *platform;
};
extern Task task;

}
#endif
