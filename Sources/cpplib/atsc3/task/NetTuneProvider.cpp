#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <fcntl.h>
#include <errno.h>
#include <string.h>
#include <string>
#include <time.h>
#include <map>

#include "stream_reader.h"
#include "ip_reader.h"
#include "udp_reader.h"
#include "atsc3_task.h"
#include "tune_manager.h"
#include "EventDriver.h"
#include "acba.h"

using namespace std;
using namespace acba;
using namespace atsc3;

static Log l("nettune");

static int createMulticastSocket (uint32_t srcIp, uint32_t dstIp, uint16_t port)
{
    struct sockaddr_in addr;

    int fd = socket(AF_INET, SOCK_DGRAM, 0);
    if (fd < 0) return -1;

    u_int yes=1;
    if (setsockopt(fd,SOL_SOCKET,SO_REUSEADDR,&yes,sizeof(yes)) < 0) {
        l.error("SO_REUSEADDR failed");
        close(fd);
        return -1;
    }

    /* set up destination address */
    memset(&addr,0,sizeof(addr));
    addr.sin_family=AF_INET;
    addr.sin_addr.s_addr=htonl(dstIp);
    addr.sin_port=htons(port);

    /* bind to receive address */
    if (::bind(fd, (struct sockaddr *)&addr, sizeof(addr)) < 0) {
        l.error("bind failed");
        close(fd);
        return -1;
    }

    if (srcIp) {
        struct ip_mreq_source imr;
        imr.imr_multiaddr.s_addr = htonl(dstIp);
        imr.imr_interface.s_addr = htonl(INADDR_ANY);
        imr.imr_sourceaddr.s_addr = htonl(srcIp);
        if (setsockopt(fd, IPPROTO_IP, IP_ADD_SOURCE_MEMBERSHIP,
                (const char*)&imr, sizeof (struct ip_mreq_source)) < 0) {
            l.error("IP_ADD_SPOURCE_MEMBERSHIP failed");
            close(fd);
            return -1;
        }
    } else {
        /* use setsockopt() to request that the kernel join a multicast group */
        struct ip_mreq mreq;
        mreq.imr_multiaddr.s_addr=htonl(dstIp);
        mreq.imr_interface.s_addr=htonl(INADDR_ANY);
        if (setsockopt(fd, IPPROTO_IP, IP_ADD_MEMBERSHIP, &mreq,
                sizeof(mreq)) < 0) {
            l.error("IP_ADD_MEMBERSHIP failed");
            close(fd);
            return -1;
        }
    }

    return fd;
}

struct NetTune: Tune, Reactor {

    EventDriver &eventDriver;
    StreamReaderRef consumer;
    TuneContext tuneContext;
    volatile bool closeRequested;

    NetTune (const string &l, const StreamReaderRef &cons, EventDriver &ed):
            Tune(l), consumer(cons), eventDriver(ed), closeRequested(false) {
        tuneContext.tune = this;
    }

    void start () {
        l.info("Start");
        ref();
        eventDriver.setTimeout(0, this);
    }

    void stop () {
        closeRequested = true;
        eventDriver.setTimeout(0, this);
    }

    void setRoutingTable (const acba::Ref<RoutingTable> &rt) override {
        eventDriver.setTimeout(0, this);
    }

    struct Reader: RefCounter<Reader>, Reactor {
        Reader (const StreamReaderRef &cons, TuneContext &tc): sourceIp(0),
                socket(-1), consumer(cons), tuneContext(tc) {}
        uint32_t sourceIp;
        int socket;
        StreamReaderRef consumer;
        TuneContext &tuneContext;
        byte_t buffer[128*1024];

        IpContext ipContext;
        UdpContext udpContext;

        void processEvent (int ev) override {
            while (true) {
                int r = recv(socket, buffer, sizeof(buffer), MSG_DONTWAIT);
                if (r == -1) {
                    if (errno == EAGAIN || errno == EWOULDBLOCK) return;
                    l.error("recv failed.");
                    getEventDriver()->unwatchFile(this);
                    return;
                }

                consumer->read(buffer, r, &udpContext);
            }
        }
    };
    std::map<uint64_t, Ref<Reader> > readers;

    void processEvent (int evts) override {

        if (!locked) {
            locked = true;
            notify(EVENT_TUNER_LOCKED);
        }

        map<uint64_t, RoutingTable::Entry> routingEntries;

        if (!closeRequested) {
            routingEntries = task.tuneManager.routingTable->entries;
        }

        // leave
        for (auto ri = readers.begin(); ri != readers.end();) {
            auto rei = routingEntries.find(ri->first);
            if (rei != routingEntries.end() &&
                    rei->second.sourceIp == ri->second->sourceIp) {
                ri++;
                continue;
            }
            eventDriver.unwatchFile(ri->second.get());
            ::close(ri->second->socket);
            l.info("Leave %08x-%08x:%d", ri->second->sourceIp,
                    ri->first >> 32, (ri->first >> 16) & 0xffff);
            ri = readers.erase(ri);
        }

        // join
        for (auto &rei: routingEntries) {
            uint32_t dstIp = rei.first >> 32;
            uint16_t port = (rei.first >> 16) & 0xffff;
            uint32_t srcIp = rei.second.sourceIp;

            auto ri = readers.find(rei.first);
            if (ri != readers.end()) {
                if (ri->second->sourceIp == srcIp) continue;
                l.warn("Conflicting multicast source ip. request:%08 "
                        "previous:%08x multicast:%08x:%d", srcIp,
                        ri->second->sourceIp, dstIp, port);
                continue;
            }

            int fd = createMulticastSocket(srcIp, dstIp, port);
            if (fd < 0) {
                l.warn("Failed to create multicast socket for %08x-%08x:%d",
                        srcIp, dstIp, port);
                continue;
            }

            auto &r = readers[rei.first];
            r = new Reader(consumer, tuneContext);
            r->sourceIp = srcIp;
            r->socket = fd;
            r->ipContext.parent = &tuneContext;
            r->ipContext.sourceIp = srcIp;
            r->ipContext.destinationIp = dstIp;
            r->udpContext.parent = &r->ipContext;
            r->udpContext.srcPort = 0;
            r->udpContext.dstPort = port;
            eventDriver.watchFile(fd, EVENT_READ, r.get());
            l.info("Join %08x-%08x:%d", srcIp, dstIp, port);
        }

        if (closeRequested) {
            l.info("Stop");
            assert(readers.empty());
            notify(EVENT_TUNER_UNLOCKED);
            deref();
        }
    }
};

struct NetTuneProvider: TuneProvider {
    NetTuneProvider (): TuneProvider("net") {
        pthread_t t;
        pthread_create(&t, NULL, loop, this);
        pthread_detach(t);
    }

    Tune *create (const std::string &loc, const StreamReaderRef &cons)
            override {
        if (loc != "net:*") {
            l.error("Expected net:* but got %s", loc.c_str());
            return nullptr;
        }

        if (netTune) return nullptr;

        netTune = new NetTune(loc, cons, eventDriver);
        netTune->start();
        return netTune.get();
    }

    void destroy (const acba::Ref<Tune> &t) override {
        if (!netTune || t.get() != netTune.get()) return;
        netTune->stop();
        netTune = nullptr;
    }

    static void *loop (void *arg) {
        auto self = reinterpret_cast<NetTuneProvider*>(arg);
        self->eventDriver.run();
        return 0;
    }

    Ref<NetTune> netTune;
    EventDriver eventDriver;
};

void NetTuneProvider_init ()
{
    task.tuneManager.registerTuneProvider(new NetTuneProvider());
}
