#include <map>

#include "atsc3_task.h"
#include "service_manager.h"
#include "datetime.h"
#include "acba.h"

#define TOO_BIG_MDAT_SIZE (32 * 1024 * 1024)

#ifndef ADJUST_MFU_DATA_SIZE
#define ADJUST_MFU_DATA_SIZE 1
#endif

using namespace std;
using namespace acba;
using namespace atsc3;

static Log l("ms");

MmtpSessionTask::MmtpSessionTask (const acba::Ref<MmtpSession> &ms,
        const acba::Ref<MmtpService> &msrv, StreamReader *sr):
        session(ms), service(msrv), mmtpReader(sr),
        mutex(PTHREAD_MUTEX_INITIALIZER), dataUnitQueueIndex(0)
{
}

void MmtpSessionTask::start ()
{
    task.tuneManager.join(0, session->destinationIp, session->destinationPort,
            &mmtpReader);
}

void MmtpSessionTask::stop ()
{
    task.tuneManager.leave(0, session->destinationIp, session->destinationPort);
    task.eventDriver.unsetTimeout(&mpuFragmentReader);
}

#define SAFEREAD_LOG(args...) l.warn("expected " args)
#define SAFEREAD_RETURN return sz

int MmtpSessionTask::MmtpReader::read (const byte_t *data, int sz, Chain *pctx)
{
    const byte_t *d = data; // current cursor
    const byte_t *e = data + sz;

    uint16_t flags = SAFEREAD_U16(d, e, "mmtp first 2 bytes");
    // Support version 1 only
    int ver = flags >> 14;
    if (ver != 1) {
        if (IS_FIRST_TIME) l.warn("DROPPING MMTP PACKET OF VERSION %d", ver);
        return sz;
    }
    bool C = !!(flags & 0x2000);
    bool X = !!(flags & 0x0400);
    int fecType = (flags >> 11) & 0x3;
    int type = flags & 0xf;

    MmtpContext meta;
    meta.packetId = SAFEREAD_U16(d, e, "mmtp packetId");
    meta.timestamp = SAFEREAD_U32(d, e, "mmtp timestamp");
    meta.packetSequenceNumber = SAFEREAD_U32(d, e, "mmtp packet seq");

    if (C) SAFEREAD_SKIP(d, e, 4, "packet counter");
    SAFEREAD_SKIP(d, e, 2, "r, TB, DS, TP, flow_label");

    if (X) {
        // skip extension_header
        SAFEREAD_SKIP(d, e, 2, "extension header type");
        uint16_t extlen = SAFEREAD_U16(d, e, "extension header len");
        SAFEREAD_SKIP(d, e, extlen, "extension header value");
    }

    switch (fecType) {
    case 0: // w/o source_FEC_payload_ID
        break;

    case 1: // w/ source_FEC_payload_ID
        e -= 4;
        if (d > e) {
            l.warn("Expected source_FEC_payload_ID");
            return sz;
        }
        break;

    case 2: // TODO: FEC repair packet for FEC Payload Mode 0
    case 3: // TODO: FEC repair packet for FEC Payload Mode 1
        if (IS_FIRST_TIME) l.warn("NOT IMPLEMENTED FOR FEC REPAIR PACKET");
        return sz;
    }

    switch (type) {
    case 0: // MPU Fragment
        meta.parent = pctx;
        container_of(this, &MmtpSessionTask::mmtpReader)->mpuFragmentReader.
                read(d, e - d, &meta);
        break;

    case 2: // Signaling Message
        if (meta.packetId != 0 && IS_FIRST_TIME) {
            l.warn("SLS comes from non-zero packetId:%u", meta.packetId);
        }
        meta.parent = pctx;
        if (slsReader) slsReader->read(d, e - d, &meta);
        break;

    default:
        l.error("Unsupported mmtp type:%u", type);
        break;
    }

    return sz;
}

struct MpuFragmentContext: ChainNode<MpuFragmentContext> {
    uint32_t mpuSequenceNumber;
    uint8_t fragmentType;
};

struct DuHeader {
    uint32_t fragmentSequenceNumber;
    uint32_t sampleNumber;
    uint32_t sampleOffset;

#define READ_U32(x) (((x)[0] << 24) + ((x)[1] << 16) + ((x)[2] << 8) + (x)[3])
    int read (const byte_t *&d, const byte_t *e, MpuFragmentContext *ctx) {
        if (ctx->fragmentType == 2) {
            if (d + 14 > e) {
                l.warn("Expected du header");
                return -1;
            }

            fragmentSequenceNumber = READ_U32(d);
            d += 4;
            sampleNumber = READ_U32(d);
            d += 4;
            sampleOffset = READ_U32(d);
            d += 4;

            d += 2; // priority, dep_counter
        } else {
            if (d + 4 > e) {
                l.warn("Expected du header");
                return -1;
            }
        }
        return 0;
    }
};

int MmtpSessionTask::MpuFragmentReader::read (const byte_t *data, int sz,
        Chain *pctx)
{
    const byte_t *d = data;
    const byte_t *e = data + sz;

    if (2 + SAFEREAD_U16(d, e, "mmtp payload size") != sz) {
        l.warn("Invalid size for mpu fragment:%d", sz);
        return sz;
    }

    MpuFragmentContext meta;
    meta.parent = pctx;

    uint8_t flags = SAFEREAD_U8(d, e, "mmtp payload flags");
    meta.fragmentType = flags >> 4;
    bool T = !!((flags >> 3) & 0x1);
    if (!T) {
        if (IS_FIRST_TIME) l.warn("mmtp payload has non-timed data");
        return sz;
    }
    int fragIA = flags & 0x7; //  f_i + A
    uint8_t fragCnt = SAFEREAD_U8(d, e, "frag_counter");

    meta.mpuSequenceNumber = SAFEREAD_U32(d, e, "MPU_sequence_number");

    DuHeader dh;

    auto mmtp = pctx->as<MmtpContext>();
    auto pdu = getPartialDataUnit(mmtp->packetId);

    switch (fragIA) {
    case 0: // single complete du
        if (dh.read(d, e, &meta)) break;
        sendDataUnit(d, e - d, &meta);
        break;

    case 1: // multiple complete DUs
        while (d < e) {
            uint16_t dulen = SAFEREAD_U16(d, e, "DU_length");
            SAFEREAD_CHECKSIZE(d, e, dulen, "DU_Header & DU payload");

            const byte_t *de = d + dulen;
            if (dh.read(d, de, &meta)) break;
            SAFEREAD_CHECKSIZE(d, de, 0, "DU payload");
            sendDataUnit(d, de - d, &meta);

            d = de;
        }
        break;

    case 2: // du first fragment
        if (dh.read(d, e, &meta)) break;

        pdu->fragmentCounter = fragCnt;
        pdu->packetSequenceNumber = mmtp->packetSequenceNumber;

        pdu->data.clear();
        pdu->data.writeBytes(d, e - d);
        break;

    case 4: // du middle fragment
    case 6: // du last fragment
        if (pdu->fragmentCounter < 0) break;
        if (dh.read(d, e, &meta)) {
            pdu->fragmentCounter = -1;
            break;
        }

        if (!(fragCnt == pdu->fragmentCounter - 1 &&
                mmtp->packetSequenceNumber == pdu->packetSequenceNumber + 1)) {
            l.info("Dropped DU fragmentCounter:%u(expected:%u) "
                    "packetSequenceNumber:%u(%u) packetId:%u",
                    fragCnt, pdu->fragmentCounter - 1,
                    mmtp->packetSequenceNumber, pdu->packetSequenceNumber + 1,
                    mmtp->packetId);
            pdu->fragmentCounter = -1;
            break;
        }

        pdu->data.writeBytes(d, e - d);

        if (fragIA == 4) {
            pdu->fragmentCounter = fragCnt;
            pdu->packetSequenceNumber = mmtp->packetSequenceNumber;
        } else {
            pdu->fragmentCounter = -1;
            sendDataUnit(pdu->data.begin(), pdu->data.size(), &meta);
        }
        break;

    default:
        l.warn("Invalid fragmentation:%02x", fragIA);
        break;
    }

    return sz;
}

void MmtpSessionTask::MpuFragmentReader::sendDataUnit (const byte_t *data,
        int sz, Chain *pctx)
{
    auto *self = container_of(this, &MmtpSessionTask::mpuFragmentReader);
    auto mpuf = pctx->as<MpuFragmentContext>();
    auto mmtp = pctx->as<MmtpContext>();

    ScopedLock sl(self->mutex);

    auto &duq = self->dataUnitQueues[self->dataUnitQueueIndex];
    duq.writeI16(mmtp->packetId);
    duq.writeI32(mpuf->mpuSequenceNumber);
    duq.writeI8(mpuf->fragmentType);
    duq.writeI32(sz);
    duq.writeBytes(data, sz);

    task.eventDriver.setTimeout(0, this);
}

void MmtpSessionTask::MpuFragmentReader::processEvent (int evt)
{
    auto *self = container_of(this, &MmtpSessionTask::mpuFragmentReader);

    Buffer *pduq;
    {
        ScopedLock sl(self->mutex);
        pduq = &self->dataUnitQueues[self->dataUnitQueueIndex];
        self->dataUnitQueueIndex ^= 1;
        assert(self->dataUnitQueues[self->dataUnitQueueIndex].size() == 0);
    }
    auto &duq = *pduq;

    uint16_t pktId;
    uint32_t seq;
    uint8_t ft;
    while (duq.size() > 0) {
        pktId = (uint16_t)duq.readI16();
        seq = (uint32_t)duq.readI32();
        ft = (uint8_t)duq.readI8();
        int sz = duq.readI32();

        processDataUnit(pktId, seq, ft, duq.begin(), sz);
        duq.drop(sz);
    }
}

void MmtpSessionTask::MpuFragmentReader::processDataUnit (uint16_t pktId,
        uint32_t seq, uint8_t ft, const byte_t *data, int sz)
{
    auto *self = container_of(this, &MmtpSessionTask::mpuFragmentReader);
    auto &mfs = self->service->mpus;

    switch (ft) {
    case 0: // MPU metadata

        // Check if previous MPU was incomplete.
        do {
            auto mfi = mfs.find({pktId, seq - 1});
            if (mfi == mfs.end()) break;
            auto &mf = mfi->second;

            if (mf->flags == Mpu::FLAGS_ALL) break;

            l.warn("DROP MPU %u.%u size:%d-%d-%d(%d/%d) pts:%llu",
                    pktId, seq - 1,
                    mf->mpuMetadata.size(), mf->movieFragmentMetadata.size(),
                    mf->mediaData.size(), mf->receivedMdatSize, mf->mdatSize,
                    mf->presentationTime);
        } while (false);

        l.debug("MPU %u.%u start", pktId, seq);
        {
            auto &mf = mfs[{pktId, seq}];
            mf = new Mpu(pktId, seq);
            mf->expireTime = currentUnixTime() + 30.0;

            // Check if presentation time is available.
            Ref<MmtPackage> &pkg = self->service->currentPackage;
            if (pkg) {
                for (auto asset: pkg->assets) {
                    if (asset.second->packetId != pktId) continue;

                    auto ts = asset.second->timestamps.find(seq);
                    if (ts == asset.second->timestamps.end()) {
                        l.warn("Timestamp for MPU %u.%u is not yet available.",
                                pktId, seq);
                        break;
                    }

                    mf->presentationTime = double(ts->second) / 0x100000000UL
                            - 2208988800UL;
                    mf->flags |= Mpu::FLAG_MPU_PTS;
                    self->service->mpuUpdate.notify(mf, 4, nullptr);
                    break;
                }
            }

            // send metadata
            mf->mpuMetadata.writeBytes(data, sz);
            mf->flags |= Mpu::FLAG_MPU_METADATA;
            self->service->mpuUpdate.notify(mf, 0, nullptr);
        }
        break;

    case 1: // Movie fragment metadata
        if (sz < 8 || strncmp((const char*)data + sz - 4, "mdat", 4) != 0) {
            l.warn("Movie fragment metadata doesn't end with mdat box");
            return;
        }
        {
            auto mfi = mfs.find({pktId, seq});
            if (mfi == mfs.end()) break;
            auto &mf = mfi->second;

            mf->movieFragmentMetadata.writeBytes(data, sz);
            mf->mdatSize = READ_U32(data + sz - 8) - 8;
            if (mf->mdatSize > TOO_BIG_MDAT_SIZE) {
                l.warn("mdat size is too big. sz:%u", mf->mdatSize);
            } else {
                mf->flags |= Mpu::FLAG_MOVIE_FRAGMENT_METADATA;
            }

            if (mf->receivedMdatSize == mf->mdatSize) {
                l.info("MPU %u.%u OOO size:%d-%d-%d(%d)", pktId, seq,
                        mf->mpuMetadata.size(),
                        mf->movieFragmentMetadata.size(),
                        mf->mediaData.size(), mf->mdatSize);
                mf->flags |= Mpu::FLAG_FULL_MEDIA_DATA;
            }

            self->service->mpuUpdate.notify(mf, 1, nullptr);

            l.debug("MPU %u.%u collecting media data... size:%u", pktId, seq,
                    mf->mdatSize);
        }
        break;

    case 2: // MFU
        if (sz < 34) {
            l.warn("Too short MFU. sz:%d", sz);
            break;
        }
        {
            // read sample hint
            uint32_t mfuseq = READ_U32(data);
            uint32_t mfseq = READ_U32(data + 5);
            uint32_t samn = READ_U32(data + 9);
            uint32_t off = READ_U32(data + 15);
            uint32_t len = READ_U32(data + 19);
            int samsz = READ_U32(data + 23) + 23; // 34 or 38

#if ADJUST_MFU_DATA_SIZE
            if (samsz + len > sz && samsz < sz) {
                len = sz - samsz;
                l.warn("Adjust data size in mfu", len);
            }
#endif

            if (samsz + len != sz) {
                l.warn("MFU corrupted? MMTHSample(%d) + data(%d) != MFU(%d)",
                        samsz, len, sz);
                l.warn("    mpuSeq:%u mfuSeq:%u mfSeq:%u sample:%u pos:%u,%u",
                        seq, mfuseq, mfseq, samn, off, len);
                break;
            }

            auto mfi = mfs.find({pktId, seq});
            if (mfi == mfs.end()) break;
            auto &mf = mfi->second;
            Buffer &buf = mf->mediaData;

            // missing mfu
            if (off > buf.size()) {
                auto &buf = mf->mediaData;
                l.warn("MPU %u.%u MFU is missing. %d-%d", pktId, seq,
                        buf.size(), off);

                uint32_t jmpsz = off - buf.size();
                buf.ensureMargin(jmpsz);
                memset(buf.end(), 0, jmpsz);
                buf.grow(jmpsz);

                self->service->mpuUpdate.notify(mf, 3, &jmpsz);
            } else if (off < buf.size()) {
                l.warn("TODO: MPU %u.%u MFU(seq:%u) was retrieved out of order",
                        pktId, seq, mfuseq);
                break;
            }

            if (buf.size() < off + len) {
                int inc = off + len - buf.size();
                buf.ensureMargin(inc);
                buf.grow(inc);
            }
            memcpy(buf.begin() + off, data + samsz, len);
            mf->receivedMdatSize += sz;

            if (mf->receivedMdatSize == mf->mdatSize) {
                l.info("MPU %u.%u size:%d-%d-%d(%d)", pktId, seq,
                        mf->mpuMetadata.size(),
                        mf->movieFragmentMetadata.size(),
                        mf->mediaData.size(), mf->mdatSize);
                mf->flags |= Mpu::FLAG_FULL_MEDIA_DATA;
            }

            self->service->mpuUpdate.notify(mf, 2, data);
        }
        break;

    default:
        l.error("Invalid fragmentType:%d", ft);
        break;
    }
}
