#include "acba.h"
#include "tune_manager.h"

using namespace atsc3;
using namespace acba;

void Tune::addListener (acba::Listener<EVENT> *l)
{
    ScopedLock sl(mutex);
    update.addListener(l);
}

void Tune::removeListener (acba::Listener<EVENT> *l)
{
    ScopedLock sl(mutex);
    update.removeListener(l);
}

void Tune::notify (EVENT e)
{
    ScopedLock sl(mutex);
    switch (e) {
    case EVENT_TUNER_LOCKED:
        locked = true; break;
    case EVENT_TUNER_UNLOCKED:
        locked = false; break;
    }
    update.notify(e);
}
