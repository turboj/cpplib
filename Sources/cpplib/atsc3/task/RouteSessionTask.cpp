#include <sys/stat.h>
#include <sys/types.h>

#include <sstream>

#include "ip_reader.h"
#include "udp_reader.h"
#include "mime_reader.h"
#include "atsc3_task.h"
#include "acba.h"
#include "acba_crc32.h"
#include "pugixml.hpp"
#include "datetime.h"

using namespace std;
using namespace acba;
using namespace pugi;
using namespace atsc3;

#define READ_U32(x) (((x)[0] << 24) + ((x)[1] << 16) + ((x)[2] << 8) + (x)[3]);

// OPTIONAL: Show download progress for each receival of the given size
#define TRACE_OBJECT_SIZE (3*1024*1024)
#define TRACE_OBJECT_SIZE_UNIT (256*1024)

#define ROUTE_SESSION_GC_SIZE (5*1024*1024)

#define PARTIAL_FILE_TIMEOUT 60

static Log l("rs");

static void resolvePath (string &s, uint32_t toi)
{
#define TOI "$TOI$"

    char str[256];
    strcpy(str, s.c_str());

    char *c = strstr(str, TOI);
    if (!c) return;
    int off = c - str + strlen(TOI);
    sprintf(c, "%u%s", toi, &s[off]);

    s = str;
}

RouteSessionTask::RouteSessionTask (const Ref<RouteSession> &rs,
        const Ref<RouteService> &rsrv): session(rs), service(rsrv),
        mutex(PTHREAD_MUTEX_INITIALIZER), receivedBytes(0)
{
}

#define SAFEREAD_LOG(args...) l.warn("expected " args)
#define SAFEREAD_RETURN return sz

int RouteSessionTask::LctReader::read (const byte_t *data, int sz, Chain *pctx)
{
    auto self = container_of(this, &RouteSessionTask::lctReader);
    auto rs = self->session.get();

    const byte_t *d = data;
    const byte_t *end = data + sz;

    SAFEREAD_CHECKSIZE(d, end, 4, "lct header");

    int ver = d[0] >> 4;
    int c = (d[0] >> 2) & 0x3;
    int psi = d[0] & 0x3;
    int s = d[1] >> 7;
    int o = (d[1] >> 5) & 0x3;
    int h = (d[1] >> 4) & 0x1;
    int t = (d[1] >> 3) & 0x1;
    int r = (d[1] >> 2) & 0x1;
    int a = (d[1] >> 1) & 0x1;
    int b = d[1] & 0x1;
    int hlen = d[2] * 4;
    int cp = d[3];
    d += 4 + 4 * (c+1);

    const int tsiLen = 4 * s + 2 * h;
    uint64_t tsi;
    if (tsiLen > 0) {
        SAFEREAD_CHECKSIZE(d, end, tsiLen, "tsi");
        tsi = 0;
        for (int i = 0; i < tsiLen; i++) tsi = (tsi << 8) + d[i];
        d += tsiLen;
    } else {
        tsi = -1;
    }

    const int toiLen = 4 * o + 2 * h;
    uint64_t toi;
    if (toiLen > 0) {
        SAFEREAD_CHECKSIZE(d, end, toiLen, "toi");
        toi = 0;
        for (int i = 0; i < tsiLen; i++) toi = (toi << 8) + d[i];
        d += toiLen;
    } else {
        toi = -1;
    }

    uint32_t sct = 0;
    if (t) sct = SAFEREAD_U32(d, end, "sct");

    uint32_t ert = 0;
    if (r) ert = SAFEREAD_U32(d, end, "ert");

    l.trace("lct ver:%d tsi:%llu toi:%llu hlen:%d cp:%d sz:%d sct:%u ert:%u "
            "psi:%d a:%d b:%d",
            ver, tsi, toi, hlen, cp, sz, sct, ert, psi, a, b);

    int64_t objsz = -1;
    int32_t fdtid = -1;

    const byte_t *he = data + hlen;
    SAFEREAD_CHECKSIZE(d, he, 0, "hextsz:%d", he - d);

    while (d + 4 <= he) {
        int hext_len = 4;
        if (d[0] <= 127) {
            hext_len = d[1] * 4;
        }
        SAFEREAD_CHECKSIZE(d, he, hext_len,
                "hext_len:%d remaining:%d", hext_len, he - d);

        switch (d[0]) {
        case 0: // EXT_NOP
            // nop
            break;

        case 192: // EXT_FDT
            {
                int fver = d[1] >> 4;
                if (fver != 2) l.warn("flute ver:%d", fver);

                fdtid = ((d[1] & 0x15) << 16) + ((d[2] & 0xff) << 8) + d[3];
            }
            break;

        case 194: // EXT_TOL (24bit)
            objsz = (d[1] << 16) + (d[2] << 8) + d[3];
            break;

        case 67: // EXT_TOL (48bit)
            if (hext_len >= 8) {
                objsz = (uint64_t(d[2]) << 40) + (uint64_t(d[3]) << 32) +
                        (uint64_t(d[4]) << 24) + (uint64_t(d[5]) << 16) +
                        (uint64_t(d[6]) << 8) + d[7];
            }
            break;

        case 64: // EXT_FTI (Compact No-Code Scheme where FEC Encoding ID = 0)
            if (hext_len >= 16) {
                objsz = (uint64_t(d[2]) << 40) + (uint64_t(d[3]) << 32) +
                        (uint64_t(d[4]) << 24) + (uint64_t(d[5]) << 16) +
                        (uint64_t(d[6]) << 8) + d[7];
            }
            break;

        case 2: // EXT_TIME (rfc5651, Used unless @maxExpiresDelta exists)
            if (IS_FIRST_TIME) l.warn("EXT_TIME");
            break;

        case 66: // EXT_ROUTE_PRESENTATION_TIME (A/331)
            if (IS_FIRST_TIME) l.warn("EXT_ROUTE_PRESENTATION_TIME");
            break;

        default:
            l.warn("Unknown/Unsupported hext:%d", d[0]);
            break;
        }

        d += hext_len;
    }

    if (d != data + hlen) {
        l.warn("Expected FEC Payload Id.");
        return sz;
    }

    uint32_t poff = SAFEREAD_U32(d, end, "FEC Payload ID (start_offset)");
    uint32_t plen = end - d;

    Ref<PartialFile> pf;
    if (toi == 0) { // fdt

        if (fdtid == -1) {
            if (IS_EACH_TIME(100)) l.warn("Expected EXT_FDT for FDT packet");
            // fall through. we need FDT anyway
            //return sz;
        }

        auto pfi = self->partialFdts.find({tsi, fdtid});
        if (pfi != self->partialFdts.end()) {
            pf = pfi->second;
        } else {

            // Drop if file info is not available yet.
            if (objsz == -1) {
                l.warn("Expected object size for FDT packet");
                return sz;
            }

            auto &pfr = self->partialFdts[{tsi, fdtid}];
            pfr = new PartialFile(tsi, toi);
            pf = pfr;
        }
        if (pf->state != PartialFile::DOWNLOADING) return sz;
    } else {
        ScopedLock sl(self->mutex);

        auto pfi = self->partialFiles.find({tsi, toi});
        if (pfi != self->partialFiles.end()) {
            pf = pfi->second;
        } else {
            // Drop if file info is not available yet.
            if (objsz == -1) return sz;

            auto &pfr = self->partialFiles[{tsi, toi}];
            pfr = new PartialFile(tsi, toi);
            pf = pfr;

            // Generate RouteFile when corresponding fileTemplate exists.
            auto fti = self->fileTemplates.find(tsi);
            if (fti != self->fileTemplates.end()) {
                auto &rf = pf->routeFile;

                rf = new RouteFile(tsi, toi);
                rf->path = fti->second.name;
                resolvePath(rf->path, toi);
                rf->expireTime = fti->second.expireTime;
                rf->attributes["hasTemplateGeneratedPath"] = true;
            }
        }
        if (pf->state != PartialFile::DOWNLOADING) return sz;
    }

    // set object size
    if (objsz >= 0) {
        if (pf->size >= 0 && pf->size != objsz) {
            l.warn("EXT_TOL is inconsistent. obj:%llu.%llu size:%lld prev:%d",
                    tsi, toi, objsz, pf->size);
        }
        pf->size = objsz;
    }

    // set codePoint
    if (pf->codePoint == -1) {
        pf->codePoint = cp;
    } else if (pf->codePoint != cp) {
        l.warn("codePoint is inconsistent. %llu.%llu cp:%d prev:%d", tsi, toi,
                cp, pf->codePoint);
    }

    auto &buf = pf->content;
    int szdiff = poff + plen - buf.size();
    if (szdiff > 0) {
        buf.ensureMargin(szdiff);
        buf.grow(szdiff);
    }
    memcpy(buf.begin() + poff, d, plen);
    auto now = currentUnixTime();
    pf->expireTime = now + PARTIAL_FILE_TIMEOUT;

#if TRACE_OBJECT_SIZE
    int prevsz;
    if (pf->size > TRACE_OBJECT_SIZE) {
        prevsz = pf->tracer.getTotalSize() / TRACE_OBJECT_SIZE_UNIT;
    }
    pf->tracer.markFragment(poff, plen);
    if (pf->size > TRACE_OBJECT_SIZE) {
        int cursz = pf->tracer.getTotalSize();
        if (b || prevsz != cursz / TRACE_OBJECT_SIZE_UNIT) {
            l.info("DOWNLOADING %llu.%llu %s (%.0lf%%) size:%d/%d b:%d",
                    tsi, toi,
                    pf->routeFile ? pf->routeFile->path.c_str() : "<NOPATH>",
                    (double)cursz / pf->size * 100, cursz, pf->size, b);
        }
    }
#else
    pf->tracer.markFragment(poff, plen);
#endif

    // Collect expired partial files
    self->receivedBytes += plen;
    if (self->receivedBytes > ROUTE_SESSION_GC_SIZE) {
        self->trim();
        self->receivedBytes = 0;
    }

    if (pf->size == -1 || pf->tracer.getContinuousSize() < pf->size) {
        if (b) {
            // NOTE: Some server set b flag for last packet of file
            // while it is repeatedly transmitted. Give grace period
            // instead of dropping it immeidately.

            stringstream ss;
            map<uint32_t, uint32_t> chunks;
            pf->tracer.getChunks(chunks);
            for (auto &ci: chunks) {
                ss << ci.first << '-' << ci.second << ' ';
            }
            l.info("DROP %llu.%llu %s size:%d/%d chunks:%s", tsi, toi,
                    pf->routeFile ? pf->routeFile->path.c_str() : "<NO_PATH>",
                    pf->tracer.getTotalSize(), pf->size, ss.str().c_str());
            pf->expireTime = now + 5;
            return sz;
        }
        return sz;
    }

    if (pf->tracer.getContinuousSize() != pf->size) {
        l.warn("Size mismatch! %llu.%llu expected:%d received:%d", tsi, toi,
                pf->size, pf->tracer.getContinuousSize());
    }

    // On download completion
    if (toi == 0) { // fdt
        pf->tracer.clear();
        pf->state = PartialFile::DOWNLOAD_COMPLETE;
        l.info("FDT %llu.%d", tsi, fdtid);
        self->fdtHandler.processFdt(tsi, pf);
    } else {
        ScopedLock sl(self->mutex);
        pf->tracer.clear();
        pf->state = PartialFile::DOWNLOAD_COMPLETE;
        self->sender.send(pf);
    }
    return sz;
}

void RouteSessionTask::Sender::send (const Ref<PartialFile> &pf)
{
    RouteSessionTask *self = container_of(this, &RouteSessionTask::sender);

    assert(pf->state == PartialFile::DOWNLOAD_COMPLETE);

    self->fileQueue.push_back(pf);
    task.eventDriver.setTimeout(0, this);
}

struct EntityProcessor: StreamReader {
    EntityProcessor (RouteSessionTask *rst): sessionTask(rst) {}
    RouteSessionTask *sessionTask;
    Ref<RouteSessionTask::PartialFile> entityFile;
    Ref<RouteSessionTask::PartialFile> partialFile;

    void read (Ref<RouteSessionTask::PartialFile> &pf) {
        partialFile = pf;
        MimeReader mr(this);
        auto &buf = pf->content;
        mr.read(buf.begin(), buf.size(), nullptr);
        pf->content.clear();
    }

    int read (const byte_t *data, int sz, Chain *pctx) override {
        MimeContext *mctx = pctx->as<MimeContext>();

        string type;
        string location;
        string range;
        string expires;

        for (auto i: mctx->headers) {
            l.debug("mime header %s: %s", i.first.c_str(), i.second.c_str());
            if (strcmp(i.first.c_str(), "content-type") == 0) {
                type = mime_value(i.second.c_str());
                transform(type.begin(), type.end(), type.begin(), ::tolower);
            } else if (strcmp(i.first.c_str(), "content-location") == 0) {
                location = mime_value(i.second.c_str());
            } else if (strcmp(i.first.c_str(), "content-range") == 0) {
                range = i.second.c_str();
            } else if (strcmp(i.first.c_str(), "expires") == 0) {
                expires = mime_value(i.second.c_str());
            }
        }

        if (location.empty()) {
            l.warn("Entity without content-location");
            return 0;
        }

        auto &ef = sessionTask->partialEntities[location];
        if (!ef) {
            auto tsi = partialFile->tsi;
            ef = new RouteSessionTask::PartialFile(tsi, 0);
            ef->codePoint = partialFile->codePoint;
            ef->routeFile = new RouteFile(tsi, 0);
            ef->routeFile->path = location;
        }
        entityFile = ef;

        if (!type.empty()) ef->routeFile->type = type;

        // TODO: Handle expire attribute.
        if (!expires.empty()) {
            l.warn("TODO: Entity file contains Expire: %s", expires.c_str());
        }
        ef->routeFile->expireTime = currentUnixTime() + 30.0;

        if (range.empty()) {
            ef->content.writeBytes(data, sz);
            if (ef->state == RouteSessionTask::PartialFile::DOWNLOADING) {
                ef->state = RouteSessionTask::PartialFile::DOWNLOAD_COMPLETE;
            }
            return sz;
        }

        // parse content-range
        int b, e, tsz = -1;
        if (sscanf(range.c_str(), " bytes %d - %d / %d", &b, &e, &tsz) == 4) {
            if (ef->size >= 0 && ef->size != tsz) {
                l.warn("Entity file has inconsistent size. prev:%d cur:%d",
                        ef->size, tsz);
            }
            ef->size = tsz;
        } else if (sscanf(range.c_str(), "bytes %d - %d / *", &b, &e) == 2) {
            // fall through
        } else {
            l.warn("Couldn't parse content-range:%s", range.c_str());
            return 0;
        }
        if (b < 0 || e - b + 1 != sz) {
            l.warn("Size mismatch. given:%d content-range:%s", sz,
                    range.c_str());
            return 0;
        }

        // Append to partial entity file
        auto &buf = ef->content;
        int szdiff = b + sz - buf.size();
        if (szdiff > 0) {
            buf.ensureMargin(szdiff);
            buf.grow(szdiff);
        }
        memcpy(buf.begin() + b, data, sz);
        ef->tracer.markFragment(b, sz);

        // Check completion
        if (ef->size >= 0 && ef->tracer.getContinuousSize() == ef->size) {
            if (ef->state == RouteSessionTask::PartialFile::DOWNLOADING) {
                ef->state = RouteSessionTask::PartialFile::DOWNLOAD_COMPLETE;
            }
        }

        return sz;
    }
};

static bool updateFile (Ref<RouteSessionTask::PartialFile> &pf, Ref<RouteService> &rsrv,
        const RouteSessionTask::Payload *p)
{
    auto &rf = pf->routeFile;
    if (!rf) return false; // FDT is not availbale.

    // mark update time
    rf->updateTime = currentUnixTime();
    // FIXME: Ad-hoc expire time on segment files
    if (rf->attributes["hasTemplateGeneratedPath"].asBool()) {
        rf->expireTime = rf->updateTime + 30.0;
    }

    rf->content.take(pf->content);
    rf->codePoint = pf->codePoint;

    assert(p);
    rf->attributes["formatId"] = p->formatId;
    rf->attributes["frag"] = p->frag;
    rf->attributes["order"] = p->order;
    l.info("OBJECT %u.%u %s cp:%d(fmt:%d frag:%d order:%d) "
            "size:%d time:%.2lf expremain:%.2f",
            rf->tsi, rf->toi, rf->path.c_str(), rf->codePoint,
            p->formatId, p->frag, p->order,
            rf->content.size(), rf->updateTime,
            (rf->expireTime - rf->updateTime));

    rsrv->files[rf->path] = rf;
    rsrv->fileUpdate.notify(rf);
    return true;
}

void RouteSessionTask::Sender::processEvent (int evt)
{
    RouteSessionTask *self = container_of(this, &RouteSessionTask::sender);

    // take out updated files
    vector<Ref<PartialFile> > fq;
    {
        ScopedLock sl(self->mutex);
        self->fileQueue.swap(fq);
    }

    auto &rsrv = self->service;
    for (auto &pf: fq) {
        if (pf->state != PartialFile::DOWNLOAD_COMPLETE) continue;
        auto pi = self->payloads.find({pf->tsi, pf->codePoint});
        const Payload *p = nullptr;
        if (pi != self->payloads.end()) {
            p = &pi->second;
        } else {
            // A331 Table A.3.6
            static const map<int, Payload> predefinedPayloads = {
                {1, {1, 0, true}},
                {2, {2, 0, true}},
                {3, {3, 0, true}},
                {4, {4, 0, true}},
                {5, {1, 0, true}},
                {6, {1, 0, true}},
                {7, {1, 0, true}},
                {8, {1, 1, true}},
                {9, {2, 1, true}},
            };
            auto ppi = predefinedPayloads.find(pf->codePoint);
            if (ppi != predefinedPayloads.end()) p = &ppi->second;
        }
        if (!p && pf->tsi == 0) {
            l.warn("Encountered SLS with invalid code point %d. "
                    "fallback to FILE_MODE", pf->codePoint);
            static Payload slsFallbackPayload = {1, 0, true};
            p = &slsFallbackPayload;
        }

        switch (p? p->formatId: RouteFile::UNKNOWN) {
        case RouteFile::FILE_MODE:
        case RouteFile::UNSIGNED_PACKAGE_MODE:
        case RouteFile::SIGNED_PACKAGE_MODE:
            if (updateFile(pf, rsrv, p)) pf->state = PartialFile::CACHED;
            break;

        case RouteFile::ENTITY_MODE:
            {
                int psz = pf->content.size();
                EntityProcessor ep(self);
                ep.read(pf);
                if (!ep.entityFile) {
                    l.warn("DROP INVALID ENTITY %u.%u", pf->tsi, pf->toi);
                    pf->expireTime = currentUnixTime() + 5.0;
                    break;
                }
                auto &ef = ep.entityFile;
                assert(ef->routeFile);
                pf->state = PartialFile::CACHED;
                pf->routeFile = ef->routeFile;

                l.info("ENTITY %u.%u %s cp:%d(fmt:%d frag:%d order:%d) "
                        "size:%d",
                        pf->tsi, pf->toi, ef->routeFile->path.c_str(),
                        pf->codePoint, p->formatId, p->frag, p->order,
                        psz);

                if (ef->state == PartialFile::DOWNLOAD_COMPLETE) {
                    if (updateFile(ef, rsrv, p)) {
                        ef->state = PartialFile::CACHED;
                    }
                }
            }
            break;

        case RouteFile::UNKNOWN:
        case RouteFile::RESERVED:
        default:
            l.info("UNKNOWN %u.%u %s cp:%d size:%d", pf->tsi, pf->toi,
                    pf->routeFile? pf->routeFile->path.c_str(): "<NOPATH>",
                    pf->codePoint, pf->content.size());
            continue;
        }
    }
}

void RouteSessionTask::start ()
{
    // TODO: Sample stream doesn't respect sourceip address
    //task.tuneManager.join(session->sourceIp, session->destinationIp,
    //        session->destinationPort, &lctReader);
    task.tuneManager.join(0, session->destinationIp, session->destinationPort,
            &lctReader);

    // Check if xmlRs already exists
    fdtHandler.onUpdate();
}

void RouteSessionTask::stop ()
{
    task.tuneManager.leave(0, session->destinationIp, session->destinationPort);
    task.eventDriver.unsetTimeout(&sender);
}

void RouteSessionTask::FdtHandler::onUpdate ()
{
    auto self = container_of(this, &RouteSessionTask::fdtHandler);

    if (!self->session->xmlRs) return;

    self->payloads.clear();
    for (auto ls: self->session->xmlRs.children("LS")) {
        const char *name;
        uint32_t tsi = ls.attribute("tsi").as_uint();
        xml_node fdti = ls.child("SrcFlow").child("EFDT").child("FDT-Instance");

        for (auto pl: ls.child("SrcFlow").children("Payload")) {
            int cp = pl.attribute("codePoint").as_int(0);
            if (cp < 128 || cp > 255) continue;

            auto &p = self->payloads[{tsi, cp}];
            p.formatId = pl.attribute("formatId").as_int(-1);
            p.frag = pl.attribute("frag").as_int(0);
            p.order = pl.attribute("order").as_bool(true);
        }

        processFdtInstance(tsi, fdti);
    }
}

void RouteSessionTask::FdtHandler::processFdt (uint32_t tsi,
        const Ref<PartialFile> &pf)
{
    // handle fdt for envelop
    xml_document doc;
    Buffer &buf = pf->content;
    xml_parse_result res = doc.load_buffer(buf.begin(), buf.size());
    if (!res) {
        l.warn("Failed to parse FDT! %s\n", res.description());
        return;
    }

    auto fdti = doc.document_element();

    if (strcmp(fdti.name(), "EFDT") == 0) fdti = fdti.child("FDT-Instance");

    // Set expire time for the received fdt
    double exp = fdti.attribute("Expires").as_double(0.0);
    if (!exp) {
        l.warn("FDT has no expire time");
        return;
    }
    pf->expireTime = exp;

    processFdtInstance(tsi, fdti);
}

void RouteSessionTask::FdtHandler::processFdtInstance (uint32_t tsi,
        const pugi::xml_node fdti)
{
    double exp = fdti.attribute("Expires").as_double(0.0);
    if (!exp) {
        l.warn("FDT has no expire time");
        return;
    }

    for (auto fi: fdti.select_nodes("*[self::File or self::fdt:File]")) {
        addFileInfo(tsi, fi.node(), exp);
    }

    auto name = fdti.attribute("afdt:fileTemplate").value();
    if (name[0]) addFileTemplate(tsi, name, exp);
}

void RouteSessionTask::FdtHandler::addFileInfo (uint32_t tsi,
        const xml_node info, double exp)
{
    auto self = container_of(this, &RouteSessionTask::fdtHandler);

    // extract path and toi
    const char *path = info.attribute("Content-Location").value();
    if (!path[0]) return;
    auto xmlToi = info.attribute("TOI");
    if (!xmlToi) return;
    uint32_t toi = xmlToi.as_uint();

    l.info("MAP %u.%u -> %s", tsi, toi, path);

    Ref<RouteFile> rf = new RouteFile(tsi, toi);
    rf->path = path;
    rf->expireTime = exp;

    auto clen = info.attribute("Content-Length");
    if (clen) rf->attributes[clen.name()] = clen.as_int();
    auto tlen = info.attribute("Transfer-Length");
    if (tlen) rf->attributes[tlen.name()] = tlen.as_int();
    auto ctype = info.attribute("Content-Type");
    if (ctype) rf->type = ctype.value();
    auto cenc = info.attribute("Content-Encoding");
    if (cenc) rf->attributes[cenc.name()] = cenc.value();
    auto appctxlst1 = info.attribute("afdt:appContextIdList");
    auto appctxlst2 = info.parent().attribute("afdt:appContextIdList");
    if (appctxlst1 || appctxlst2) {
        stringstream ss;
        ss << " " << appctxlst1.value() << " " << appctxlst2.value() << " ";
        rf->attributes["appContextIdList"] = ss.str();
    }

    ScopedLock sl(self->mutex);
    auto &pf = self->partialFiles[{tsi, toi}];
    if (pf) {
        if (pf->state == PartialFile::CACHED) {
            l.warn("Ignored fdt::File which is already in use. path:%s",
                    pf->routeFile->path.c_str());
            return;
        }
    } else {
        pf = new PartialFile(tsi, toi);
    }
    if (tlen) pf->size = tlen.as_int();
    pf->routeFile = rf;
    if (pf->state == PartialFile::DOWNLOAD_COMPLETE) self->sender.send(pf);
}

void RouteSessionTask::FdtHandler::addFileTemplate (uint32_t tsi,
        const string &name, double exp)
{
    l.info("MAP %u.* -> %s", tsi, name.c_str());

    auto self = container_of(this, &RouteSessionTask::fdtHandler);

    auto &ft = self->fileTemplates[tsi];
    ft.name = name;
    ft.expireTime = exp;

    // As @fileTemplate comes together with fdt:File elements and
    // addFileTemplate is called after addFileInfo,
    // we assume that any missing files are subject to file template.
    {
        ScopedLock sl(self->mutex);
        for (auto &pf: self->partialFiles) {
            if (pf.first.first != tsi) continue;
            auto &rf = pf.second->routeFile;
            if (rf) continue;

            rf = new RouteFile(pf.first.first, pf.first.second);
            rf->path = name;
            resolvePath(rf->path, pf.first.second);
            rf->expireTime = exp;
            rf->attributes["hasTemplateGeneratedPath"] = true;

            if (pf.second->state == PartialFile::DOWNLOAD_COMPLETE) {
                self->sender.send(pf.second);
            }
        }
    }
}

void RouteSessionTask::trim ()
{
    double now = currentUnixTime();

    int nLive = 0;
    int nExpired = 0;
    int memLive = 0;
    int memExpired = 0;

    auto pfi = partialFdts.begin();
    while (pfi != partialFdts.end()) {
        auto &pf = pfi->second;

        if (now < pf->expireTime) {
            pfi++;

            // stat
            nLive++;
            memLive += pf->content.size();

            continue;
        }

        // stat
        nExpired++;
        memExpired += pf->content.size();

        l.debug("COLLECT FDT %u.%u", pfi->first.first, pfi->first.second);
        pfi = partialFdts.erase(pfi);
    }

    auto efi = partialEntities.begin();
    while (efi != partialEntities.end()) {
        auto &pf = efi->second;

        // Check if yet to be expired
        if (now < pf->routeFile->expireTime) {
            efi++;

            // stat
            nLive++;
            memLive += pf->content.size();

            continue;
        }

        // stat
        nExpired++;
        memExpired += pf->content.size();

        l.debug("COLLECT ENTITY %s", pf->routeFile->path.c_str());
        efi = partialEntities.erase(efi);
    }

    ScopedLock sl(mutex);
    pfi = partialFiles.begin();
    while (pfi != partialFiles.end()) {
        auto &pf = pfi->second;

        // Check if yet to be expired
        if (now < (pf->routeFile? pf->routeFile->expireTime: pf->expireTime)) {
            pfi++;

            // stat
            nLive++;
            memLive += pf->content.size();

            continue;
        }

        // stat
        nExpired++;
        memExpired += pf->content.size();

        l.debug("COLLECT FILE %u.%u", pfi->first.first, pfi->first.second);
        pfi = partialFiles.erase(pfi);
    }

    l.info("MEMINFO %08x%08x:%d live:%d(%.2fMB) expired:%d(%.2fMB)",
            session->sourceIp, session->destinationIp, session->destinationPort,
            nLive, memLive / 1000000.0, nExpired, memExpired / 1000000.0);
}
