#include <string>
#include <map>

#include "stream_reader.h"
#include "atsc3_task.h"
#include "tune_manager.h"
#include "acba.h"

using namespace std;
using namespace acba;
using namespace atsc3;

static Log l("bs");

void BroadcastStreamTask::start ()
{
    tune = task.tuneManager.requestTune(broadcastStream->location);
    if (!tune) {
        l.error("Tuning %s failed.", broadcastStream->location.c_str());
        return;
    }
    tune->addListener(this);
}

void BroadcastStreamTask::stop ()
{
    tune->removeListener(this);
    task.tuneManager.releaseTune(tune);
    tune = nullptr;
}

void BroadcastStreamTask::onUpdate (Tune::EVENT e)
{
    static map<Tune::EVENT, const string> strs = {
        {Tune::EVENT_TUNER_LOCKED, "EVENT_TUNER_LOCKED"},
        {Tune::EVENT_TUNER_UNLOCKED, "EVENT_TUNER_UNLOCKED"},
        {Tune::EVENT_SERVICE_LIST_RECEIVED, "EVENT_SERVICE_LIST_RECEIVED"},
    };

    auto ni = strs.find(e);
    if (ni == strs.end()) {
        l.warn("Unknown tune event:%d", e);
        return;
    }
    l.info("%s", ni->second.c_str());
}
