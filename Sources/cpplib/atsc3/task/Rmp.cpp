#include <string>

#include "acba.h"
#include "rmp.h"
#include "datetime.h"
#include "atsc3_task.h"
#include "atsc3_platform.h"
#include "pugixml.hpp"

using namespace std;
using namespace acba;
using namespace atsc3;
using namespace pugi;

static Log l("rmp");
typedef struct EventMessage EventMessage;
static std::list<EventMessage> eventMsgList;

static bool startsWith (const string *str, const string prefix)
{
    return ((prefix.size() <= str->size()) &&
            equal(prefix.begin(), prefix.end(), str->begin()));
}

static void startUsage ()
{
    if (!task.rmp.started && task.rmp.serviceTask->usage) return;

    double now = currentUnixTime();
    task.rmp.serviceTask->usage->startBroadcast(now, 1.0, now);
    task.rmp.serviceTask->usage->startComponent("v1", "1", "0", "Primary Video", "eng", now);
    task.rmp.serviceTask->usage->startSourceDeliveryPath("v1", "0", now);
    task.rmp.serviceTask->usage->startComponent("a1", "0", "0", "Primary Audio", "eng", now);
    task.rmp.serviceTask->usage->startSourceDeliveryPath("a1", "0", now);
}

static void stopUsage ()
{
    if (!task.rmp.serviceTask->usage) return;

    double now = currentUnixTime();
    task.rmp.serviceTask->usage->stopSourceDeliveryPath("v1", now);
    task.rmp.serviceTask->usage->stopComponent("v1", now);
    task.rmp.serviceTask->usage->stopSourceDeliveryPath("a1", now);
    task.rmp.serviceTask->usage->stopComponent("a1", now);
    task.rmp.serviceTask->usage->stopBroadcast(currentUnixTime());
}

static void setRmpPreference ()
{
    string ccEnabled;
    model.preference.get("ccEnabled", ccEnabled);
    string lang;
    model.preference.get("preferredCaptionSubtitleLang", lang);
    string displayPref;
    model.preference.get("captionDisplay", displayPref);
    string audioLang;
    model.preference.get("preferredAudioLang", audioLang);
    string videoDesc;
    model.preference.get("videoDescriptionService", videoDesc);
    json jvideoDesc = json::parse(videoDesc);
    bool videoDescEnabled = jvideoDesc["enabled"];
    string str = jvideoDesc["language"];

    task.platform->rmpSetPreference(ccEnabled=="true"?true:false,
            lang.c_str(), displayPref.c_str(), audioLang.c_str(),
            videoDescEnabled, str.c_str());
}

struct BlockedByRatingListener: acba::Reactor, acba::Listener<bool>
{
    bool isBlocked = false;
    void onUpdate (bool blocked) override {
        l.info("BlockedByRatingListener %d", blocked);
        if (blocked) {
            if (task.rmp.isSignaling) {
                isBlocked = true;
                task.eventDriver.setTimeout(1000000, this);

            }
        } else {
            if (task.rmp.isSignaling) {
                isBlocked = false;
                task.eventDriver.setTimeout(1000000, this);
            }

        }
    }

    void processEvent(int evt) override {
        if (isBlocked)
            task.rmp.stopRmp();
        else
            task.rmp.resumeService();
    }
} ratingBlockedListener;

static xml_node findContentProtection (xml_node mpd)
{
    for (auto as: mpd.select_nodes("//AdaptationSet")) {
        for (auto rep: as.node().children("Representation")) {
            for (auto cp: as.node().children("ContentProtection")) {
                if (strcmp(cp.attribute("schemeIdUri").value(),
                    "urn:uuid:edef8ba9-79d6-4ace-a3c8-27dcd51d21ed") == 0)
                    return cp; //Widevine
                else if (strcmp(cp.attribute("schemeIdUri").value(),
                    "urn:uuid:9a04f079-9840-4286-ab92-e65be0885f95") == 0)
                    return cp; //Playready
                else if (strcmp(cp.attribute("schemeIdUri").value(),
                    "urn:uuid:1077efec-c0b2-4d02-ace3-3c1e52e2fb4b") == 0)
                    return cp; //Clear Key
            }
        }
    }

    return xml_node();
}

void Rmp::onUpdate ()
{
    tryStart();
}

void Rmp::onUpdate (RMP_MEDIA_EVENT evt, double value)
{
    if (evt == RMP_MEDIA_TIME_CHANGE) {
        if (!task.rmp.nextContentByApp.empty()) {
            l.info("onUpdate RMP_MEDIA_TIME_CHANGE");
            if (task.rmp.isSignaling == true) {
                if (!task.rmp.nextContentByApp.empty() &&
                        task.rmp.syncTime > 0 && task.rmp.syncTime <= value) {
                    task.rmp.startRmp(&task.rmp.nextContentByApp);
                    task.rmp.nextContentByApp = "";
                    task.rmp.syncTime = 0;
                }
            }
        } else {
            if (task.rmp.syncTime > 0 && task.rmp.syncTime <= value) {
                task.rmp.stopRmp();
            }
        }

    } else if (evt == RMP_PLAY_STATE_CHANGE) {
        if (task.rmp.rmpPlaybackState == RMP_PLAY_STATE_ENDED) {
            l.info("onUpdate RMP_PLAY_STATE_CHANGE END_OF_STREAM");
            if (task.rmp.isSignaling == false && !task.rmp.nextContentByApp.empty()) {
                task.rmp.startRmp(&task.rmp.nextContentByApp);
                task.rmp.nextContentByApp = "";
                task.rmp.syncTime = 0;
            } else {
                task.rmp.started = false;
                task.rmp.tryStart();
            }
        } else if (task.rmp.rmpPlaybackState == RMP_PLAY_STATE_PLAYING) {
            // nothing to do
        } else if (task.rmp.rmpPlaybackState == RMP_PLAY_STATE_FAILED) {
            // This scenario is not defined in ATSC3.0 spec(neither A344)
            if (!task.rmp.isSignaling) {
                task.rmp.nextContentByApp = "";
                task.rmp.syncTime = 0;
                task.rmp.resumeService();
            }
        }
    }
}

void Rmp::tryStart ()
{
    if (started) return;
    assert(serviceTask);
    if (!serviceTask->playReady) return;

    isSignaling = true;

    switch (serviceTask->service->protocol) {
    case 1: // ROUTE
        {
            auto rsrv = static_cast<RouteService*>(serviceTask->service.get());
            if (!rsrv->resolvedMpdFile) break;

            char url[256];
            sprintf(url, "http://localhost:8080/dash/%d/%d/_mpd.xml",
                    rsrv->bsid, rsrv->id);
            l.info("Starting dash stream at %s", url);
            task.platform->rmpStart(url);
            started = true;
        }
        break;

    case 2: // MMT
        {
            auto msrv = static_cast<MmtpService*>(serviceTask->service.get());
            char url[256];
            sprintf(url, "mpu://localhost:8080/mpu/%d/%d",
                    msrv->bsid, msrv->id);
            l.info("Starting mpu stream at %s", url);
            // TODO drm
            task.platform->rmpStart(url);
            started = true;
        }
        break;
    }

    if (!started) return;

    startUsage();
}

void Rmp::play (ServiceTask *stask)
{
    if (!stask) return;
    if (serviceTask) stop();

    nextContentByApp = "";
    syncTime = 0;

    serviceTask = stask;
    serviceTask->playReadyUpdate.addListener(this);
    task.rmp.rmpMediaTimeChangeUpdate.addListener(this);
    task.rmp.rmpPlaybackStateChangeUpdate.addListener(this);
    task.ratingHandler.blockedByRatingUpdate.addListener(&ratingBlockedListener);
    tryStart();
}

void Rmp::startRmp (const std::string *url)
{
    l.info("startRmp url %s", url->c_str());
    if (!serviceTask) return;

    isSignaling = false;
    started = true;
    // TODO MPD Change Notification
    // TODO DRM for Mpd url
    if (startsWith(url, "http"))
        task.platform->rmpStart(url->c_str());
    else {
        char chrUrl[256];
        sprintf(chrUrl, "%s%s%s",
                task.applicationManager.applicationTask->contextCache->baseUri.c_str(),
                "/",url->c_str());
        l.info("startRmp url %s", chrUrl);
        task.platform->rmpStart(chrUrl);
    }
    stopUsage();
}

void Rmp::stopRmp ()
{
    l.info("stopRmp");
    if (!serviceTask) return;

    started = false;
    syncTime = 0;
    task.platform->rmpStop();
    if (isSignaling == true) {
        stopUsage();
    }
}

void Rmp::resumeService ()
{
    l.info("resumeService");
    if (!serviceTask) return;

    if (started == false) {
        tryStart();
    } else if ( isSignaling == false ) {
        started = false;
        task.platform->rmpStop();
        task.eventDriver.setTimeout(500, this);
    }
}

void Rmp::processEvent (int evt) {
    l.info("processEvent tryStart");
    tryStart();
}

void Rmp::getRMPMediaTime (atsc3_RMPMediaTime *rmpMediaTime)
{
    task.platform->rmpGetRMPMediaTime(rmpMediaTime);
}

double Rmp::getPlaybackRate ()
{
    return task.platform->rmpGetPlaybackRate();
}

void Rmp::setScalePosition (double scaleFactor, double xPos, double yPos)
{
    task.platform->rmpSetScalePosition(scaleFactor, xPos, yPos);
}

void Rmp::initPreference ()
{
    setRmpPreference();
}

const char* Rmp::findDRMSchemeId ()
{
    drmSchemeId = "";
    // for test
    //drmSchemeId = "urn:uuid:edef8ba9-79d6-4ace-a3c8-27dcd51d21ed";
    // read Period from MPD
    if (task.serviceManager.currentServiceTask) {
        auto routeSrvTask = static_cast<RouteServiceTask*>
                    (task.serviceManager.currentServiceTask.get());
        RouteService *rsrv = static_cast<RouteService*>(routeSrvTask->service.get());
        if (!rsrv->resolvedMpdFile) {
           l.error("mpd is not available");
            return drmSchemeId.c_str();
        }
        acba::Buffer &b = rsrv->resolvedMpdFile->content;

        xml_document doc;
        xml_parse_result res = doc.load_buffer(b.begin(), b.size());
        if (!res) {
            l.error("Failed to parse mpd xml");
            return drmSchemeId.c_str();
        }

        xml_node mpd = doc.child("MPD");

        xml_node contprotect = findContentProtection(mpd);
            if (contprotect) {
                //Drm SchemeID
                const char *_drmSchemeId = contprotect.attribute("schemeIdUri").value();
                l.info("ContentProtection SchemeId %s", _drmSchemeId);
                drmSchemeId = string(_drmSchemeId);
            }
    }
    return drmSchemeId.c_str();
}

void Rmp::receivedLicenseResponse (const char *systemId, const char *message) {
    task.platform->rmpReceivedLicenseResponse(systemId, message);
    l.info("receivedLicenseResponse");
}

void Rmp::cacheLicense (const char *systemId, unsigned char* buf, int size) {
    task.platform->rmpCacheLicense(systemId, buf, size);
    l.info("cacheLicense");
}

void Rmp::stop ()
{
    if (!serviceTask) return;

    if (started) {
        l.info("Stopping playback");
        task.platform->rmpStop();
        started = false;
        stopUsage();
    }
    eventMsgList.clear();

    serviceTask->playReadyUpdate.removeListener(this);
    serviceTask = 0;
    task.rmp.rmpMediaTimeChangeUpdate.removeListener(this);
    task.rmp.rmpPlaybackStateChangeUpdate.removeListener(this);
    task.ratingHandler.blockedByRatingUpdate.removeListener(&ratingBlockedListener);
}

void Rmp::PreferenceUpdateListener::onUpdate (const string &key)
{
    if (key == "rating" ||
        key == "ccEnabled" ||
        key == "preferredCaptionSubtitleLang" ||
        key == "captionDisplay" ||
        key == "videoDescriptionService" ||
        key == "preferredAudioLang") {
        setRmpPreference();
    }
}

struct EventMessage {
    string schemeIdUri;
    double id;
    string value;
    double presentationTimeUs;
    double durationMs;
    string data;
    double mediaStartMs;
};

void Rmp::setEventMessageFromPeriod (const string& _schemeIdUri, double _id,
            const string& _value, double _presentationTimeUs,
            double _durationMs, const string& _data, double _mediaStartMs)
{
    l.info("setEventMessageFromPeriod");
    EventMessage evtMsg = {
        string(_schemeIdUri), _id, string(_value), _presentationTimeUs,
        _durationMs, string(_data), _mediaStartMs
    };
    eventMsgList.push_back(evtMsg);
}

void Rmp::checkStoredEventMessage (const string& _schemeIdUri,
        const string& _value)
{
    l.info("checkStoredEventMessage EventMessageList size %d",
            eventMsgList.size());

    auto it = eventMsgList.begin();
    while (it != eventMsgList.end()) {
        if (strcmp(it->schemeIdUri.c_str(), _schemeIdUri.c_str()) == 0) {

            if (!_value.empty() &&
                strcmp(it->value.c_str(), _value.c_str()) != 0)
                continue;

            task.rmp.eventStreamEvent.notify(it->schemeIdUri, it->id,
                    it->value, it->presentationTimeUs, it->durationMs,
                    it->data, it->mediaStartMs);

            eventMsgList.erase(it);
        }
        it++;
    }
}
