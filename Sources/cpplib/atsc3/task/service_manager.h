#ifndef SERVICE_MANAGER_H
#define SERVICE_MANAGER_H

#include <pthread.h>
#include <math.h>

#include <limits>

#include "stream_reader.h"
#include "tune_manager.h"
#include "usage_manager.h"
#include "acba.h"
#include "DownloadTracer.h"
#include "mpd_parser.h"

namespace atsc3 {

struct ServiceTask: acba::RefCounter<ServiceTask> {

    ServiceTask (const acba::Ref<Service> &s): service(s), playReady(false) {}

    virtual void start () = 0;
    virtual void stop () = 0;

    acba::Ref<Service> service;
    acba::Ref<ServiceUsage> usage;

    bool playReady;
    acba::Update<> playReadyUpdate;
};

struct RouteSessionTask: acba::RefCounter<RouteSessionTask> {

    RouteSessionTask (const acba::Ref<RouteSession> &rs,
            const acba::Ref<RouteService> &rsrv);

    void start ();
    void stop ();

    acba::Ref<RouteSession> session;
    acba::Ref<RouteService> service;

    struct Payload {
        int formatId;
        int frag;
        bool order;
    };
    std::map<const std::pair<uint32_t,int>, Payload> payloads; // k:{tsi,cp}

    ////////////////////////////////////////////////////////////////////////////
    // fields for session specific thread

    struct PartialFile: acba::RefCounter<PartialFile> {
        PartialFile (uint32_t s, uint32_t o): tsi(s), toi(o), size(-1),
                state(DOWNLOADING), codePoint(-1), expireTime(0) {}

        uint32_t tsi, toi;
        int size;
        enum { DOWNLOADING, DOWNLOAD_COMPLETE, CACHED } state; // under mutex
        int codePoint;
        acba::DownloadTracer tracer;
        acba::Buffer content;
        /**
         * Early expire time during the retrival.
         * Used only when routeFile is not bound.
         */
        double expireTime;

        // output file either defined from FDT or entity mode header.
        acba::Ref<RouteFile> routeFile;
    };

    std::map<const std::pair<uint32_t,uint32_t>, acba::Ref<PartialFile> >
            partialFdts;
    uint32_t receivedBytes;

    pthread_mutex_t mutex;
    /**
     * If routeFile exists, a partial file is removed when the corresponding
     * routeFile is expired.
     * Otherwise, removed when itself expires or received flag B
     * which means no further transmission.
     */
    std::map<const std::pair<uint32_t,uint32_t>, acba::Ref<PartialFile> >
            partialFiles;

    std::map<const std::string, acba::Ref<PartialFile> > partialEntities;

    struct FileTemplate {
        FileTemplate (): expireTime(0.0) {}
        std::string name;
        double expireTime;
    };
    std::map<uint32_t, FileTemplate> fileTemplates; // tsi
    std::vector<acba::Ref<PartialFile> > fileQueue;

    struct LctReader: RefCountedStreamReader {
        int read (const byte_t *data, int sz, Chain *pctx) override;
        void ref () override {
            container_of(this, &RouteSessionTask::lctReader)->ref();
        }
        void deref () override {
            container_of(this, &RouteSessionTask::lctReader)->deref();
        }
    } lctReader;

    struct FdtHandler: acba::Listener<> {
        void onUpdate () override; // process EFDT from S-TSID
        void processFdt (uint32_t tsi, const acba::Ref<PartialFile> &pf);
        void processFdtInstance (uint32_t tsi, const pugi::xml_node fdti);
    private:
        void addFileInfo (uint32_t tsi, const pugi::xml_node info, double exp);
        void addFileTemplate (uint32_t tsi, const std::string &name,
                double exp);
    } fdtHandler;

    // Send complete route file to route service model
    struct Sender: acba::Reactor {
        // must be called during mutex lock
        void send (const acba::Ref<PartialFile> &pf);
        void processEvent (int evt) override;
    } sender;

    // Garbage collect expired partial files
    void trim ();
};

struct RouteServiceTask: ServiceTask {

    RouteServiceTask (const acba::Ref<Service> &s): ServiceTask(s) {}
    void start () override;
    void stop () override;
    void processUsd (const byte_t *data, int sz);
    void processHeld (const byte_t *data, int sz);

    std::vector<acba::Ref<RouteSessionTask> > sessionTasks;

    struct SlsHandler: acba::Listener<const acba::Ref<RouteFile>&>, StreamReader {
        void onUpdate (const acba::Ref<RouteFile> &f) override;
        int read (const byte_t *data, int sz, Chain *pctx) override;
    } slsHandler;

    struct DashHandler: acba::Reactor, acba::Listener<>,
            acba::Listener<const acba::Ref<RouteFile>&> {
        DashHandler (): offset(NAN) {}
        void onUpdate () override; // trigger play ready state
        void onUpdate (const acba::Ref<RouteFile> &f) override;
        void processEvent (int evt) override;
        bool forgeLiveMpd (const acba::Ref<XmlDocument> &doc);
        double deduceAst (dash::PeriodInfo &pi);

        struct SegmentStreamInfo {
            double receiveStartTime;
            acba::Ref<RouteFile> last;

            SegmentStreamInfo (): receiveStartTime(NAN) {}
        };
        std::map<uint32_t, SegmentStreamInfo> segments; // tsi

        acba::Ref<File> lastMpd;
        double offset;
    } dashHandler;

    struct EsgProcessor: acba::Listener<const acba::Ref<RouteFile>&> {
        void onUpdate (const acba::Ref<RouteFile> &f) override;
        void processSgdd (const acba::Ref<RouteFile> &f);
        void processSgdu (const acba::Ref<RouteFile> &f);
        void processFragment (byte_t type, int tid, uint32_t ver,
                const byte_t *data, int sz, const std::string &ids);
    } esgProcessor;
};

struct MmtpSessionTask: acba::RefCounter<MmtpSessionTask> {

    MmtpSessionTask (const acba::Ref<MmtpSession> &rs,
            const acba::Ref<MmtpService> &msrv, StreamReader *sr);

    void start ();
    void stop ();

    acba::Ref<MmtpSession> session;
    acba::Ref<MmtpService> service;

    pthread_mutex_t mutex;
    acba::Buffer dataUnitQueues[2];
    int dataUnitQueueIndex;

    struct MmtpReader: RefCountedStreamReader {
        MmtpReader (StreamReader *u = 0): slsReader(u) {}
        // Called from non-main thread.
        int read (const byte_t *data, int sz, Chain *pctx) override;
        StreamReader *slsReader;
        void ref () override {
            container_of(this, &MmtpSessionTask::mmtpReader)->ref();
        }
        void deref () override {
            container_of(this, &MmtpSessionTask::mmtpReader)->deref();
        }
    } mmtpReader;

    struct MpuFragmentReader: StreamReader, acba::Reactor {
        int read (const byte_t *data, int sz, Chain *pctx) override;
        void sendDataUnit (const byte_t *data, int sz, Chain *pctx);
        void processDataUnit (uint16_t pktId, uint32_t seq, uint8_t ft,
                const byte_t *data, int sz);
        void processEvent (int evt) override;

        struct PartialDataUnit: acba::RefCounter<PartialDataUnit> {
            PartialDataUnit () : fragmentCounter(-1) {}
            int fragmentCounter;
            uint32_t packetSequenceNumber; // valid only when fragmentCounter != -1
            acba::Buffer data;
        };
        std::map<uint16_t, acba::Ref<PartialDataUnit> > partialDataUnits;
        PartialDataUnit *getPartialDataUnit (int16_t pkgId) {
            auto &pdu = partialDataUnits[pkgId];
            if (!pdu) pdu = new PartialDataUnit();
            return pdu.get();
        }
    } mpuFragmentReader;
};

struct MmtpContext: ChainNode<MmtpContext> {
    uint16_t packetId;
    uint32_t timestamp;
    uint32_t packetSequenceNumber;
};

struct MmtpServiceTask: ServiceTask {

    MmtpServiceTask (const acba::Ref<Service> &s): ServiceTask(s),
            mutex(PTHREAD_MUTEX_INITIALIZER), messageQueueIndex(0) {}

    void start () override;
    void stop () override;

    std::vector<acba::Ref<MmtpSessionTask> > sessionTasks;
    std::vector<std::string> caRatingInUsbd;

    pthread_mutex_t mutex;
    acba::Buffer messageQueues[2];
    int messageQueueIndex;

    struct SlsReader: StreamReader, acba::Reactor {
        int read (const byte_t *data, int sz, Chain *pctx) override;
        void sendMessage (const byte_t *data, int sz);
        void processMessage (const byte_t *data, int sz);
        void processEvent (int events) override;
    } slsReader;

    struct MpuHandler: acba::Listener<const acba::Ref<Mpu>&, uint8_t, const void*> {
        void onUpdate (const acba::Ref<Mpu> &f, uint8_t, const void*) override;
    } mpuHandler;
};

struct BroadcastStreamTask: acba::RefCounter<BroadcastStreamTask>,
        acba::Listener<Tune::EVENT> {

    BroadcastStreamTask (BroadcastStream *bs): broadcastStream(bs) {}

    acba::Ref<BroadcastStream> broadcastStream;
    acba::Ref<Tune> tune;

    void start ();
    void stop ();
    void onUpdate (Tune::EVENT e) override;
};

struct ServiceManager {

    void reselectServices (BroadcastStream &bs); // for pcap stream rewind

    acba::Ref<ServiceTask> currentServiceTask;
    std::map<Service*, acba::Ref<ServiceTask> > serviceTasks;
    std::map<BroadcastStream*, acba::Ref<BroadcastStreamTask> >
            broadcastStreamTasks;

    acba::Update<> currentServiceTaskUpdate;

    struct Selector: acba::Reactor, acba::Listener<>,
            acba::Listener<const acba::Ref<Service>&> {
        void onUpdate (const acba::Ref<Service>& srv) override;
        void onUpdate () override;
        void processEvent (int events) override;
    } selector;

    struct Trimmer: acba::Reactor {
        void processEvent (int events) override;
    } trimmer;

    struct LlsReader: RefCountedStreamReader, acba::Reactor {
        LlsReader (): mutex(PTHREAD_MUTEX_INITIALIZER), llsQueueIndex(0) {}
        pthread_mutex_t mutex;
        acba::Buffer llsQueues[2];
        int llsQueueIndex;
        int read (const byte_t *data, int sz, Chain *pctx) override;
        void processEvent (int events) override;
        int readLls (const byte_t *data, int sz, const std::string &loc);
    } llsReader;

    struct AlertFlusher: acba::Listener<> {
        void onUpdate () override;
    } alertFlusher;

    struct LlsContext {
        uint8_t tableId;
        uint8_t groupId;
        uint8_t groupCountMinus1;
        uint8_t version;
        acba::Ref<ServiceGroup> serviceGroup;
        acba::Ref<BroadcastStream> broadcastStream;
    };

    void processLls (const byte_t *data, int sz, LlsContext *ctx);
    void processSlt (const byte_t *data, int sz, LlsContext *ctx);
    void processAeat (const byte_t *data, int sz, LlsContext *ctx);
    void processOsmn (const byte_t *data, int sz, LlsContext *ctx);
    void processMultiLls (const byte_t *data, int sz, LlsContext *ctx);

    void save ();
    void load ();
    void clear ();

    struct AutoSaver: acba::Reactor {
        void processEvent (int events) override;
    } autoSaver;
};

}
#endif
