#include <stdio.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/types.h>

#include <sstream>

#include "atsc3_task.h"
#include "pcap_reader.h"
#include "ip_reader.h"
#include "tune_manager.h"
#include "datetime.h"
#include "service_manager.h"
#include "pugixml.hpp"
#include "gzip_reader.h"
#include "acba.h"

#define AUTOSAVE_DELAY 10
#define SERVICE_LIST_TYPE "srv0"

using namespace std;
using namespace acba;
using namespace atsc3;
using namespace pugi;

static Log l("srvmgr");

void ServiceManager::Selector::onUpdate (const acba::Ref<Service>& srv)
{
    if (!srv || srv->category == 4) task.eventDriver.setTimeout(0, this);
}

void ServiceManager::Selector::onUpdate ()
{
    task.eventDriver.setTimeout(0, this);
}

void ServiceManager::Selector::processEvent (int events)
{
    auto self = container_of(this, &ServiceManager::selector);

    // construct list of services to be selected
    set<Service*> srvs;
    if (model.currentService) srvs.insert(model.currentService.get());
    for (auto s: model.backgroundServices) srvs.insert(s.get());

    // construct list of broadcast streams to be used
    set<BroadcastStream*> bss;
    for (auto s: srvs) {
        // TODO: handle multiple bs
        bss.insert(model.broadcastStreams[s->bsid].get());
    }

    // Add esg/drm services from determined broadcast streams.
    for (auto bs: bss) {
        for (auto sg: bs->serviceGroups) {
            for (auto s: sg.second->services) {
                switch(s.second->category) {
                case 4: // esg
                case 6: // drm
                    srvs.insert(s.second.get());
                }
            }
        }
    }

    // stop unused service tasks
    vector<ServiceTask*> sts;
    for (auto st: self->serviceTasks) {
        if (srvs.find(st.first) == srvs.end()) {
            sts.push_back(st.second.get());
        }
    }
    for (auto st: sts) {
        st->stop();
        self->serviceTasks.erase(st->service.get());
    }

    // stop unused broadcast stream tasks
    vector<BroadcastStreamTask*> bsts;
    for (auto bst: self->broadcastStreamTasks) {
        if (bss.find(bst.first) == bss.end()) {
            bsts.push_back(bst.second.get());
        }
    }
    for (auto bst: bsts) {
        bst->stop();
        self->broadcastStreamTasks.erase(bst->broadcastStream.get());
    }

    // start missing broadcast stream tasks
    for (auto bs: bss) {
        auto &bst = self->broadcastStreamTasks[bs];
        if (bst) continue;

        bst = new BroadcastStreamTask(bs);
        bst->start();
    }

    // start service tasks
    for (auto s: srvs) {
        auto &st = self->serviceTasks[s];
        if (st) continue;

        // Start new service
        switch (s->protocol) {
        case 1: // ROUTE
            st = new RouteServiceTask(s);
            break;
        case 2: // MMT
            st = new MmtpServiceTask(s);
            break;
        }
        l.info("Start service %s", s->xml.attribute("shortServiceName").
                value());
        st->start();
    }

    // Check current service
    Service *newsrv = model.currentService.get();
    Service *prevsrv = 0;
    if (self->currentServiceTask) {
        prevsrv = self->currentServiceTask->service.get();
    }
    if (newsrv == prevsrv) return;

    if (newsrv) {
        self->currentServiceTask = self->serviceTasks[newsrv];
        self->currentServiceTaskUpdate.notify();

        task.rmp.play(self->currentServiceTask);
    } else {
        // unnecessary
        //task.rmp.stop();

        self->currentServiceTask = 0;
        self->currentServiceTaskUpdate.notify();
    }
}

void ServiceManager::reselectServices (BroadcastStream &bs)
{
    // stop tasks
    for (auto sg: bs.serviceGroups) {
        for (auto sv: sg.second->services) {
            Service *s = sv.second.get();
            if (serviceTasks.find(s) == serviceTasks.end()) continue;
            auto &t = serviceTasks[s];

            t->stop();
            if (t == currentServiceTask) {
                currentServiceTask = 0;
                currentServiceTaskUpdate.notify();
            }
            serviceTasks.erase(s);
        }
    }

    // start tasks
    task.eventDriver.setTimeout(0, &selector);
}

void ServiceManager::Trimmer::processEvent (int events)
{
    double now = currentUnixTime();

    int nLive = 0;
    int nExpired = 0;
    int memLive = 0;
    int memExpired = 0;

    for (auto &bs: model.broadcastStreams) {
        for (auto &sg: bs.second->serviceGroups) {
            for (auto &srv: sg.second->services) {
                if (srv.second->protocol == 1) {
                    auto rsrv = static_cast<RouteService*>(srv.second.get());
                    auto &fs = rsrv->files;
                    auto rfi = fs.begin();
                    while (rfi != fs.end()) {
                        auto &rf = rfi->second;

                        if (rf->expireTime > now) {
                            rfi++;

                            // stat
                            nLive++;
                            memLive += rf->content.size();

                            continue;
                        }

                        // stat
                        nExpired++;
                        memExpired += rf->content.size();

                        l.info("COLLECT FILE %u.%u %s", rf->tsi, rf->toi,
                                rf->path.c_str());
                        rfi = fs.erase(rfi);
                    }
                } else {
                    auto msrv = static_cast<MmtpService*>(srv.second.get());
                    auto &fs = msrv->mpus;
                    auto mfi = fs.begin();
                    while (mfi != fs.end()) {
                        auto &mf = mfi->second;

                        if (mf->expireTime > now) {
                            mfi++;

                            // stat
                            nLive++;
                            memLive += mf->mpuMetadata.size();
                            memLive += mf->movieFragmentMetadata.size();
                            memLive += mf->mediaData.size();

                            continue;
                        }

                        // stat
                        nExpired++;
                        memExpired += mf->mpuMetadata.size();
                        memExpired += mf->movieFragmentMetadata.size();
                        memExpired += mf->mediaData.size();

                        l.info("COLLECT MPU %u.%u",
                                mf->packetId, mf->sequenceNumber);
                        mfi = fs.erase(mfi);
                    }
                }
            }
        }
    }

    l.info("MEMINFO live:%d(%.2fMB) expired:%d(%.2fMB)", nLive,
            memLive / 1000000.0, nExpired, memExpired / 1000000.0);

    task.eventDriver.setTimeout(10 * 1000000, this);
}

int ServiceManager::LlsReader::read (const byte_t *data, int sz, Chain *pctx)
{
    auto self = container_of(this, &ServiceManager::llsReader);

    TuneContext *tctx = pctx->as<TuneContext>();
    if (!tctx) {
        l.error("Dropped LLS without TuneContext");
        return sz;
    }
    Tune *tune = tctx->tune;

    if (sz < 4) {
        l.warn("Too short LLS");
        return sz;
    }

    uint16_t llsId = ((uint16_t)data[0] << 8) + data[1]; // groupId + tableId
    uint8_t ver = data[3];
    auto lvi = tune->llsVersions.find(llsId);
    if (lvi != tune->llsVersions.end() && lvi->second == ver) return sz;

    tune->llsVersions[llsId] = ver;

    {
        ScopedLock sl(mutex);
        auto &lq = llsQueues[llsQueueIndex];
        int len = tune->location.size();
        lq.writeI32(len);
        lq.writeBytes((const uint8_t*)tune->location.c_str(), len);
        lq.writeI32(sz);
        lq.writeBytes(data, sz);
        task.eventDriver.setTimeout(0, this);
    }

    return sz;
}

void ServiceManager::LlsReader::processEvent (int evt)
{
    Buffer *plq;
    {
        ScopedLock sl(mutex);
        plq = &llsQueues[llsQueueIndex];
        llsQueueIndex ^= 1;
        assert(llsQueues[llsQueueIndex].size() == 0);
    }
    Buffer &lq = *plq;

    int sz;
    for (; lq.size() > 0; lq.drop(sz)) {
        int len = lq.readI32();
        string loc((char*)lq.begin(), len);
        lq.drop(len);

        sz = lq.readI32();

        readLls(lq.begin(), sz, loc);
    }
}

void ServiceManager::AlertFlusher::onUpdate ()
{
    auto &srv = model.currentService;
    auto &aai = model.activeAeatInfo;
    auto &aoi = model.activeOsmnInfo;

    if (!(srv && aai && srv->bsid == aai->bsid && srv->sgid == aai->sgid)) {

        // flush aeat cache
        if (srv) {
            auto &bs = model.broadcastStreams[srv->bsid];
            assert(bs);
            auto &sg = bs->serviceGroups[srv->sgid];
            assert(sg);
            sg->llsFiles.erase(4);
        }

        // remove active aeat
        if (aai) {
            aai = nullptr;
            model.activeAeatInfoUpdate.notify();
        }
    }

    if (!(srv && aoi && srv->bsid == aoi->bsid)) {

        // flush osn cache
        if (srv) {
            auto &bs = model.broadcastStreams[srv->bsid];
            assert(bs);
            for (auto sg: bs->serviceGroups) {
                sg.second->llsFiles.erase(5);
            }
        }

        // flush active osn
        if (aoi) {
            aoi = nullptr;
            model.activeOsmnInfoUpdate.notify();
        }
    }
}

int ServiceManager::LlsReader::readLls (const byte_t *data, int sz,
        const string &loc)
{
    auto self = container_of(this, &ServiceManager::llsReader);

    LlsContext meta;

    // find broadcaststream
    auto &bs = model.broadcastStreamByLocation[loc];
    if (!bs) {
        bs = new BroadcastStream();
        bs->location = loc;
        l.error("Discovered new BroadcastStream %s", loc.c_str());
    }
    meta.broadcastStream = bs;

    // parse LLS
    const byte_t *d = data;
    const byte_t *end = data + sz;

    meta.tableId = *d++;
    meta.groupId = *d++;
    meta.groupCountMinus1 = *d++;
    meta.version = *d++;

    // find service group
    // FIXME: bs->id is still -1. need to get from driver...
    auto &sg = bs->serviceGroups[meta.groupId];
    if (!sg) sg = new ServiceGroup(meta.groupId, bs->id);
    meta.serviceGroup = sg;

    self->processLls(d, end - d, &meta);
    return sz;
}

void ServiceManager::processSlt (const byte_t *data, int sz, LlsContext *ctx)
{
    Ref<XmlDocument> sltDoc = new XmlDocument();
    xml_parse_result res = sltDoc->load_buffer(data, sz);
    if (!res) {
        l.error("Failed to parse SLT! %s\n", res.description());
        return;
    }

    // Get BroadcastStream matching with bsid
    int bsid = sltDoc->child("SLT").attribute("bsid").as_int(-1);
    if (bsid < 0) {
        l.error("No bsid attribute found from SLT");
        return;
    }

    Ref<Service> cursrv = model.currentService;
    Ref<Service> nextsrv = cursrv;

    // Handle later determination of bsid especialy for pcap stream
    // TODO: In case of real atsc3.0 tuner, bsid should be available ealier
    auto &bs = ctx->broadcastStream;
    if (bs->id == -1) {
        bs->id = bsid;
        for (auto sg: bs->serviceGroups) sg.second->bsid = bsid;
        model.broadcastStreams[bsid] = bs;
        l.info("BROADCAST STREAM bsid:%d", bsid);
    }

    // TODO: Fine-grained update
    auto &sg = ctx->serviceGroup;
    if (cursrv && cursrv->bsid == bs->id && cursrv->id == sg->id) {
        nextsrv = nullptr;
    }
    sg->services.clear();
    for (xml_node s: sltDoc->child("SLT").children("Service")) {
        const int sid = s.attribute("serviceId").as_int(-1);
        const string gsid = s.attribute("globalServiceID").value();
        int sver = s.attribute("sltSvcSeqNum").as_int();
        int cat = s.attribute("serviceCategory").as_int(-1);

        l.info("SERVICE %d %s ch:%s.%s cat:%d name:%s ver:%d",
                sid, gsid.c_str(),
                s.attribute("majorChannelNo").value(),
                s.attribute("minorChannelNo").value(),
                cat,
                s.attribute("shortServiceName").value(),
                sver);

        xml_node b = s.child("BroadcastSvcSignaling");
        int proto = b.attribute("slsProtocol").as_int();
        l.info("    proto:%d %s-%s:%s", proto,
                b.attribute("slsSourceIpAddress").as_string("any"),
                b.attribute("slsDestinationIpAddress").value(),
                b.attribute("slsDestinationUdpPort").value());

        Ref<Service> &srv = sg->services[sid];
        if (srv && srv->version == sver) continue;

        switch (proto) {
        case 1: // ROUTE
            {
                // TODO: Reuse instance...
                auto rsrv = new RouteService();
                srv = rsrv;

                RouteSession *rs = new RouteSession(
                        ip2u(b.attribute("slsSourceIpAddress").value()),
                        ip2u(b.attribute("slsDestinationIpAddress").value()),
                        atoi(b.attribute("slsDestinationUdpPort").value()));
                rsrv->mainSession = rs;
                rsrv->sessions.push_back(rs);
            }
            break;

        case 2: // MMTP
            {
                // TODO: Reuse instance...
                auto msrv = new MmtpService();
                srv = msrv;

                auto ms = new MmtpSession(
                        ip2u(b.attribute("slsDestinationIpAddress").value()),
                        atoi(b.attribute("slsDestinationUdpPort").value()));
                msrv->sessions.push_back(ms);
            }
            break;

        default:
            l.error("Skipped adding a service with unknown slsProtocol:%d",
                    proto);
            sg->services.erase(sid);
            continue;
        }

        srv->bsid = bs->id;
        srv->sgid = sg->id;
        srv->id = sid;
        srv->globalId = gsid;
        srv->version = sver;
        srv->protocol = proto;
        srv->category = cat;
        srv->sltDocument = sltDoc;
        srv->xml = s;

        model.serviceUpdate.notify(srv);

        // Check if this service replaces currently selected service.
        if (cursrv && cursrv->globalId == srv->globalId) {
            nextsrv = srv;
        }
    }

    // TODO: check service group completness
    auto thi = task.tuneManager.tunes.find(bs->location);
    if (thi != task.tuneManager.tunes.end()) {
        thi->second.tune->notify(Tune::EVENT_SERVICE_LIST_RECEIVED);
    }

    // Reselect current service if updated (could be null)
    if (cursrv != nextsrv) model.selectService(nextsrv);
}

void ServiceManager::processAeat (const byte_t *data, int sz,
        LlsContext *ctx)
{
    Ref<XmlDocument> aeatDoc = new XmlDocument();
    xml_parse_result res = aeatDoc->load_buffer(data, sz);
    if (!res) {
        l.error("Failed to parse AEIT! %s\n", res.description());
        return;
    }

    double now = currentUnixTime();

    l.info("AEAT DUMP");

    for (xml_node a: aeatDoc->child("AEAT").children("AEA")) {

        const char* aeaId = a.attribute("aeaId").value();
        const char* issuer = a.attribute("issuer").value();
        const char* audience = a.attribute("audience").value();
        const char* aeaType = a.attribute("aeaType").value();
        const char* refAEAId = a.attribute("refAEAId").value();
        const int priority = a.attribute("priority").as_int(-1);
        l.info("AEA id:%s issue:%s audience:%s type:%s refId:%s "
                "priority:%d wakeup:%d",
                aeaId, issuer, audience, aeaType, refAEAId, priority,
                a.attribute("wakeup").as_bool(false));

        // check validity
        if ( strlen(aeaId) > 62 ) {
            l.warn("length of aeaId is over 62");
        }
        if ( strlen(issuer) > 32 ) {
            l.warn("length of issuer is over 32");
        }
        if ( strcmp(audience, "public") && strcmp(audience, "restricted")
                && strcmp(audience, "private") ) {
            l.warn("audience has unknown value");
        }
        if ( !strcmp(aeaType, "alert") ) {
            if ( strlen(refAEAId) > 0 ) {
                l.warn("alert type has refAEAId");
            }
            if ( priority < 0 || priority > 4 ) {
                l.warn("alert type has invalid priority");
            }
        } else if ( !strcmp(aeaType, "update") ) {
            if ( strlen(refAEAId) == 0 ) {
                l.warn("update type doesn't have refAEAId");
            }
            if ( priority < 0 || priority > 4 ) {
                l.warn("update type has invalid priority");
            }
        } else if ( !strcmp(aeaType, "cancel") ) {
            if ( strlen(refAEAId) == 0 ) {
                l.warn("update type doesn't have refAEAId");
            }
        } else {
            l.warn("aeaType has unknown value");
        }

        xml_node h = a.child("Header");
        xml_node h_evtCode = h.child("EventCode");

        l.info("<Header>");
        l.info(" - effective:%s expires:%s",
                h.attribute("effective").as_string("immediately"),
                h.attribute("expires").value());
        l.info(" - EventCode:%s (type=%s)",
                h_evtCode.text().get(), h_evtCode.attribute("type").value());
        for (xml_node desc: h.children("EventDesc")) {
            l.info(" - EventDesc:%s (lang=%s/%s)",
                    desc.text().get(), desc.attribute("lang").value(),
                    desc.attribute("xml:lang").value());
        }
        for (xml_node loc: h.children("Location")) {
            l.info(" - Location:%s (type=%s)",
                    loc.text().get(), loc.attribute("type").value());
        }

        // AEAText : 0..N
        for (xml_node t: a.children("AEAText")) {
            l.info("<AEAText>");
            l.info(" - lang(%s/%s) text(len=%d):%s",
                t.attribute("lang").value(),
                t.attribute("xml:lang").value(),
                strlen(t.text().get()), t.text().get());
        }

        // LiveMedia : 0..1
        xml_node live = a.child("LiveMedia");
        if ( live.empty() == false ) {
            l.info("<LiveMedia>");
            l.info(" - bsid:%s serviceId:%d",
                    live.attribute("bsid").value(),
                    live.attribute("serviceId").as_int());
            for (xml_node sn: live.children("ServiceNmae")) {
                l.info(" - serviceName : %s (lang=%s/%s)",
                        sn.value(), sn.attribute("lang").value(),
                        sn.attribute("xml:lang").value());
            }
        }

        // Media : 0..N
        for (xml_node m: a.children("Media")) {
            l.info("<Media>");
            l.info(" - lang:%s/%s desc:%s type:%s",
                m.attribute("lang").value(),
                m.attribute("xml:lang").value(),
                m.attribute("mediaDesc").value(),
                m.attribute("mediaType").value());
            l.info(" - url:%s",
                m.attribute("url").value());
            l.info(" - alternateUrl:%s",
                m.attribute("altenateUrl").value());
            l.info(" - type:%s length:%d",
                m.attribute("contentType").value(),
                m.attribute("contentLength").as_int());
            l.info(" - mediaAssoc:%s",
                m.attribute("mediaAssoc").value());
        }
        l.info("");
    }

// process AEAT which belongs to current BS/SG.
    int bsid = ctx->broadcastStream->id;
    int sgid = ctx->serviceGroup->id;
    if (model.currentService) {
        if ( model.currentService->bsid == bsid
                && model.currentService->sgid == sgid ) {
            Ref<AeatInfo> actAeatInfo = new AeatInfo(bsid, sgid);
            actAeatInfo->receiveTime = now;
            actAeatInfo->aeatDocument = aeatDoc;
            model.activeAeatInfo = actAeatInfo;
            model.activeAeatInfoUpdate.notify();
        }
    } else {
        l.info("model.currentService is NOT SET yet!!!");
    }
}

void ServiceManager::processOsmn (const byte_t *data, int sz,
        LlsContext *ctx)
{
    Ref<XmlDocument> osmnDoc = new XmlDocument();
    xml_parse_result res = osmnDoc->load_buffer(data, sz);
    if (!res) {
        l.error("Failed to parse OSMN! %s\n", res.description());
        return;
    }

    double now = currentUnixTime();

    l.info("OnscreenMessageNotification DUMP");

    xml_node osmn = osmnDoc->child("OnscreenMessageNotification");
    for (xml_node k: osmn.children("KeepScreenClear")) {
        const int bsid = k.attribute("bsid").as_int(-1);
        const int serviceId = k.attribute("serviceId").as_int(-1);
        const int serviceIdRange = k.attribute("serviceIdRange").as_int(0);
        const char* duration =
                k.attribute("notificationDuration").as_string("PT1M");
        const bool kscFlag = k.attribute("kscFlag").as_bool(true);
        const int version = k.attribute("version").as_int(-1);

        l.info("KeepScreenClear");
        l.info(" - bsid:%d serviceId:%d serviceIdRange:%d",
                bsid, serviceId, serviceIdRange);
        l.info(" - notificationDuration:%s kscFlag:%d version:%d",
                duration, kscFlag, version);

        if ( bsid == -1 ) {
            l.warn("OSMN : bsid is NOT available");
        }

        if ( serviceId == -1 && serviceIdRange != 0 ) {
            l.warn("OSMN : no serviceId, but serviceIdRange exists");
        }

        if ( version == -1 ) {
            l.warn("OSMN : version is NOT available");
        }
    }

    // process OSMN which belongs to current BS.
    int bsid = ctx->broadcastStream->id;
    int sgid = ctx->serviceGroup->id;
    if (model.currentService) {
        if ( model.currentService->bsid == bsid ) {
            Ref<OsmnInfo> actOsmnInfo = new OsmnInfo(bsid, sgid);
            actOsmnInfo->receiveTime = now;
            actOsmnInfo->osmnDocument = osmnDoc;
            model.activeOsmnInfo = actOsmnInfo;
            model.activeOsmnInfoUpdate.notify();
        }
    } else {
        l.info("model.currentService is NOT SET yet!!!");
    }
}

void ServiceManager::processLls (const byte_t *data, int sz, LlsContext *ctx)
{
    // get cached lls entry
    auto &lf = ctx->serviceGroup->llsFiles[ctx->tableId];
    if (!lf) lf = new File();
    auto &buf = lf->content;
    auto &ver = lf->attributes["version"];

    // skip if lls message was unchanged.
    if (buf.size() == sz && memcmp(data, buf.begin(), buf.size()) == 0 &&
            ver.getType() == VARIANT_TYPE_INT && ver.asI32() == ctx->version) {
        return;
    }

    // update msg
    ver = ctx->version;
    buf.clear();
    buf.writeBytes(data, sz);
    lf->attributes["groupCountMinus1"] = ctx->groupCountMinus1; // for load/save
    l.info("lls from %s group:%d/%d table:%d ver:%d\n",
            ctx->broadcastStream->location.c_str(),
            ctx->groupId, ctx->groupCountMinus1 + 1,
            ctx->tableId, ctx->version);
    ctx->broadcastStream->llsUpdate.notify(ctx->tableId, ctx->groupId, lf);

    task.eventDriver.setTimeout(AUTOSAVE_DELAY * 1000000,
            &autoSaver);

    // unzip if applicable
    GzipReader gr;
    Buffer &buf2 = gr.outBuffer;
    switch (ctx->tableId) {
    case 1: // slt
    case 2: // rrt
    case 3: // systemtime
    case 4: // aeat
    case 5: // onscreenmessagenotification
        if (gr.read(buf.begin(), buf.size(), nullptr) != buf.size()) {
            l.warn("gunzip failed");
            return;
        }
        lf->attributes["compressed"] = true;
    }

    // process table
    switch (ctx->tableId) {
    case 1: // slt
        processSlt(buf2.begin(), buf2.size(), ctx);
        break;

    case 4: // aeat
	processAeat(buf2.begin(), buf2.size(), ctx);
	break;

    case 5: // onscreenmessagenotification
	processOsmn(buf2.begin(), buf2.size(), ctx);
        break;

    case 254: // signedmultitable
        processMultiLls(buf.begin(), buf.size(), ctx);
        break;

    case 2: // rrt
    case 3: // systemtime
    case 6: // security
        l.warn("TODO: NOT IMPLEMENTED FOR LLS TABLE_ID:%u", ctx->tableId);
        break;

    default:
        l.warn("Unsupported lls table id:%d\n", ctx->tableId);
        break;
    }
}

#define SAFEREAD_LOG(args...) l.warn("expected " args)
#define SAFEREAD_RETURN return

void ServiceManager::processMultiLls (const byte_t *data, int sz,
        LlsContext *ctx)
{
    const byte_t *d = data;
    const byte_t *e = data + sz;

    LlsContext meta = *ctx;

    int cnt = SAFEREAD_U8(d, e, "LLS_payload_count");
    for (int i = 0; i < cnt; i++) {
        meta.tableId = SAFEREAD_U8(d, e, "LLS_payload_id");
        meta.version = SAFEREAD_U8(d, e, "LLS_payload_version");
        int len = SAFEREAD_U16(d, e, "LLS_payload_length");
        SAFEREAD_CHECKSIZE(d, e, len, "Invalid lls size");
        processLls(d, len, &meta);
        d += len;
    }

    int siglen = SAFEREAD_U16(d, e, "signature_length");
    // TODO: impl
    SAFEREAD_CHECKSIZE(d, e, siglen, "signature()");
}

void ServiceManager::save () {
    Buffer buf;

    buf.writeBytes((const uint8_t*)SERVICE_LIST_TYPE, 4);

    for (auto &bs: model.broadcastStreams) {
        for (auto &sg: bs.second->serviceGroups) {
            auto slti = sg.second->llsFiles.find(1);
            if (slti == sg.second->llsFiles.end()) continue;

            auto &slt = slti->second;

            auto &loc = bs.second->location;
            buf.writeI32(loc.size()); // bs location
            buf.writeBytes((uint8_t*)loc.c_str(), loc.size());

            // reconstruct lls message
            auto &ct = slt->content;
            buf.writeI32(ct.size() + 4);
            buf.writeI8(1); // table id
            buf.writeI8(sg.first); // group id
            buf.writeI8(slt->attributes["groupCountMinus1"].asI32());
            buf.writeI8(slt->attributes["version"].asI32());
            buf.writeBytes(ct.begin(), ct.size());
        }
    }
    buf.preWriteI32(buf.size());

    stringstream ss;
    ss << model.storageRoot << "/service_list";
    FILE *fp = fopen(ss.str().c_str(), "w");
    if (!fp) {
        l.error("Failed to save service list: %s", strerror(errno));
        return;
    }

    fwrite(buf.begin(), 1, buf.size(), fp);
    fclose(fp);
    l.info("Saved service list");
}

void ServiceManager::load ()
{
    stringstream ss;
    ss << model.storageRoot << "/service_list";
    FILE *fp = fopen(ss.str().c_str(), "r");
    if (!fp) {
        l.info("Failed to load service list: %s", strerror(errno));
        return;
    }

    // load file
    Buffer buf;
    while (true) {
        buf.ensureMargin(512);
        auto sz = fread(buf.end(), 1, buf.margin(), fp);
        if (sz == 0) break;
        buf.grow(sz);
    }
    fclose(fp);

    // check header
    auto sz = buf.readI32();
    if (sz != buf.size() || sz < 4 ||
            memcmp(SERVICE_LIST_TYPE, buf.begin(), 4) != 0) {
        l.info("Invalid service list header");
        return;
    }
    buf.drop(4);

    // clear any existing service list from memory.
    clear();

    while (buf.size() > 0) {
        // read location
        int len = buf.readI32();
        string loc = {(char*)buf.begin(), (size_t)len};
        buf.drop(len);

        // read lls
        len = buf.readI32();
        llsReader.readLls(buf.begin(), len, loc);
        buf.drop(len);
    }

    // Suppress auto saver from triggering by this loading.
    task.eventDriver.unsetTimeout(&autoSaver);

    l.info("Loaded service list");
}

void ServiceManager::clear ()
{
    model.broadcastStreams.clear();
    model.broadcastStreamByLocation.clear();
    model.backgroundServices.clear();
    model.selectService(nullptr);

    model.serviceUpdate.notify(nullptr);

    l.info("Cleared service list");
}

void ServiceManager::AutoSaver::processEvent (int events)
{
    l.info("Auto saving...");

    auto self = container_of(this, &ServiceManager::autoSaver);
    self->save();
}
