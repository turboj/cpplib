#include <arpa/inet.h>
#include <sys/socket.h>
#include <netdb.h>
#include <stdlib.h>
#include <pthread.h>
#include <unistd.h> //to refer sleep()
#include <stdio.h>

#include <iostream>
#include <algorithm> // to case insensitive find
#include <string>
#include <cctype>
#include <boost/asio.hpp>
#include <boost/array.hpp>
#include <boost/bind.hpp>

#include "cd_manager.h"

#define DISCOVER_IP "239.255.255.250"
#define DISCOVER_PORT 1900

#define HTTP_PORT 8080
// Launcher discovery max response duration : Ignore response after 2s
#define DIS_MX 2

using namespace std;
using namespace acba;
using namespace atsc3;
using namespace boost::asio::ip;

static Log l("cdmgr");

CdManager *CdManager::instance = nullptr;
CdManager *CdManager::getInstance() {
    if (!instance) {
        instance = new CdManager();
    }
    return instance;
}

static CdManager* cdm = CdManager::getInstance();
static int lastDiscoveryCount;
static uint64_t timeSentLastDisvovery;

int CdManager::findStrIc(const string & strHaystack, const string & strNeedle)
{
  auto it = search(
    strHaystack.begin(), strHaystack.end(),
    strNeedle.begin(),   strNeedle.end(),
    [](char ch1, char ch2) { return toupper(ch1) == toupper(ch2); }
  );
  if (it != strHaystack.end()) return it - strHaystack.begin();
  return -1;
}

uint64_t CdManager::currentTimeMillis() {
  using namespace chrono;
  return duration_cast<milliseconds>(system_clock::now().time_since_epoch()).count();
}

uint64_t CdManager::currentTimeNanos() {
  using namespace chrono;
  return system_clock::now().time_since_epoch().count();
}

static string ipAddr("");

static void updateLocalIpAddrV4() {
    vector<uint32_t> addrs;
    getIpV4Addresses(addrs);

    for (auto ip: addrs) {
        if ((ip & 0xff000000) == 0x7f000000) continue; // skip loopback

        static uint32_t prevIp = 0;
        if (prevIp != ip) {
            char buf[32];
            sprintf(buf, "%u.%u.%u.%u", ip >> 24, (ip >> 16) & 0xff,
                    (ip >> 8) & 0xff, ip & 0xff);

            prevIp = ip;
            ipAddr = buf;
            l.info("updateLocalIpAddrV4() : NEW IP = [%s]", ipAddr.c_str());
        }
        break;
    }
}

static string getLocalIpAddrV4() {
    if (ipAddr.size() == 0) {
        updateLocalIpAddrV4();
    }
    return ipAddr;
}

string CdManager::getUpnpDescriptionXml() {
    //TODO : Device related information is required according to UPnP
    stringstream ss;
    ss << "<root xmlns=\"urn:schemas-upnp-org:device-1-0\" configId=\"\">";
    ss << "<specVersion>";
    ss << "<major>1</major>";
    ss << "<minor>1</minor>";
    ss << "</specVersion>";
    ss << "<device>";
    ss << "</device>";
    ss << "</root>";
    return ss.str();
}

// return WS url such as "ws://localhost:8080"
static void setBaseWsUrl(stringstream &ss) {
    ss << "ws://";
    ss << getLocalIpAddrV4();
    ss << ":" << HTTP_PORT;
}

// base WS url doesn't end with "/" based on A344-2019 8.2
// And also it's same as the param used when launch PD app by Terminal
static string getBaseWsUrl() {
    stringstream ss;
    setBaseWsUrl(ss);
    return ss.str();
}

// App2App url ends with "/" based on HbbTV 8.2.6.1
string CdManager::getLocalWsEndPoint() {
    stringstream ss;
    ss << "ws://127.0.0.1:8080";
    ss << CD_LOCAL_WS_ENDPOINT << "/";
    return ss.str();
}

// App2App url ends with "/" based on HbbTV 8.2.6.1
string CdManager::getRemoteWsEndPoint() {
    stringstream ss;
    setBaseWsUrl(ss);
    ss << CD_REMOTE_WS_ENDPOINT << "/";
    return ss.str();
}

string CdManager::getAppToAppUrlXml() {
    stringstream ss;
    ss << "<?xml version=\"1.0\" encoding=\"UTF8\"?>";
    ss << "<service xmlns=\"urn:dialmultiscreenorg:schemas:dial\" dialVer=\"1.7\">";
    ss << "<name>ATSC</name>";
    ss << "<options allowStop=\"false\"/>";
    ss << "<state>running</state>";
    ss << "<additionalData>";
    ss << "<X_ATSC_App2AppURL>";
    ss << getRemoteWsEndPoint();
    ss << "</X_ATSC_App2AppURL>";
    ss << "<X_ATSC_WSURL>";
    ss << getBaseWsUrl();
    ss << "</X_ATSC_WSURL>";
    ss << "<X_ATSC_UserAgent>";
    ss << "Google Chromium";
    ss << "</X_ATSC_UserAgent>";
    ss << "</additionalData>";
    ss << "</service>";
    return ss.str();
}

string CdManager::getWebEndPoint() {
    stringstream ss;
    ss << "http://";
    ss << getLocalIpAddrV4();
    ss << ":" << HTTP_PORT << CD_WEB_CONTEXT_ROOT << CD_WEB_ENDPOINT << "/";
    return ss.str();
}

static string getDiscoveryLocationUrl() {
    stringstream ss;
    ss << "http://";
    ss << getLocalIpAddrV4();
    ss << ":" << HTTP_PORT << CD_WEB_CONTEXT_ROOT << CD_DESC_XML;
    return ss.str();
}

////////////////////////////////////////////
// Send UDP packet once asynchronously
////////////////////////////////////////////
class UdpSender
{
public:
    UdpSender(boost::asio::io_context& io_context,
            const address& multicast_address, string message):
            endpoint_(multicast_address, DISCOVER_PORT),
            socket_(io_context, endpoint_.protocol()), message_(message) {
        do_send();
    }

private:
    void do_send() {
        socket_.async_send_to(boost::asio::buffer(message_), endpoint_,
                [this] (boost::system::error_code ec, size_t ) {
            // TODO: check error and print.
            socket_.close();
        });
    }

private:
    udp::endpoint endpoint_;
    udp::socket socket_;
    string message_;
};

static void sendUdp(string msg)
{
    boost::asio::io_context io_context;
    UdpSender s(io_context, make_address(DISCOVER_IP), msg);
    io_context.run();
}

static string buf2Str(boost::asio::streambuf &buf)
{
    return {(istreambuf_iterator<char>(&buf)), istreambuf_iterator<char>()};
}

// host : ex) "10.0.2.15"
// port : ex) "8080"
// path : ex) "/companion/Description.xml"
static string doSyncHttpInternal(string method, string host, string port,
        string path, string body)
{
    l.debug("doSyncHttpInternal : %s, %s, %s, %s, %s", method.c_str(),
            host.c_str(), port.c_str(), path.c_str(), body.c_str());
    try
    {
        boost::asio::io_service io_service;

        // Get a list of endpoints corresponding to the server name.
        tcp::resolver resolver(io_service);
        tcp::resolver::query query(host, port);
        tcp::resolver::iterator endpoint_iterator = resolver.resolve(query);
        // Try each endpoint until we successfully establish a connection.
        tcp::socket socket(io_service);
        boost::asio::connect(socket, endpoint_iterator);

        // Form the request. We specify the "Connection: close" header so that the
        // server will close the socket after transmitting the response. This will
        // allow us to treat all data up until the EOF as the content.
        boost::asio::streambuf request;
        ostream request_stream(&request);
        request_stream << method << " " << path << " HTTP/1.1\r\n";
        request_stream << "Host: " << host << ":" << port << "\r\n";
        request_stream << "Accept: */*\r\n";
        request_stream << "Connection: close\r\n\r\n";
        if (method == "POST") request_stream << body;

        // Send the request.
        boost::asio::write(socket, request);

        // Read the response status line. The response streambuf will automatically
        // grow to accommodate the entire line. The growth may be limited by passing
        // a maximum size to the streambuf constructor.
        boost::asio::streambuf response;
        boost::asio::read_until(socket, response, "\r\n");

        return buf2Str(response);
    }
    catch (exception& e) {
        cout << "doSyncHttpInternal : Exception: " << e.what() << "\n";
    } catch (...) {
        cout << "doSyncHttpInternal : Exception" << "\n";
    }
    return "";
}

string CdManager::doSyncHttp(string method, string host,
        string port, string path, string body)
{
    return doSyncHttpInternal(method, host, port, path, body);
}

string CdManager::trim(const string& str)
{
    size_t start = str.find_first_not_of(" \n\r\t");
    size_t until = str.find_last_not_of(" \n\r\t");
    string::const_iterator i
        = start==string::npos ? str.begin() : str.begin() + start;
    string::const_iterator x
        = until==string::npos ? str.end()   : str.begin() + until+1;
    return string(i,x);
}

Url CdManager::parse_url(const string& raw_url) //no boost
{
    string path,domain,x,protocol,port,query;
    int offset = 0;
    size_t pos1,pos2,pos3,pos4;
    x = trim(raw_url);
    offset = offset==0 && x.compare(0, 8, "https://")==0 ? 8 : offset;
    offset = offset==0 && x.compare(0, 7, "http://" )==0 ? 7 : offset;
    pos1 = x.find_first_of('/', offset+1 );
    path = pos1==string::npos ? "" : x.substr(pos1);
    domain = string( x.begin()+offset, pos1 != string::npos ? x.begin()+pos1 : x.end() );
    path = (pos2 = path.find("#"))!=string::npos ? path.substr(0,pos2) : path;
    port = (pos3 = domain.find(":"))!=string::npos ? domain.substr(pos3+1) : "";
    domain = domain.substr(0, pos3!=string::npos ? pos3 : domain.length());
    protocol = offset > 0 ? x.substr(0,offset-3) : "";
    query = (pos4 = path.find("?"))!=string::npos ? path.substr(pos4+1) : "";
    path = pos4!=string::npos ? path.substr(0,pos4) : path;

    Url url;
    url.path = path;
    url.protocol = protocol;
    url.domain = domain;
    url.query = query;
    url.port = port;
    return url;
}

// return B during A:B
string CdManager::getHeaderValue(string &str) {
    int colonIdx = str.find(":");
    if (colonIdx == string::npos) return str;
    return trim(str.substr(colonIdx+1, str.size()));
}

void LauncherInfo::printInfo() {
    l.debug("");
    l.debug("Launcher printInfo");
    l.debug("---------->");
    l.debug("   launcherId         = <%d>", launcherId);
    l.debug("   valid              = <%d>", valid);
    l.debug("   myDiscoveryCount   = <%d>", myDiscoveryCount);
    l.debug("   xmlUrl             = <%s>", xmlUrl.c_str());
    l.debug("   uuid               = <%s>", uuid.c_str());
    l.debug("   applicationUrl     = <%s>", applicationUrl.c_str());
    l.debug("   friendlyName       = <%s>", friendlyName.c_str());
    l.debug("   csOsId             = <%s>", csOsId.c_str());
    l.debug("<----------");
    l.debug("");
}

void LauncherInfo::getLauncherPropertiesFromCd() {
    Url url = cdm->parse_url(xmlUrl);
    string emptyBody;
    // ex) "10.0.2.15", "8080", "/companion/Description.xml"
    string response =
        doSyncHttpInternal("GET", url.domain, url.port, url.path, emptyBody);
    if (response.size() == 0) {
        valid = false;
        return;
    }
    /* Next response format will be optained according to
        Boost implementation

    HTTP/1.1 200 OK
    CONTENT-TYPE: text/xml; charset="utf-8"
    CONTENT-LANGUAGE: en-US
    Application-URL: http://10.0.2.15:8080/LauncherEndPoint/
    Friendly-Name: "XXX's android phone"
    CS-OS-ID: Android
    Access-Control-Allow-Origin:*
    Content-Length: 141

    <root xmlns="urn:schemas-upnp-org:device-1-0" configId=""><specVersion><major>1</major><minor>1</minor></specVersion><device></device></root>
    */
    stringstream response_stream(response);
    string http_version;
    response_stream >> http_version;
    l.debug("http_version = %s", http_version.c_str());
    unsigned int status_code;
    response_stream >> status_code;
    l.debug("status_code = %d", status_code);
    string status_message;
    getline(response_stream, status_message);
    l.debug("status_message = %s", cdm->trim(status_message).c_str());
    if (!response_stream || http_version.substr(0, 5) != "HTTP/")
    {
        l.error("launcher : Invalid response");
        return;
    }
    if (status_code != 200)
    {
        l.error("launcher : Response returned with status code : %d", status_code);
        return;
    }
    string aLine;
    while (getline(response_stream, aLine) && aLine!= "\r") {
        //l.debug("aLine = %s", aLine.c_str());
        if (cdm->findStrIc(aLine, "Application-URL") >= 0) {
            applicationUrl = cdm->getHeaderValue(aLine);
        } else if (cdm->findStrIc(aLine, "Friendly-Name") >= 0) {
            friendlyName = cdm->getHeaderValue(aLine);
        } else if (cdm->findStrIc(aLine, "CS-OS-ID") >= 0) {
            csOsId = cdm->getHeaderValue(aLine);
        }
        aLine = "";
    }
    l.debug("applicationUrl = %s", applicationUrl.c_str());
    // Validate mandatory fields.
    if (xmlUrl.size() > 0 && uuid.size() > 0 && applicationUrl.size() > 0) {
        valid = true;
    }
}

struct LauncherManager {
    set<LauncherInfo *> connectedLaunchers;

    int getNewLauncherId() {
        static int lId = 0;
        if (lId > 100000000) lId = 0;
        return ++lId;
    }

    // Must be called with cdm->connected_launchers_mutex
    set<LauncherInfo *> &getConnectedLaunchers() {
        return connectedLaunchers;
    }

    void addLauncher(LauncherInfo *launcher) {
        ScopedLock sl(cdm->connected_launchers_mutex);
        // set discovery count to new launcher
        launcher->myDiscoveryCount = lastDiscoveryCount;
        for (auto iter = connectedLaunchers.begin();
                iter != connectedLaunchers.end() ; iter++) {
            LauncherInfo *cached = *iter;
            if (cached->uuid == launcher->uuid) {
                // Already cached launcher
                l.debug("cached launcher : %s, %d -> %d"
                    , launcher->friendlyName.c_str()
                        , cached->myDiscoveryCount
                        , launcher->myDiscoveryCount);
                cached->myDiscoveryCount = launcher->myDiscoveryCount;
                delete(launcher);
                return;
            }
        }
        l.debug("added launcher : %s" , launcher->friendlyName.c_str());
        launcher->launcherId = getNewLauncherId();
        connectedLaunchers.insert(launcher);
    }

    // filter out old launchers
    void resetLaunchers () {
        ScopedLock sl(cdm->connected_launchers_mutex);
        l.debug("resetLauchers() : before size = %d", connectedLaunchers.size());
        for (auto iter = connectedLaunchers.begin();
                iter != connectedLaunchers.end() ;) {
            LauncherInfo *cached = *iter;
            //if (cached->myDiscoveryCount != lastDiscoveryCount) {
            // Keep alive launchers for last 3 discovery
            // It's because UDP discovery is not reliable enough.
            if ((lastDiscoveryCount - cached->myDiscoveryCount) > 3) {
                l.info("erase launcher : %s : %d-%d"
                    , cached->friendlyName.c_str()
                    , cached->myDiscoveryCount, lastDiscoveryCount);
                iter = connectedLaunchers.erase(iter);
                delete(cached);
            } else {
                ++ iter;
            }
        }
        if (lastDiscoveryCount > 100000000) lastDiscoveryCount = 0;
        lastDiscoveryCount ++;
        l.debug("lastDiscoveryCount increased to %d", lastDiscoveryCount);
        l.debug("resetLauchers() : after size = %d", connectedLaunchers.size());
    }
};

static LauncherManager launcherManager;

set<LauncherInfo *>& CdManager::getConnectedLaunchers () {
    return launcherManager.getConnectedLaunchers();
}

static void handleReceivedUdp (const string &msg) {
    // TODO: Parse message rather than keyword matching.
    // TODO : Versioning in defined in UPnP like below :
    // Devices MUST respond to M-SEARCH requests for any supported version.
    // For example, if a device implements “urn:schemas-upnp-org:service:xyz:2”,
    // it MUST respond to search requests for both that type and “urn:schemas-upnp-org:service:xyz:1”.
    // The response MUST specify the same version as was contained in the search request.
    /*
    l.debug("|-->");
    l.debug("%s", msg.c_str());
    l.debug("<--|");
    l.debug("");
    */
    bool isMSearchMsg = cdm->findStrIc(msg, "M-SEARCH") == 0;
    bool isHttpOk = cdm->findStrIc(msg, "HTTP/1.1 200 OK") == 0;
    if (isMSearchMsg) {
        bool hasSsdpDiscover = cdm->findStrIc(msg, "ssdp:discover") > 0;
        bool hasPdUrn = cdm->findStrIc(msg
                , "urn:schemas-atsc.org:device:primaryDevice:1.0") > 0;
        if (hasSsdpDiscover && hasPdUrn) {
            // Discovery Request from CD App
            l.debug("Discovery req from CD App");
            stringstream ss;
            ss << "HTTP/1.1 200 OK\r\n";
            ss << "CACHE-CONTROL: max-age = 1\r\n";
            ss << "EXT: \r\n";
            ss << "LOCATION: ";
            ss << getDiscoveryLocationUrl() << endl;
            ss << "SERVER: \r\n";
            ss << "ST: urn:schemas-atsc.org:device:primaryDevice:1.0\r\n";
            ss << "USN: uuid:";
            ss << task.platform->deviceInfo.deviceId;
            ss << ":urn:schemas-atsc.org:device:primaryDevice:1.0\r\n";
            string msg = ss.str();
            sendUdp(msg);
        }
    } else if (isHttpOk) {
        int idxLocation = cdm->findStrIc(msg, "LOCATION: ");
        int idxUuid = cdm->findStrIc(msg, "uuid:");
        int idxSt = cdm->findStrIc(msg, "ST: ");
        int idxUrn = cdm->findStrIc(msg,
            "urn:schemas-atsc.org:device:companionDevice:1.0");
        if (idxLocation > 0 && idxUuid > 0 && idxSt > 0 && idxUrn > 0) {
            l.debug("Discovery Res from CD Launcher");
            // Discovery Response from CD Launcher
            // TODO: use currentUnixTime()
            uint64_t curTime = cdm->currentTimeMillis();
            if ((curTime - timeSentLastDisvovery) > (DIS_MX * 1000)) {
                l.debug("Discovery MX timed out");
                // Not to use the response which is exceeded "Discovery MX"
                return;
            }
            int idxXml = cdm->findStrIc(msg, ".xml");
            if (idxXml < 0 || idxLocation >= idxXml) {
                l.error("Invalid launcher description");
                return;
            }
            try {
                // 10 means the length of "LOCATION: "
                // 4 means the length of ".xml"
                string launcherDescXmlUrl;
                if (msg.size() >= (idxXml+4)) {
                    launcherDescXmlUrl = msg.substr(idxLocation+10);
                } else {
                    l.error("xml_url is out of format.");
                    return;
                }
                l.debug("launcherDescXmlUrl = %s", launcherDescXmlUrl.c_str());
                // 5 means the length of "uuid:"
                // 36 means the length of UUID (Refer UPnP 1.1.4)
                string launcherUuid;
                if (msg.size() >= (idxUuid+5+36)) {
                    launcherUuid = msg.substr(idxUuid+5, 36);
                } else {
                    l.error("cannot get launcherUuid.");
                }
                l.debug("launcherUuid = %s", launcherUuid.c_str());
                if (launcherUuid.size() != 36) {
                    l.error("launcherUuid is out of format.");
                    return;
                }
                LauncherInfo *launcher =
                        new LauncherInfo(launcherDescXmlUrl, launcherUuid);
                launcher->getLauncherPropertiesFromCd();
                if (launcher->valid) {
                    launcherManager.addLauncher(launcher);
                } else {
                    l.info("this launcher is NOT valid, so delete");
                    delete(launcher);
                }
            } catch (exception &e) {
                l.error("error parsing launcher response : %s", e.what());
                return;
            } catch (...) {
                l.error("error parsing launcher response");
                return;
            }
        }
    }
}

class UdpReceiver
{
public:
    typedef UdpReceiver C;
    UdpReceiver (boost::asio::io_context& io_context): socket_(io_context) {}

    bool start (const address& listen_address,
            const address& multicast_address) {
        // Create the socket so that multiple may be bound to the same addr.
        udp::endpoint listen_endpoint(listen_address, DISCOVER_PORT);
        socket_.open(listen_endpoint.protocol());
        socket_.set_option(udp::socket::reuse_address(true));

        boost::system::error_code ec;
        socket_.bind(listen_endpoint, ec);
        if (ec) return false;

        // Join the multicast group.
        socket_.set_option(multicast::join_group(multicast_address), ec);
        if (ec) return false;

        do_receive();
        return true;
    }

private:
    void do_receive() {
        std::fill( std::begin(data_), std::end(data_), 0 );
        socket_.async_receive_from(boost::asio::buffer(data_), sender_endpoint_,
                bind(&C::onReceive, this, placeholders::_1, placeholders::_2));
    }

    void onReceive(boost::system::error_code ec, size_t length) {
        if (ec) {
            l.error("Datagram receiving error %s", ec);
        } else {
            string senderIp = sender_endpoint_.address().to_string();
            l.debug("UPD sender ip [%s]", senderIp.c_str());
            if (senderIp == getLocalIpAddrV4()) {
                l.debug("skip udp-packet from local");
            } else {
                handleReceivedUdp({data_.data(), length});
            }
        }
        do_receive();
    }

    udp::socket socket_;
    udp::endpoint sender_endpoint_;
    array<char, 1024> data_;
};

static void *receiver(void *arg)
{
    boost::asio::io_context io_context;

    while (true) {
        UdpReceiver r(io_context);
        if (r.start(make_address("0.0.0.0"), make_address(DISCOVER_IP))) {
            io_context.run();
            break;
        }

        sleep(1);
    }

    return 0;
}

static void startUdpServer()
{
    l.info("startUdpServer() : tid = %d", this_thread::get_id());
    pthread_t t;
    int tId = pthread_create(&t, 0, receiver, 0);
    if (tId < 0) {
        l.error("Thread create error");
        return;
    }
    pthread_detach(t);
}

static void *advertizingLoop(void *arg)
{
    while (true) {
        sleep(3); // per 3 seconds
        stringstream ss;
        ss << "NOTIFY * HTTP/1.1\r\n";
        ss << "Host: " DISCOVER_IP ":" << DISCOVER_PORT << "\r\n";
        ss << "CACHE-CONTROL: max-age = 1\r\n";
        ss << "LOCATION: ";
        ss << getDiscoveryLocationUrl() << endl;
        ss << "NT: urn:schemas-atsc.org:device:primaryDevice:1.0\r\n";
        ss << "NTS: ssdp:alive\r\n";
        ss << "SERVER: \r\n";
        ss << "USN: uuid:" << task.platform->deviceInfo.deviceId;
        ss << ":urn:schemas-atsc.org:device:primaryDevice:1.0\r\n";
        string msg = ss.str();
        sendUdp(msg);
    }

    // never reach here.
    return 0;
}

static void startAdvertizing() {
    l.info("startAdvertizing()");
    pthread_t t;
    int tId = pthread_create(&t, 0, advertizingLoop, 0);
    if (tId < 0) {
        l.error("Thread create error");
        return;
    }
    pthread_detach(t);
}

// Refresh the list of active launcher periodically
static void *launcherDiscoveryLoop(void *arg)
{
    while (true) {
        sleep(2); // 2 seconds
        updateLocalIpAddrV4(); // periodic IP addr update
        stringstream ss;
        ss << "M-SEARCH * HTTP/1.1\r\n";
        ss << "HOST: " << getLocalIpAddrV4() << ":" << DISCOVER_PORT << "\r\n";
        ss << "MAN: \"ssdp:discover\"\r\n";
        ss << "MX: " << DIS_MX << "\r\n";
        ss << "ST: urn:schemas-atsc.org:device:companionDevice:1.0\r\n";
        string msg = ss.str();
        timeSentLastDisvovery = cdm->currentTimeMillis();
        l.debug("launcherDiscoveryLoop() : send UDP");
        sendUdp(msg);
        sleep(DIS_MX+1); // MX + 1 seconds
        // reset launchers after retrival of all response from Launchers.
        launcherManager.resetLaunchers();
    }

    // never reach here.
    return 0;
}

static void startLauncherDiscovery() {
    pthread_t t;
    int tId = pthread_create(&t, 0, launcherDiscoveryLoop, 0);
    if (tId < 0) {
        l.error("Thread create error");
        return;
    }
    pthread_detach(t);
}

void CdManager::start() {
    if (!serverStarted) {
        //l.setLevel(ACBA_LOG_DEBUG);
        startUdpServer();
        startAdvertizing();
        startLauncherDiscovery();
        serverStarted = true;
    }
}
