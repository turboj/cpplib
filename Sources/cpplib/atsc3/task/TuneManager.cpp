#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <fcntl.h>
#include <errno.h>
#include <string.h>
#include <string>
#include <algorithm>
#include <time.h>
#include <map>

#include "stream_reader.h"
#include "pcap_reader.h"
#include "ip_reader.h"
#include "udp_reader.h"
#include "atsc3_task.h"
#include "tune_manager.h"
#include "EventDriver.h"
#include "acba.h"

using namespace std;
using namespace acba;
using namespace atsc3;

static Log l("tunemgr");

int TuneManager::join (uint32_t srcIp, uint32_t dstIp, uint16_t port,
        const StreamReaderRef &r)
{
    l.info("Join %08x-%08x:%u", srcIp, dstIp, port);

    Ref<RoutingTable> rt = new RoutingTable(routingTable);

    auto &re = rt->entries[uint64_t(dstIp) << 32 | uint64_t(port) << 16];
    re.sourceIp = srcIp;
    re.streamReader = r;

    updateRoutingTable(rt);
    return 0;
}

int TuneManager::leave (uint32_t srcIp, uint32_t dstIp, uint16_t port)
{
    l.info("Leave %08x-%08x:%u", srcIp, dstIp, port);

    Ref<RoutingTable> rt = new RoutingTable(routingTable);

    rt->entries.erase(uint64_t(dstIp) << 32 | uint64_t(port) << 16);

    updateRoutingTable(rt);
    return 0;
}

void TuneManager::updateRoutingTable (const acba::Ref<RoutingTable> &rt)
{
    routingTable = rt;

    for (auto &thi: tunes) {
        auto &th = thi.second;
        th.tune->setRoutingTable(rt);
        th.consumer->setRoutingTable(rt);
    }
}

void TuneManager::Discovery::onUpdate ()
{
    task.eventDriver.setTimeout(0, this);
}

void TuneManager::Discovery::processEvent (int events)
{
    TuneManager *self = container_of(this, &TuneManager::discovery);

    for (auto loc: model.knownStreamLocations) {
        auto thi = self->tunes.find(loc);
        if (thi != self->tunes.end()) continue;

        const char *cloc = loc.c_str();
        l.info("Discovered: %s", cloc);
        Ref<Tune> t = self->requestTune(loc);
        if (!t) {
            l.error("Failed to tune %s", cloc);
        }
    }
}

void NetTuneProvider_init ();
void PcapTuneProvider_init ();
void Atsc3TuneProvider_init ();
void VirtualAtsc3TuneProvider_init ();

void TuneManager::init ()
{
    NetTuneProvider_init();
    PcapTuneProvider_init();
#if USE_TUNER_CLIENT
    VirtualAtsc3TuneProvider_init();
#else
    Atsc3TuneProvider_init();
#endif
}

void TuneManager::registerTuneProvider (const acba::Ref<TuneProvider> &prov)
{
    tuneProviders.push_back(prov);
}

Tune *TuneManager::requestTune (const string &loc)
{
    // reuse tune if exists
    auto ti = tunes.find(loc);
    if (ti != tunes.end()) {
        auto &th = ti->second;
        th.useCount++;
        l.info("Share tune %s count:%d", loc.c_str(), th.useCount);
        return th.tune.get();
    }

    // determine provider
    string prefix = loc.substr(0, loc.find_first_of(':'));
    auto provi = find_if(tuneProviders.begin(), tuneProviders.end(),
            [&prefix] (const Ref<TuneProvider> &p) { return p->type == prefix; });
    if (provi == tuneProviders.end()) {
        l.error("Unsupported location type %s", prefix.c_str());
        return nullptr;
    }
    auto &prov = *provi;

    // create new tune
    Ref<TuneConsumer> cons = new TuneConsumer(routingTable);

    Ref<Tune> t = prov->create(loc, cons.get());
    if (!t) {
        l.error("Unable to tune %s", loc.c_str());
        return nullptr;
    }
    t->setRoutingTable(routingTable);

    auto &th = tunes[loc];
    th.useCount++;
    th.tune = t;
    th.provider = prov;
    th.consumer = cons;
    l.info("Create tune %s", loc.c_str());

    // TODO: lls?

    return t.get();
}

void TuneManager::releaseTune (const acba::Ref<Tune> &t)
{
    auto thi = tunes.find(t->location);
    if (thi == tunes.end()) {
        l.error("Invalid tune %s", t->location.c_str());
        return;
    }

    auto &th = thi->second;
    assert(th.tune == t);
    th.useCount--;
    if (th.useCount > 0) {
        l.info("Unshare tune %s count:%d", th.tune->location.c_str(),
                th.useCount);
        return;
    }

    l.info("Destroy tune %s", th.tune->location.c_str());
    th.provider->destroy(th.tune);
    tunes.erase(thi);
}
