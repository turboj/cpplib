#include <set>
#include <sstream>

#include "atsc3_model.h"
#include "atsc3_task.h"
#include "application_manager.h"
#include "acba.h"
#include "acba_crc32.h"
#include "pugixml.hpp"
#include "datetime.h"
#include "dnld.h"

using namespace std;
using namespace acba;
using namespace atsc3;
using namespace pugi;

static Log l("am");

ApplicationContextCache *ApplicationManager::getContextCache (const char *cid)
{
    auto &caches = model.applicationContextCaches;

    if (caches.find(cid) != caches.end()) {
        return caches[cid];
    }

    auto *cc = new ApplicationContextCache(cid);
    caches[cid] = cc;

    // FIXME: Avoid ip collision
    uint32_t crc32 = (uint32_t)acba_crc32(0, (const uint8_t*)cid,
            cc->id.length());
    uint32_t ip = 0x7f000002 + (crc32 & 0xffffff);

    char uri[64];
    sprintf(uri, "http://%u.%u.%u.%u:8080/app/", (ip >> 24) & 0xff,
            (ip >> 16) & 0xff, (ip >> 8) & 0xff, (ip >> 0) & 0xff);
    cc->baseUri = uri;
    cc->baseUri += cid;
    cc->hostIp = ip;

    return cc;
}

static map<string, string> keyGroups = {
    {"0", "Numeric"},
    {"1", "Numeric"},
    {"2", "Numeric"},
    {"3", "Numeric"},
    {"4", "Numeric"},
    {"5", "Numeric"},
    {"6", "Numeric"},
    {"7", "Numeric"},
    {"8", "Numeric"},
    {"9", "Numeric"},
};

void ApplicationManager::init ()
{
    int n = 0;
    auto supportedKeys = task.platform->getSupportedKeys(&n);
    for (int i = 0; i < n; i++) {
        auto &ki = supportedKeys[i];

        auto gi = keyGroups.find(ki.name);
        if (gi == keyGroups.end()) {
            KeyBinding &kb = keyBindings[ki.name];
            kb.name = ki.name;
            kb.indexes.push_back(i);
            kb.infos.push_back(&ki);
            kb.flags = ki.flags;
        } else {
            auto &g = gi->second;
            KeyBinding &kb = keyBindings[g];
            kb.name = g;
            kb.indexes.push_back(i);
            kb.infos.push_back(&ki);
            kb.flags = ki.flags;
        }
    }
}

void ApplicationManager::startSystemUi (const std::string &url0)
{
    assert(!overridesAllKeys);
    assert(!systemWebWindow);
    assert(!applicationTask);

    systemWebWindow = new WebWindow();

    string url = url0;
    url += (url.find('?') == string::npos) ? '?' : '&';
    url += "wsURL=ws://127.0.0.1:8080";
    url += "&privateId=" + systemWebWindow->privateId;
    systemWebWindow->open(url, 99);

    vector<int> keys;
    for (auto kb: keyBindings) {
        keys.insert(keys.end(), kb.second.indexes.begin(),
                kb.second.indexes.end());
    }
    task.platform->routeKeys(keys.data(), keys.size(), systemWebWindow->peer);
}

void ApplicationManager::setKeyOverride (bool ovr)
{
    if (ovr == overridesAllKeys) return;
    overridesAllKeys = ovr;

    if (ovr) {
        // Route all keys to system ui regardless of key binding
        vector<int> keys;
        for (auto kbi: keyBindings) {
            auto &kb = kbi.second;
            if (kb.flags & atsc3_KEYFLAG_APPLICATION_DEFAULT ||
                    kb.applicationBound) {
                l.info("Overriding key %s for system ui", kb.name.c_str());
                keys.insert(keys.end(), kb.indexes.begin(), kb.indexes.end());
            }
        }
        if (keys.size() > 0) {
            task.platform->routeKeys(keys.data(), keys.size(),
                    systemWebWindow->peer);
        }
    } else {
        // Back to normal
        vector<int> keys;
        for (auto kbi: keyBindings) {
            auto &kb = kbi.second;
            if (kb.flags & atsc3_KEYFLAG_APPLICATION_DEFAULT ||
                    kb.applicationBound) {
                l.info("Giving key %s back to the web app", kb.name.c_str());
                keys.insert(keys.end(), kb.indexes.begin(), kb.indexes.end());
            }
        }
        if (keys.size() > 0 && applicationTask && applicationTask->webWindow) {
            task.platform->routeKeys(keys.data(), keys.size(),
                    applicationTask->webWindow->peer);
        }
    }
}

struct CacheDownloader: HttpDownloadCompletionHandler, acba::Reactor {

    string sourceUrl;
    string targetUrl;
    string url;
    Ref<ApplicationContextCache> ctxCache;
    Ref<Download> dnld;
    Ref<File> file;
    Log l;

    CacheDownloader(const string &s_url, const string &t_url,
            const string &f_url) : sourceUrl(s_url), targetUrl(t_url),
            url(f_url), l("cacheDl", "["+f_url+"]") {}

    void start() {
        file = new File();
        HttpDownloadRequest req;
        req.url = sourceUrl+url;
        req.bodyBuffer = &(file->content);
        req.completionHandler = this;
        l.info("startDownload : %s", req.url.c_str());
        dnld = task.downloadManager.download(req);
    }

    void abort() {
        if (dnld) dnld->abort();
    }

    void onCompletion(HttpDownloadResponse &resp) override {
        l.info("status:%d", resp.status());
        if (resp.status() != 200 ) return;

        int len = resp.bodyLength();
        l.info("length:%d", len);
        assert(len == file->content.size());
        ref();
        task.eventDriver.setTimeout(0, this);
    }

    void onError(int ec) override {
        l.error("Failed to download! ec:%d", ec);
    }

    void processEvent(int evt) override {
        l.info("processEvent called");
        if (!ctxCache) return;
        string dst = targetUrl + url;
        l.info("processEvent : store to %s", dst.c_str());
        ctxCache->files[dst] = file;

        deref();
    }
};

int ApplicationManager::processCache(const CacheRequestInfo &reqInfo)
{
    if (!task.applicationManager.applicationTask
            || !task.applicationManager.applicationTask->contextCache) {
        l.warn("processCache : App cache ctx not avail");
        return 0;
    }
    ApplicationContextCache *ctxCache
            = task.applicationManager.applicationTask->contextCache;


    vector<string> flist;
// extract file list
    if (reqInfo.type == CACHE_FROM_MPD || reqInfo.type == CACHE_FROM_PERIOD) {
        Ref<XmlDocument> mpdDoc = new XmlDocument();
        xml_node root;
        double mpdDuration = 0.0;
        double pStart = 0.0; // startTime of last Period
        double pDuration = 0.0; //duration of last Period

        if (reqInfo.type == CACHE_FROM_PERIOD ) {
            xml_parse_result res = mpdDoc->load_buffer(reqInfo.dashSrc.data(),
                    reqInfo.dashSrc.size());
            if (!res) {
                l.warn("processCache : Period parse error");
                return -17;
            }
            root = mpdDoc->root();
        } else {
            string mpdPath = reqInfo.targetUrl + reqInfo.dashSrc;
            if (ctxCache->files.find(mpdPath) == ctxCache->files.end()) {
                l.warn("processCache : MPD not found");
                return -18;
            }

            Ref<File> mpdFile = ctxCache->files[mpdPath];
            xml_parse_result res = mpdDoc->load_buffer(mpdFile->content.begin(), mpdFile->content.size());
            if (!res) {
                l.warn("processCache : MPD parse error");
                return -18;
            }
            root = mpdDoc->child("MPD");
            mpdDuration = parseDuration(
                    root.attribute("mediaPresentationDuration").as_string("PT0S"));
        }

        for (xml_node p: root.children("Period")) {
            l.info("Period Found");
            if (!p.attribute("start").empty()) {
                pStart = parseDuration(p.attribute("start").as_string());
            } else {
                pStart = pStart+pDuration;
            }

            if (!p.attribute("duration").empty()) {
                pDuration = parseDuration(p.attribute("duration").as_string());
            } else {
                xml_node np = p.next_sibling("Period");
                if (!np.empty()) {
                    if (!np.attribute("start").empty()) {
                       pDuration =
                               parseDuration(np.attribute("start").as_string())
                               - pStart;
                    } else {
                    // error
                        return -17;
                    }
                } else {
                    pDuration = mpdDuration - pStart;
                    if (pDuration < 0 ) {
                        //error
                        return -17;
                    }
                }
            }
            // Find all SegmentTemplate in a Period
            for (auto st: p.select_nodes("//SegmentTemplate")) {
                xml_node stnode = st.node();
                if (!stnode.attribute("initialization").empty()) {
                    flist.push_back(
                            stnode.attribute("initialization").as_string());
                }
                if (!stnode.attribute("media").empty()) {
                    string stmedia = stnode.attribute("media").as_string();
    //TODO : need to handle pattern like %0[width]d and different Identifier like $Time$
                    size_t s,e;
                    if ((s = stmedia.find('$')) == string::npos) {
                        l.info("SegmentTemplate has one segment");
                        flist.push_back(stmedia);
                        continue;
                    }
                    do {
                        e = stmedia.find('$', s+1);
                        if (e == string::npos) {
                            l.warn("Unescaped $ in media of SegmentTemplate");
                            break;
                        }
                        string fmt = stmedia.substr(s, e-s+1);
                        size_t nf = fmt.find("Number");
                        if (nf != string::npos) {
                            int width = 1;
                            size_t ff = fmt.find("%0");
                            if (ff != string::npos) {
                                l.info("fmt.substr %s", fmt.substr(ff+2).c_str());
                                width = std::stoi(fmt.substr(ff+1));
                                l.info("width set to %d", width);
                            }
                            unsigned int sn
                                    = stnode.attribute("startNumber").as_uint(1);
                            unsigned int ts
                                    = stnode.attribute("timescale").as_uint(1);
                            unsigned int segnum = 0;
                            if (!stnode.attribute("duration").empty()) {
                                unsigned int dur
                                        = stnode.attribute("duration").as_uint();
                                if (dur == 0) {
                                    l.warn("processCache : invalid duration(0)");
                                    break;
                                }
                                segnum = pDuration*ts/dur;
                            } else if (!stnode.child("SegmentTimeline").empty()) {
                                for (xml_node s: stnode.child("SegmentTimeline")
                                        .children("S")) {
                                    segnum += (s.attribute("r").as_uint(0)+1);
                                }
                            } else {
                                l.warn("processCache : no duration and "
                                        "SegmentTimeline");
                                break;
                            }

                            for (int i = 0; i < segnum; i++) {
                                string format ="%s%0"+ to_string(width) +"d%s";
                                char mpath[128];
                                sprintf(mpath, format.c_str(),
                                        stmedia.substr(0, s).c_str(),
                                        sn+i, stmedia.substr(e+1).c_str());
                                string path = mpath;
                                flist.push_back(path);
                            }
                            break;
                        }
                    } while ((s = stmedia.find('$', e+1)) != string::npos);
                }
            }
            // Find all SegmentList in a Period
            for (auto sl: p.select_nodes("//SegmentList")) {
                xml_node slnode = sl.node();
                for (auto su: slnode.children("SegmentURL")) {
                    if (!su.attribute("media").empty()) {
                        flist.push_back(su.attribute("media").as_string());
                    }
                }
            }
            // Final all Initialization in a Period
            for (auto i: p.select_nodes("//Initialization")) {
                xml_node inode = i.node();
                if (!inode.attribute("sourceURL").empty()) {
                    flist.push_back(inode.attribute("sourceURL").as_string());
                }
            }
        }
    } else if (reqInfo.type == CACHE_URL_LIST) {
        flist = reqInfo.urlList;
    } else { //Can't happen
        l.warn("processCache : unknown CacheRequestInfo type");
        return 0;
    }

    l.info("processCache : DUMP file list");
    for (string s: flist) {
        l.info("\t %s",s.c_str());
    }
// first, check cached
// what is expected when array length is 0? For now, cached=true
    bool cached = true;
    vector<string> dlist;
    for (auto f: flist) {
        string fname = reqInfo.targetUrl + f;

        if (ctxCache->files.find(fname) == ctxCache->files.end()) {
            l.info("processCache : %s doesn't exit", fname.c_str());
            cached = false;
            dlist.push_back(f);
        }
    }

//download start
    if (!cached && reqInfo.sourceUrl != "") { //download
        Ref<CacheDownloader> cd;
        for(auto f: dlist) {
            cd = new CacheDownloader(reqInfo.sourceUrl, reqInfo.targetUrl, f);
            cd->ctxCache = ctxCache;
            cd->start();
        }
    }

    if ( cached ) {
        return 1;
    }
    return 0;
}

void ApplicationManager::processHeld ()
{
    // Wait until initial sls is received.
    if (currentService && currentService->slsFiles.size() == 0) return;

    l.info("Checking HELD of %s...",
            currentService ? currentService->globalId.c_str() : "<no_service>");

    double now = currentUnixTime();

    auto bep = findBestEntryPackage(now);

    // Abort if bep matches current running application.
    auto &app = applicationTask;
    if (bep && app) {
        bool s = app->contextCache->id == bep.attribute("appContextId").value();
        if (app->entryPackage.empty()) {
            // broad band
            s=s&& app->url == bep.attribute("bbandEntryPageUrl").value();
        } else {
            // broadcast
            s=s&& app->entryPackage ==
                    bep.attribute("bcastEntryPackageUrl").value();
            s=s&& app->url == bep.attribute("bcastEntryPageUrl").value();
        }
        if (s) {
            l.info("Keep application running");

            // schedule next wakeup
            setTimeout(bep.attribute("validUntil"), now);
            return;
        }
    }

    // stop previous application
    if (app) {
        app->stop();
        app = nullptr;
    }

    // start new application
    if (bep) {
        app = new ApplicationTask();
        app->contextCache = getContextCache(
                bep.attribute("appContextId").value());
        app->entryPackage = bep.attribute("bcastEntryPackageUrl").value();
        if (app->entryPackage.empty()) {
            app->url = bep.attribute("bbandEntryPageUrl").value();
            app->id = app->url;
        } else {
            app->url = bep.attribute("bcastEntryPageUrl").value();
            app->id = app->contextCache->id + ":" + app->url;
        }

        // Attach service usage object if available.
        auto sti = task.serviceManager.serviceTasks.find(currentService.get());
        if (sti != task.serviceManager.serviceTasks.end()) {
            app->serviceUsage = sti->second->usage;
        }

        app->start();

        // Re-iterate files which might be applicable to current cc.
        // It launches app immeidately if entry package is provided.
        for (auto &fi: currentService->files) app->tryAddPackage(fi.second);

        setTimeout(bep.attribute("validUntil"), now);
    }

    applicationTaskUpdate.notify();
}

xml_node ApplicationManager::findBestEntryPackage (double now)
{
    if (!currentService || !currentService->heldDocument) return {};

    xml_node bep; // best entry package

    double nvf = 0.0; // nearest validFrom time from now if non-zero
    for (xml_node ep: currentService->heldDocument->child("HELD").
                children("HTMLEntryPackage")) {
        const char *appContextId = ep.attribute("appContextId").value();
        const char *requiredCapabilities =
                ep.attribute("requiredCapabilities").as_string(0);
        bool appRendering = ep.attribute("appRendering").as_bool();
        const char *clearAppContextCacheDate =
                ep.attribute("clearAppContextCacheDate").as_string(0);
        const char *bcastEntryPackageUrl =
                ep.attribute("bcastEntryPackageUrl").as_string(0);
        const char *bcastEntryPageUrl =
                ep.attribute("bcastEntryPageUrl").as_string(0);
        const char *bbandEntryPageUrl =
                ep.attribute("bbandEntryPageUrl").as_string(0);
        const char *validFrom = ep.attribute("validFrom").as_string(0);
        const char *validUntil = ep.attribute("validUntil").as_string(0);
        const char *coupledServices =
                ep.attribute("coupledServices").as_string(0);
        const char *lctTSIRef = ep.attribute("lctTSIRef").as_string(0);

        l.info("HELDEntryPackage appContextID:%s requiredCapabilities:%s "
                "appRendering:%d clearAppContextCacheDate:%s "
                "bcastEntryPackageUrl:%s bcastEntryPageUrl:%s "
                "bbandEntryPageUrl:%s validFrom:%s validUntil:%s "
                "coupledServices:%s lctISIRef:%s",
                appContextId, requiredCapabilities, appRendering,
                clearAppContextCacheDate, bcastEntryPackageUrl,
                bcastEntryPageUrl, bbandEntryPageUrl, validFrom, validUntil,
                coupledServices, lctTSIRef);

        // check valid time of the entry.
        if (validFrom) {
            double vf = parseDatetime(validFrom);
            if (vf > now) {
                if (nvf == 0.0 || vf < nvf) nvf = vf;
                continue;
            }
        }
        if (validUntil && now > parseDatetime(validUntil)) continue;

        // check requirements
        if (requiredCapabilities) {
            l.warn("TODO: Handle requiredCapabilities:%s",
                    requiredCapabilities);
        }

        // check url validity
        if (!((bcastEntryPackageUrl && bcastEntryPageUrl) ||
                bbandEntryPageUrl)) {
            continue;
        }

        bep = ep; // choose last one among supported ep.
    }

    return bep;
}

void ApplicationManager::setTimeout (xml_attribute validUntil, double now)
{
    double t;

    auto vu = validUntil.as_string(0);
    if (vu) {
        t = parseDatetime(vu) - now;
        if (t > 60.0) t = 60.0; // limit sleep time
    } else {
        t = 60.0;
    }

    task.eventDriver.setTimeout(int64_t(t * 1000000), &heldListener);
}

void ApplicationManager::Selection::onUpdate ()
{
    task.eventDriver.setTimeout(0, this);
}

void ApplicationManager::Selection::processEvent (int evt)
{
    auto self = container_of(this, &ApplicationManager::selection);

    RouteService *rsrv = 0;
    if (model.currentService && model.currentService->protocol == 1) {
        rsrv = static_cast<RouteService*>(model.currentService.get());
    }

    auto &csrv = self->currentService;
    if (csrv.get() == rsrv) return;

    if (csrv) {
        task.eventDriver.unsetTimeout(&self->heldListener);
        csrv->heldDocumentUpdate.removeListener(&self->heldListener);

        csrv->fileUpdate.removeListener(&self->packageListener);
    }

    csrv = rsrv;

    if (rsrv) {
        rsrv->heldDocumentUpdate.addListener(&self->heldListener);
        rsrv->fileUpdate.addListener(&self->packageListener);

        auto &app = self->applicationTask;
        if (app) {
            // Re-iterate files which might be applicable to current cc.
            for (auto &fi: rsrv->files) app->tryAddPackage(fi.second);
        }
    }

    self->processHeld();
}

void ApplicationManager::HeldListener::onUpdate ()
{
    task.eventDriver.setTimeout(0, this);
}

void ApplicationManager::HeldListener::processEvent (int evt)
{
    container_of(this, &ApplicationManager::heldListener)->processHeld();
}

void ApplicationManager::PackageListener::onUpdate (const acba::Ref<RouteFile>
        &f) {
    auto self = container_of(this, &ApplicationManager::packageListener);
    if (!self->applicationTask) return;
    self->applicationTask->tryAddPackage(f);
}
