// Based on https://github.com/morristech/android-ifaddrs

#if __linux__
#include <string.h>
#include <stdlib.h>
#include <errno.h>
#include <unistd.h>
#include <sys/socket.h>
#include <net/if_arp.h>

#include <netinet/in.h>
#include <linux/netlink.h>
#include <linux/rtnetlink.h>
#else
#include <ifaddrs.h>
#include <arpa/inet.h>
#endif

#include <string>

#include "acba.h"
#include "cd_manager.h"

using namespace std;

static acba::Log l("net");

#if __linux__
static int netlink_socket (void)
{
    int sock = socket(PF_NETLINK, SOCK_RAW, NETLINK_ROUTE);
    if(sock < 0)
    {
        return -1;
    }

    struct sockaddr_nl l_addr;
    memset(&l_addr, 0, sizeof(l_addr));
    l_addr.nl_family = AF_NETLINK;
    if(::bind(sock, (struct sockaddr *)&l_addr, sizeof(l_addr)) < 0)
    {
        close(sock);
        return -1;
    }

    return sock;
}

static int netlink_send (int sock, int p_request)
{
    char buf[NLMSG_ALIGN(sizeof(struct nlmsghdr)) +
            NLMSG_ALIGN(sizeof(struct rtgenmsg))];
    memset(buf, 0, sizeof(buf));
    struct nlmsghdr *hdr = (struct nlmsghdr *)buf;
    struct rtgenmsg *l_msg = (struct rtgenmsg *)NLMSG_DATA(hdr);

    hdr->nlmsg_len = NLMSG_LENGTH(sizeof(*l_msg));
    hdr->nlmsg_type = p_request;
    hdr->nlmsg_flags = NLM_F_ROOT | NLM_F_MATCH | NLM_F_REQUEST;
    hdr->nlmsg_pid = 0;
    hdr->nlmsg_seq = sock;
    l_msg->rtgen_family = AF_UNSPEC;

    struct sockaddr_nl l_addr;
    memset(&l_addr, 0, sizeof(l_addr));
    l_addr.nl_family = AF_NETLINK;
    return (sendto(sock, hdr, hdr->nlmsg_len, 0, (struct sockaddr *)&l_addr,
            sizeof(l_addr)));
}

static int netlink_recv (int sock, void *p_buffer, size_t p_len)
{
    struct msghdr l_msg;
    struct iovec l_iov = { p_buffer, p_len };
    struct sockaddr_nl l_addr;
    int l_result;

    for(;;)
    {
        l_msg.msg_name = (void *)&l_addr;
        l_msg.msg_namelen = sizeof(l_addr);
        l_msg.msg_iov = &l_iov;
        l_msg.msg_iovlen = 1;
        l_msg.msg_control = NULL;
        l_msg.msg_controllen = 0;
        l_msg.msg_flags = 0;
        int l_result = recvmsg(sock, &l_msg, 0);

        if(l_result < 0)
        {
            if(errno == EINTR)
            {
                continue;
            }
            return -2;
        }

        if(l_msg.msg_flags & MSG_TRUNC)
        { // buffer was too small
            return -1;
        }
        return l_result;
    }
}

static void interpretAddr (struct nlmsghdr *p_hdr, vector<uint32_t> &addrs)
{
    struct ifaddrmsg *l_info = (struct ifaddrmsg *)NLMSG_DATA(p_hdr);

    if (l_info->ifa_family != AF_INET) return;

    size_t l_rtaSize = NLMSG_PAYLOAD(p_hdr, sizeof(struct ifaddrmsg));
    for (auto l_rta = (struct rtattr *)(((char *)l_info) +
            NLMSG_ALIGN(sizeof(struct ifaddrmsg)));
            RTA_OK(l_rta, l_rtaSize); l_rta = RTA_NEXT(l_rta, l_rtaSize)) {
        void *l_rtaData = RTA_DATA(l_rta);
        size_t l_rtaDataSize = RTA_PAYLOAD(l_rta);
        switch(l_rta->rta_type) {
        case IFA_ADDRESS:
        {
            struct in_addr addr;
            memcpy(&addr, l_rtaData, l_rtaDataSize);

            uint32_t ip = ntohl(addr.s_addr);
            addrs.push_back(ip);
            break;
        }
        case IFA_LABEL:
        {
            string ifa_name = {(char*)l_rtaData, l_rtaDataSize};
            break;
        }
        case IFA_LOCAL:
        default:
            break;
        }
    }
}

static bool readResponse (int sock, vector<uint32_t> &addrs)
{
    void *buf = nullptr;
    int r;

    size_t sz = 4096;
    do {
        buf = realloc(buf, sz);
        sz *= 2;
        r = netlink_recv(sock, buf, sz);
    } while (r == -1);
    if (r == -2) {
        free(buf);
        return false;
    }

    pid_t pid = getpid();
    for (auto hdr = (struct nlmsghdr *)buf;
            NLMSG_OK(hdr, (unsigned int)r);
            hdr = (struct nlmsghdr *)NLMSG_NEXT(hdr, r)) {
        if((pid_t)hdr->nlmsg_pid != pid || (int)hdr->nlmsg_seq != sock) {
            continue;
        }

        if(hdr->nlmsg_type == NLMSG_DONE) {
            free(buf);
            return false;
        }

        if(hdr->nlmsg_type == NLMSG_ERROR)
        {
            free(buf);
            return false;
        }

        if(hdr->nlmsg_type == RTM_NEWADDR)
        {
            interpretAddr(hdr, addrs);
        }
    }
    free(buf);
    return true;
}

int atsc3::getIpV4Addresses (vector<uint32_t> &addrs)
{
    int sock = netlink_socket();
    if(sock < 0) return -1;

    if(netlink_send(sock, RTM_GETADDR) < 0) return -1;

    while (readResponse(sock, addrs));

    close(sock);
    return 0;
}

#else

int atsc3::getIpV4Addresses (vector<uint32_t> &addrs)
{
    struct ifaddrs* ifAddrStruct = NULL;
    struct ifaddrs* ifa = NULL;

    getifaddrs(&ifAddrStruct);

    for (ifa = ifAddrStruct; ifa != NULL; ifa = ifa->ifa_next) {
        if (!ifa->ifa_addr) {
            continue;
        }
        if (ifa->ifa_addr->sa_family == AF_INET) {
            uint32_t ip =
                ntohl(((struct sockaddr_in *)ifa->ifa_addr)->sin_addr.s_addr);
            addrs.push_back(ip);
        }
    }

    if (ifAddrStruct != NULL) {
        freeifaddrs(ifAddrStruct);
    }

    return 0;
}
#endif
