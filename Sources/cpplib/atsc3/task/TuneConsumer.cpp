#include "ip_reader.h"
#include "udp_reader.h"
#include "tune_manager.h"
#include "acba.h"

using namespace std;
using namespace acba;
using namespace atsc3;

static Log l("tune");

int TuneConsumer::read (const byte_t *data, int sz, Chain *ctx)
{
    if (routingTable != nextRoutingTable) routingTable = nextRoutingTable;

    auto ipctx = ctx->as<IpContext>();
    auto udpctx = ctx->as<UdpContext>();

    auto rei = routingTable->entries.find(
            uint64_t(ipctx->destinationIp) << 32 |
            uint64_t(udpctx->dstPort) << 16);
    if (rei == routingTable->entries.end()) return sz;

    auto &re = rei->second;
    if (re.sourceIp && ipctx->sourceIp != re.sourceIp) return sz;

    re.streamReader->read(data, sz, ctx);
    return sz;
}

void TuneConsumer::setRoutingTable (const acba::Ref<RoutingTable> &rt)
{
    nextRoutingTable = rt;
}
