#include <stdint.h>
#include <map>
#include <sstream>

#include "stream_reader.h"
#include "gzip_reader.h"
#include "atsc3_task.h"
#include "datetime.h"
#include "service_manager.h"
#include "acba.h"
#include "acba_crc32.h"

using namespace std;
using namespace acba;
using namespace atsc3;
using namespace pugi;

#define SAFEREAD_LOG(args...) l.warn("expected " args)
#define SAFEREAD_RETURN return

static Log l("msrv");

void MmtpServiceTask::start ()
{
    auto msrv = static_cast<MmtpService*>(service.get());

    switch (msrv->category) {
    case 1: // Linear A/V service
    case 2: // Linear audio only service
        usage = UsageManager::createServiceUsage("<unknown_country>",
                to_string(msrv->bsid).c_str(), to_string(msrv->id).c_str(),
                msrv->globalId.c_str(), to_string(msrv->category).c_str(),
                currentUnixTime(), "0", "<unknown_type>", "<unknown_cid>");
        if (!usage) l.warn("Failed to get service usage");
        break;
    }

    for (auto ms: msrv->sessions) {
        auto mst = new MmtpSessionTask(ms, msrv, &slsReader);
        mst->start();

        sessionTasks.push_back(mst);
    }

    msrv->mpuUpdate.addListener(&mpuHandler);
}

void MmtpServiceTask::stop ()
{
    auto *msrv = static_cast<MmtpService*>(service.get());

    msrv->mpuUpdate.removeListener(&mpuHandler);

    for (auto mst: sessionTasks) {
        mst->stop();
    }
    sessionTasks.clear();
    msrv->mpus.clear();

    msrv->currentPackage = nullptr;
    if (task.rmp.serviceTask.get() == this) task.rmp.stop();
    task.eventDriver.unsetTimeout(&slsReader);

    if (usage) {
        usage->setStopTime(currentUnixTime());
        usage = nullptr;
    }
}

void MmtpServiceTask::MpuHandler::onUpdate (const acba::Ref<Mpu>&f, uint8_t ft,
        const void *param) {
    MmtpServiceTask *self = container_of(this, &MmtpServiceTask::mpuHandler);

    if (self->playReady) return;
    if (ft != 2) return; // unless ft is MFU
    if (static_cast<MmtpService*>(self->service.get())->
            getDefaultVideoPacketId() < 0) {
        // return if video packet id was not yet determined
        l.info("video packet id was not yet determined");
        return;
    }

    l.info("MFU is available");
    self->playReady = true;
    self->playReadyUpdate.notify();
}

int MmtpServiceTask::SlsReader::read (const byte_t *data, int sz, Chain *pctx)
{
    if (sz < 2) {
        l.warn("Too short sls message");
        return sz;
    }

    const byte_t *cur = data;
    const byte_t *end = data + sz;

    int fragType = cur[0] & 0xc1; // f_i + A
    switch (fragType) {
    case 0: // single complete msg
        cur += 2;
        sendMessage(cur, end - cur);
        return sz;

    case 1: // multiple complete msgs
    case 0x40: // first fragment msg
    case 0x80: // middle fragment msg
    case 0xc0: // last fragment msg
        if (IS_FIRST_TIME) {
            l.warn("TODO: IMPLEMENT MMTP FRAGMENTATION TYPE:%02x", fragType);
        }
        return sz;

    default:
        l.warn("Invalid fragmentation:%02x", fragType);
        return sz;
    }

    // TODO: implement fragmented messages

    bool H = cur[0] & 2;
    cur++;

    int frag_counter = *cur++;
    return -1;
}

struct MessageHandler {
    string name;
    // @arg d locates data after length field
    void (*handle) (const byte_t *d, uint32_t sz, Chain*);
};

struct MessageContext: ChainNode<MessageContext> {
    uint16_t messageId;
    uint8_t version;
    MmtpServiceTask *mmtpServiceTask;
};

static int lengthBytes (uint16_t descTag)
{
    switch (descTag) {
    case 0x0000: // CRI descriptor
        return 2;

    case 0x0001: // MPU timestamp descriptor
        return 1;

    case 0x0002: // Dependency descriptor
        return 2;

    case 0x0003: // GFDT descriptor
        return 4;

    case 0x0004: // SI descriptor
        return 2;
    }

    switch (descTag >> 12) {
    case 0:
    case 1:
    case 2:
    case 3: // Reserved for ISO use (8-bit length descriptor)
        return 1;

    case 4:
    case 5:
    case 6: // Reserved for ISO use (16-bit length descriptor)
        return 2;

    case 7: // Reserved for ISO use (32-bit length descriptor)
        return 4;
    }

    return 0; // Reserved for private use
}

/////////////////////////////////////
// MMT_general_location_info
/////////////////////////////////////
struct MmtGeneralLocationInfo {
public:
    MmtGeneralLocationInfo (const byte_t* &d, const byte_t *&end): packetId(-1)
            {
        parseLocInfo(d, end);
    }
    uint16_t packetId;
    uint8_t ipv4SrcAddr[4];
    uint8_t ipv4DstAddr[4];
    uint16_t dstPort;
    uint16_t ipv6SrcAddr[8];
    uint16_t ipv6DstAddr[8];
    uint16_t networkId;
    uint16_t mpeg2TsId;
    uint16_t mpeg2Pid;
    string url;
    string privateLoc;
    uint16_t messageId;

    uint16_t getLocType() {
        return locType;
    }

private:
    uint8_t locType;
    void parseLocInfo (const byte_t *&d, const byte_t *&end) {
        locType = SAFEREAD_U8(d, end, "tblId");
        switch (locType) {
        case 0x00: // same MMTP session
            {
                packetId = SAFEREAD_U16(d, end, "packetId");
            }
            break;
        case 0x01: // MMTP session ipv4
            {
                for (int i = 0 ; i < 4 ; i++) {
                    ipv4SrcAddr[i] = SAFEREAD_U8(d, end, "srcIp");
                }
                for (int i = 0 ; i < 4 ; i++) {
                    ipv4DstAddr[i] = SAFEREAD_U8(d, end, "dstIp");
                }
                dstPort = SAFEREAD_U16(d, end, "dstPort");
                packetId = SAFEREAD_U16(d, end, "packetId");
            }
            break;
        case 0x02: // MMTP session ipv6
            {
                for (int i = 0 ; i < 8 ; i++) {
                    ipv6SrcAddr[i] = SAFEREAD_U16(d, end, "srcIp");
                }
                for (int i = 0 ; i < 8 ; i++) {
                    ipv6DstAddr[i] = SAFEREAD_U16(d, end, "dstIp");
                }
                dstPort = SAFEREAD_U16(d, end, "dstPort");
                packetId = SAFEREAD_U16(d, end, "packetId");
            }
            break;
        case 0x03: // MEPG-2 TS ES (broadcast)
            {
                networkId = SAFEREAD_U16(d, end, "networkId");
                mpeg2TsId = SAFEREAD_U16(d, end, "mpeg2TsId");
                mpeg2Pid = SAFEREAD_U16(d, end, "mpeg2Pid") - 0xe000; // 11100000 00000000
            }
            break;
        case 0x04: // MPEG-2 TS ES (IP)
            {
                for (int i = 0 ; i < 8 ; i++) {
                    ipv6SrcAddr[i] = SAFEREAD_U16(d, end, "srcIp");
                }
                for (int i = 0 ; i < 8 ; i++) {
                    ipv6DstAddr[i] = SAFEREAD_U16(d, end, "dstIp");
                }
                dstPort = SAFEREAD_U16(d, end, "dstPort");
                mpeg2Pid = SAFEREAD_U16(d, end, "mpeg2Pid") - 0xe000; // 11100000 00000000
            }
            break;
        case 0x05: // URL
            {
                uint8_t urlLen = SAFEREAD_U8(d, end, "urlLen");
                url = {(char*)d, urlLen};
                d += urlLen;
            }
            break;
        case 0x06: // Reserved for private location information
            {
                uint8_t len = SAFEREAD_U8(d, end, "len");
                privateLoc = {(char*)d, len};
                d += len;
            }
            break;
        case 0x07: // In the same signalling message. No more info required.
            break;
        case 0x08: // Signalling message in the same data path
            {
                messageId = SAFEREAD_U16(d, end, "messageId");
            }
            break;
        case 0x09: // Signalling message in the same data path in the same UDP/IP flow
            {
                packetId = SAFEREAD_U16(d, end, "packetId");
                messageId = SAFEREAD_U16(d, end, "messageId");
            }
            break;
        case 0x0A: // Signalling message in UDP/IP (v4)
            {
                for (int i = 0 ; i < 4 ; i++) {
                    ipv4SrcAddr[i] = SAFEREAD_U8(d, end, "srcIp");
                }
                for (int i = 0 ; i < 4 ; i++) {
                    ipv4DstAddr[i] = SAFEREAD_U8(d, end, "dstIp");
                }
                dstPort = SAFEREAD_U16(d, end, "dstPort");
                packetId = SAFEREAD_U16(d, end, "packetId");
                messageId = SAFEREAD_U16(d, end, "messageId");
            }
            break;
        case 0x0B: // Signalling message in UDP/IP (v6)
            {
                for (int i = 0 ; i < 8 ; i++) {
                    ipv6SrcAddr[i] = SAFEREAD_U16(d, end, "srcIp");
                }
                for (int i = 0 ; i < 8 ; i++) {
                    ipv6DstAddr[i] = SAFEREAD_U16(d, end, "dstIp");
                }
                dstPort = SAFEREAD_U16(d, end, "dstPort");
                packetId = SAFEREAD_U16(d, end, "packetId");
                messageId = SAFEREAD_U16(d, end, "messageId");
            }
            break;
        case 0x0C: // MPEG-2 TS ES over ip4 broadcast
            {
                for (int i = 0 ; i < 4 ; i++) {
                    ipv4SrcAddr[i] = SAFEREAD_U8(d, end, "srcIp");
                }
                for (int i = 0 ; i < 4 ; i++) {
                    ipv4DstAddr[i] = SAFEREAD_U8(d, end, "dstIp");
                }
                dstPort = SAFEREAD_U16(d, end, "dstPort");
                // 11100000 00000000
                mpeg2Pid = SAFEREAD_U16(d, end, "mpeg2Pid") - 0xe000;
            }
            break;
        default:
            l.warn("Invalid locationType:%d", locType);
        }
    }
};

static void hrbmHandler (const byte_t *d, uint32_t sz, Chain* pctx)
{
    auto end = d + sz;

    uint32_t maxBufSz = SAFEREAD_U32(d, end, "maxBufSz");
    uint32_t fixedE2ESz = SAFEREAD_U32(d, end, "fixedE2ESz");
    uint32_t maxDelay = SAFEREAD_U32(d, end, "maxDelay");
    l.warn("TODO: HRBM maxBuf:%d endToEnd:%d maxDelay:%d", maxBufSz, fixedE2ESz,
            maxDelay);
}

static void hrbmrHandler (const byte_t *d, uint32_t sz, Chain *pctx)
{
    auto end = d + sz;

    int numMode = SAFEREAD_U8(d, end, "numMode");
    for (int i = 0 ; i < numMode ; i++) {
        int bufType = SAFEREAD_U8(d, end, "bufType");
        int maxDecapSz = SAFEREAD_U32(d, end, "maxDecapSz");
        l.warn("TODO: HRBMR type:%d maxDecap:%d", bufType, maxDecapSz);
    }
    int validFlag = (SAFEREAD_U8(d, end, "validFlag") >> 7) & 0xFF;
}

static void parseMpTable (const byte_t *&d, const byte_t *&end,
        int targetSubset, MmtpService &msrv)
{
    auto pkg = msrv.currentPackage.get();

    // read table id
    int tblId = SAFEREAD_U8(d, end, "tblId");

    // subset check
    if (targetSubset > 0) {
        if ((tblId - 0x11) != targetSubset) {
            l.warn("MPT subset mismatch : tblId(%d) vs targetSubset(%d)", tblId,
                    targetSubset);
            return;
        }
    }

    // read table version
    int tblVer = SAFEREAD_U8(d, end, "tblVer");
    int tblLen = SAFEREAD_U16(d, end, "tblLen");
    if (d + tblLen != end) {
        l.warn("MPT has length:%d expected:%u", tblLen, end - d);
        return;
    }

    // read table mode
    int tblMode = SAFEREAD_U8(d, end, "tblMode") & 0x3;

    if (tblId == 0x11 || tblId == 0x20) {
        // update package info when base Or Complete table
        size_t idLen = SAFEREAD_U8(d, end, "package idLen");
        SAFEREAD_CHECKSIZE(d, end, idLen, "package id bytes");
        pkg->id = {(char*)d, idLen};
        d += idLen;

        uint16_t descLen = SAFEREAD_U16(d, end, "MP_table_descriptors_length");
        SAFEREAD_SKIP(d, end, descLen, "MPT Description");

        switch (tblMode) {
        case 0: // sequential_order_processing_mode
            pkg->assets.clear();
            pkg->mpTableVersion = tblVer;
            pkg->lastMpTableId = tblId;
            break;

        case 1: // order_irrelevant_processing_mode
            pkg->assets.clear();
            pkg->mpTableVersion = tblVer;
            pkg->lastMpTableId = -1;
            break;

        case 2: // independent_processing_mode
            pkg->mpTableVersion = -1;
            pkg->lastMpTableId = -1;
            break;
        }
    } else {
        switch (tblMode) {
        case 0: // sequential_order_processing_mode
            if (tblVer != pkg->mpTableVersion) return;
            if (tblId != pkg->lastMpTableId + 1) return;
            pkg->lastMpTableId = tblId;
            break;

        case 1: // order_irrelevant_processing_mode
            if (tblVer != pkg->mpTableVersion) return;
            break;
        }
    }

    l.info("New Table %d ver:%d mode:%d", tblId, tblVer, tblMode);

    int nAssets = SAFEREAD_U8(d, end, "numAssets");
    for (int i = 0; i < nAssets; i++) {
        // Read Identifier_mapping() and asset type
        byte_t idType = SAFEREAD_U8(d, end, "identifier_type");
        if (idType != 0) {
            l.warn("identifier_type != 0 is not supported");
            return;
        }

        uint32_t idScheme = SAFEREAD_U32(d, end, "asset id scheme");
        if (idScheme != 0 && idScheme != 1) {
            l.warn("Unexpected asset id scheme:0x%08x", idScheme);
            return;
        }

        uint32_t idLen = SAFEREAD_U32(d, end, "idLen");

        SAFEREAD_CHECKSIZE(d, end, idLen + 4, "asset id bytes or type");
        string id = {(char*)d, (size_t)idLen};
        d += idLen;
        string type = {(char*)d, 4};
        d += 4;

        //l.info("asset Id = %s", id.c_str());
        auto &asset = pkg->assets[id];
        if (!asset || asset->type != type) {
            asset = new Asset();
            asset->id = id;
            asset->type = type;
        }

        // read default_asset_flag, asset_cock_relation_flag
        byte_t assetFlags = SAFEREAD_U8(d, end, "asset flags");
        asset->isDefault = !(assetFlags & 2);
        bool clkRelFlag = !!(assetFlags & 1);
        if (clkRelFlag) {
            SAFEREAD_SKIP(d, end, 1, "clock relation info");
            bool atsf = !!(SAFEREAD_U8(d, end, "asset_timescale_flag")
                    & 1);
            if (atsf) SAFEREAD_SKIP(d, end, 4, "asset_timescale");
        }

        // read asset_location
        byte_t locCount = SAFEREAD_U8(d, end, "location_count");
        if (locCount > 1) {
            l.warn("TODO: Multiple location is not supported yet. locCount=%d",
                    locCount);
        }
        for (int j = 0; j < locCount; j++) {
            MmtGeneralLocationInfo loc(d, end);
            byte_t locType = loc.getLocType();
            if (loc.packetId >= 0) {
                asset->packetId = loc.packetId;
            } else {
                l.warn("TODO: locType:%d other than packetId is unsupported",
                        locType);
            }
        }

        l.info("asset %s pktid:%d type:%s default:%d", asset->id.c_str(),
                asset->packetId, asset->type.c_str(), asset->isDefault);

        // read asset_descriptors
        uint16_t descLoopLen = SAFEREAD_U16(d, end, "asset_descriptors_length");
        const byte_t *dle = d + descLoopLen; // descriptor loop end
        if (dle > end) {
            l.warn("MPT is broken. no bytes for asset_descriptors_byte");
            return;
        }
        while (d < dle) {
            uint16_t descTag = SAFEREAD_U16(d, dle, "descTag");
            uint32_t descLen;
            switch (lengthBytes(descTag)) {
            case 1:
                descLen = SAFEREAD_U8(d, dle, "desc length");
                break;

            case 2:
                descLen = SAFEREAD_U16(d, dle, "16-bit length field");
                break;

            case 4:
                descLen = SAFEREAD_U32(d, dle, "32-bit length field");
                break;

            default:
                l.warn("Cannot determine length of descriptor %u", descTag);
                return;
            }
            SAFEREAD_CHECKSIZE(d, dle, descLen, "asset descriptor %u",
                    descTag);

            const byte_t *de = d + descLen; // descriptor end
            switch (descTag) {
            case 0x0001: // MPU Timestamp descriptor
                if (descLen % 12 != 0) {
                    l.warn("MPU Timestamp descriptor length should be 12*N");
                    break;
                }
                //asset->timestamps.clear();
                while (d < de) {
                    uint32_t seq = SAFEREAD_U32(d, de, "timestamp seq");
                    uint64_t pts = SAFEREAD_U64(d, de, "timestamp pts");
                    asset->timestamps[seq] = pts;

                    // try to set mpu presentation time late

                    auto mpui = msrv.mpus.find({asset->packetId, seq});
                    if (mpui == msrv.mpus.end()) continue;
                    auto &mpu = mpui->second;
                    if (mpu->presentationTime != 0) continue;

                    l.info("Timestamp for MPU %u.%u is available.",
                            asset->packetId, seq);
                    mpu->presentationTime = double(pts) / 0x100000000UL
                            - 2208988800UL;
                    mpu->flags |= Mpu::FLAG_MPU_PTS;
                    msrv.mpuUpdate.notify(mpu, 4, nullptr);
                }
                break;

            default:
                l.warn("TODO: Skipped asset descriptor tag:%u", descTag);
                d += descLen;
                break;
            }
        }
        if (d != dle) {
            l.warn("MPT is broken. descriptor loop lenth is not consistent.");
            return;
        }
    }
}

static void mptHandler (const byte_t *d, uint32_t sz, Chain* pctx)
{
    auto end = d + sz;

    auto mctx = pctx->as<MessageContext>();
    auto msrv = static_cast<MmtpService*>(mctx->mmtpServiceTask->service.get());

    if (!msrv->currentPackage) msrv->currentPackage = new MmtPackage();
    auto pkg = msrv->currentPackage.get();

    SAFEREAD_CHECKSIZE(d, end, 6, "Too short MPT length:%d", end - d);

    parseMpTable(d, end, mctx->messageId - 0x11, *msrv);
}

static void parsePiTable (const byte_t*& d, const byte_t*& end,
        Ref<MmtPackage> pkg, int targetSubset)
{
    // read table id
    int tblId = SAFEREAD_U8(d, end, "tblId");

    // subset check
    if (targetSubset > 0) {
        if ((tblId - 0x01) != targetSubset) {
            l.warn("MPI subset mismatch : tblId(%d) vs targetSubset(%d)", tblId,
                    targetSubset);
            return;
        }
    }

    // read table version
    int tblVer = SAFEREAD_U8(d, end, "tblVer");
    int tblLen = SAFEREAD_U16(d, end, "tblLen");

    int piMode = (SAFEREAD_U8(d, end, "piMode") >> 2) & 0x03;
    if (tblId == 0x01 || tblId == 0x10) {
        switch (piMode) {
        case 0: // sequential_order_processing_mode
            pkg->assets.clear();
            pkg->piTableVersion = tblVer;
            pkg->lastPiTableId = tblId;
            break;

        case 1: // order_irrelevant_processing_mode
            pkg->assets.clear();
            pkg->piTableVersion = tblVer;
            pkg->lastPiTableId = -1;
            break;

        case 2: // independent_processing_mode
            pkg->piTableVersion = -1;
            pkg->lastPiTableId = -1;
            break;
        }
    } else {
        switch (piMode) {
        case 0: // sequential_order_processing_mode
            if (tblVer != pkg->piTableVersion) return;
            if (tblId != pkg->lastPiTableId + 1) return;
            pkg->lastPiTableId = tblId;
            break;

        case 1: // order_irrelevant_processing_mode
            if (tblVer != pkg->piTableVersion) return;
            break;
        }
    }

    int mpitDescLen = SAFEREAD_U16(d, end, "mpit_descriptors_length");
    l.warn("TODO: MPIT Descriptor parsing is required.");
    d += mpitDescLen;
    int piContCnt = SAFEREAD_U8(d, end, "PI Content Count");
    for (int i = 0 ; i < piContCnt ; i++) {
        uint8_t piContTypeLen = SAFEREAD_U8(d, end, "PI Content Type Length");
        string piContType = {(char*)d, piContTypeLen};
        d += piContTypeLen;
        uint8_t piContNameLen = SAFEREAD_U8(d, end, "PI Content Name Length");
        string piContName = {(char*)d, piContNameLen};
        d += piContNameLen;
        int piContDescLen = SAFEREAD_U16(d, end, "PI Content Descriptors length");
        l.warn("TODO: PI Content Descriptor parsing is required.");
        d += piContDescLen;
        int piContLen = SAFEREAD_U16(d, end, "PI Content length");
        l.warn("TODO: PI Content parsing is required.");
        d += piContLen;
    }
}

static void mpiHandler (const byte_t *d, uint32_t sz, Chain* pctx) {
    auto end = d + sz;

    bool mptExist = !!(SAFEREAD_U8(d, end, "associated_MP_Table_flag") & 0x01);

    auto mctx = pctx->as<MessageContext>();
    auto msrv = static_cast<MmtpService*>(mctx->mmtpServiceTask->service.get());

    if (!msrv->currentPackage) msrv->currentPackage = new MmtPackage();
    auto pkg = msrv->currentPackage.get();

    // MPI Table
    parsePiTable(d, end, pkg, mctx->messageId - 0x01);
    // MP Table
    if (mptExist) {
        l.info("mpiHandler has Mp Table.");
        parseMpTable(d, end, mctx->messageId - 0x11, *msrv);
    }
}

static void parsePaTable (const byte_t *&d, const byte_t *&end)
{
    // read table id
    int tblId = SAFEREAD_U8(d, end, "tblId");
    // read table version
    int tblVer = SAFEREAD_U8(d, end, "tblVer");
    int tblLen = SAFEREAD_U16(d, end, "tblLen");

    int tblNum = SAFEREAD_U8(d, end, "tblNum");
    for (int i = 0 ; i < tblNum ; i++) {
        int signallingTblId = SAFEREAD_U8(d, end, "signallingTblNum");
        int signallingTblVer = SAFEREAD_U8(d, end, "signallingTblVer");
        MmtGeneralLocationInfo loc(d, end);
        int alterLocFlag = SAFEREAD_U8(d, end, "alterLocFlag");
        if (alterLocFlag == 0xff) { //1111 1111
            MmtGeneralLocationInfo alterLoc(d, end);
        }
    }
    // Extension -> parsing not required
}

static void paHandler (const byte_t *d, uint32_t sz, Chain* pctx)
{
    auto end = d + sz;

    auto mctx = pctx->as<MessageContext>();
    auto msrv = static_cast<MmtpService*>(mctx->mmtpServiceTask->service.get());

    if (!msrv->currentPackage) msrv->currentPackage = new MmtPackage();
    auto pkg = msrv->currentPackage.get();

    int tableNum = SAFEREAD_U8(d, end, "number of tables in PA message");
    int ids [tableNum];
    for (int i = 0 ; i < tableNum ; i++) {
        ids[i] = SAFEREAD_U8(d, end, "tableId");
        int tableVer = SAFEREAD_U8(d, end, "tableVer");
        int tableLen = SAFEREAD_U16(d, end, "tableLen");
        l.info("PA message includes table of Id:%d, Ver:%d", ids[i], tableVer);
    }
    for (int i = 0 ; i < tableNum ; i++) {
        if (ids[i] == 0x00) { // PA table
            parsePaTable(d, end);
        } else if (ids[i] == 0x01 || ids[i] == 0x10) {
            // MPI table (base Or complete)
            parsePiTable(d, end, pkg, -1);
        } else if (ids[i] == 0x11 || ids[i] == 0x20) {
            // MP table (base Or complete)
            parseMpTable(d, end, -1, *msrv);
        } else if (ids[i] == 0x21) { // CRI table
        } else if (ids[i] == 0x22) { // DCI table
        } else {
            l.warn("PA message includes unsupported table Id:%d", ids[i]);
        }
    }
}

static void dciHandler (const byte_t *d, uint32_t sz, Chain* pctx)
{
    auto end = d + sz;

    // read table id
    int tblId = SAFEREAD_U8(d, end, "tblId");

    // read table version
    int tblVer = SAFEREAD_U8(d, end, "tblVer");
    int tblLen = SAFEREAD_U16(d, end, "tblLen");
    if (d + tblLen != end) {
        l.warn("DCI has length:%d expected:%u", tblLen, end - d);
        return;
    }
    int assetNum = SAFEREAD_U8(d, end, "asset number");
    for (int i = 0 ; i < assetNum ; i++) {
        int assetIdScheme = SAFEREAD_U32(d, end, "asset_id_scheme");
        uint32_t assetIdLen= SAFEREAD_U32(d, end, "asset_id_length");
        string assetId = {(char*)d, assetIdLen};
        d += assetIdLen;
        uint32_t mimeTypeLen = SAFEREAD_U32(d, end, "mime_type_length");
        string mimeType = {(char*)d, mimeTypeLen};
        d += mimeTypeLen;
        bool codecComplexityFlag = !!(SAFEREAD_U8(d, end, "codec_complexity_flag") & 0x01);
        if (codecComplexityFlag) {
            if (mimeType.find("video") == 0) { // mimeType starts with "video"
                int videoAverageBitrate = SAFEREAD_U16(d, end, "video_average_bitrate");
                int videoMaxBitrate = SAFEREAD_U16(d, end, "video_max_bitrate");
                int horizontalResolution = SAFEREAD_U16(d, end, "horizantal_resolution");
                int verticalResolution = SAFEREAD_U16(d, end, "vertical_resolution");
                int temporalResolution = SAFEREAD_U8(d, end, "temporal_resolution");
                int videoMinBufferSize = SAFEREAD_U16(d, end, "video_minimum_buffer_size");
            } else if (mimeType.find("audio") == 0) { // mimeType starts with "audio"
                int audioAverageBitrate = SAFEREAD_U16(d, end, "audio_average_bitrate");
                int audioMaxBitrate = SAFEREAD_U16(d, end, "audio_max_bitrate");
                int audioMinBufferSize = SAFEREAD_U16(d, end, "audio_minimum_buffer_size");
            }
        } else {
            int requiredStorage = SAFEREAD_U32(d, end, "required_storage");
        }
    }
}

static void criHandler (const byte_t *d, uint32_t sz, Chain* pctx)
{
    auto end = d + sz;

    // read table id
    int tblId = SAFEREAD_U8(d, end, "tblId");

    // read table version
    int tblVer = SAFEREAD_U8(d, end, "tblVer");
    int tblLen = SAFEREAD_U16(d, end, "tblLen");
    if (d + tblLen != end) {
        l.warn("CRI has length:%d expected:%u", tblLen, end - d);
        return;
    }

    int criDescNum = SAFEREAD_U8(d, end, "CRI descriptor number");
    for (int i = 0 ; i < criDescNum ; i++) {
        int descriptorTag = SAFEREAD_U16(d, end, "descriptorTag");
        int descriptorLen = SAFEREAD_U16(d, end, "descriptorLen");
        int clockRelationId = SAFEREAD_U8(d, end, "clockRelationId");
        uint8_t stcSample[6];
        for (int j = 0 ; j < 6 ; j++) {
            stcSample[j] = SAFEREAD_U8(d, end, "stc_sample");
            if (j == 0) {
                stcSample[0] = stcSample[0] & 0x03;
            }
        }
        uint64_t ntpTimestampSample = SAFEREAD_U64(d, end, "ntpTimestampSample");
    }
}


struct Usbd {
    struct Channel {
        int genre; // 0..1
        std::string iconUri; // 1
        // 0..N (descTtxt(1), descLang(0..1))
        std::vector<std::pair<std::string, std::string>> desc;
    };

    struct MpuComp {
        std::string pkgId; // 1
        std::string contIdSchemeIdUri; // 0..1
        std::string contIdValue; // 0..1
        std::string nextPkgId; // 0..1
        std::string nextContIdSchemeIdUri; // 0..1
        std::string nextContIdValue; // 0..1
    };

    struct RouteComp {
        std::string sTsidUri; // 1
        std::string apdUri; // 0..1
        std::string sTsidDstIpAddr; // 0..1
        std::string sTsidDstUdpPort; // 1
        std::string sTsidSrcIpAddr; // 1
        std::string sTsidMjrPtclVer; // 0..1
        std::string sTsidMnrPtclVer; // 0..1
    };

    struct BbComp {
        std::string mpdUri; // 1
    };

    struct CompInfo {
        int type; // 1
        int role; // 1
        bool protectFlag; // 0..1
        std::string id; // 1
        std::string name; // 0..1
    };

    std::string srvId; // 1
    int srvStatus; // 0..1
    // 0..N (name(1), nameLang(1))
    std::vector<std::pair<std::string, std::string>> names;
    std::vector<std::string> srvLang; // 0..N
    std::vector<std::string> caRating; // 0..N
    std::vector<std::string> otherRatings; // 0..N
    Channel ch; // 1
    MpuComp mpu; // 0..1
    RouteComp route; // 0..1
    BbComp broadband; // 0..1
    std::vector<CompInfo> compInfo; // 0..N

    void readXml(uint8_t *buf, int size) {
        xml_document doc;
        xml_parse_result result = doc.load_buffer(buf, size, parse_default,
                encoding_auto);
        xml_node usbd = doc.child("BundleDescriptionMMT");
        xml_node usd = usbd.child("UserServiceDescription");
        srvId = usd.attribute("serviceId").as_string();
        xml_attribute attr;
        if (attr = usd.attribute("serviceStatus")) {
            srvStatus = usd.attribute("serviceStatus").as_int();
        } else {
            srvStatus = -1;
        }
        for (xml_node node = usd.child("Name"); node;
                node = node.next_sibling("Name")) {
            names.push_back(make_pair(node.value(),
                    node.attribute("lang").as_string()));
        }
        for (xml_node node = usd.child("ServiceLanguage"); node;
                node = node.next_sibling("ServiceLanguage")) {
            srvLang.push_back(node.child_value());
        }
        for (xml_node node = usd.child("ContentAdvisoryRating"); node;
                node = node.next_sibling("ContentAdvisoryRating")) {
            caRating.push_back(node.child_value());
        }
        for (xml_node node = usd.child("otherRatings"); node;
                node = node.next_sibling("otherRatings")) {
            otherRatings.push_back(node.child_value());
        }
        xml_node n = usd.child("Channel");
        if (attr = n.attribute("serviceGenre")) ch.genre = attr.as_int();
        ch.iconUri = n.attribute("serviceIcon").as_string();
        for (xml_node node = usd.child("ServiceDescription"); node;
                node = node.next_sibling("ServiceDescription")) {
            ch.desc.push_back(
                    make_pair(node.attribute("serviceDescrText").as_string(),
                    node.attribute("serviceDescrLang").as_string()));
        }
        if (n = usd.child("MPUComponent")) {
            mpu.pkgId = n.attribute("mmtPackageId").as_string();
            if (attr = n.attribute("contentIdSchemeIdUri")) {
                mpu.contIdSchemeIdUri = attr.as_string();
            }
            if (attr = n.attribute("contentIdValue")) {
                mpu.contIdValue = attr.as_string();
            }
            if (attr = n.attribute("nextMMTPackageId")) {
                mpu.nextPkgId = attr.as_string();
            }
            if (attr = n.attribute("nextContentIdSchemeIdUri")) {
                mpu.nextContIdSchemeIdUri = attr.as_string();
            }
            if (attr = n.attribute("nextContentIdValue")) {
                mpu.nextContIdValue = attr.as_string();
            }
        }
        if (n = usd.child("ROUTEComponent")) {
            route.sTsidUri = n.attribute("sTSIDUri").as_string();
            if (attr = n.attribute("apdUri")) route.apdUri  = attr.as_string();
            if (attr = n.attribute("sTSIDDestinationIpAddress")) {
                route.sTsidDstIpAddr = attr.as_string();
            }
            if (attr = n.attribute("sTSIDDestinationUdpPort")) {
                route.sTsidDstUdpPort = attr.as_string();
            }
            if (attr = n.attribute("sTSIDDSourceIpAddress")) {
                route.sTsidSrcIpAddr = attr.as_string();
            }
            if (attr = n.attribute("sTSIDMajorProtocolVersion")) {
                route.sTsidMjrPtclVer = attr.as_string();
            }
            if (attr = n.attribute("sTSIDMinorProtocolVersion")) {
                route.sTsidMnrPtclVer = attr.as_string();
            }
        }
        if (n = usd.child("BroadbandComponent")) {
            broadband.mpdUri = n.attribute("fullMPDUri").as_string();
        }
        for (xml_node node = usd.child("ComponentInfo"); node;
                node = node.next_sibling("ComponentInfo")) {
            CompInfo info;
            if (attr = node.attribute("componentType")) {
                info.type = attr.as_int();
            }
            if (attr = node.attribute("componentRole")) {
                info.role = attr.as_int();
            }
            if (attr = node.attribute("componentProtectedFlag")) {
                info.protectFlag = attr.as_bool();
            }
            if (attr = node.attribute("componentId")) {
                info.id = attr.as_string();
            }
            if (attr = node.attribute("componentName")) {
                info.name = attr.as_string();
            }
            compInfo.push_back(info);
        }
    }
};

static void ma3Handler (const byte_t *d, uint32_t sz, Chain* pctx)
{
    auto mctx = pctx->as<MessageContext>();
    auto msrv = static_cast<MmtpService*>(mctx->mmtpServiceTask->service.get());

    auto end = d + sz;

    uint16_t srvId = SAFEREAD_U16(d, end, "srvId");
    if (srvId != msrv->id) {
        l.error("Unexpected service_id:%u of ma3 message. expected:%d",
                srvId, msrv->id);
        return;
    }

    uint16_t contType = SAFEREAD_U16(d, end, "contType");
    uint8_t v = SAFEREAD_U8(d, end, "v");
    uint8_t compr = SAFEREAD_U8(d, end, "compr");
    uint8_t uriLen = SAFEREAD_U8(d, end, "uriLen");
    SAFEREAD_CHECKSIZE(d, end, uriLen, "%u uriLen", uriLen);
    string uri = {(char*)d, uriLen};
    d += uriLen;
    l.info("MA3 type:%d v:%d %s", contType, v,
            (compr ==2)? "Compressed": "Not compressed");
    uint32_t contLen = SAFEREAD_U32(d, end, "contLen");
    SAFEREAD_CHECKSIZE(d, end, contLen, "atsc3_message_content_bytes");

    // calculate checksum
    int crc32 = (int)acba_crc32(0, d, contLen);

    // Compare with existing message if any
    auto &msg = msrv->ma3Messages[contType];
    if (msg && msg->attributes["version"].asI32() == mctx->version &&
            msg->attributes["checksum"].asI32() == crc32) {
        return;
    }

    // on receiving newer message.
    msg = new File();
    msg->attributes["version"] = mctx->version;
    msg->attributes["checksum"] = crc32;

    if (compr == 2) {
        GzipReader gr;
        if (gr.read(d, end - d, nullptr) != end - d) {
            l.warn("Failed to uncompress MA3 content");
            return;
        }
        msg->content.writeBytes(gr.outBuffer.begin(), gr.outBuffer.size());
    } else {
        msg->content.writeBytes(d, end - d);
    }

    switch (contType) {
    case 0x0001: // USBD
        if (IS_FIRST_TIME) l.warn("TODO: USBD");
        {
            Usbd usbd;
            usbd.readXml(msg->content.begin(), msg->content.size());
            mctx->mmtpServiceTask->caRatingInUsbd = usbd.caRating;
        }
        break;

    case 0x0002: // MPD : DASH-IF : Not parsing
        if (IS_FIRST_TIME) l.warn("TODO: MPD");
        break;

    case 0x0003:
        if (IS_FIRST_TIME) l.warn("TODO: HELD");
        {
            xml_document doc;
            xml_parse_result result = doc.load_buffer(msg->content.begin(),
                    msg->content.size(), parse_default, encoding_auto);
            xml_node held = doc.child("HELD");
            for (xml_node node = held.child("HTMLEntryPackage"); node;
                    node = node.next_sibling("HTMLEntryPackage")) {
                string appCtxId = node.attribute("appContextId").as_string();
                xml_attribute attr;
                if (attr = node.attribute("requiredCapabilities")) {
                    string capas = attr.as_string();
                }
                if (attr = node.attribute("appRendering")) {
                    bool rendering = (attr.as_string() == "true")?true:false;
                }
                if (attr = node.attribute("clearAppContextCacheDate")) {
                    string clearDate = attr.as_string();
                }
                if (attr = node.attribute("bcastEntryPackageUrl")) {
                    string bcastPkgUrl = attr.as_string();
                }
                if (attr = node.attribute("bcastEntryPageUrl")) {
                    string bcastPageUrl = attr.as_string();
                }
                if (attr = node.attribute("bbandEntryPageUrl")) {
                    string bbandPageUrl = attr.as_string();
                }
                if (attr = node.attribute("validFrom")) {
                    string validFrom = attr.as_string();
                }
                if (attr = node.attribute("validUntil")) {
                    string validUntil = attr.as_string();
                }
                if (attr = node.attribute("coupledServices")) {
                    string coubledSrvs = attr.as_string();
                }
                if (attr = node.attribute("lctTSIRef")) {
                    string tsis = attr.as_string();
                }
            }
        }
        break;

    case 0x0004: // AEI (Application Event Information)
        if (IS_FIRST_TIME) l.info("MMT_SLS: AEI");
        {
            xml_document doc;
            xml_parse_result result = doc.load_buffer(msg->content.begin(),
                    msg->content.size(), parse_default, encoding_auto);
            xml_node aei = doc.child("AEI");
            string assetId = aei.attribute("assetId").as_string();
            uint32_t mpuSeqNum = aei.attribute("mpuSeqNum").as_int();
            uint64_t timeStamp = aei.attribute("timeStamp").as_ullong();
            xml_attribute attr;
            for (xml_node node = aei.child("EventStream"); node;
                    node = node.next_sibling("EventStream")) {
                string schemeIdUri = node.attribute("schemeIdUri").as_string();
                if (attr = node.attribute("value")) {
                    string value = attr.as_string();
                }
                if (attr = node.attribute("timescale")) {
                    uint32_t ts = attr.as_int();
                }
                for (xml_node e = aei.child("Event"); e;
                        e = e.next_sibling("Event")) {
                    string event = e.child_value();
                    if (attr = e.attribute("presentationTime")) {
                        uint64_t pt = attr.as_ullong();
                    }
                    if (attr = e.attribute("duration")) {
                        uint64_t dur = attr.as_ullong();
                    }
                    if (attr = e.attribute("id")) uint32_t id = attr.as_uint();
                }
            }
        }
        break;

    case 0x0005: // Video Stream Properties Descriptor
        if (IS_FIRST_TIME) l.warn("TODO: Video Stream Properties Desc");
        {
            const byte_t *d = msg->content.begin();
            const byte_t *end = d + msg->content.size();

            uint16_t tag = SAFEREAD_U16(d, end, "descriptor_tag");
            uint16_t descriptorLen = SAFEREAD_U16(d, end,
                    "descriptor_length");
            uint8_t assetNum = SAFEREAD_U8(d, end, "number_of_asset");
            for (int i = 0 ; i < assetNum ; i++) {
                uint32_t idLen = SAFEREAD_U32(d, end,
                        "asset_id_length");
                string assetId = {(char *)d, idLen};
                d += idLen;
            }
            string codecCode = {(char *)d, 4};
            d += 4;
            uint8_t flags = SAFEREAD_U8(d, end, "flags");
            bool temporalScalabilityPresent = !!(flags >> 7);
            bool scalabilityInfoPresent = !!((flags >> 6) & 0x01);
            bool multiviewInfoPresent = !!((flags >> 5) & 0x01);
            bool resCfBdInfoPresent = !!((flags >> 4) & 0x01);
            bool prInfoPresent = !!((flags >> 3) & 0x01);
            bool brInfoPresent = !!((flags >> 2) & 0x01);
            bool colorInfoPresent = !!((flags >> 1) & 0x01);
            bool subLayerProfileTierLevelInfoPresent;
            uint8_t maxSubLayersInstream;
            if (temporalScalabilityPresent) {
                uint8_t aByte = SAFEREAD_U8(d, end, "aByte at %d", __LINE__);
                maxSubLayersInstream = aByte >> 2;
                subLayerProfileTierLevelInfoPresent = !!((aByte >> 1) & 0x01);
                bool temporalFilterPresent = !!(aByte & 0x01);
                aByte = SAFEREAD_U8(d, end, "aByte at %d", __LINE__);
                uint8_t tidMax = aByte >> 5;
                uint8_t tidMin = (aByte >> 2) & 0x03;
                if (temporalFilterPresent) {
                    uint8_t tfweight = aByte & 0x03;
                }
            }
            if (scalabilityInfoPresent) {
                uint8_t aByte = SAFEREAD_U8(d, end, "aByte at %d", __LINE__);
                uint8_t assetLayerId = aByte >> 2;
            }
            if (multiviewInfoPresent) {
                uint16_t two = SAFEREAD_U16(d, end, "twoBytes");
                uint8_t viewNuhLayerId = two >> 10;
                uint8_t viewPos = (two >> 4) & 0x3f;
                uint32_t three = SAFEREAD_U24(d, end, "threeBytes");
                uint16_t minDispWithOffet = three >> 13;
                uint16_t maxDispRange = (three >> 2) & 0x07ff;
            }
            if (resCfBdInfoPresent) {
                uint16_t picWidthInLumaSamples = SAFEREAD_U16(d, end,
                        "pic_width_in_luma_samples");
                uint16_t picHeightInLumaSamples = SAFEREAD_U16(d, end,
                        "pic_height_in_luma_samples");
                uint8_t aByte = SAFEREAD_U8(d, end, "aByte at %d", __LINE__);
                uint8_t chromaFormatIdc = aByte >> 6;
                if (chromaFormatIdc == 3) {
                    bool separateColourPlaneFlag = !!((aByte >> 5) & 0x01);
                }
                bool videoStillPresent = !!((aByte >> 1) & 0x01);
                bool video24hrPicPresent = !!(aByte & 0x01);
                aByte = SAFEREAD_U8(d, end, "aByte at %d", __LINE__);
                uint8_t bitDepthLumaMinus8 = aByte >> 4;
                uint8_t bitDepthChromaMinus8 = aByte & 0x0F;
            }
            if (prInfoPresent) {
                uint8_t len = 0;
                if (subLayerProfileTierLevelInfoPresent) {
                    len = maxSubLayersInstream - 1;
                }
                for (int i = 0 ; i <= len ; i++) {
                    uint8_t pictureRateCode = SAFEREAD_U8(d, end,
                            "picture_rate_code");
                    if (pictureRateCode == 255) {
                        uint16_t averagePictureRate = SAFEREAD_U16(d, end,
                                "average_picture_rate");
                    }
                }
            }
            if (brInfoPresent) {
                uint8_t len = 0;
                if (subLayerProfileTierLevelInfoPresent) {
                    len = maxSubLayersInstream - 1;
                }
                for (int i = 0 ; i <= len ; i++) {
                    uint16_t averageBitrate = SAFEREAD_U16(d, end,
                            "average_bitrate");
                    uint16_t maximumBitrate = SAFEREAD_U16(d, end,
                            "maximum_bitrate");
                }
            }
            if (colorInfoPresent) {
                uint8_t colourPrimaries = SAFEREAD_U8(d, end,
                        "colour_primaries");
                uint8_t transferCharacteristics = SAFEREAD_U8(d, end,
                        "transfer_characteristics");
                uint8_t matrixCoeffs = SAFEREAD_U8(d, end,
                        "matrix_coeffs");
                if (colourPrimaries >= 9) {
                    uint8_t aByte = SAFEREAD_U8(d, end, "aByte at %d", __LINE__);
                    bool cgCompatibility = !!(aByte >> 7);
                }
                if (transferCharacteristics >= 16) {
                    uint8_t tmp = d[0];
                    bool eotfInfoPresent = !!(tmp >> 7);
                    if (eotfInfoPresent) {
                        uint16_t two = SAFEREAD_U16(d, end, "two");
                        uint16_t eotfInfoLenMinus1 = two & 0x7fff;
                        uint8_t numSeisMinus1 = SAFEREAD_U8(d, end,
                                "num_SEIs_minus1");
                        for (int i = 0 ; i <= numSeisMinus1 ; i++) {
                            uint32_t seiNutLengthMinus1 = SAFEREAD_U16(d, end,
                                    "SEI_NUT_length_minus1");
                            string seiNutData = {(char *)d,
                                    seiNutLengthMinus1 + 1};
                            d += (seiNutLengthMinus1 + 1);
                        }
                    } else {
                        uint8_t aByte = SAFEREAD_U8(d, end, "two");
                    }
                }
            }
            if (subLayerProfileTierLevelInfoPresent) {
                // TODO : Refer H.265 spec chapter 7.3.3
            }
        }
        break;

    case 0x0006: // ATSC Staggercast Descriptor
        if (IS_FIRST_TIME) l.warn("TODO: Staggercast included");
        {
            const byte_t *d = msg->content.begin();
            const byte_t *end = d + msg->content.size();

            uint16_t tag = SAFEREAD_U16(d, end, "descriptor_tag");
            uint16_t descriptorLen = SAFEREAD_U16(d, end,
                    "descriptor_length");
            uint8_t assetNum = SAFEREAD_U8(d, end,
                    "number_of_staggercast_assets");
            for (int i = 0 ; i < assetNum ; i++) {
                uint16_t staggerPktId = SAFEREAD_U16(d, end,
                        "staggercast_packet_id");
                uint16_t mainPktId = SAFEREAD_U16(d, end,
                        "main_asset_packet_id");
            }
        }
        break;

    case 0x0007: // Inband Event Descriptor
        if (IS_FIRST_TIME) l.warn("TODO: Inband Event");
        {
            const byte_t *d = msg->content.begin();
            const byte_t *end = d + msg->content.size();

            uint16_t tag = SAFEREAD_U16(d, end, "descriptor_tag");
            uint16_t descriptorLen = SAFEREAD_U16(d, end, "descriptor_length");
            uint8_t assetNum = SAFEREAD_U8(d, end, "number_of_assets");
            for (int i = 0 ; i < assetNum ; i++) {
                uint32_t idLen = SAFEREAD_U32(d, end, "asset_id_length");
                string assetId = {(char *)d, idLen};
                d += idLen;
                uint8_t uriLen = SAFEREAD_U8(d, end, "scheme_id_uri_length");
                string uri  = {(char *)d, uriLen};
                d += uriLen;
                uint8_t valueLen = SAFEREAD_U8(d, end, "event_value_length");
                string eValue  = {(char *)d, valueLen};
                d += valueLen;
            }
        }
        break;

    case 0x0008: // Caption Asset Descriptor
        {
            const byte_t *d = msg->content.begin();
            const byte_t *end = d + msg->content.size();

            uint16_t tag = SAFEREAD_U16(d, end, "descriptor_tag");
            uint16_t descriptorLen = SAFEREAD_U16(d, end, "descriptor_length");
            uint8_t assetNum = SAFEREAD_U8(d, end, "number_of_assets");
            l.info("TODO: Caption Asset Desc - %u assets", assetNum);
            for (int i = 0 ; i < assetNum ; i++) {
                uint32_t idLen = SAFEREAD_U32(d, end, "asset_id_length");
                string assetId = {(char *)d, idLen};
                d += idLen;

                uint8_t langLen = SAFEREAD_U8(d, end, "language_length");
                string lang  = {(char *)d, langLen};
                d += langLen;

                uint8_t role_ar = SAFEREAD_U8(d, end, "role / aspect_ratio");
                uint8_t flags = SAFEREAD_U8(d, end, "easy_reader/profile/3d");

                l.info("Caption asset %s. lang:%s role:%u ar:%u easy_reader:%u "
                        "profile:%u 3d_support:%u", assetId.c_str(),
                        lang.c_str(), role_ar >> 4, role_ar & 0xf,
                        flags >> 7, (flags >> 5) & 0x3, (flags >> 4) & 0x1);
            }
        }
        break;

    case 0x0009: // Audio Stream Properties Descriptor
        if (IS_FIRST_TIME) l.warn("TODO: Audio Stream properties");
        {
            const byte_t *d = msg->content.begin();
            const byte_t *end = d + msg->content.size();

            uint16_t tag = SAFEREAD_U16(d, end, "descriptor_tag");
            uint16_t descriptorLen = SAFEREAD_U16(d, end, "descriptor_length");
            uint8_t assetNum = SAFEREAD_U8(d, end, "number_of_assets");
            for (int i = 0 ; i < assetNum ; i++) {
                uint32_t idLen = SAFEREAD_U32(d, end,
                        "asset_id_length");
                string assetId = {(char *)d, idLen};
                d += idLen;
                string codecCode = {(char *)d, 4};
                d += 4;
                uint8_t numPresentations = SAFEREAD_U8(d, end,
                        "num_presentations");
                uint8_t aByte = SAFEREAD_U8(d, end, "aByte at %d", __LINE__);
                bool multiStreamInfoPresent = !!(aByte >> 7);
                bool emergencyInfoTimePresent = !!((aByte >> 6) & 0x01);
                for (int j = 0 ; j < numPresentations ; j++) {
                    uint8_t presentationId = SAFEREAD_U8(d, end,
                            "presentation_id");
                    aByte = SAFEREAD_U8(d, end, "aByte at %d", __LINE__);
                    bool interactivityEnabled = !!(aByte >> 7);
                    bool profileChannelConfigPresent = !!((aByte >> 6) & 0x01);
                    uint8_t profileLong = (aByte >> 5) & 0x01;
                    uint8_t channelConfigLong = (aByte >> 4) & 0x01;
                    bool audioRenderingInfoPresent = !!((aByte >> 3) & 0x01);
                    bool languagePresent = !!((aByte >> 2) & 0x01);
                    bool accessibilityRolePresent = !!((aByte >> 1) & 0x01);
                    bool labelPresent = !!(aByte & 0x01);
                    if (profileChannelConfigPresent) {
                        if (profileLong == 1) {
                            uint32_t three = SAFEREAD_U24(d, end, "threeBytes");
                            uint8_t bitsteamVersion = three >> 17;
                            uint8_t presentationVersion = (three >> 9) & 0xff;
                            uint8_t mdcompat = (three >> 6) & 0x07;
                        } else {
                            uint8_t profileLevelIndication = SAFEREAD_U8(d, end,
                                    "profile_level_indication");
                        }
                        if (channelConfigLong == 1) {
                            uint32_t audioChannelConfig = SAFEREAD_U24(d, end,
                                    "threeBytes");
                        } else {
                            aByte = SAFEREAD_U8(d, end, "aByte at %d", __LINE__);
                            uint8_t channelConfiguration = aByte >> 2;
                        }
                    }
                    if (audioRenderingInfoPresent) {
                        uint8_t audioRenderingIndication = SAFEREAD_U8(d, end,
                                "audio_rendering_indication");
                    }
                    uint8_t numLanguagesMinus1;
                    if (languagePresent) {
                        numLanguagesMinus1 = SAFEREAD_U8(d, end,
                                "num_languages_minus1");
                        for (int k = 0 ; k < (numLanguagesMinus1+1) ; k++) {
                            uint8_t languageLength = SAFEREAD_U8(d, end,
                                    "language_length");
                            string language = {(char *)d, languageLength};
                            d += languageLength;
                        }
                    }
                    if (accessibilityRolePresent) {
                        uint8_t accessibilities [numLanguagesMinus1+1];
                        for (int k = 0 ; k < (numLanguagesMinus1+1) ; k++) {
                            accessibilities[k]= SAFEREAD_U8(d, end,
                                    "accessibility");
                        }
                        uint8_t role = SAFEREAD_U8(d, end, "role");
                    }
                    if (labelPresent) {
                        uint8_t labelLength = SAFEREAD_U8(d, end,
                                "label_length");
                        string label = {(char *)d, labelLength};
                        d += labelLength;
                    }
                    if (multiStreamInfoPresent) {
                        uint8_t numPresentationAuxStreams = SAFEREAD_U8(d, end,
                                "num_presentation_aux_streams");
                        string auxStreamId = {(char *)d,
                                numPresentationAuxStreams};
                        d += numPresentationAuxStreams;
                    }
                }
                if (multiStreamInfoPresent) {
                    aByte = SAFEREAD_U8(d, end, "aByte at %d", __LINE__);
                    bool thisIsMainStream = !!(aByte >> 7);
                    uint8_t thisStreamId = aByte & 0x7f;
                    aByte = SAFEREAD_U8(d, end, "aByte at %d", __LINE__);
                    uint8_t bundleId = SAFEREAD_U8(d, end,
                            "this_stream_id") & 0x7f;
                    if (thisIsMainStream) {
                        aByte = SAFEREAD_U8(d, end, "aByte at %d", __LINE__);
                        uint8_t numAuxiliaryStreams = aByte & 0x7f;
                        for (int j = 0 ; j < numAuxiliaryStreams ; j++) {
                            aByte = SAFEREAD_U8(d, end, "aByte at %d", __LINE__);
                            uint8_t deliveryMethod = aByte >> 7;
                            uint8_t auxiliaryStreamId = aByte & 0x7f;
                        }
                    }
                }
                if (emergencyInfoTimePresent) {
                    aByte = SAFEREAD_U8(d, end, "aByte at %d", __LINE__);
                    bool emergencyInfoStartTimePresent = !!(aByte >> 7);
                    bool emergencyInfoEndTimePresent = !!((aByte >> 6) & 0x01);
                    if (emergencyInfoStartTimePresent) {
                        uint32_t startTime = SAFEREAD_U32(d, end, "start_time");
                        uint16_t startTimeMs = SAFEREAD_U16(d, end,
                                "start_time_ms") & 0x03ff;
                    }
                    if (emergencyInfoEndTimePresent) {
                        uint32_t endTime = SAFEREAD_U32(d, end, "end_time");
                        uint16_t endTimeMs = SAFEREAD_U16(d, end,
                                "end_time_ms") & 0x03ff;
                    }
                }
            }
        }
        break;

    case 0x000A: // DWD
        if (IS_FIRST_TIME) l.warn("TODO: DWD");
        {
            xml_document doc;
            xml_parse_result result = doc.load_buffer(msg->content.begin(),
                    msg->content.size(), parse_default, encoding_auto);
            xml_node dwd = doc.child("DWD");
            xml_attribute attr;
            for (xml_node node = dwd.child("DistributionWindow"); node;
                    node = node.next_sibling("DistributionWindow")) {
                if (attr = dwd.attribute("appContentLabel")) {
                    uint32_t contLabel = attr.as_uint();
                }
                if (attr = dwd.attribute("startTime")) {
                    string startTime = attr.as_string();
                }
                if (attr = dwd.attribute("endTime")) {
                    string endTime = attr.as_string();
                }
                if (attr = dwd.attribute("lctTSIRef")) {
                    string tsis = attr.as_string();
                }
                for (xml_node n = node.child("AppContextId"); n;
                        n = n.next_sibling("AppContextId")) {
                    string uri = n.child_value();
                    if (attr = n.attribute("dwFilterCode")) {
                        string filters = attr.as_string();
                    }
                }
            }
        }
        break;

    default:
        l.warn("Unsupported contentType:%d", contType);
        break;
    }
}

static const map<uint16_t, MessageHandler> messageHandlers = {
    {0x8100, {"MA3", ma3Handler}},
    {0x0011, {"MP Table", mptHandler}},
    {0x0012, {"MP Table", mptHandler}},
    {0x0013, {"MP Table", mptHandler}},
    {0x0014, {"MP Table", mptHandler}},
    {0x0015, {"MP Table", mptHandler}},
    {0x0016, {"MP Table", mptHandler}},
    {0x0017, {"MP Table", mptHandler}},
    {0x0018, {"MP Table", mptHandler}},
    {0x0019, {"MP Table", mptHandler}},
    {0x001a, {"MP Table", mptHandler}},
    {0x001b, {"MP Table", mptHandler}},
    {0x001c, {"MP Table", mptHandler}},
    {0x001d, {"MP Table", mptHandler}},
    {0x001e, {"MP Table", mptHandler}},
    {0x001f, {"MP Table", mptHandler}},
    {0x0020, {"MP Table", mptHandler}},
    {0x0001, {"MPI", mpiHandler}},
    {0x0002, {"MPI", mpiHandler}},
    {0x0003, {"MPI", mpiHandler}},
    {0x0004, {"MPI", mpiHandler}},
    {0x0005, {"MPI", mpiHandler}},
    {0x0006, {"MPI", mpiHandler}},
    {0x0007, {"MPI", mpiHandler}},
    {0x0008, {"MPI", mpiHandler}},
    {0x0009, {"MPI", mpiHandler}},
    {0x000a, {"MPI", mpiHandler}},
    {0x000b, {"MPI", mpiHandler}},
    {0x000c, {"MPI", mpiHandler}},
    {0x000d, {"MPI", mpiHandler}},
    {0x000e, {"MPI", mpiHandler}},
    {0x000f, {"MPI", mpiHandler}},
    {0x0010, {"MPI", mpiHandler}},
    {0x0000, {"PA", paHandler}},
    {0x0200, {"CRI", criHandler}},
    {0x0201, {"DCI", dciHandler}},
    {0x0202, {"SSWR"}},
    {0x0203, {"AL_FEC"}},
    {0x0204, {"HRBM", hrbmHandler}},
    {0x0205, {"MC"}},
    {0x0206, {"AC"}},
    {0x0207, {"AF"}},
    {0x0208, {"RQF"}},
    {0x0209, {"ADC"}},
    {0x020A, {"HRBMR", hrbmrHandler}},
    {0x020B, {"LS"}},
    {0x020C, {"LR"}},
    {0x020D, {"NAMF"}},
    {0x020E, {"LDC"}},
};

void MmtpServiceTask::SlsReader::sendMessage (const byte_t *data, int sz)
{
    auto self = container_of(this, &MmtpServiceTask::slsReader);

    ScopedLock sl(self->mutex);
    auto &mq = self->messageQueues[self->messageQueueIndex];
    mq.writeI32(sz);
    mq.writeBytes(data, sz);
    task.eventDriver.setTimeout(0, &self->slsReader);
}

void MmtpServiceTask::SlsReader::processEvent (int events)
{
    auto self = container_of(this, &MmtpServiceTask::slsReader);

    // flip message queue to consume from main thread
    Buffer *pmq;
    {
        ScopedLock sl(self->mutex);
        pmq = &self->messageQueues[self->messageQueueIndex];
        self->messageQueueIndex ^= 1;
        assert(self->messageQueues[self->messageQueueIndex].size() == 0);
    }
    auto &mq = *pmq;

    while (mq.size() > 0) {
        int sz = mq.readI32();
        processMessage(mq.begin(), sz);
        mq.drop(sz);
    }
}

void MmtpServiceTask::SlsReader::processMessage (const byte_t *data, int sz)
{
    const byte_t *d = data;
    const byte_t *end = data + sz;

    MessageContext meta;
    meta.messageId = SAFEREAD_U16(d, end, "messageId");
    meta.version = SAFEREAD_U8(d, end, "messageVersion");

    // Check length field matches with actual length.
    uint32_t len;
    if (meta.messageId <= 0x0010 || meta.messageId == 0x8100 ||
            meta.messageId == 0x8101) {
        // if PA, MPI message or MA3 message
        len = SAFEREAD_U32(d, end, "32-bit message length");
    } else {
        len = SAFEREAD_U16(d, end, "16-bit message length");
    }
    if (end - d != len) {
        l.warn("Message(id:%04x) has length:%d expected:%u", meta.messageId,
                end - d, len);
        return;
    }

    // Find message handler for the given message id
    auto msgHandler = messageHandlers.find(meta.messageId);
    if (msgHandler == messageHandlers.end()) {
        l.warn("Unknown message_id:%u", meta.messageId);
        return;
    }
    if (!msgHandler->second.handle) {
        // print warning once per message id
        static map<int16_t, bool> printed;
        bool &p = printed[meta.messageId];
        if (!p) {
            l.warn("Unsupported message %s(%u)",
                    msgHandler->second.name.c_str(), meta.messageId);
            p = true;
        }
        return;
    }

    // check if message was updated
    auto self = container_of(this, &MmtpServiceTask::slsReader);
    if (meta.messageId != 0x8100 && meta.messageId != 0x8101) {

        // calculate checksum
        int crc32 = (int)acba_crc32(0, d, len);

        // Compare with existing message if any
        auto msrv = static_cast<MmtpService*>(self->service.get());
        auto &msg = msrv->messages[meta.messageId];
        if (msg && msg->version == meta.version &&
                msg->attributes["checksum"].asI32() == crc32) {
            return;
        }

        // on receiving newer message.
        msg = new MmtMessage(meta.messageId, meta.version);
        msg->content.writeBytes(d, len);
        msg->attributes["checksum"] = crc32;
    }

    // Process the message
    meta.mmtpServiceTask = self;
    msgHandler->second.handle(d, len, &meta);
}
