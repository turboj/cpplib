#include <boost/beast/core.hpp>
#include <boost/beast/http.hpp>
#include <boost/beast/version.hpp>
#include <boost/asio/connect.hpp>
#include <boost/asio/ip/tcp.hpp>
#include <cstdlib>
#include <functional>
#include <iostream>
#include <memory>
#include <string>
#include <pthread.h>
#include <list>

#include "nlohmann/json.hpp"
#include "datetime.h"
#include "acba.h"

#include "usage_manager.h"

using namespace std;
using namespace acba;
using namespace atsc3;
using json = nlohmann::json;
using tcp = boost::asio::ip::tcp;       // from <boost/asio/ip/tcp.hpp>
namespace http = boost::beast::http;    // from <boost/beast/http.hpp>

static const bool ACTIVATED = false;

// If SEND_REALTIME is false Usage report will be sent once a day
// or Usage report will be sent every time when stop_xxx api is called
const static bool SEND_REALTIME = true;

const static int MIN_INTERVAL = 5;
const static char *host = "192.168.1.6";
const static char *port = "8080";

static Log l("usage");

typedef struct ServiceUsageImpl ServiceUsageImpl;

///////////////////////////////////////////////////////////////////////////////
// UsageManagerImpl
struct UsageManagerImpl : UsageManager {
    
    static std::list<std::pair<int, ServiceUsageImpl*>> usageList;
    
    static void stopService (json* reportInterval, double endTime);
    static void startBroadcast(json* reportInterval, double startTime, 
            float speed, double receiverStartTime);
    static void stopBroadcast (json* reportInterval, double endTime);

    static void startComponent (json* reportInterval, const char *componentID,
            const char *componentType, const char *componentRole,
            const char *componentName, const char *componentLang,
            double startTime);

    static void stopComponent (json* reportInterval, const char *componentID, 
            double endTime);

    static void startSourceDeliveryPath (json* reportInterval, 
            const char *componentID, const char *type, double startTime);

    static void stopSourceDeliveryPath (json* reportInterval, 
            const char *componentID,double endTime);

    static void startApplication (json* reportInterval, const char *appID, 
            double startTime, const char *LifeCycle, const char *Tags);

    static void stopApplication (json* reportInterval, const char *appID, 
            double endTime);

    static void addServiceUsage(int reportID, ServiceUsageImpl* usage);
    static void removeServiceUsage(ServiceUsageImpl* usage);
    static void sendRequest(std::string *target, const char* data, 
            int reportID);
    static void sendResponse(int reportID, const char* data);
    static void print();
};


///////////////////////////////////////////////////////////////////////////////
// ServiceUsage Impl

struct ServiceUsageImpl: ServiceUsage {

    json reportInterval;

    ServiceUsageImpl (json _reportInterval): reportInterval(_reportInterval),
            stopTime(0.0) {}
    // Dummy instance
    ServiceUsageImpl (): ServiceUsageImpl(nullptr) {}

    ~ServiceUsageImpl () {
        if (stopTime == 0.0) stopTime = currentUnixTime();
        stopService(stopTime);
        UsageManagerImpl::removeServiceUsage(this);
    }

    double stopTime;

    virtual void setStopTime (double endTime) {
        stopTime = endTime;
    }

    void startBroadcast (double startTime, float speed,
            double receiverStartTime) override {

        if (!ACTIVATED)
            return;

        if (!receiverStartTime) {
            l.info("One or more of mandatory arguments are missing!!");
            return ;
        }

        UsageManagerImpl::startBroadcast(&reportInterval, 
                startTime, speed, receiverStartTime);
    }

    void stopBroadcast (double endTime) override {
        if (!ACTIVATED)
            return;

        if (!endTime){
            l.info("endTime argument is  missing!!");
            return ;
        }

        UsageManagerImpl::stopBroadcast(&reportInterval, endTime);
    }

    void startComponent (const char *componentID, const char *componentType,
            const char *componentRole, const char *componentName,
            const char *componentLang, double startTime) override {
        if (!ACTIVATED)
            return;

        if (!componentType ||
            !componentRole ||
            !componentID ||
            !startTime){
            l.info("One or more of mandatory arguments are missing!!");
            return ;
        }

        UsageManagerImpl::startComponent(&reportInterval, componentID,
                componentType, componentRole, componentName, 
                componentLang, startTime);
    }

    void stopComponent (const char *componentID, double endTime) override {
        if (!ACTIVATED)
            return;

        if (!componentID ||
            !endTime){
            l.info("One or more of mandatory arguments are missing!!");
            return ;
        }

        UsageManagerImpl::stopComponent(&reportInterval, componentID, endTime);
    }

    void startSourceDeliveryPath (const char *componentID,
            const char *type, double startTime)
            override {
        if (!ACTIVATED)
            return;

        if (!componentID ||
            !type ||
            !startTime){
            l.info("One or more of mandatory arguments are missing!!");
            return ;
        }

        UsageManagerImpl::startSourceDeliveryPath(&reportInterval, componentID,
                type, startTime);
    }

    void stopSourceDeliveryPath (const char *componentID,
            double endTime) override {
        
        if (!ACTIVATED)
            return;

        if (!componentID ||
            !endTime){
            l.info("One or more of mandatory arguments are missing!!");
            return;
        }

        UsageManagerImpl::stopSourceDeliveryPath(&reportInterval, componentID, 
                endTime);
    }

    void startApplication (const char *appID, double startTime,
            const char *LifeCycle, const char *Tags) override {
        if (!ACTIVATED)
            return;

        if (!appID ||
            !startTime ||
            !LifeCycle){
            l.info("One or more of mandatory arguments are missing!!");
            return ;
        }

        UsageManagerImpl::startApplication(&reportInterval, appID, startTime,
                LifeCycle, Tags);
    }

    void stopApplication (const char *appID, double endTime) override {
        if (!ACTIVATED)
            return;

        if (!appID ||
            !endTime) {
            l.info("One or more of mandatory arguments are missing!!");
            return ;
        }

        UsageManagerImpl::stopApplication(&reportInterval, appID, endTime);
    }

protected:

    void stopService (double endTime) {
        if (!ACTIVATED)
            return;
        if (!endTime) {
            l.info("endTime argument is missing!!");
            return ;
        }
    
        UsageManagerImpl::stopService(&reportInterval, endTime);
    }

};

////////////////////////////////////////////////////////////////////////////////
// Usage Manager - Reporter

static Log r("report");

static pthread_mutex_t mutex_t = PTHREAD_MUTEX_INITIALIZER;
static CondVar jobsAvailable;
static boost::asio::io_context ioc;
static int nJobs = 0;

static void *background_run (void *arg)
{
    bool stopped = false;
    ScopedLock sl(mutex_t);

    while (true) {
        if (nJobs == 0) {
            r.info("Waiting for jobs");
            jobsAvailable.wait(&mutex_t);
            continue;
        }
        nJobs = 0;

        ScopedUnlock su(mutex_t);

        r.info("io_context.run()...");
        if (stopped) {
            ioc.restart();
            stopped = false;
        }

        ioc.run();
        stopped = true;
        r.info("io_context.run() returned");
    }

    // never reach here.
    return 0;
}

// Report a failure
void fail (boost::system::error_code ec, char const* what)
{
    r.error("%s: %s", what, ec.message().c_str());
}

// Performs an HTTP GET and prints the response
class http_session: public enable_shared_from_this<http_session>
{
    tcp::resolver resolver_;
    tcp::socket socket_;
    boost::beast::flat_buffer buffer_; // (Must persist between reads)
    http::request<http::string_body> req_;
    http::response<http::string_body> res_;
    int reportID;

public:
    // Resolver and socket require an io_context
    explicit http_session (boost::asio::io_context& ioc): resolver_(ioc),
            socket_(ioc) {}

    // Start the asynchronous operation
    void run (char const* host, char const* port, char const* target,
            char const* data, int reportID) {
        r.info("run %s", host);
        r.info("run %s", port);
        r.info("run %s", target);
        r.info("run %s", data);

        this->reportID = reportID;

        // Set up an HTTP PUT request message
        req_.version(11);
        req_.method(http::verb::put);
        req_.target(target);
        req_.set(http::field::host, host);
        req_.set(http::field::user_agent, BOOST_BEAST_VERSION_STRING);
        req_.set(http::field::content_type, "application/json");
        req_.set(http::field::content_length, strlen(data));
        req_.body() = string(data);
        req_.prepare_payload();

        r.info("run reslover");
        // Look up the domain name
        resolver_.async_resolve(host, port,
                bind(&http_session::on_resolve, shared_from_this(),
                placeholders::_1, placeholders::_2));
    }

    void on_resolve (boost::system::error_code ec,
            tcp::resolver::results_type results) {
        r.info("on_resolve");
        if(ec)
            return fail(ec, "resolve");

        // Make the connection on the IP address we get from a lookup
        boost::asio::async_connect(socket_, results.begin(), results.end(),
                bind(&http_session::on_connect, shared_from_this(),
                placeholders::_1));
    }

    void on_connect (boost::system::error_code ec) {
        r.info("on_connect");
        if(ec)
            return fail(ec, "connect");

        // Send the HTTP request to the remote host
        http::async_write(socket_, req_,
                bind(&http_session::on_write, shared_from_this(),
                placeholders::_1, placeholders::_2));
    }

    void on_write (boost::system::error_code ec, size_t bytes_transferred) {
        r.info("on_write");
        boost::ignore_unused(bytes_transferred);

        if(ec)
            return fail(ec, "write");

        // Receive the HTTP response
        http::async_read(socket_, buffer_, res_,
                bind(&http_session::on_read, shared_from_this(),
                placeholders::_1, placeholders::_2));
    }

    void on_read (boost::system::error_code ec, size_t bytes_transferred) {
        //r.info("on_read");
        boost::ignore_unused(bytes_transferred);

        if(ec)
            return fail(ec, "read");

        // Write the message to standard out
        r.info("response body %s", res_.body().c_str());

        if (reportID != 0)
            UsageManagerImpl::sendResponse(reportID, res_.body().c_str());

        // Gracefully close the socket
        socket_.shutdown(tcp::socket::shutdown_both, ec);

        // not_connected happens sometimes so don't bother reporting it.
        if(ec && ec != boost::system::errc::not_connected)
            return fail(ec, "shutdown");

        // If we get here then the connection is closed gracefully
    }
};

////////////////////////////////////////////////////////////////////////////////
// Usage Manager Impl

static json avServices = json::array();

// device info
static string deviceID, deviceModel, deviceManufacturer, deviceOS,
        peripheralDevice, latitude, longitude, clockSource;

static json* findComponent (json* reportInterval,
        const char* componentID)
{

    int idxC = 0;
    // find component with startTime in the last reportInterval
    for (auto& _c : reportInterval->at("Component")) {
        if (_c["componentID"] == componentID &&
                _c["endTime"] == 0) {
            break;
        }
        idxC++;
    }

    return &(reportInterval->at("Component")[idxC]);
}

static void removeReportIntervalFromService(json* reportInterval)
{
    int idx = 0;
    int idxR = 0;
    bool existService = false;
    for (auto& _s : avServices) {
        if (_s["serviceID"] == reportInterval->at("serviceID")) {
            for (auto& _report : _s["reportInterval"]) {
                if (_report["id"] == reportInterval->at("id")) {
                    existService = true;
                    break;
                }
                idxR++;
            }
            break;
        }
        idx++;
    }

    if (existService) {
        avServices[idx]["reportInterval"].erase(idxR);
        l.info("removed reportInterval from AVServices");
    }
}

static json getCDUHeader ()
{
    static json cduBody = nullptr;
    if (cduBody != nullptr)
        return cduBody;
    json j = {};
    j["CDM"] = {};
    j["CDM"]["protocolVersion"] = "0x00";
    j["CDM"]["DeviceInfo"] = {};
    j["CDM"]["DeviceInfo"]["deviceID"] = deviceID;
    j["CDM"]["DeviceInfo"]["deviceModel"] = deviceModel;
    j["CDM"]["DeviceInfo"]["deviceManufacturer"] = deviceManufacturer;
    j["CDM"]["DeviceInfo"]["peripheralDevice"] = peripheralDevice;
    j["CDM"]["DeviceInfo"]["deviceLocation"] = {};
    j["CDM"]["DeviceInfo"]["deviceLocation"]["latitude"] = latitude;
    j["CDM"]["DeviceInfo"]["deviceLocation"]["longitude"] = longitude;
    j["CDM"]["DeviceInfo"]["clockSource"] = clockSource;

    cduBody = j;
    return cduBody;
}

///////////////////////////////////////////////////////////////////////////////
// UsageManager Impl
void UsageManager::setDeviceInfo (const char *_deviceID,
        const char *_deviceModel, const char *_deviceManufacturer,
        const char *_deviceOS, const char *_peripheralDevice,
        const char *_latitude, const char *_longitude,
        const char *_clockSource)
{
    if (!ACTIVATED) return;

    // Start background ioc runner
    static bool runningPThread = false;
    if (runningPThread == false) {
        runningPThread = true;
        pthread_t t;
        pthread_create(&t, 0, background_run, 0);
        pthread_detach(t);
    }

    deviceID = _deviceID;
    deviceModel = _deviceModel;
    deviceManufacturer = _deviceManufacturer;
    deviceOS = _deviceOS;
    peripheralDevice = _peripheralDevice;
    latitude = _latitude;
    longitude = _longitude;
    clockSource = _clockSource;
}

ServiceUsage *UsageManager::createServiceUsage (const char *country,
        const char *bsid, const char *_serviceID, const char *globalServiceID,
        const char *serviceType, double startTime,
        const char *DestinationDeviceType, const char *contentType,
        const char *contentCid)
{
    if (!ACTIVATED) return new ServiceUsageImpl();

    if (!country || !bsid || !_serviceID || !globalServiceID || !serviceType ||
            !startTime || !DestinationDeviceType) {
        l.info("One or more of mandatory arguments are missing!!");
        return nullptr;
    }

    string serviceID = _serviceID;
    json s = nullptr;
    // Find AVService from avServices
    int idx = 0;
    int sizeService = avServices.size();
    bool needToAdd = true;
    for (auto& _s : avServices) {
        //l.info("services %s \n", _s.dump(4).c_str());
        if (_s["serviceID"] == serviceID) {
            needToAdd = false;
            s = _s;
            break;
        }
        idx++;
    }
    // create reportInterval
    json reportInterval = {};
    reportInterval["startTime"] = startTime;
    reportInterval["DestinationDeviceType"] = DestinationDeviceType;
    reportInterval["ContentID"] = {};
    reportInterval["ContentID"]["type"] = contentType;
    reportInterval["ContentID"]["cid"] = contentCid;
    reportInterval["UsageID"] = nullptr;  // dummy 
    reportInterval["serviceID"] = serviceID; // dummy
    reportInterval["endTime"] = nullptr;
    reportInterval["broadcastInterval"] = json::array();
    reportInterval["Component"] = json::array();

    // add Identifire
    static int identifierForSelectedService = 0;
    reportInterval["id"] = ++identifierForSelectedService; // dummy
    int reportID = identifierForSelectedService;

    // if it does not exist, create new AVService
    if (needToAdd) {
        s = {};
        s["country"] = country;
        s["bsid"] = bsid;
        s["serviceID"] = serviceID;
        s["globalServiceID"] = globalServiceID;
        s["serviceType"] = serviceType;
        s["reportInterval"] = json::array();
        s["reportInterval"][0] = reportInterval;
        idx = sizeService;
    } else {
        avServices[idx]["reportInterval"].push_back(reportInterval);
    }

    if (SEND_REALTIME) {

        json sendData = getCDUHeader();
        sendData["CDM"]["AVService"] = json::array();
        sendData["CDM"]["AVService"][0] = s;
        sendData["CDM"]["AVService"][0]["reportInterval"] = json::array();
        sendData["CDM"]["AVService"][0]["reportInterval"][0] = reportInterval;

        // remove id from data to be stored into Database
        sendData["CDM"]["AVService"][0]["reportInterval"][0].erase("id");
        sendData["CDM"]["AVService"][0]["reportInterval"][0].erase("UsageID");
        sendData["CDM"]["AVService"][0]["reportInterval"][0].erase("ServiceID");
        
        string target = "/service/start";
        l.info("select_service target: %s", target.c_str());
        l.info("select_service data %s \n", sendData.dump().c_str());

        UsageManagerImpl::sendRequest(&target, sendData.dump().c_str(), reportID);
    }

    if (needToAdd) avServices[idx] = s;

    l.info("total avServices %s \n", avServices.dump().c_str());

    int sizeR = avServices[idx]["reportInterval"].size();
    ServiceUsageImpl* impl = new ServiceUsageImpl((avServices[idx]
            ["reportInterval"][sizeR-1]));
    UsageManagerImpl::addServiceUsage(reportID, impl);

    return impl;
}

///////////////////////////////////////////////////////////////////////////////
// UsageManagerImpl impl
std::list<std::pair<int, ServiceUsageImpl*>> UsageManagerImpl::usageList; 

void UsageManagerImpl::stopService (json* reportInterval, double endTime) {

    reportInterval->at("endTime") = endTime;

    double startTime = reportInterval->at("startTime");
    // If interval is less than 5 sec, don't send usage report and delete it.
    if ((endTime - startTime) < MIN_INTERVAL) {
        l.info("stop_service: interval is less than %d ms", MIN_INTERVAL);
        removeReportIntervalFromService(reportInterval);

        if (SEND_REALTIME) {
            // TODO remove from database
        }

        return;
    }

    // update endTime of the last reportInterval
    //avServices[idx]["reportInterval"][idxR]["endTime"] = endTime;

    if (SEND_REALTIME) {

        json sendData = {};
        sendData["reportInterval"] = json::array();
        sendData["reportInterval"][0] = {};
        sendData["reportInterval"][0]["startTime"] = startTime;
        sendData["reportInterval"][0]["endTime"] = endTime;

        if (reportInterval->at("UsageID") != nullptr) {
            string _id = reportInterval->at("UsageID");
            string target = "/service/stop/id/" + _id;

            l.info("stop_service target: %s", target.c_str());
            l.info("stop_service data %s \n", sendData.dump().c_str());

            // delete reportInterval;
            removeReportIntervalFromService(reportInterval);

            UsageManagerImpl::sendRequest(&target, 
                    sendData.dump().c_str(), 0);
        }
    }
    l.info("stop_service %s \n", avServices.dump().c_str());
}

void UsageManagerImpl::startBroadcast(json* reportInterval, double startTime,
        float speed, double receiverStartTime) {

    // create broadcastInterval
    json b = {};
    b["broadcastStartTime"] = startTime;
    b["broadcastEndTime"] = nullptr;
    b["speed"] = speed;
    b["receiverStartTime"] = receiverStartTime;

    // add boradcastInterval in the last repportInterval
    reportInterval->at("broadcastInterval").push_back(b);

    l.info("start_broadcast %s \n", 
        reportInterval->at("broadcastInterval").dump().c_str());

}

void UsageManagerImpl::stopBroadcast (json* reportInterval, double endTime) {

    // find the last broadcastInterval in the last reportInterval
    int sizeB = reportInterval->at("broadcastInterval").size();
    reportInterval->at("broadcastInterval")[sizeB-1]
        ["broadcastEndTime"] = endTime;

    if (SEND_REALTIME) {
        double startTime = reportInterval->at("startTime");
        json s = {};
        s["reportInterval"] = json::array();
        s["reportInterval"][0] = {};
        s["reportInterval"][0]["startTime"] = startTime;
        s["reportInterval"][0]["broadcastInterval"] = json::array();
        s["reportInterval"][0]["broadcastInterval"][0] =
                reportInterval->at("broadcastInterval")[sizeB-1];

        if (reportInterval->at("UsageID") != nullptr) {
            string id = reportInterval->at("UsageID");
            string target = "/service/broadcastinterval/id/"+id;
            l.info("stop_broadcast url: %s", target.c_str());
            l.info("stop_broadcast data: %s", s.dump().c_str());

            UsageManagerImpl::sendRequest(&target, s.dump().c_str(), 0);

        }
    }

    l.info("stop_broadcast %s \n",
            reportInterval->at("broadcastInterval").dump().c_str());

}

void UsageManagerImpl::startComponent (json* reportInterval, 
        const char *componentID, const char *componentType, 
        const char *componentRole, const char *componentName, 
        const char *componentLang, double startTime) {

    // create component
    json b = {};
    b["componentType"] = componentType;
    b["componentRole"] = componentRole;
    b["componentName"] = componentName;
    b["componentID"] = componentID;
    b["componentLang"] = componentLang;
    b["startTime"] = startTime;
    b["endTime"] = 0;
    b["SourceDeliveryPath"] = json::array();

    // add Component to the last reportInterval
    reportInterval->at("Component").push_back(b);

    if (SEND_REALTIME) {
        double startTime = reportInterval->at("startTime");
        json s = {};
        s["reportInterval"] = json::array();
        s["reportInterval"][0] = {};
        s["reportInterval"][0]["startTime"] = startTime;
        s["reportInterval"][0]["Component"] = json::array();
        s["reportInterval"][0]["Component"][0] = b;

        if (reportInterval->at("UsageID") != nullptr) {
            string id = reportInterval->at("UsageID");
            string target = "/service/component/start/id/"+id;
            l.info("start_component target: %s", target.c_str());
            l.info("start_componentt data: %s", s.dump().c_str());

            UsageManagerImpl::sendRequest(&target, s.dump().c_str(), 0);
        }
    }

    l.info("start_component %s \n", reportInterval->at("Component").dump().c_str());


}

void UsageManagerImpl::stopComponent (json* reportInterval, 
        const char *componentID, double endTime) {

    int idxC = 0;
    // find Component in the last reportInterval
    for (auto& _c : reportInterval->at("Component")) {
        if (_c["componentID"] == componentID && _c["endTime"] == 0) {
            break;
        }
        idxC++;
    }

    reportInterval->at("Component")[idxC]["endTime"] = endTime;

    if (SEND_REALTIME) {
        double startTime = reportInterval->at("startTime");
        json s = {};
        s["reportInterval"] = json::array();
        s["reportInterval"][0] = {};
        s["reportInterval"][0]["startTime"] = startTime;
        s["reportInterval"][0]["Component"] = json::array();
        s["reportInterval"][0]["Component"][0] =
            reportInterval->at("Component")[idxC];

        if (reportInterval->at("UsageID") != nullptr) {
            string id = reportInterval->at("UsageID");
            string target = "/service/component/stop/id/"+id;
            l.info("stop_component target: %s", target.c_str());
            l.info("stop_componentt data: %s", s.dump().c_str());

            UsageManagerImpl::sendRequest(&target, s.dump().c_str(), 0);

        }
    }

    l.info("stop_componet %s \n",
        reportInterval->at("Component").dump().c_str());


}

void UsageManagerImpl::startSourceDeliveryPath (json* reportInterval, 
        const char *componentID, const char *type, 
        double startTime) {
    // find component
    json* component = findComponent(reportInterval,
        componentID);

    if (!component) {
        l.info("no component");
        return;
    }

    // create SourceDeliveryPath
    json b = {};
    b["type"] = type;
    b["startTime"] = startTime;

    component->at("SourceDeliveryPath").push_back(b);

    l.info("start_sourceDeliveryPath %s \n",
        component->at("SourceDeliveryPath").dump().c_str());

}

void UsageManagerImpl::stopSourceDeliveryPath (json* reportInterval,
        const char *componentID,
        double endTime) {
    
    // find component
    json* component = findComponent(reportInterval, componentID);

    if (!component) {
        l.info("no component");
        return;
    }

    int sizeS = component->at("SourceDeliveryPath").size();
    component->at("SourceDeliveryPath")[sizeS-1]["endTime"] = endTime;

    if (SEND_REALTIME) {
        double startTime = reportInterval->at("startTime");
        json s = {};
        s["reportInterval"] = json::array();
        s["reportInterval"][0] = {};
        s["reportInterval"][0]["startTime"] = startTime;
        s["reportInterval"][0]["Component"] = json::array();
        s["reportInterval"][0]["Component"][0] = {};
        s["reportInterval"][0]["Component"][0]["componentID"] =
            componentID;
        s["reportInterval"][0]["Component"][0]["startTime"] = 
            component->at("startTime");
        s["reportInterval"][0]["Component"][0]["SourceDeliveryPath"] =
            json::array();
        s["reportInterval"][0]["Component"][0]["SourceDeliveryPath"][0] =
            component->at("SourceDeliveryPath")[sizeS-1];

        if (reportInterval->at("UsageID") != nullptr) {
            string id = reportInterval->at("UsageID");
            string target = "/service/component/sourcedeliverypath/id/"+id;
            l.info("stop_sourceDeliveryPath target: %s", target.c_str());
            l.info("stop_sourceDeliveryPath data: %s", s.dump().c_str());

            UsageManagerImpl::sendRequest(&target, s.dump().c_str(), 0);

        }
    }

    l.info("stop_sourceDeliveryPath %s \n",
                component->at("SourceDeliveryPath").dump().c_str());

}

void UsageManagerImpl::startApplication (json* reportInterval, 
        const char *appID, double startTime,
        const char *LifeCycle, const char *Tags) {

    // create appInterval
    json b = {};
    b["appID"] = appID;
    b["startTime"] = startTime;
    b["endTime"] = 0;
    b["LifeCycle"] = LifeCycle;
    b["Tags"] = Tags;

    try {
        l.info("start_app AppInterval %s",
            reportInterval->at("AppInterval").dump().c_str());
    } catch (exception &e) {
        l.info("start_app not exist AppInterval");
        reportInterval->emplace("AppInterval", json::array());
    }

    // add to last reportInterval
    reportInterval->at("AppInterval").push_back(b);

    l.info("start_app %s \n", 
        reportInterval->at("AppInterval").dump().c_str());


}

void UsageManagerImpl::stopApplication (json* reportInterval, 
        const char *appID, double endTime) {

    int sizeB = reportInterval->at("AppInterval").size();
    reportInterval->at("AppInterval")[sizeB-1]
        ["endTime"] = endTime;

    if (SEND_REALTIME) {
        double startTime = reportInterval->at("startTime");
        json s = {};
        s["reportInterval"] = json::array();
        s["reportInterval"][0] = {};
        s["reportInterval"][0]["startTime"] = startTime;
        s["reportInterval"][0]["AppInterval"] = json::array();
        s["reportInterval"][0]["AppInterval"][0] =
            reportInterval->at("AppInterval")[sizeB-1];

        if (reportInterval->at("UsageID") != nullptr) {
            string id = reportInterval->at("UsageID");
            string target = "/service/appinterval/id/"+id;
            l.info("stop_app target: %s", target.c_str());
            l.info("stop_app data: %s", s.dump().c_str());
            UsageManagerImpl::sendRequest(&target, s.dump().c_str(), 0);
        }
    }

    l.info("stop_app %s \n",
        reportInterval->at("AppInterval").dump().c_str());


}


void UsageManagerImpl::print () {
    l.info("usage %s", avServices.dump(2).c_str());
}


void UsageManagerImpl::addServiceUsage(int reportID, ServiceUsageImpl* usage) {
    usageList.push_back(make_pair(reportID, usage));
};

void UsageManagerImpl::removeServiceUsage(ServiceUsageImpl* usage) {
    auto it = usageList.begin();
    while (it != usageList.end()) {
        if (it->second == usage) {
            r.info("UsageManagerImpl::removeServiceUsage");
            usageList.erase(it);
            break;
        }
        it++;
    }
}

void UsageManagerImpl::sendResponse(int reportID, const char* data) {
    r.info("sendResponse reportID ");
    
    for (auto& el : usageList) {
        //r.info(" reportID %d", reportID);
        //r.info(" el.first %d", el.first);
        if (reportID == el.first) {
            json req = json::parse(data);

            if (req != nullptr && req["result"]["id"] != nullptr) {
                el.second->reportInterval.at("UsageID") = req["result"]["id"];
                r.info("sendResonse %s", el.second->reportInterval.dump().c_str());
                //el.second->at("UsageID") = req["result"]["id"];
                //r.info("sendResponse %s", el.second->dump().c_str());
            }
            
            break;
        }
    }
};

void UsageManagerImpl::sendRequest(string* target, const char* data, 
        int reportID) {
    
    make_shared<http_session>(ioc)->
        run(host, port, target->c_str(), data, reportID);

    // notify to background runner
    {
        ScopedLock sl(mutex_t);
        nJobs++;
        jobsAvailable.signal();
    }
    r.info("sendRequest");

};


