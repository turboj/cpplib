#ifndef CD_MANAGER_H
#define CD_MANAGER_H

#include "acba.h"
#include "atsc3_task.h"
#include "nlohmann/json.hpp"
#include "atsc3_config.h"

#define CD_DESC_XML "/Description.xml"
#define CD_WEB_CONTEXT_ROOT "/companion"
#define CD_WEB_ENDPOINT "/ATSC"
#define CD_REMOTE_WS_ENDPOINT "/app2appremote"
#define CD_LOCAL_WS_ENDPOINT "/app2applocal"

// Companion API path
#define ATSC_CD "/atscCD"

// NEW A338
#define JSON_FUNC_org_atsc_query_companionDevices "org.atsc.query.companionDevices"
#define JSON_FUNC_org_atsc_cdApp_launch "org.atsc.cdApp.launch"

namespace atsc3 {

// It represents a companion device.
struct LauncherInfo {
    // URL of description.xml of launcher
    std::string xmlUrl;
    // Unique ID of launcher. It doesn't represent a device but an app.
    std::string uuid;
    // Used by Companion API.
    // It's renewed whenever the launcher rejoin to the same network with PD
    int launcherId;
    // Values from a launcher
    std::string applicationUrl; // url to send launch POST request
    std::string friendlyName; // optional
    std::string csOsId; // mandatory
    // flag whether or not all of mandatory data is acquired.
    bool valid;
    // the number of count discovered.
    int myDiscoveryCount;
    LauncherInfo (std::string _xmlUrl, std::string _uuid): xmlUrl(_xmlUrl),
            uuid(_uuid), launcherId(-1), valid(false), myDiscoveryCount(-1),
            applicationUrl(""), friendlyName(""), csOsId("") {}
    // ALTICAST launcher has to include the next specific information.
    // 1. Application-URL (Standard Protocol defined in ATSC3.0)
    //     -> HTTP POST URL to send launchCSApp request.
    // 2. Friendly-Name (Proprietary Protocol defined by Alticast)
    //     -> optional info
    // 3. CS-OS-ID (Proprietary Protocol defined by Alticast)
    //     -> OS info of Companion Device such as Android/iOS
    void getLauncherPropertiesFromCd();
    void printInfo();
};

struct Url {
    std::string protocol;
    std::string domain;
    std::string port;
    std::string path;
    std::string query;
};

class CdManager {
private:
    static CdManager *instance;
    CdManager() : serverStarted(false)
        , connected_launchers_mutex(PTHREAD_MUTEX_INITIALIZER) {
    };
    ~CdManager(){
    }
    bool serverStarted;

public:
    static CdManager* getInstance();
    pthread_mutex_t connected_launchers_mutex;
    static uint64_t currentTimeMillis();
    static uint64_t currentTimeNanos();
    void start();
    std::string getUpnpDescriptionXml();
    std::string getAppToAppUrlXml();
    std::string getWebEndPoint();
    set<LauncherInfo *>& getConnectedLaunchers ();
    std::string getLocalWsEndPoint();
    std::string getRemoteWsEndPoint();
    static Url parse_url(const std::string& raw_url);
    std::string doSyncHttp(std::string method, std::string host,
            std::string port, std::string path, std::string body);
    static std::string trim(const std::string& str);
    static std::string getHeaderValue(std::string &str);

    static int findStrIc(const std::string & strHaystack,
            const std::string & strNeedle);
};

int getIpV4Addresses (std::vector<uint32_t> &addrs);

}
#endif
