#ifndef RATING_HANDLER_H
#define RATING_HANDLER_H

#include <string>

#include "acba.h"
#include "EventDriver.h"

namespace atsc3 {

struct RatingHandler {

    RatingHandler (): blocked(false) {}

    acba::Update<bool> blockedByRatingUpdate;

    bool blocked;
    std::string routeMpdRatingOfCurrentService;

    void retrieveContentAdvisoryRating (const acba::Ref<Service> &srv, std::string &rating);

    struct ServiceChangeListener: acba::Reactor, acba::Listener<> {
        void onUpdate () override;
        void processEvent (int evt) override;
    } serviceChangeListener;

    struct EsgUpdateListener: acba::Reactor,
        acba::Listener<ESG_FRAGMENT_EVENT, const std::string&,
        const acba::Ref<EsgFragmentSlide>&, const acba::Ref<BroadcastStream>&> {

        void onUpdate (ESG_FRAGMENT_EVENT evt, const std::string &fid,
            const acba::Ref<EsgFragmentSlide>& fs,
            const acba::Ref<BroadcastStream>& bs) override;
        void processEvent (int evt) override;
    } esgUpdateListener;

    struct PreferenceChangeListener: acba::Listener<const std::string&> {
        void onUpdate (const std::string &key) override;
    } preferenceChangeListener;

    struct RMPPeriodChangeListener: acba::Reactor, acba::Listener<const std::string&> {
        void onUpdate (const std::string&) override;
        void processEvent (int evt) override;
    } rmpPeriodChangeListener;
};

}
#endif
