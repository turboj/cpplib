$(call begin-target, atsc3_task, staticlib)
    LOCAL_SRCS := $(wildcard $(LOCAL_PATH)/*.cpp)
    LOCAL_IMPORTS := boost-staticlib json-nlohmann dnld-staticlib dash-staticlib
    LOCAL_IMPORTS += atsc3_model-staticlib atsc3_platform_interface
    ifeq ($(USE_TUNER_SERVER),true)
        LOCAL_IMPORTS += tuner_server-staticlib
    endif
    ifeq ($(USE_TUNER_CLIENT),true)
        LOCAL_IMPORTS += tuner_client-staticlib
    endif

    EXPORT_CXXFLAGS := -I$(LOCAL_PATH)
    EXPORT_IMPORTS := $(LOCAL_IMPORTS)
    EXPORT_LDFLAGS := -lz
$(end-target)
