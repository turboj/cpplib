#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <fcntl.h>
#include <errno.h>
#include <string.h>
#include <time.h>

#include <string>
#include <algorithm>
#include <map>
#include <thread>
#include <functional>

#include "stream_reader.h"
#include "pcap_reader.h"
#include "stltp_reader.h"
#include "ctp_reader.h"
#include "alp_reader.h"
#include "ethernet_reader.h"
#include "ip_reader.h"
#include "udp_reader.h"
#include "atsc3_task.h"
#include "tune_manager.h"
#include "EventDriver.h"
#include "acba.h"

using namespace std;
using namespace acba;
using namespace atsc3;

static Log l("pcaptune");

// RF Stream configuration
//
//  pcap -> ethernet -> ip -> udp -> consumer
//   |                  ^
//   +------------------+
//   |                  |
//   +------>  alp   ---+
//
// STLTP Configuration
//
//  pcap -> ethernet -> ctpIp -> ctpUdp -> ctp -> stltp -> alp -> ip -> ...
//   |                   ^
//   +-------------------+
struct PcapTune: Tune {

    Buffer buffer;
    volatile bool stopped;

    TuneContext tuneContext;
    PcapReader pcapReader;
    AlpReader alpReader;
    EthernetReader ethernetReader;
    IpReader ipReader;
    UdpReader udpReader;
    StreamReaderRef consumer;

    // STLTP Support
    IpReader ctpIpReader; // for ip converying ctp packet
    UdpReader ctpUdpReader; // for udp converying ctp packet
    CtpReader ctpReader;
    StltpReader stltpReader;

    Log l;

    PcapTune (const string &loc, const StreamReaderRef &cons): Tune(loc),
            l("pcaptune", loc+" "), pcapReader(), alpReader(),
            ethernetReader(&ipReader), ipReader(&udpReader),
            udpReader(cons.get()), stopped(false), consumer(cons),
            ctpIpReader(&ctpUdpReader), ctpUdpReader(&ctpReader),
            ctpReader(&stltpReader), stltpReader(&alpReader) {
        tuneContext.tune = this;
        pcapReader.setStreamReader(/*LINKTYPE_ETHERNET*/1, &ethernetReader);
        pcapReader.setStreamReader(/*LINKTYPE_IPV4*/228, &ipReader);
        pcapReader.setStreamReader(/*ALP(private)*/999, &alpReader);
        alpReader.setStreamReader(/*IP*/0, &ipReader);

        // Re-configure to read STLTP stream.
        if (strcasestr(loc.c_str(), "stltp")) {
            l.info("Opening as STLTP stream");
            ethernetReader.upper = &ctpIpReader;
            pcapReader.setStreamReader(/*LINKTYPE_IPV4*/228, &ctpIpReader);
        }
    }

    void run () {
        int fd = open(location.substr(5).c_str(), O_RDONLY);
        if (fd < 0) {
            notify(EVENT_TUNER_UNLOCKED);
            l.error("Failed to open the file");
            return;
        }
        notify(EVENT_TUNER_LOCKED);
        l.info("file open");

        while (!stopped) read(fd);

        close(fd);
        notify(EVENT_TUNER_UNLOCKED); // nop. No listener.
        l.info("file closed");
        deref();
    }

    void start () {
        ref();
        thread t(&PcapTune::run, this);
        t.detach();
    }

    void stop () {
        stopped = true;
    }

    static const int READ_SIZE = 64 * 1024; // must be bigger than 2 packets
    void read (int fd) {
        ssize_t sz;

        while (true) {
            sz = pcapReader.read(buffer.begin(), buffer.size(), &tuneContext);
            if (sz <= 0) break;
            buffer.drop(sz);
        }
        if (sz < 0) buffer.ensureMargin(-sz);

        l.trace("read %d", sz);

        if (buffer.size() >= READ_SIZE / 2) return;

        buffer.ensureMargin(READ_SIZE);
        sz = ::read(fd, buffer.end(), READ_SIZE);
        if (sz > 0) {
            buffer.grow(sz);
        } else if (sz == 0) {
            // TODO: Enhance ad-hoc stream rewinding
            l.info("Rewinding......");
            buffer.clear();
            pcapReader.reset();
            lseek(fd, 0, SEEK_SET);

            task.eventDriver.setTimeout(0, &reselector);
        } else { // sz < 0
            l.error("Disconnecting %s. %s", location.c_str(), strerror(errno));
            abort();
        }
    }

    struct Reselector: Reactor {
        void processEvent (int events) override {
            auto *self = container_of(this, &PcapTune::reselector);
            for (auto bs: model.broadcastStreams) {
                if (bs.second->location == self->location) {
                    task.serviceManager.reselectServices(*bs.second);
                    break;
                }
            }
        }
    } reselector;
};

struct PcapTuneProvider: TuneProvider {
    PcapTuneProvider (): TuneProvider("pcap") {}

    Tune *create (const std::string &loc, const StreamReaderRef &cons)
            override {
        auto pt = new PcapTune(loc, cons);
        pt->start();
        return pt;
    }

    void destroy (const acba::Ref<Tune> &t) override {
        static_cast<PcapTune*>(t.get())->stop();
    }
};

void PcapTuneProvider_init ()
{
    task.tuneManager.registerTuneProvider(new PcapTuneProvider());
}
