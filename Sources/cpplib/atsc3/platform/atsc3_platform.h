#ifndef ATSC3_PLATFORM_H
#define ATSC3_PLATFORM_H

#ifdef __cplusplus
extern "C" {
#endif

typedef struct atsc3_Platform atsc3_Platform;
typedef struct atsc3_WebWindow atsc3_WebWindow;
typedef struct atsc3_Tuner atsc3_Tuner;
typedef struct atsc3_TunerStatus atsc3_TunerStatus;

enum atsc3_CALLBACK_TYPE {
    atsc3_CALLBACK_END, // notify that callback will be no longer called.
    atsc3_CALLBACK_TUNER_LOCKED,
    atsc3_CALLBACK_TUNER_UNLOCKED,
    atsc3_CALLBACK_IP,
    atsc3_CALLBACK_LMT,
};

struct atsc3_TuneParams {
    int frequency;
    void (*callback) (atsc3_CALLBACK_TYPE type, const byte_t *data, int sz,
            void *ctx);
    void *context;
};

struct atsc3_RMPMediaTime {
    double startDateTimeMs; // wall-clock time ms
    double currentTime; // in seconds
};

#define atsc3_KEYFLAG_SYSTEM_ONLY (1<<0)
#define atsc3_KEYFLAG_APPLICATION_DEFAULT (1<<1)
struct atsc3_KeyInfo {
    const char *name;
    /**
     * Key code corresponding the name.
     * Used on deviceInput message construction.
     * Put -1 if not applicable.
     */
    int32_t code;
    uint32_t flags;
};

typedef struct atsc3_MulticastAddress atsc3_MulticastAddress;
struct atsc3_MulticastAddress {
    uint32_t sourceIp, destinationIp;
    uint16_t /*unused*/sourcePort, destinationPort;
};

typedef struct atsc3_DeviceInfo atsc3_DeviceInfo;
struct atsc3_DeviceInfo {
    const char *deviceMake;
    const char *deviceModel;
    const char *deviceId;
    const char *advertisingId;
};

struct atsc3_Platform {
    atsc3_DeviceInfo deviceInfo;
    atsc3_WebWindow *(*createWebWindow) (const char *url, int zIndex);
    void (*destroyWebWindow) (atsc3_WebWindow *ww);

    void (*rmpStart) (const char *url);
    void (*rmpStop) ();

    /**
     * Returns list of key information.
     */
    const atsc3_KeyInfo *(*getSupportedKeys) (int *num);

    /**
     * Binds given keys to the specified web window, or release keys in case
     * @arg ww is nullptr.
     * It's guaranteed that all keys are released before destroying the web
     * window.
     */
    void (*routeKeys) (int *keys, int cnt, atsc3_WebWindow *ww);

    /**
     * Each tuner has its dedicated thread which calls callbacks.
     * Multiple tuner may share a single thread.
     * but single tuner cannot span to multiple threads.
     * tuners: [out] Array of atsc3_Tuner to store
     * nTuners: [in/out] Give size of @arg tuners and then get number of tuners
     *          set.
     */
    void (*getTuners) (atsc3_Tuner **tuners, int *nTuners);
    void (*getTunerStatus) (atsc3_Tuner *tuner, atsc3_TunerStatus *st);
    bool (*startTune) (atsc3_Tuner *tuner, atsc3_TuneParams *tuneParams);
    void (*stopTune) (atsc3_Tuner *tuner);

    /**
     * Infom every multicast addresses receiver is listening to.
     * Tuner may ignore addresses which is out of scoped of received LMT.
     */
    void (*setMulticasts) (atsc3_Tuner *tuner,
            const atsc3_MulticastAddress *addrs, int numAddrs);

    void (*rmpSetScalePosition) (double scale, double xPos, double yPos);
    void (*rmpGetRMPMediaTime) (atsc3_RMPMediaTime *rmpMediaTime);
    /**
     * 1.0 if the playback speed is normal
     */
    double (*rmpGetPlaybackRate) ();
    void (*rmpReceivedLicenseResponse) (const char *systemId,  const char *message);
    void (*rmpCacheLicense) ( const char *systemId, unsigned char* buf, int size);
    /**
     * ccDisplayPrefs: According to A344 9.2.5 "org.atsc.query.captionDisplay"
     *                 it looks like
     *                {"cta708": {
     *                      "characterColor": "#F00000",
     *                      "characterOpacity": 0.5,
     *                      "characterSize": 80,
     *                      "fontStyle": "MonospacedNoSerifs",
     *                      "backgroundColor": "#808080",
     *                      "backgroundOpacity": 0,
     *                      "characterEdge": "None",
     *                      "characterEdgeColor": "#000000",
     *                      "windowColor": "#000000", "windowOpacity": 0
     *                 },
     */
    void (*rmpSetPreference) (bool ccEnabled, const char *ccLang,
            const char *ccDisplayPrefs, const char *audioLang,
            bool videoDescEnabled, const char *videoDescLang);
    const char* (*rmpGetTracks) ();
    bool (*rmpSelectTrack) (int selectionId);
    bool (*rmpSelectTrackExt) (bool select, const char* ext);
    double (*rmpAudioVolume) (double volume);
};

#ifdef __cplusplus
}
#endif
#endif
