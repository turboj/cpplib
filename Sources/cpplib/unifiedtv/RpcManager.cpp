#include <assert.h>
#include <set>
#include <string>
#include <sstream>
#include <algorithm>

#include "acba.h"
#include "unifiedtv.h"
#include "nlohmann/json.hpp"
#include "JsonRef.h"

using namespace std;
using namespace acba;
using namespace unifiedtv;
using json = nlohmann::json;

static Log l("unifiedtv", "rpc: ");

namespace {

struct RpcServer: HttpRequestHandler {

    RpcServer (RpcManager *m): manager(*m) {}

    Ref<RpcPeer> peer;
    RpcManager &manager;

    void handleWebsocketOpen (HttpReactor &r) override {

        l.info("open %p", this);
        for (auto h: r.headers) {
            l.info("    %s: %s", h.first.c_str(), h.second.c_str());
        }
        assert(!peer);

        peer = new RpcPeer(this, &r, manager);
    }

    void handleWebsocketTextMessage (HttpReactor &r) override {
        l.info("text %p", this);
        json req = json::parse({r.contentBuffer.begin(), r.contentBuffer.end()},
                nullptr, false);
        if (req.is_discarded()) {
            string str((char*)r.contentBuffer.begin(), r.contentBuffer.size());
            l.error("Invalid json format %s", str.c_str());
            r.contentBuffer.dump();
            return; // ignore request
        }
        l.info("msg: %s", req.dump(4).c_str());

        JsonRef jref = req;

        // TODO: send error
        if (jref["jsonrpc"].asString() != "2.0") return;
        if (!jref["method"].isString()) return;

        auto &m = jref["method"].asString();

        auto rhi = manager.rpcHandlers.find(m);
        if (rhi == manager.rpcHandlers.end()) {
            l.warn("couldn't find rpc handler for %s", m.c_str());
            peer->sendMessage({
                {"jsonrpc", "2.0"},
                {"error", {
                    {"code", /* Method not found */ -32601},
                    {"message", "Unknown method"}
                }},
                {"id", req["id"]}
            });
            return;
        }

        rhi->second(req, peer);
    }

    void handleWebsocketClose (HttpReactor &r) override {
        l.info("close %p", &r);
        if (!peer) return;

        peer->dispose();
        peer = nullptr;
    }
};

}

HttpRequestHandler *RpcManager::acceptHttpRequest (const string &basePath,
        HttpReactor *r)
{
    if (!r->websocketVersion) return 0;
    return new RpcServer(this);
}

void RpcManager::registerHandler (const string &name, RpcHandler h)
{
    // make sure handlers are not conflicting.
    auto hi = rpcHandlers.find(name);
    assert(hi == rpcHandlers.end());

    Log l("unifiedtv", "rpc: "); // Static l may not be initialized this time.
    l.debug("registered handler for method %s", name.c_str());
    rpcHandlers[name] = h;
}

void RpcManager::registerNotificationHandler (const string &name,
        RpcNotificationHandler *h)
{
    // make sure handlers are not conflicting.
    auto hi = notificationHandlers.find(name);
    assert(hi == notificationHandlers.end());

    Log l("unifiedtv", "rpc: "); // Static l may not be initialized this time.
    l.debug("registered notification handler for message type %s", name.c_str());
    notificationHandlers[name] = h;
}
