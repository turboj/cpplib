LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)

LOCAL_MODULE := unifiedtv

LOCAL_SRC_FILES += $(wildcard $(LOCAL_PATH)/*.cpp)
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../json/single_include
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../json/acba
LOCAL_STATIC_LIBRARIES := acba

LOCAL_EXPORT_C_INCLUDES := $(LOCAL_PATH)

include $(BUILD_STATIC_LIBRARY)
