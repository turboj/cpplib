#include <assert.h>
#include <set>
#include <string>
#include <sstream>
#include <algorithm>

#include "unifiedtv.h"
#include "acba.h"
#include "nlohmann/json.hpp"

using namespace std;
using namespace acba;
using namespace unifiedtv;
using json = nlohmann::json;

static Log l("unifiedtv" "rpcpeer: ");

bool RpcPeer::sendMessage (const json &msg) {
    if (!webSocket) return false;

    try {
        webSocket->sendFrame(msg.dump(4));
    } catch (...) {
        l.error("Failed to deserialize json message (not utf-8 encoded)");
        return false;
    }
    return true;
}

bool RpcPeer::subscribe (const string &evt, const json &opt)
{
    auto &nhs = manager.notificationHandlers;
    auto nhi = nhs.find(evt);
    if (nhi == nhs.end()) {
        l.warn("Unknown notificaton type %s", evt.c_str());
        return false;
    }

    auto &nh = nhi->second;
    auto ok = nh->subscribe(evt, opt, this);
    if (ok) subscriptions.insert(evt);
    return ok;
}

bool RpcPeer::unsubscribe (const string &evt, const json &opt)
{
    if (subscriptions.find(evt) == subscriptions.end()) {
        l.info("Never subscribed for %s", evt.c_str());
        return false;
    }

    auto &nhs = manager.notificationHandlers;
    auto nhi = nhs.find(evt);
    assert(nhi != nhs.end());
    auto &nh = nhi->second;

    auto ok = nh->unsubscribe(evt, opt, this);
    if (ok) subscriptions.erase(evt);
    return ok;
}

void RpcPeer::dispose ()
{
    if (!webSocket) return;

    // flush subscriptions
    auto ss = subscriptions;
    for (auto s: ss) {
        l.info("Unsubscribing %s on disconnect", s.c_str());
        unsubscribe(s);
    }
    assert(subscriptions.empty());

    webSocket = nullptr;
    webSocketUpdate.notify(*this);
}
