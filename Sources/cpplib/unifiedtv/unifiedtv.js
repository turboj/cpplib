'use strict'

// UnifiedTv instance gives websocket-like interface which is placed
// between user and multiple tv cores.
// Note that, unlike original websocket api,
// send and onmessage both accepts/passes non-serialized json object.
function UnifiedTv (urls) {
    var self = this
    this.connections = []
    this.connectionByDomainId = {}
    this.domainIdByServiceId = {}
    // where current service being selected or already selected belongs to.
    // nullified when selected null.
    this.currentConnection = undefined
    this.pendingSelectRequest = undefined

    for (var i = 0; i < urls.length; i++) {
        var c = new UnifiedTv.Connection(urls[i], this)
        c.onopen = onopen
        c.onclose = console.warn.bind(console)
        c.onmessage = function (msg) { self.onmessage(msg) }
        c.onerror = console.error.bind(console)
        this.connections.push(c)
    }

    function onopen (e) {
        for (var i = 0; i < self.connections.length; i++) {
            if (self.connections[i].webSocket.readyState != /*OPEN*/1) return
        }

        console.log('All connections are connected')
        if (self.onopen) self.onopen(e)
    }
}

// Connection is websocket wrapper for jsonrpc
UnifiedTv.Connection = function (url, utv) {
    var self = this
    var ws = new WebSocket(url)
    ws.onopen = function (e) { if (self.onopen) self.onopen(e) }
    ws.onclose = function (e) { if (self.onclose) self.onclose(e) }
    ws.onerror = function (e) { if (self.onerror) self.onerror(e) }
    ws.onmessage = this._onmessage.bind(this)

    this.webSocket = ws
    this.unifiedTv = utv;
    this.returns = {}
}

UnifiedTv.Connection.prototype._onmessage = function (e) {
    try {
        var msg = JSON.parse(e.data)
    } catch (e) {
        console.error('Expected jsonrpc message:' + e.data)
        return
    }

    // process callback
    if (msg.method) {
        var cb = this.unifiedTv.callbacks[msg.method]
        if (!cb) {
            console.warn('Unknown callback:' + msg.method)
            return
        }
        return cb.bind(this.unifiedTv)(msg, this)
    }

    // process return message
    var ret = this.returns[msg.id]
    if (ret) {
        delete this.returns[msg.id]
        return ret(msg)
    }

    // otherwise
    if (this.onmessage) this.onmessage(msg)
}

UnifiedTv.Connection.prototype.call = function (msg, ret) {
    this.webSocket.send(JSON.stringify(msg))
    if (msg.id && ret) this.returns[msg.id] = ret
}

UnifiedTv.prototype.send = function (msg) {
    var m = this.methods[msg.method]
    if (m) return m.bind(this)(msg)

    // we don't support return from client.
    log.error('Unsupported message:' + JSON.stringify(msg))
}

// Call to every connections
UnifiedTv.prototype._callAll = function (msg, ret) {
    var remain = this.connections.length;
    var results = new Array(remain)

    this.connections.forEach(function (conn, i) {
        conn.call(msg, function (resp) {
            results[i] = resp
            remain--
            if (remain == 0) ret(results)
        })
    })

    // trivial case
    if (remain == 0) setTimeout(ret, 0, results)
}

UnifiedTv.prototype.methods = {}
UnifiedTv.prototype.callbacks = {}

UnifiedTv.prototype.methods['subscribe'] =
UnifiedTv.prototype.methods['unsubscribe'] = function (req) {
    var self = this
    this._callAll(req, function (resps) {
        var methods = resps.reduce(function (cur, resp) {
            if (resp && resp.result && Array.isArray(resp.result.methods)) {
                resp.result.methods.forEach (function (m) {
                    if (!cur.includes(m)) cur.push(m)
                })
            }
            return cur
        }, [])

        self.onmessage({
            'jsonrpc': '2.0',
            'id': req['id'],
            'result': {
                'methods': methods
            }
        })
    })
}

UnifiedTv.prototype.methods['queryDomains'] = function (req) {
    var self = this
    this._callAll(req, function (resps) {
        self.connectionByDomainId = {}
        var domains = []
        for (var i = 0; i < resps.length; i++) {
            var resp = resps[i]
            var conn = self.connections[i]
            if (!resp || !resp.result) continue
            if (!Array.isArray(resp.result)) continue
            resp.result.forEach(function (d) {
                domains.push(d)
                self.connectionByDomainId[d.id] = conn
            })
        }

        self.onmessage({
            'jsonrpc': '2.0',
            'id': req['id'],
            'result': domains
        })
    })
}

UnifiedTv.prototype.methods['select'] = function (req) {
    var sid = req.params
    var did = this.domainIdByServiceId[sid]
    var conn = this.connectionByDomainId[did]

    if (sid && !(did && conn)) {
        console.warn('Domain or Connection for ' + sid + ' is unknown. ' +
                'fallback to unselect')
        sid = null
        did = undefined
        conn = undefined
    }

    // Override pending service if it exists.
    if (this.pendingSelectRequest) {
        this.pendingSelectRequest = req
        return
    }

    // Handle unselect (including when crossing the domain)
    if (this.currentConnection && this.currentConnection != conn) {
        this.currentConnection.call({
            'jsonrpc': '2.0',
            'method': 'select',
            'params': null
        })
        this.pendingSelectRequest = req
        return
    }

    // Otherwise, select the service at last.
    if (conn) {
        this.currentConnection = conn
        conn.call(req)
    }
}

UnifiedTv.prototype.callbacks['updateSelection'] = function (req, conn) {
    var sid = req['params']['serviceId']
    var st = req['params']['state']

    // unset current connection
    if (st == 'selected' && !sid) {
        this.currentConnection = undefined

        // resume pending request if exists
        var psr = this.pendingSelectRequest
        this.pendingSelectRequest = undefined
        if (psr && psr.params) {
            this.methods['select'].bind(this)(psr)
            return
        }
    }

    if (this.onmessage) this.onmessage(req)
}

UnifiedTv.prototype.callbacks['updateServiceList'] = function (req, conn) {
    var self = this

    var dels = req['params']['remove'] || []
    dels.forEach(function (s) {
        delete self.domainIdByServiceId[s['id']]
    })

    var adds = req['params']['add'] || []
    adds.forEach(function (s) {
        self.domainIdByServiceId[s['id']] = s['domainId']
    })

    if (this.onmessage) this.onmessage(req)
}

UnifiedTv.prototype.methods['saveServiceList'] =
UnifiedTv.prototype.methods['loadServiceList'] =
UnifiedTv.prototype.methods['clearServiceList'] = function (req) {
    var self = this
    this._callAll(req, function (resps) {
        var eresp = resps.find(function (resp) { return resp.error })
        var msg = {
            'jsonrpc': '2.0',
            'id': req['id'],
        }
        if (eresp) {
            msg['error'] = eresp.error
        } else {
            msg['result'] = null
        }
        self.onmessage(msg)
    })
}

UnifiedTv.prototype.methods['setComponentsPreference'] = function (req) {
    var self = this
    this._callAll(req, function (resps) {
        var eresp = resps.find(function (resp) { return resp.error })

        var msg = {
            'jsonrpc': '2.0',
            'id': req['id'],
        }

        if (eresp) {
            msg['error'] = eresp.error
        } else {
            msg['result'] = null
        }

        self.onmessage(msg)
    })
}

// TODO: need to manage states per connection
UnifiedTv.prototype.callbacks['updateComponents'] = function (req, conn) {

    // ignore callback if pending select request exists.
    if (this.pendingSelectRequest) return

    if (this.onmessage) this.onmessage(msg)
}

UnifiedTv.prototype.methods['requestTune'] =
UnifiedTv.prototype.methods['releaseTune'] = function (req) {
    var conn = this.connectionByDomainId[req['params']['domainId']]
    if (!conn) throw new Error('Unknown domain id')
    conn.call(req)
}

UnifiedTv.prototype.callbacks['updateTuneState'] = function (req, conn) {
    this.onmessage(req)
}

UnifiedTv.prototype.methods['queryEsg'] = function (req) {
    var self = this
    this._callAll(req, function (resps) {
        var esg = {}
        resps.forEach(function (resp) {
            var res = resp['result']
            if (typeof(res) !== 'object') return
            for (var k in res) esg[k] = res[k]
        })
        self.onmessage({
            'jsonrpc': '2.0',
            'id': req['id'],
            'result': esg
        })
    })
}

UnifiedTv.prototype.callbacks['notifyEsgUpdate'] = function (req, conn) {
    this.onmessage(req)
}
