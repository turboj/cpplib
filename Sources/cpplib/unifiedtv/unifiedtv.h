#ifndef UNIFIEDTV_H
#define UNIFIEDTV_H

#include <map>
#include <set>
#include <string>
#include <functional>

#include "acba.h"
#include "http_server.h"
#include "nlohmann/json.hpp"
#include "JsonRef.h"
#include "EventDriver.h"

namespace unifiedtv {

struct RpcManager;

struct RpcPeer: acba::RefCounter<RpcPeer> {

    RpcPeer (acba::HttpRequestHandler *h, acba::HttpReactor *r, RpcManager &m):
            webSocketHandler(h), webSocket(r), manager(m) {}
    virtual ~RpcPeer () {}

    acba::Ref<acba::HttpRequestHandler> webSocketHandler;
    acba::HttpReactor *webSocket;
    RpcManager &manager;

    acba::Update<RpcPeer&> webSocketUpdate;

    bool sendMessage (const nlohmann::json &msg);

    std::set<std::string> subscriptions;
    // Returns true on success
    bool subscribe (const std::string &evt, const nlohmann::json &opt = nullptr);
    // Returns true if fully unsubscribed.
    bool unsubscribe (const std::string &evt,
            const nlohmann::json &opt = nullptr);

    void dispose ();
};

typedef std::function<void(const acba::JsonRef, const acba::Ref<RpcPeer>)>
        RpcHandler;

struct RpcNotificationHandler {
    // Returns true on success
    virtual bool subscribe (const std::string &type, const nlohmann::json &opt,
            const acba::Ref<RpcPeer> &peer) {
        // default to simpler version
        return subscribe(type, peer);
    }
    virtual bool subscribe (const std::string &type,
            const acba::Ref<RpcPeer> &peer) {
        return false;
    }

    // Returns true if fully unsubscribed.
    virtual bool unsubscribe (const std::string &type,
            const nlohmann::json &opt, const acba::Ref<RpcPeer> &peer) {
        // default to simpler version
        return unsubscribe(type, peer);
    }
    virtual bool unsubscribe (const std::string &type,
            const acba::Ref<RpcPeer> &peer) {
        return false;
    }
};

struct RpcManager: acba::HttpRequestAcceptor {
    acba::HttpRequestHandler *acceptHttpRequest (const std::string &basePath,
            acba::HttpReactor *r) override;
    void registerHandler (const std::string &name, RpcHandler h);
    void registerNotificationHandler (const std::string &name,
            RpcNotificationHandler *h);

    std::map<const std::string, RpcHandler> rpcHandlers;
    std::map<const std::string, RpcNotificationHandler*> notificationHandlers;
};

}
#endif
