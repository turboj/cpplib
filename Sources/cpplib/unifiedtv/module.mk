$(call begin-target, unifiedtv, staticlib)
    LOCAL_SRCS := $(wildcard $(LOCAL_PATH)/*.cpp)
    LOCAL_IMPORTS := acba-staticlib json-nlohmann

    EXPORT_CXXFLAGS := -I$(LOCAL_PATH)
    EXPORT_IMPORTS := $(LOCAL_IMPORTS)
$(end-target)
