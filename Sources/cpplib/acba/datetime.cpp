#include <math.h>
#include <time.h>
#include <string.h>

#ifdef __APPLE__
#include <sys/time.h>
#endif

#include "datetime.h"

using namespace std;

double acba::parseDatetime (const char *str)
{
    struct tm tm;
    memset(&tm, 0, sizeof(tm));

    double sec;
    char tz[256];
    int c = sscanf(str, "%d-%d-%dT%d:%d:%lf%s",
            &tm.tm_year,
            &tm.tm_mon,
            &tm.tm_mday,
            &tm.tm_hour,
            &tm.tm_min,
            &sec,
            tz);
    if (c != 7) return 0.0;
    tm.tm_mon -= 1; // change to zero-based value
    tm.tm_year -= 1900;

    time_t t = mktime(&tm) - timezone;

    char sign;
    int h;
    int m;
    c = sscanf(tz, "%c%d:%d", &sign, &h, &m);
    if (c == 3) {
        int s;
        switch (sign) {
            case '+': s = -60; break;
            case '-': s = 60; break;
            default: s = 0; break;
        }
        t += (h * 60 + m) * s;
    }

    return t + sec;
}

string acba::toDatetime (double t)
{
    double f, i;
    f = modf(t, &i);

    time_t ti = (time_t)i;
    struct tm tm;
    gmtime_r(&ti, &tm);

    char str[128];
    sprintf(str, "%04d-%02d-%02dT%02d:%02d:%02d.%03dZ",
            tm.tm_year + 1900,
            tm.tm_mon + 1,
            tm.tm_mday,
            tm.tm_hour,
            tm.tm_min,
            tm.tm_sec,
            int(f * 1000) % 1000);
    return str;
}

double acba::currentUnixTime ()
{
#ifdef __APPLE__
    timeval t;
    gettimeofday(&t,0);
    return (double)t.tv_sec + ((double)t.tv_usec / 1000000);
#else
    timespec ts;
    clock_gettime(CLOCK_REALTIME, &ts);
    return ts.tv_sec + ts.tv_nsec / 1000000000.0;
#endif
}

double acba::parseDuration (const char *str)
{
    double dur = 0.0;

    if (*str++ != 'P') return dur;

    bool timeDesignated = false;
    while (*str) {
        if (!timeDesignated && *str == 'T') {
            timeDesignated = true;
            str++;
            continue;
        }

        char *dsg; // designator
        double num = strtod(str, &dsg);
        if (str == dsg) return dur; // no number specified

        if (!timeDesignated) {
            switch (*dsg) {
            case 'Y': dur += num * 365.25 * 24 * 60 * 60; break;
            case 'M': dur += num * 30.5 * 24 * 60 * 60; break;
            case 'W': dur += num * 7 * 24 * 60 * 60; break;
            case 'D': dur += num * 24 * 60 * 60; break;
            default: return dur;
            }
        } else {
            switch (*dsg) {
            case 'H': dur += num * 60 * 60; break;
            case 'M': dur += num * 60; break;
            case 'S': dur += num; break;
            default: return dur;
            }
        }

        str = dsg + 1;
    }

    return dur;
}
