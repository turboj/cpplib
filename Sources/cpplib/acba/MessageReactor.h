#ifndef ACBA_MESSAGE_REACTOR_H
#define ACBA_MESSAGE_REACTOR_H

#include <pthread.h>

#include <functional>

#include "EventDriver.h"
#include "Buffer.h"
#include "Log.h"

namespace acba {

struct MessageReactor: FileReactor {

    Buffer inBuffer;
    Buffer outBuffer;
    uint32_t partialMessageSize; // byte size of a message not yet sent.

    MessageReactor (bool safe = false): FileReactor(safe), closed(false),
            mutex(PTHREAD_MUTEX_INITIALIZER), partialMessageSize(0) {
    }

    virtual void readMessage (const uint8_t *buf, int32_t len) {
        abort("Not Supported");
    }

    // mt-safe
    void sendMessage (std::function<void(Buffer&)> writer);

    void sendMessage (const uint8_t *msg, int32_t len) {
        sendMessage([=](Buffer &buf) {
            buf.writeBytes(msg, len);
        });
    }

    void sendMessage (uint32_t val) {
        sendMessage([=](Buffer &buf) {
            buf.writeI32(val);
        });
    }

    bool flushSendBufferIfFull (uint32_t fullsz);

    // FileReactor implementations
    void read () override;
    void write () override;
    void close () override;

private:

    pthread_mutex_t mutex;
    bool closed;

    bool tryWrite ();
};

}
#endif
