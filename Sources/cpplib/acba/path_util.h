#ifndef ACBA_PATH_UTIL_H
#define ACBA_PATH_UTIL_H

#include <string>

namespace acba {

// Create directory up to last '/' in the given path.
void makeBaseDir (const char *path);

std::string escapeSlash (std::string path);

}

#endif
