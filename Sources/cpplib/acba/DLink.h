#ifndef ACBA_DLINK_H
#define ACBA_DLINK_H

#include <cstddef>

namespace acba {

struct DLink {
    DLink (): prev(this), next(this) {}

    inline bool isLinked () {
        return next != this;
    }

    inline void addAfter (DLink *l) {
        next = l->next;
        prev = l;
        l->next->prev = this;
        l->next = this;
    }

    inline void addBefore (DLink *l) {
        prev = l->prev;
        next = l;
        l->prev->next = this;
        l->prev = this;
    }

    inline void leave () {
        next->prev = prev;
        prev->next = next;
        prev = next = this;
    }

    DLink *prev;
    DLink *next;
};

template <int C>
struct DLinkArray {
    DLink dlinks[C];
    static const int DLinkArraySize = C;
};

template <class T, int C>
struct IntrusiveDoubleList {

    static DLink *getLink (T *t) {
        return &t->dlinks[C];
    }

    // Refer https://gist.github.com/pallas/10023808
    static T *getContainer (DLink *l) {
        static const T *null = reinterpret_cast<const T*>(0);
        static const ptrdiff_t offset = reinterpret_cast<const ptrdiff_t>(
                &null->dlinks[C]);
        return reinterpret_cast<T*>(reinterpret_cast<char*>(l) - offset);
    }

    inline bool isEmpty () {
        return !list.isLinked();
    }

    inline void addFront (T *item) {
        DLink *l = getLink(item);
        if (l->isLinked()) l->leave();
        l->addAfter(&list);
    }

    inline void addBack (T *item) {
        DLink *l = getLink(item);
        if (l->isLinked()) l->leave();
        l->addBefore(&list);
    }

    inline static void remove (T *item) {
        getLink(item)->leave();
    }

    inline static void addAfter (T *item, T *after) {
        DLink *l = getLink(item);
        if (l->isLinked()) l->leave();
        l->addAfter(getLink(after));
    }

    inline T *removeFront () {
        DLink *l = list.next;
        if (l == &list) return 0;
        l->leave();
        return getContainer(l);
    }

    inline T *removeBack () {
        DLink *l = list.prev;
        if (l == &list) return 0;
        l->leave();
        return getContainer(l);
    }

    inline T *first () {
        return toContainer(list.next);
    }

    inline T *last () {
        return toContainer(list.prev);
    }

    inline T *next (T *item) {
        return toContainer(getLink(item)->next);
    }

    inline T *prev (T *item) {
        return toContainer(getLink(item)->prev);
    }

private:

    inline T *toContainer (DLink *l) {
        if (l == &list) return 0;
        return getContainer(l);
    }

    DLink list;
};

}

#endif
