#include <stdint.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <arpa/inet.h>

#include "MessageReactor.h"
#include "ScopedLock.h"

using namespace std;
using namespace acba;

static Log l("ed", "msg ");

void MessageReactor::sendMessage (function<void(Buffer&)> writer)
{
    ScopedLock sl(mutex);

    if (closed) return;

    auto oldsz = outBuffer.size();

    // leave 4 bytes for length field and then write contents
    outBuffer.ensureMargin(4);
    outBuffer.grow(4);
    writer(outBuffer);

    // write length field
    auto msz = outBuffer.size() - oldsz;
    auto c = outBuffer.end() - msz;
    (*c++) = msz >> 24;
    (*c++) = msz >> 16;
    (*c++) = msz >> 8;
    (*c++) = msz;

    if (oldsz > 0) return; // already being sent

    if (!tryWrite()) {
        abort("write failed");
        return;
    }

    // continue writing only when outBuffer has remaining data
    if (outBuffer.size() > 0) setWritable(true);
}

bool MessageReactor::tryWrite ()
{
    int fd = getFd();
    if (fd < 0) {
        l.debug("fd already closed");
        return false;
    }

    while (true) {
        if (outBuffer.size() == 0) break;
        int w = ::send(fd, outBuffer.begin(), outBuffer.size(), (
                    MSG_DONTWAIT
#ifdef __linux__
                    | MSG_NOSIGNAL
#endif
                    ));
        if (w < 0) {
            if (errno == EAGAIN || errno == EWOULDBLOCK) return true;
            l.warn("Write failed. %s", strerror(errno));
            return false;
        }

        while (w > 0) {
            if (partialMessageSize == 0) {
                partialMessageSize = outBuffer.peekI32();
            }
            if (w <= partialMessageSize) {
                outBuffer.drop(w);
                partialMessageSize -= w;
                break;
            }

            outBuffer.drop(partialMessageSize);
            w -= partialMessageSize;
            partialMessageSize = 0;
        }
    }
    return true;
}

bool MessageReactor::flushSendBufferIfFull (uint32_t fullsz)
{
    ScopedLock sl(mutex);

    if (outBuffer.size() < fullsz) return false;

    // drop all pending messages except for partial one.
    outBuffer.drop(outBuffer.size() - partialMessageSize);

    return true;
}

void MessageReactor::read ()
{
    int fd = getFd();
    // fill in buffer
    while (true) {
        inBuffer.ensureMargin(1);
        int r = ::recv(fd, inBuffer.end(), inBuffer.margin(), MSG_DONTWAIT);
        l.debug("read from fd:%d returned %d", fd, r);
        if (r == 0) {
            // stop reading
            // changeMask(mask & ~EVENT_READ);
            abort("Received EOF");
            return;
        }
        if (r < 0) {
            if (errno == EAGAIN || errno == EWOULDBLOCK) break;
            abort(strerror(errno));
            return;
        }
        inBuffer.grow(r);
    }

    while (true) {
        auto sz = inBuffer.size();
        if (sz < 4) break;
        auto msz = inBuffer.peekI32(0);
        if (msz < 4 || sz < msz) break;

        readMessage(inBuffer.begin() + 4, msz - 4);
        inBuffer.drop(msz);
    }
}

void MessageReactor::write ()
{
    ScopedLock sl(mutex);

    if (!tryWrite()) {
        abort("write fail");
        return;
    }

    if (outBuffer.size() == 0) setWritable(false);
}

void MessageReactor::close ()
{
    {
        ScopedLock sl(mutex);
        closed = true;
    }

    FileReactor::close();
}

