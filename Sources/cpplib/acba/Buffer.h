#ifndef BUFFER_H
#define BUFFER_H

#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <stdio.h>
#include <string>
#include <assert.h>

namespace acba {

template <typename F, typename T>
inline static T bitwise_cast(F f) {
    union {
       F f;
       T t;
    } u;
    u.f = f;
    return u.t;
}

class Buffer {

    // for prepending bytes on payload.
    static const int32_t RESERVED_SIZE = 16; // should be double-aligned.
    static const int32_t INITIAL_CAPACITY = 512;

    enum {
        STACK_ALLOCATED,
        HEAP_ALLOCATED,
        EXTERNAL
    } type;

    uint8_t internalBuffer[INITIAL_CAPACITY];

    uint8_t *buffer, *_end;
    uint8_t *rpos, *wpos;

public:

    void *appData; // for passing registry

    // Construct dynamic buffer, initially empty.
    Buffer (): type(STACK_ALLOCATED),
            buffer(internalBuffer), _end(buffer + INITIAL_CAPACITY),
            rpos(buffer + RESERVED_SIZE), wpos(buffer + RESERVED_SIZE) {}

    // Construct buffer from pre-defined byte array.
    Buffer (uint8_t *buf, int32_t len): type(EXTERNAL),
            buffer(buf), _end(buf + len),
            rpos(buf), wpos(buf+len) {}

    ~Buffer () {
        if (type == HEAP_ALLOCATED) {
            free(buffer);
        }
    }

    inline int32_t capacity () const { return _end - buffer; }
    inline int32_t size () const { return wpos - rpos; }
    inline int32_t margin () const { return _end - wpos; }

    inline uint8_t read () { return *rpos++; }
    inline void readBytes (uint8_t *buf, int32_t len) {
        while (len-- > 0) {
            *buf++ = *rpos++;
        }
    }
    inline uint8_t peekI8 (int32_t i = 0) const { return rpos[i]; }
    inline uint16_t peekI16 (int32_t i = 0) const {
        return (rpos[i] << 8) | rpos[i+1];
    }
    inline uint32_t peekI32 (int32_t i = 0) const {
        int32_t j = (int32_t(rpos[i]) << 24) | (int32_t(rpos[i+1]) << 16) |
                (int32_t(rpos[i+2]) << 8) | rpos[i+3];
        return j;
    }

    inline void write (uint8_t d) {
        *wpos++ = d;
    }
    inline void writeBytes (const uint8_t *buf, int32_t len) {
        ensureMargin(len);
        memcpy(wpos, buf, len);
        wpos += len;
    }
    inline void clear () { rpos = wpos = buffer + RESERVED_SIZE; }
    inline void reset () {
        if (type == HEAP_ALLOCATED) {
            free(buffer);
            buffer = internalBuffer;
            _end = buffer + INITIAL_CAPACITY;
            type = STACK_ALLOCATED;
        }
        clear();
    }
    inline void take (Buffer &o) {
        if (o.type == STACK_ALLOCATED) {
            reset();
            writeBytes(o.begin(), o.size());
            o.clear();
            return;
        }

        if (type == HEAP_ALLOCATED) free(buffer);
        type = o.type;
        buffer = o.buffer;
        _end = o.buffer;
        rpos = o.rpos;
        wpos = o.wpos;

        o.type = STACK_ALLOCATED;
        o.buffer = o.internalBuffer;
        o._end = o.buffer + INITIAL_CAPACITY;
        o.clear();
    }

    inline void grow (int32_t s) { wpos += s; }
    inline void drop (int32_t s) { rpos += s; }
    inline uint8_t *begin () const { return rpos; }
    inline uint8_t *end () const { return wpos; }

    inline void ensureMargin (int32_t m) {
        if (type == EXTERNAL || margin() >= m) return;

        int32_t off = rpos - buffer;
        int32_t sz = size();
        int32_t cap = capacity();

        // try to compact without reallocation.
        // if current capacity is big enough and
        // front half of buffer is empty.
        int32_t ncap = RESERVED_SIZE + sz + m;
        if (ncap <= cap && off * 2 >= cap) {
            rpos = (uint8_t*)memmove(buffer + RESERVED_SIZE, rpos, sz);
            wpos = rpos + sz;
            return;
        }

        // increase ncap with extra margin.
        ncap = (cap * 3 / 2) + (m - margin());

        // try relloc
        if (buffer != internalBuffer) {
            uint8_t *nbuf = reinterpret_cast<uint8_t*>(realloc(buffer, ncap));
            if (nbuf) {
                rpos = nbuf + off;
                wpos = rpos + sz;
                buffer = nbuf;
                _end = nbuf + ncap;
                return;
            }
            // fallthrough
        }

        // allocate new buffer & copy
        uint8_t *nbuf = reinterpret_cast<uint8_t*>(malloc(ncap));
        rpos = (uint8_t*)memcpy(nbuf + RESERVED_SIZE, rpos, sz);
        wpos = rpos + sz;
        if (buffer != internalBuffer) free(buffer);
        buffer = nbuf;
        _end = nbuf + ncap;
        type = HEAP_ALLOCATED;
    }

    int8_t readI8 () {
#ifdef ROP_USE_SAFE_BUFFER
        if (size() == 0) return 0;
#endif
        return *rpos++;
    }

    int16_t readI16 () {
#ifdef ROP_USE_SAFE_BUFFER
        if (size() < 2) return 0;
#endif
        int16_t i = (int16_t(rpos[0]) << 8) | rpos[1];
        rpos += 2;
        return i;
    }

    int32_t readI32 () {
#ifdef ROP_USE_SAFE_BUFFER
        if (size() < 4) return 0;
#endif
        int32_t i = (int32_t(rpos[0]) << 24) | (int32_t(rpos[1]) << 16) |
                (int32_t(rpos[2]) << 8) | rpos[3];
        rpos += 4;
        return i;
    }

    int64_t readI64 () {
#ifdef ROP_USE_SAFE_BUFFER
        if (size() < 8) return 0l;
#endif
        uint32_t i = (uint32_t(rpos[0]) << 24) | (uint32_t(rpos[1]) << 16) |
                (uint32_t(rpos[2]) << 8) | rpos[3];
        uint32_t j = (uint32_t(rpos[4]) << 24) | (uint32_t(rpos[5]) << 16) |
                (uint32_t(rpos[6]) << 8) | rpos[7];
        rpos += 8;
        return (int64_t(i) << 32) + j;
    }

    float readF32 () {
#ifdef ROP_USE_SAFE_BUFFER
        if (size() < 4) return 0.0f;
#endif
        return bitwise_cast<int32_t, float>(readI32());
    }

    double readF64 () {
#ifdef ROP_USE_SAFE_BUFFER
        if (size() < 8) return 0.0;
#endif
        return bitwise_cast<int64_t, double>(readI64());
    }

    void writeI8 (int8_t i)  {
        ensureMargin(1);
        write(i);
    }

    void writeI16 (int16_t i) {
        ensureMargin(2);
        write(i >> 8);
        write(i);
    }

    void writeI32 (int32_t i) {
        ensureMargin(4);
        write(i >> 24);
        write(i >> 16);
        write(i >> 8);
        write(i);
    }

    void writeI64 (int64_t i) {
        writeI32(i >> 32);
        writeI32(i);
    }

    void writeF32 (float f) {
        writeI32(bitwise_cast<float, int32_t>(f));
    }

    void writeF64 (double f) {
        writeI64(bitwise_cast<double, int64_t>(f));
    }

    void write (const std::string &str) {
        writeBytes(reinterpret_cast<const uint8_t*>(str.data()), str.size());
    }

    void preWriteI8 (int8_t i) {
        rpos--;
        *rpos = i;
    }

    void preWriteI32 (int32_t i) {
        rpos -= 4;
        rpos[0] = i >> 24;
        rpos[1] = i >> 16;
        rpos[2] = i >> 8;
        rpos[3] = i;
    }

    void dump () const {
        int col = 0;
        for (uint8_t *s = rpos; s < wpos; s++) {
            printf("%02x ", *s);
            col++;
            if (col == 16) printf("\n");
        }
        printf("\n");
    }

    bool operator== (const Buffer &other) const {
        if (this == &other) return true;
        int sz = size();
        if (sz != other.size()) return false;
        return (memcmp(begin(), other.begin(), size()) == 0);
    }
};

}
#endif
