#include "CondVar.h"
#include <stdint.h>
#include <pthread.h>
#include <time.h>
#include <sys/time.h>

#define ROP_CONDVAR_LEGACY 0
#define ROP_CONDVAR_MONOTONIC 1
#define ROP_CONDVAR_BIONIC 2

#ifndef ROP_CONDVAR_IMPL
#if defined(_POSIX_C_SOURCE) && _POSIX_C_SOURCE >= 200112                    
#define ROP_CONDVAR_IMPL ROP_CONDVAR_MONOTONIC
#elif defined(__BIONIC__)
#define ROP_CONDVAR_IMPL ROP_CONDVAR_BIONIC
#else
#define ROP_CONDVAR_IMPL ROP_CONDVAR_LEGACY
#endif
#endif

using namespace acba;

CondVar::CondVar ()
{
    pthread_condattr_t attr;
    pthread_condattr_init(&attr);
#if ROP_CONDVAR_IMPL == ROP_CONDVAR_MONOTONIC
    pthread_condattr_setclock(&attr, CLOCK_MONOTONIC);
#endif
    pthread_cond_init(&cond, &attr);
}

int CondVar::wait (pthread_mutex_t *mon, int32_t delay)
{
    timespec ts;
#if ROP_CONDVAR_IMPL == ROP_CONDVAR_LEGACY
    timeval tv;
    gettimeofday(&tv, 0);
    ts.tv_sec = tv.tv_sec + delay / 1000;
    ts.tv_nsec = (tv.tv_usec * 1000) + ((delay % 1000) * 1000000);
    return pthread_cond_timedwait(&cond, mon, &ts);
#else
    clock_gettime(CLOCK_MONOTONIC, &ts);
    ts.tv_sec += delay / 1000;
    ts.tv_nsec += (delay % 1000) * 1000000;
#if ROP_CONDVAR_IMPL == ROP_CONDVAR_BIONIC
    return pthread_cond_timedwait_monotonic_np(&cond, mon, &ts);
#else
    return pthread_cond_timedwait(&cond, mon, &ts);
#endif
#endif
}
