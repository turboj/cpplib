/*
 * Copyright (C) 2001-2015 Alticast Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Alticast Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Alticast
 * Corporation and you.
 */

#ifndef ACBA_REF_H
#define ACBA_REF_H

#include <stdint.h>
#include <pthread.h>

namespace acba {

/**
 * Generic smart pointer which points to reference counted object.
 * Reference counted object should have ref()/deref() methods, and
 * reference count starts from zero.
 */
template <typename T>
class Ref {
    T *object;
public:
    Ref (): object(0) {}
    Ref (T *o): object(o) {
        if (o) {
            o->ref();
        }
    }
    Ref (const Ref<T> &r): object(r.object) {
        if (object) {
            object->ref();
        }
    }
    ~Ref () {
        if (object) {
            object->deref();
        }
    }
    T &operator* () {
        return *object;
    }
    T *operator-> () const {
        return object;
    }
    T *get () const {
        return object;
    }
    template <typename U>
    U *getAs () const {
        return static_cast<U*>(object);
    }
    Ref<T> &operator=(T *o) {
        while (true) {
            T *t = object;
            if (t == o) return *this;

            // safe exchange
            if (o) o->ref();
            if (__sync_bool_compare_and_swap (&object, t, o)) {
                if (t) t->deref();
                return *this;
            }

            // rollback
            if (o) o->deref();
        }
    }
    Ref<T> &operator=(const Ref<T> &r) {
        return operator=(r.get());
    }
    operator T*() const { return object; }
    bool operator==(const Ref<T>& r) {
        return object == r.object;
    }
    bool operator!=(const Ref<T>& r) {
        return object != r.object;
    }
};

template <typename T>
class RefCounter {
protected:
    int refCount;

    void collect () {
        delete static_cast<T*>(this);
    }

public:

    RefCounter (): refCount(0) {}
    virtual ~RefCounter () {}

    void ref () {
        __sync_add_and_fetch(&refCount, 1);
    }

    void deref () {
        if (__sync_sub_and_fetch(&refCount, 1) == 0) {
            static_cast<T*>(this)->collect();
        }
    }

    friend class Ref<T>;
};

template <typename T>
struct RefCounted: T, RefCounter<RefCounted<T> > {

    template <typename ...TS>
    RefCounted (TS ...args): T(args...) {}
};

template <class T>
inline Ref<T> ref(T *t) {
    return t;
}

}

#endif
