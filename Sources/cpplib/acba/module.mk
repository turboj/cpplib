$(call begin-target, acba, staticlib)
    LOCAL_SRCS := $(wildcard $(LOCAL_PATH)/*.cpp $(LOCAL_PATH)/*.c)
    LOCAL_CXXFLAGS := -DROP_SHA1_IMPL=ROP_SHA1_INTERNAL
    EXPORT_CXXFLAGS := -I$(LOCAL_PATH)
$(end-target)
