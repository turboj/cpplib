#ifndef ACBA_CRC32_H
#define ACBA_CRC32_H

#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif

uint32_t acba_crc32 (uint32_t crc, const unsigned char *buf, int len);
uint32_t acba_crc32be (uint32_t crc, const unsigned char *data, int len);

#ifdef __cplusplus
}
#endif
#endif
