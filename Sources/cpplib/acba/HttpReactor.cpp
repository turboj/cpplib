// Websocket implementaion was based on Hiroshi Ichikawa's websocket-ruby.
// https://github.com/gimite/web-socket-ruby

#include <assert.h>
#include <stdint.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <strings.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <arpa/inet.h>
#include <map>
#include <sstream>
#include <string>
#include <stdio.h>

#include "base64.h"
#include "HttpReactor.h"
#include "ScopedLock.h"

#define ROP_SHA1_OPENSSL 0
#define ROP_SHA1_INTERNAL 1
#ifndef ROP_SHA1_IMPL
#define ROP_SHA1_IMPL ROP_SHA1_OPENSSL
#endif

#define ROP_TEXT_RAW 0
#define ROP_TEXT_BASE64 1
#ifndef ROP_TEXT_HANDLE
#define ROP_TEXT_HANDLE ROP_TEXT_RAW
#endif

#if ROP_SHA1_IMPL == ROP_SHA1_OPENSSL
#include "openssl/sha.h"
#elif ROP_SHA1_IMPL == ROP_SHA1_INTERNAL
#include "sha1.h"
#endif

#define ALLOW_ANY_ORIGIN 1

using namespace std;
using namespace acba;

static void apply_mask (uint8_t *payload, int len, uint8_t *mask) {
    for (int i = 0; i < len; i++) {
        payload[i] ^= mask[i % 4];
    }
}

static string makeAcceptString (string key)
{
    uint8_t msg[20];
    uint8_t encoded[29];
    key.append("258EAFA5-E914-47DA-95CA-C5AB0DC85B11");

    // digest key.
#if ROP_SHA1_IMPL == ROP_SHA1_OPENSSL
    {
        SHA_CTX c;
        SHA1_Init(&c);
        SHA1_Update(&c, key.c_str(), key.length());
        SHA1_Final(msg, &c);
    }
#elif ROP_SHA1_IMPL == ROP_SHA1_INTERNAL
    sha1sum(key.c_str(), key.length(), msg);
#endif

    int len = base64_encode(msg, (char*)encoded, 20);
    encoded[len] = 0;
    return (char*)encoded;
}

static void bitdump (const Log &log, uint8_t *s, int off, int len) {
    string str = "";
    while (len-- > 0) {
        int i = off / 8;
        int j = off % 8;
        off++;
        str += (s[i]&(1<<(7-j))) ? '1' : '0';
        if (j == 7) {
            str += " | ";
        }
    }

    log.trace("%s\n", str.c_str());
}

void HttpReactor::send (const uint8_t *buf, int32_t len)
{
    ScopedLock sl(mutex);

    if (outBuffer.size()) {
        // already being sent.
        outBuffer.writeBytes(buf, len);
        return;
    }

    outBuffer.writeBytes(buf, len);
    if (!tryWrite()) {
        abort("write failed");
        return;
    }

    // continue writing only when outBuffer has remaining data
    if (outBuffer.size()) setWritable(true);
}

void HttpReactor::send (const string &msg)
{
    send(reinterpret_cast<const uint8_t*>(msg.data()), msg.size());
}

void HttpReactor::sendFrame (OPCODE op, const uint8_t *msg, int32_t len)
{
    if (websocketVersion <= 0) {
        assert(!"Not Supported");
    }

    // work-around for WEBMW-325
    // if (!isText) return sendFrameAsBase64(msg, len);

    Buffer buf;
    buf.writeI8(0x80 | op);
    if (len < 126) {
        buf.writeI8(len);
    } else if (len < 65536) {
        buf.writeI8(126);
        buf.writeI16(len);
    } else {
        buf.writeI8(127);
        buf.writeI64(len);
    }

    buf.writeBytes(msg, len);

    send(buf.begin(), buf.size());
    log.debug("sent a frame (%d bytes).\n", buf.size());
}

void HttpReactor::sendFrameAsBase64 (const uint8_t *msg, int32_t len)
{
    if (websocketVersion <= 0) {
        assert(!"Not Supported");
    }

    Buffer buf;

    // use base64 for text encoding.
    int64_t plen = (len + 2) / 3 * 4;
    buf.writeI8(0x80 | OPCODE_TEXT);
    if (plen < 126) {
        buf.writeI8(plen);
    } else if (plen < 65536) {
        buf.writeI8(126);
        buf.writeI16(plen);
    } else {
        buf.writeI8(127);
        buf.writeI64(plen);
    }

    buf.ensureMargin(plen);
    int l = base64_encode(msg, (char*)buf.end(), len);
    assert(l == plen);
    buf.grow(l);

    send(buf.begin(), buf.size());
    log.debug("sent a frame as base64 (%d bytes).\n", buf.size());
}

void HttpReactor::sendHead (int32_t status, const vector<string> &headers, int32_t len)
{
    stringstream ss;
    ss << "HTTP/1.1 " << status << " ";
    switch (status) {
    case 200: ss << "OK"; break;
    case 404: ss << "Not Found"; break;
    default: ss << "Unknown Code"; break;
    }
    ss << "\r\n";
    for (auto h: headers) ss << h << "\r\n";
#if ALLOW_ANY_ORIGIN
    ss << "Access-Control-Allow-Origin: *\r\n";
#endif
    if (len >= 0) ss << "Content-Length: " << len << "\r\n";
    ss << "\r\n";
    send(ss.str());
}

void HttpReactor::sendResponse (int32_t status, const vector<string> &headers,
        const std::string &content)
{
    sendHead(status, headers, content.length());
    sendBody(content);
    endResponse();
}

static string unescape (const string &url) {
    string res;
    res.reserve(url.length());

    const char *s = url.c_str();;
    for (const char *p; (p = strchr(s, '%')); s = p + 3) {
        res.append(s, p - s);

        if (!p[1] || !p[2]) break;
        char hex[3] = {p[1], p[2], '\0'};
        res += (char)strtol(hex, nullptr, 16);
    }
    res.append(s);

    return res;
}

bool HttpReactor::parseHeader ()
{
    // check if in-buffer contains complete http heads.
    int n = 0;
    char *s = reinterpret_cast<char*>(inBuffer.begin());
    char *end = reinterpret_cast<char*>(inBuffer.end());
    for (; s < end; s++) {
        if (*s == 0x0d) continue;
        if (*s == 0x0a) {
            n++;
            if (n == 2) {
                end = s + 1;
                s = reinterpret_cast<char*>(inBuffer.begin());
                inBuffer.drop(end - s);
                break;
            }
        } else {
            n = 0;
        }
    }
    if (n != 2) return false;

    // string from s to end(exclusive)
    char *colon = 0;
    char *start = 0;
    for (; s < end; s++) {
        if (!start) {
            if (*s != 0x0d && *s != 0x0a) {
                start = s;
            }
            continue;
        }
        if (!colon && *s == ':') {
            colon = s;
            continue;
        }
        if (*s != '\r' && *s != '\n') continue;
        *s = '\0';

        // a line starts from start
        if (requestLine.empty()) {
            requestLine = start;
            log.trace("REQ %s\n", start);
            colon = 0;
            start = 0;
            size_t f = requestLine.find(" ");
            size_t s = requestLine.find(" ", f + 1);
            if (f != string::npos && s != string::npos) {
                requestMethod = requestLine.substr(0, f);
                requestUri = requestLine.substr(f + 1, s - (f + 1));
                size_t q = requestUri.find("?");
                if (q == string::npos) {
                    requestPath = unescape(requestUri);
                    requestQuery = "";
                } else {
                    requestPath = unescape(requestUri.substr(0, q));
                    requestQuery = requestUri.substr(q);
                }
                requestProtocol = requestLine.substr(s + 1);
            }
            continue;
        }

        if (!colon) {
            log.warn("header field without colon:%s\n", start);
            continue;
        }

        // normalize header key
        do {
            *colon++ = '\0';
        } while (*colon == ' ');
        for (char *t = start; *t; t++) {
            char c = *t;
            if (c >= 'A' && c <= 'Z') {
                *t = c + ('a' - 'A');
            }
        }
        log.trace("REQ %s: %s\n", start, colon);
        headers[start] = colon;
        colon = 0;
        start = 0;
    }

    if (headers.find("content-length") != headers.end()) {
        contentLength = atoi(headers["content-length"].c_str());
    }
    return true;
}

bool HttpReactor::handshake () {
    if (strcasecmp(headers["upgrade"].c_str(), "websocket") != 0) return true;

    // determine websocket version
    if (headers.find("sec-websocket-version") != headers.end()) {
        websocketVersion = atoi(headers["sec-websocket-version"].c_str());
    } else if (headers.find("sec-websocket-key1") != headers.end() &&
            headers.find("sec-websocket-key2") != headers.end()) {
        websocketVersion = -76; // hixie-76
    }
    if (websocketVersion == 0) return true;

    if (websocketVersion < 0) { // hixie-76
        assert(!"Not Supported websocket version");
        uint8_t key3[8];
        inBuffer.readBytes(key3, 8);
        {
            string reply =
                    "HTTP/1.1 101 Switching Protocols\r\n"
                    "Upgrade: websocket\r\n"
                    "Connection: Upgrade\r\n"
                    "Sec-WebSocket-Origin: http://localhost\r\n"
                    "Sec-WebSocket-Location: ws://localhost\r\n";
            string keys;
            keys = headers["sec-websocket-key1"];
            // write md5
            // def hixie_76_security_digest(key1, key2, key3)
            // bytes1 = websocket_key_to_bytes(key1)
            // bytes2 = websocket_key_to_bytes(key2)
            // def websocket_key_to_bytes(key)
              // num = key.gsub(/[^\d]/n, "").to_i() / key.scan(/ /).size
              // return [num].pack("N")
            // end
            // return Digest::MD5.digest(bytes1 + bytes2 + key3)
            reply += "\r\n\r\n";
            //send(reply);
        }
        return true;
    } else { // rfc6455
        string reply =
                "HTTP/1.1 101 Switching Protocols\r\n"
                "Upgrade: websocket\r\n"
                "Connection: Upgrade\r\n"
                "Sec-WebSocket-Accept: ";
        reply += makeAcceptString(headers["sec-websocket-key"]);
        reply += "\r\n\r\n";
        log.debug("sending... %s\n", reply.c_str());
        send(reply);
        return true;
    }
}

bool HttpReactor::readWebSocketHeader ()
{
    if (inBuffer.size() < 2) {
        return false;
    }
    int h0 = inBuffer.peekI8(0);
    int h1 = inBuffer.peekI8(1);
    int minread = 2;

    if (log.T()) bitdump(log, inBuffer.begin(), 0, 16);

    bool mask = (h1 & 0x80) != 0;
    if (mask) {
        minread += 4;
    } else {
        abort("received unmasked frame.");
        return false;
    }
    int64_t plen = h1 & 0x7f;
    if (plen == 126) {
        minread += 2;
    } else if (plen == 127) {
        minread += 8;
    }
    if (inBuffer.size() < minread) {
        return false;
    }
    inBuffer.drop(2);

    if (plen == 126) {
        plen = inBuffer.readI16();
    } else if (plen == 127) {
        plen = inBuffer.readI64();
    }
    contentLength = plen;
    inBuffer.readBytes(frameMask, 4);

    fin = (h0 & 0x80) != 0;
    OPCODE op = OPCODE(h0 & 0x0f);
    if (op != OPCODE_CONTINUATION) opcode = op;

    return true;
}

bool HttpReactor::tryWrite ()
{
    int fd = getFd();
    if (fd < 0) {
        log.debug("fd already closed\n");
        return false;
    }

    while (true) {
        if (outBuffer.size() == 0) break;
        int w = ::send(fd, outBuffer.begin(), outBuffer.size(), (
                MSG_DONTWAIT
#ifdef __linux__
                | MSG_NOSIGNAL
#endif
                ));
        if (w < 0) {
            if (errno == EAGAIN || errno == EWOULDBLOCK) return true;
            log.debug("Write failed\n");
            return false;
        }
        outBuffer.drop(w);
    }
    return true;
}

void HttpReactor::read ()
{
    int fd = getFd();
    // fill in buffer
    while (true) {
        inBuffer.ensureMargin(1);
        int r = ::recv(fd, inBuffer.end(), inBuffer.margin(), MSG_DONTWAIT);
        log.debug("read from fd:%d returned %d\n", fd, r);
        if (r == 0) {
            // stop reading
            // changeMask(mask & ~EVENT_READ);
            abort("Received EOF");
            return;
        }
        if (r < 0) {
            if (errno == EAGAIN || errno == EWOULDBLOCK) break;
            abort(strerror(errno));
            return;
        }
        inBuffer.grow(r);
    }

handle_message:

    switch (state) {

    case BEGIN_READ:
        requestLine.clear();
        contentBuffer.clear();
        contentLength = -1;
        headers.clear();
        state = READ_HEADS;
        // fall through

    case READ_HEADS:
        if (!parseHeader()) return; // header is incomplete.
        state = TRY_HANDSHAKE;
        // fall through

    case TRY_HANDSHAKE:
        if (!handshake()) return; // handshake is incomplete.

        // if this is websocket
        if (websocketVersion) {
            state = (websocketVersion > 0) ? READ_WEBSOCKET :
                READ_WEBSOCKET_76;
            readRequest();
            break;
        }

        if (contentLength == -1) {
            state = BEGIN_READ;
            readRequest();
            break;
        }

        state = READ_BODY;
        // fall through

    case READ_BODY:
        if (inBuffer.size() < contentLength) return;

#if ROP_TEXT_HANDLE == ROP_TEXT_RAW
        contentBuffer.writeBytes(inBuffer.begin(), contentLength);
        inBuffer.drop(contentLength);
#elif ROP_TEXT_HANDLE == ROP_TEXT_BASE64
        // decode base64-encoded payload to contentBuffer
        {
            int est = (contentLength + 3) / 4 * 3;
            contentBuffer.ensureMargin(est);
            string s;
            s.assign(reinterpret_cast<char*>(inBuffer.begin()), contentLength);
            int len = base64_decode(reinterpret_cast<char*>(inBuffer.begin()),
                    contentBuffer.end(), contentLength);
            s.assign(reinterpret_cast<char*>(inBuffer.begin()), contentLength);
            inBuffer.drop(contentLength);
            contentBuffer.grow(len);
            contentLength = len;
        }
#endif

        state = BEGIN_READ;
        readRequest();
        contentBuffer.clear();
        break;

    case READ_WEBSOCKET:
        if (!readWebSocketHeader()) return;
        state = READ_WEBSOCKET_PAYLOAD;
        // fall through

    case READ_WEBSOCKET_PAYLOAD:
        if (inBuffer.size() < contentLength) return;

        apply_mask(inBuffer.begin(), contentLength, frameMask);
        state = READ_WEBSOCKET;

        switch (opcode) {
        case OPCODE_TEXT:
#if ROP_TEXT_HANDLE == ROP_TEXT_RAW
            contentBuffer.writeBytes(inBuffer.begin(), contentLength);
            inBuffer.drop(contentLength);
#elif ROP_TEXT_HANDLE == ROP_TEXT_BASE64
            // decode base64-encoded payload to contentBuffer
            {
                int est = (contentLength + 3) / 4 * 3;
                contentBuffer.ensureMargin(est);
                int len = base64_decode(
                        reinterpret_cast<char*>(inBuffer.begin()),
                        contentBuffer.end(), contentLength);
                inBuffer.drop(contentLength);
                contentBuffer.grow(len);
            }
#endif
            if (!fin) break;
            contentLength = contentBuffer.size();
            readFrame();
            contentBuffer.clear();
            break;

        case OPCODE_BINARY:
            contentBuffer.writeBytes(inBuffer.begin(), contentLength);
            inBuffer.drop(contentLength);
            if (!fin) break;
            contentLength = contentBuffer.size();
            readFrame();
            contentBuffer.clear();
            break;

        case OPCODE_CLOSE:
            abort("websocket closed by peer.\n");
            return;

        case OPCODE_PING:
            contentBuffer.writeBytes(inBuffer.begin(), contentLength);
            inBuffer.drop(contentLength);
            if (!fin) break;
            log.warn("responding to ping");
            sendFrame(OPCODE_PONG, contentBuffer.begin(), contentBuffer.size());
            contentBuffer.clear();
            break;

        case OPCODE_PONG:
            log.warn("unexpected pong.");
            break;

        default:
            log.error("unknown opcode:%d", opcode);
            break;
        }
        break;

    case READ_WEBSOCKET_76:
        /*
        packet = gets("\xff")
        return nil if !packet
        if packet =~ /\A\x00(.*)\xff\z/nm
        return force_encoding($1, "UTF-8")
      elsif packet == "\xff" && read(1) == "\x00" # closing
        close(1005, "", :peer)
        return nil
      else
        raise(WebSocket::Error, "input must be either '\\x00...\\xff' or '\\xff\\x00'")
      end
      */
        break;

    case READ_WEBSOCKET_PAYLOAD_76:
        log.error("Unsupported websocket version\n");
        break;

    default:
        log.error("Illegal state:%d\n", state);
        break;
    }

    if (getFd() != -1) goto handle_message;
}

void HttpReactor::write ()
{
    {
        ScopedLock sl(mutex);

        if (!tryWrite()) {
            abort("write fail\n");
            return;
        }

        if (outBuffer.size() > 0) return;
    }

    setWritable(false);
    idle();
}

void HttpReactor::sendFile (const string &path, const vector<string> &headers)
{
    int fd = open(path.c_str(), O_RDONLY);
    if (fd < 0) {
        sendResponse(404, {"Content-Type: plain/text"}, "File not found");
        return;
    }

    struct stat st;
    if (fstat(fd, &st)) {
        sendResponse(404, {"Content-Type: plain/text"}, "File size unknown");
        return;
    }
    int sz = st.st_size;

    // load file content into buf
    Buffer buf;
    buf.ensureMargin(sz);
    while (buf.size() < sz) {
        ssize_t r = ::read(fd, buf.end(), sz - buf.size());
        if (r <= 0) break;
        buf.grow(r);
    }
    if (buf.size() != sz) {
        sendResponse(404, {"Content-Type: plain/text"}, "File read failed");
        return;
    }

    // TODO: Content-Type
    sendHead(200, {}, sz);
    sendBody(buf.begin(), sz);
    endResponse();
}

void HttpReactor::sendBody (const uint8_t *buf, int32_t len)
{
    if (requestMethod == "HEAD") return;
    send(buf, len);
}

void HttpReactor::sendBody (const string &msg)
{
    if (requestMethod == "HEAD") return;
    send(reinterpret_cast<const uint8_t*>(msg.data()), msg.size());
}
