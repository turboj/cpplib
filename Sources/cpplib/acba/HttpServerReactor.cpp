#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <ctype.h>

#include "HttpReactor.h"
#include "http_server.h"
#include "socket_util.h"

using namespace std;
using namespace acba;

struct RequestReactor: HttpReactor {
    RequestReactor (HttpServerReactor *s): HttpReactor(), server(s) {}

    void readRequest () override {
        dump();

        if (!handler && !updateHandler()) {
            sendResponse(404, {"Content-Type: text/plain"}, "Unsupported path");
            return;
        }

        if (websocketVersion) {
            handler->handleWebsocketOpen(*this);
        } else {
            setReadable(false);
            handler->handleHttpRequest(*this);
        }
    }

    void readFrame () override {
        assert(handler);
        if (opcode == OPCODE_TEXT) {
            handler->handleWebsocketTextMessage(*this);
        } else {
            handler->handleWebsocketMessage(*this);
        }
    }

    void close () override {
        HttpReactor::close();
        if (websocketVersion && handler) handler->handleWebsocketClose(*this);
        delete this;
    }

    bool updateHandler () {
        int mlen = -1; // matched len
        string bp;
        HttpRequestAcceptor *acc = 0;
        for (auto a: server->requestAcceptors) {
            int len = a.first.length();

            if (len <= mlen) continue;
            if (strncmp(a.first.c_str(), requestPath.c_str(), len) != 0) continue;

            bp = a.first;
            acc = a.second;
            mlen = len;
        }
        if (!acc) return false;

        handler = acc->acceptHttpRequest(bp, this);
        if (handler) {
            requestHandlerPath = requestPath.substr(mlen);
            return true;
        } else {
            return false;
        }
    }

    void endResponse () override {
        handler = 0;
        setReadable(true);
    }

    HttpServerReactor *server;
    Ref<HttpRequestHandler> handler;
};


int HttpServerReactor::start (int port, const char* host, EventDriver *ed)
{
    int fd = create_server_socket(port, host);
    if (fd < 0) return -1;
    ed->watchFile(fd, acba::EVENT_READ, this);
    log.info("listening on host: %s, port: %d (fd:%d)\n", host, port, fd);
    return 0;
}

void HttpServerReactor::stop ()
{
    abort("webserver stopped by request");
}

void HttpServerReactor::bind (const string &path, HttpRequestAcceptor *a) {
    if (a) {
        requestAcceptors[path] = a;
    } else {
        requestAcceptors.erase(path);
    }
}

void HttpServerReactor::read ()
{
    int fd;
    {
        sockaddr_in clientAddr;
        socklen_t len = sizeof(clientAddr);
        fd = accept(getFd(), (struct sockaddr*)&clientAddr, &len);
    }
    if (fd < 0) {
        log.warn("Accept failed.\n");
        return;
    }
    log.info("new connection fd:%d\n", fd);

    getEventDriver()->watchFile(fd, EVENT_READ, new RequestReactor(this));
}

string acba::url_encode (const string &s)
{
    string ret;

    for (auto &c: s) {
        switch (c) {
        default:
            if (!isalnum(c)) break;
            // fall through

        case '-': case '_': case '.': case '~':
            ret += c;
            continue;
        }

        char esc[4];
        sprintf(esc, "%%%02x", c);
        ret += esc;
    }

    return ret;
}
