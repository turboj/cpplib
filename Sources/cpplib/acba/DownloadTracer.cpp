#include "DownloadTracer.h"

using namespace acba;
using namespace std;

void DownloadTracer::clear ()
{
    chunks.clear();
}

void DownloadTracer::markFragment (uint32_t off, uint32_t len)
{
    if (len == 0) return;

    // write into the proper chunk
    auto c = chunks.upper_bound(off);
    if (c == chunks.begin()) {
        // add new chunk at head. this occurs when chunks is empty as well.
        chunks[off] = off + len;
        c = chunks.find(off);
    } else {
        c--;
        if (c->second < off) {
            // add new chunk in the middle
            chunks[off] = off + len;
            c = chunks.find(off);
        } else {
            // extend an existing chunk
            c->second = off + len;
        }
    }

    // merge overapped chunks afterwards 
    auto d = c;
    d++;
    while (d != chunks.end() && d->first <= c->second) {
        if (d->second > c->second) c->second = d->second;
        d = chunks.erase(d);
    }
}

uint32_t DownloadTracer::getContinuousSize ()
{
    auto b = chunks.begin();
    if (b == chunks.end() || b->first > 0) return 0;
    return b->second;
}

uint32_t DownloadTracer::getTotalSize ()
{
    int sum = 0;
    for (auto &c: chunks) sum += c.second - c.first;
    return sum;
}

bool DownloadTracer::isSparse ()
{
    return chunks.size() > 1;
}

void DownloadTracer::getChunks (map<uint32_t, uint32_t> &outChunks)
{
    outChunks = chunks;
}
