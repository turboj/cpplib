#ifndef __SOCKET_UTIL_H__
#define __SOCKET_UTIL_H__

#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif

int create_socket (const char *host, int port);
int create_server_socket (int port, const char* host);
int create_unixdomain_socket (const char *path);
int create_unixdomain_server_socket (const char *path);
int create_multicast_socket (uint32_t srcIp, uint32_t dstIp, uint16_t port);

#ifdef __cplusplus
}
#endif
#endif /* __SOCKET_UTIL_H__ */
