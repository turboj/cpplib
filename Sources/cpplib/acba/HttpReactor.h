#ifndef HTTP_MONITOR_H
#define HTTP_MONITOR_H

#include "EventDriver.h"
#include "Buffer.h"
#include "Log.h"
#include <map>
#include <string>

#ifdef ED_MANGLE
#define HttpReactor ED_MANGLE(HttpReactor)
#endif

namespace acba {

struct HttpReactor: FileReactor {

    Buffer inBuffer;
    Buffer outBuffer;

    std::string requestLine;
    std::string requestMethod; // GET, POST, ...
    std::string requestUri; // /xxx/index.html?zzz
    std::string requestPath; // /xxx/index.html (decoded string)
    std::string requestHandlerPath; // cut basepath (decoded string)
    std::string requestQuery; // ?zzz
    std::string requestProtocol; // HTTP/1.1
    std::map<std::string,std::string> headers;
    Buffer contentBuffer; // received http body or websocket payload
    int32_t contentLength;

    // 0:not websocket, +:version, -:hixie version
    int websocketVersion;

    HttpReactor (bool safe = false): FileReactor(safe), websocketVersion(0),
            state(BEGIN_READ), log("ed", "http ") {
        pthread_mutex_init(&mutex, 0);
    }

    ~HttpReactor () {
        pthread_mutex_destroy(&mutex);
    }

    // callbacks
    virtual void readFrame () { abort("Not Supported"); }
    virtual void readRequest () { abort("Not Supported"); }

    // mt-safe
    void sendFrame (const uint8_t *msg, int32_t len, bool isText = false) {
        sendFrame(isText ? OPCODE_TEXT : OPCODE_BINARY, msg, len);
    }
    void sendFrame (const std::string &msg) {
        sendFrame(OPCODE_TEXT, reinterpret_cast<const uint8_t*>(msg.c_str()),
                msg.length());
    }
    void sendFrameAsBase64 (const uint8_t *msg, int32_t len);
    void sendHead (int32_t status, const std::vector<std::string> &headers = {},
            int32_t len = -1);
    void sendBody (const uint8_t *buf, int32_t len);
    void sendBody (const std::string &msg);
    virtual void endResponse () {}
    void sendResponse (int32_t status,
            const std::vector<std::string> &headers = {},
            const std::string &msg = "");
    void sendFile (const std::string &path,
            const std::vector<std::string> &headers = {});

    // FileReactor implementations
    void read ();
    void write ();

    // added for Transport::requestSend()
    virtual void idle () {}

    void dump () {
        log.debug("%s", requestLine.c_str());
        for (auto h: headers) {
            log.debug("    %s: %s", h.first.c_str(), h.second.c_str());
        }
    }

protected:

    void send (const uint8_t *buf, int32_t len);

    enum STATE {
        BEGIN_READ,
        READ_HEADS,
        TRY_HANDSHAKE,
        READ_BODY,
        READ_WEBSOCKET,
        READ_WEBSOCKET_PAYLOAD,
        READ_WEBSOCKET_76,
        READ_WEBSOCKET_PAYLOAD_76
    } state;

    enum OPCODE {
        OPCODE_CONTINUATION = 0x00,
        OPCODE_TEXT = 0x01,
        OPCODE_BINARY = 0x02,
        OPCODE_CLOSE = 0x08,
        OPCODE_PING = 0x09,
        OPCODE_PONG = 0x0a
    } opcode;
    bool fin;

    Log log;

private:

    uint8_t frameMask[4]; // temporal data
    pthread_mutex_t mutex;

    bool parseHeader ();
    bool handshake ();
    bool readWebSocketHeader ();
    bool tryWrite ();
    void sendFrame (OPCODE op, const uint8_t *msg, int32_t len);
    void send (const std::string &msg); // for convenience
};

}
#endif
