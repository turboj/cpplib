#ifndef ACBA_UPDATE_H
#define ACBA_UPDATE_H

#include <algorithm>
#include <vector>
#include <stdio.h>

namespace acba {

template <typename ...TS>
struct Listener {
    virtual void onUpdate (TS ...args) = 0;
};

template <typename ...TS>
struct Update {

    typedef Listener<TS...> L;

    void notify (TS ...args) {
        for (auto l: listeners) {
            l->onUpdate(args...);
        }
    }

    void addListener (L *l) {
        for (auto i: listeners) {
            if (i == l) return;
        }
        listeners.push_back(l);
    }

    void removeListener (L *l) {
        auto i = std::find(listeners.begin(), listeners.end(), l);
        if (i == listeners.end()) return;
        listeners.erase(i);
    }

private:
    std::vector<L*> listeners;
};

}
#endif
