#include <sys/stat.h>
#include <string.h>

#include <string>

#include "path_util.h"

using namespace acba;
using namespace std;

void acba::makeBaseDir (const char *path)
{
    const char *slash = path;
    while (true) {
        slash = strchr(slash, '/');
        if (!slash) break;

        string dir(path, slash - path);
        mkdir(dir.c_str(), 0755);
        slash++;
    }
}

string acba::escapeSlash (string path)
{
    for (auto &c: path) {
        if (c == '/') c = '_';
    }
    return path;
}
