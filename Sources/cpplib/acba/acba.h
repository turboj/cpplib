#ifndef ACBA_H
#define ACBA_H

#include <stddef.h>

#include "Ref.h"
#include "Update.h"
#include "Buffer.h"
#include "Log.h"
#include "ScopedLock.h"
#include "CondVar.h"
#include "Variant.h"

template <class T, class M>
inline constexpr ptrdiff_t offset_of (const M T::*member) {
    return reinterpret_cast<ptrdiff_t>(&(reinterpret_cast<T*>(0)->*member));
}

template <class T, class M>
inline constexpr T* container_of (M *ptr, const M T::*member) {
    return reinterpret_cast<T*>(reinterpret_cast<intptr_t>(ptr) -
            offset_of(member));
}

typedef unsigned char byte_t;

#define IS_FIRST_TIME ({static bool did = false; !(did?:(did=true,false));})
#define IS_EACH_TIME(c) ({static unsigned int _cnt = 0; !(_cnt++ % (c));})

#define SAFEREAD_CHECKSIZE(b, e, sz, msg...) do { \
    if ((b) + (sz) > (e)) { \
        SAFEREAD_LOG(msg); \
        SAFEREAD_RETURN; \
    } \
} while (0);

#define SAFEREAD_SKIP(b, e, sz, msg...) do { \
    SAFEREAD_CHECKSIZE(b, e, sz, msg); \
    b += (sz); \
} while (0);

#define SAFEREAD_U8(b, e, msg...) ({ \
    SAFEREAD_CHECKSIZE(b, e, 1, msg); \
    *b++; \
})

#define SAFEREAD_U16(b, e, msg...) ({ \
    SAFEREAD_CHECKSIZE(b, e, 2, msg); \
    uint16_t __val = (b[0] << 8) + b[1]; \
    b += 2; \
    __val; \
})

#define SAFEREAD_U24(b, e, msg...) ({ \
    SAFEREAD_CHECKSIZE(b, e, 3, msg); \
    uint32_t __val = (b[0] << 16) + (b[1] << 8) + b[2]; \
    b += 3; \
    __val; \
})

#define SAFEREAD_U32(b, e, msg...) ({ \
    SAFEREAD_CHECKSIZE(b, e, 4, msg); \
    uint32_t __val = (b[0] << 24) + (b[1] << 16) + (b[2] << 8) + b[3]; \
    b += 4; \
    __val; \
})

#define SAFEREAD_U64(b, e, msg...) ({ \
    SAFEREAD_CHECKSIZE(b, e, 8, msg); \
    uint64_t __val = *b++; \
    __val = (__val << 8) + *b++; \
    __val = (__val << 8) + *b++; \
    __val = (__val << 8) + *b++; \
    __val = (__val << 8) + *b++; \
    __val = (__val << 8) + *b++; \
    __val = (__val << 8) + *b++; \
    __val = (__val << 8) + *b++; \
})

#endif
