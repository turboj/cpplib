#ifndef ACBA_DOWNLOAD_TRACER
#define ACBA_DOWNLOAD_TRACER

#include <stdint.h>

#include <map>

namespace acba {

struct DownloadTracer {

    void clear ();

    /// Inform that a fragment of given offset and length was received.
    void markFragment (uint32_t off, uint32_t len);

    /// Returns total number of continuous bytes received from offset 0
    uint32_t getContinuousSize ();

    /// Returns total number of received bytes received
    uint32_t getTotalSize ();

    /// Returns true if multiple chunks exist.
    bool isSparse ();

    void getChunks (std::map<uint32_t, uint32_t> &outChunks);

private:
    std::map<uint32_t, uint32_t> chunks; // off, end
};

}
#endif
