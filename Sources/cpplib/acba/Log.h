#ifndef ACBA_LOG_H
#define ACBA_LOG_H

#include <stdlib.h>
#include <stdarg.h>
#include <stdio.h>
#include <string>

#define ACBA_LOG_NOLOG 0
#define ACBA_LOG_FATAL 1
#define ACBA_LOG_ERROR 2
#define ACBA_LOG_WARN 3
#define ACBA_LOG_INFO 4
#define ACBA_LOG_DEBUG 5
#define ACBA_LOG_TRACE 6

#ifndef ACBA_LOG_COMPILE_LEVEL
#ifdef AL_CF_NO_LOG
#define ACBA_LOG_COMPILE_LEVEL ACBA_LOG_NOLOG
#else
#define ACBA_LOG_COMPILE_LEVEL ACBA_LOG_TRACE
#endif
#endif

#ifndef ACBA_LOG_LEVEL
#define ACBA_LOG_LEVEL ACBA_LOG_INFO
#endif

namespace acba {

struct Log {

    struct Group {
        Group (int lv, int _id = -1): level(lv), id(_id) {}
        int level;
        int id;
    };

    Log (const std::string &mod, const std::string &pre = "");
    Log (Log &l, const std::string &pre);

    Group *group;
    std::string prefix;

    static unsigned int gettid ();

#if ACBA_LOG_COMPILE_LEVEL < ACBA_LOG_FATAL
    inline bool F () const { return false; }
    inline void fatal (const char *fmt, ...) const {}
#else
    inline bool F () const {
        return group->level >= ACBA_LOG_FATAL;
    }
    inline void fatal (const char *fmt, ...) const {
        if (group->level < ACBA_LOG_FATAL) return;

        va_list args;
        va_start(args, fmt);
        printf(ACBA_LOG_FATAL, fmt, args);
        va_end(args);
    }
#endif

#if ACBA_LOG_COMPILE_LEVEL < ACBA_LOG_ERROR
    inline bool E () const { return false; }
    inline void error (const char *fmt, ...) const {}
#else
    inline bool E () const {
        return group->level >= ACBA_LOG_ERROR;
    }
    inline void error (const char *fmt, ...) const {
        if (group->level < ACBA_LOG_ERROR) return;

        va_list args;
        va_start(args, fmt);
        printf(ACBA_LOG_ERROR, fmt, args);
        va_end(args);
    }
#endif

#if ACBA_LOG_COMPILE_LEVEL < ACBA_LOG_WARN
    inline bool W () const { return false; }
    inline void warn (const char *fmt, ...) const {}
#else
    inline bool W () const {
        return group->level >= ACBA_LOG_WARN;
    }
    inline void warn (const char *fmt, ...) const {
        if (group->level < ACBA_LOG_WARN) return;

        va_list args;
        va_start(args, fmt);
        printf(ACBA_LOG_WARN, fmt, args);
        va_end(args);
    }
#endif

#if ACBA_LOG_COMPILE_LEVEL < ACBA_LOG_INFO
    inline bool I () const { return false; }
    inline void info (const char *fmt, ...) const {}
#else
    inline bool I () const {
        return group->level >= ACBA_LOG_INFO;
    }
    inline void info (const char *fmt, ...) const {
        if (group->level < ACBA_LOG_INFO) return;

        va_list args;
        va_start(args, fmt);
        printf(ACBA_LOG_INFO, fmt, args);
        va_end(args);
    }
#endif

#if ACBA_LOG_COMPILE_LEVEL < ACBA_LOG_DEBUG
    inline bool D () const { return false; }
    inline void debug (const char *fmt, ...) const {}
#else
    inline bool D () const {
        return group->level >= ACBA_LOG_DEBUG;
    }
    inline void debug (const char *fmt, ...) const {
        if (group->level < ACBA_LOG_DEBUG) return;

        va_list args;
        va_start(args, fmt);
        printf(ACBA_LOG_DEBUG, fmt, args);
        va_end(args);
    }
#endif

#if ACBA_LOG_COMPILE_LEVEL < ACBA_LOG_TRACE
    inline bool T () const { return false; }
    inline void trace (const char *fmt, ...) const {}
#else
    inline bool T () const {
        return group->level >= ACBA_LOG_TRACE;
    }
    inline void trace (const char *fmt, ...) const {
        if (group->level < ACBA_LOG_TRACE) return;

        va_list args;
        va_start(args, fmt);
        printf(ACBA_LOG_TRACE, fmt, args);
        va_end(args);
    }
#endif

    inline int getLevel () const {
        return group->level;
    }

    inline void setLevel (int lv) {
        if (lv < ACBA_LOG_NOLOG || lv > ACBA_LOG_TRACE) return;
        group->level = lv;
    }

    void printf (int lv, const char *fmt, va_list args) const;
    static void puts (const char *s);

    static Group *getGroup (const std::string &mod);
};

}

#endif
