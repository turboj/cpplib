#include <assert.h>
#include <unistd.h>
#include <sys/syscall.h>

#include <stdlib.h>
#include <stdarg.h>
#include <stdio.h>
#include <strings.h>

#include <string>
#include <map>

#include "Log.h"

#define ACBA_LOG_STDOUT 0
#define ACBA_LOG_ANDROID 1
#define ACBA_LOG_CAPTOR 2

#ifndef ACBA_LOG_IMPL
#ifdef ACBA_LOG_USE_CAPTOR
#define ACBA_LOG_IMPL ACBA_LOG_CAPTOR
#elif __ANDROID__
#define ACBA_LOG_IMPL ACBA_LOG_ANDROID
#else
#define ACBA_LOG_IMPL ACBA_LOG_STDOUT
#endif
#endif

#if ACBA_LOG_IMPL == ACBA_LOG_CAPTOR
#include "al_debug.h"
#elif ACBA_LOG_IMPL == ACBA_LOG_ANDROID
#include <android/log.h>
#endif

#define LINE_BUFFER_SIZE 512
#define BIG_BUFFER_SIZE 65536

using namespace std;
using namespace acba;

Log::Log (const string &mod, const string &pre):
        group(getGroup(mod)), prefix(mod+": "+pre) {}

Log::Log (Log &l, const string &pre):
        group(l.group), prefix(l.prefix + pre) {}

unsigned int Log::gettid () {
    return (unsigned int)syscall(SYS_gettid);
}

void Log::printf (int lv, const char *fmt, va_list _args) const
{
    int len;
    char _buf[LINE_BUFFER_SIZE];

    char *buf = _buf;
    int buflen = sizeof(_buf);

    va_list args;

retry:

#if ACBA_LOG_IMPL == ACBA_LOG_ANDROID
    len = snprintf(buf, buflen, "%05x %s", gettid(), prefix.c_str());
#else
    {
        static int lvcs[] = { '?', 'F', 'E', 'W', 'I', 'D', 'T' };
        len = snprintf(buf, buflen, "%c %05x %s", lvcs[lv], gettid(),
                prefix.c_str());
    }
#endif
    assert(len < buflen); // condition for no truncation
    va_copy(args, _args);
    len += vsnprintf(buf + len, buflen - len, fmt, args);
    va_end(args);
    if (len >= buflen) { // buffer size is not sufficient
        if (buf == _buf) {
            buflen = BIG_BUFFER_SIZE;
            buf = (char*)malloc(buflen);
            goto retry;
        }
        sprintf(buf + buflen - 15, "...(truncated)");
    } else if (buf[len - 1] == '\n') {
        buf[len - 1] = '\0';
    }
#if ACBA_LOG_IMPL == ACBA_LOG_CAPTOR
    {
        static int lvmap[] = { EL_NONE, EL_ERR, EL_ERR, EL_WARN, EL_MSG,
                EL_DBG, EL_DBG };
        al_log((lvmap[lv], (group->id == -1) ? ED_ACBA : group->id, "%s\n",
                buf));
    }
#elif ACBA_LOG_IMPL == ACBA_LOG_ANDROID
    {
        static int lvmap[] = {
                ANDROID_LOG_SILENT,
                ANDROID_LOG_FATAL,
                ANDROID_LOG_ERROR,
                ANDROID_LOG_WARN,
                ANDROID_LOG_INFO,
                ANDROID_LOG_DEBUG,
                ANDROID_LOG_VERBOSE
        };
        __android_log_write(lvmap[lv], "atsc3", buf);
    }
#elif ACBA_LOG_IMPL == ACBA_LOG_STDOUT
    puts(buf);
#endif
    if (buf != _buf) free(buf);
}

void Log::puts (const char *s)
{
    ::puts(s);
    fflush(stdout);
}

static int getLevelFromEnv (string var)
{
    char *s = &var[0];
    for (; *s; s++) {
        if (*s >= 'a' && *s <= 'z') {
            *s -= 'a' - 'A';
        }
    }

    char *e = getenv(var.c_str());
    if (!e) return -1;

    if (strcasecmp(e, "TRACE") == 0) return ACBA_LOG_TRACE;
    if (strcasecmp(e, "DEBUG") == 0) return ACBA_LOG_DEBUG;
    if (strcasecmp(e, "INFO") == 0) return ACBA_LOG_INFO;
    if (strcasecmp(e, "WARN") == 0) return ACBA_LOG_WARN;
    if (strcasecmp(e, "ERROR") == 0) return ACBA_LOG_ERROR;
    if (strcasecmp(e, "FATAL") == 0) return ACBA_LOG_FATAL;
    if (strcasecmp(e, "NOLOG") == 0) return ACBA_LOG_NOLOG;

    fprintf(stderr, "Invalid value:%s for %s.\n", e, s);
    return -1;
}

Log::Group *Log::getGroup (const string &mod)
{
    static int defaultLevel = -1;
    static map<string,Group*> groups;

    int lv;

    // init
    if (defaultLevel == -1) {
        defaultLevel = ACBA_LOG_LEVEL;
        lv = getLevelFromEnv("LOG_LEVEL");
        if (lv != -1) defaultLevel = lv;
    }

    Group *&grp = groups[mod];
    if (!grp) {
#if ACBA_LOG_IMPL == ACBA_LOG_CAPTOR
        for (int i = 0; al_logNames[i]; i++) {
            if (strcasecmp(mod.c_str(), al_logNames[i]) != 0) continue;

            grp = new Group(ACBA_LOG_LEVEL, i);
            switch (al_logLevels[i]) {
            case EL_NONE: grp->level = ACBA_LOG_NOLOG; break;
            case EL_ERR: grp->level = ACBA_LOG_ERROR; break;
            case EL_WARN: grp->level = ACBA_LOG_WARN; break;
            case EL_MSG: grp->level = ACBA_LOG_INFO; break;
            case EL_DBG: grp->level = ACBA_LOG_TRACE; break;
            }
            lv = getLevelFromEnv(string("LOG_LEVEL_")+mod);
            if (lv != -1) grp->level = lv;
            return grp;
        }
#endif
        lv = getLevelFromEnv(string("LOG_LEVEL_")+mod);
        grp = new Group((lv != -1)? lv: defaultLevel);
    }
    return grp;
}
