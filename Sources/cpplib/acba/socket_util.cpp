#include "socket_util.h"

#include <strings.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <netdb.h>
#include <sys/select.h>
#include <sys/uio.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>

#include "Log.h"

#ifndef ROP_BIND_HOST_DEFAULT
#define ROP_BIND_HOST_DEFAULT 0
#endif

#define ACCEPT_IPV4_ONLY 1
#ifndef __linux__
#define SOCK_CLOEXEC 0
#endif

#if __APPLE__
#ifndef SOCK_NONBLOCK
#include <fcntl.h>
#define SOCK_NONBLOCK O_NONBLOCK
#endif
#endif

static acba::Log l("socket");

int create_socket (const char *host, int port)
{
    int ret = -1;
    int sfd = -1;

    // construct params for addr resolving
    struct addrinfo hints;
    bzero(&hints, sizeof(hints));
    hints.ai_family = AF_UNSPEC;    /* Allow IPv4 or IPv6 */
    hints.ai_socktype = SOCK_STREAM; /* Datagram socket */
    hints.ai_flags = 0;
    hints.ai_protocol = 0;          /* Any protocol */

    // get addr
    struct addrinfo *srv = NULL;
    if (!host || strlen(host) == 0) goto exit;
    char service[10];
    sprintf(service, "%d", port);
    if (getaddrinfo(host, service, &hints, &srv)) goto exit;

    // connect
    sfd = socket(srv->ai_family, srv->ai_socktype | SOCK_CLOEXEC |
            SOCK_NONBLOCK, srv->ai_protocol);
    if (sfd == -1) goto exit;
    if (connect(sfd, srv->ai_addr, srv->ai_addrlen)) {
        if (errno != EINPROGRESS) goto exit;
    }

    // success
    ret = sfd;

exit:
    if (ret == -1 && sfd != -1) close(sfd);
    if (srv) freeaddrinfo(srv);
    return ret;
}

int create_server_socket (int port, const char* host)
{
    int ret = -1;
    int fd = -1;

    struct addrinfo *sai = NULL; // server adddress info
    char serv[8];

    // prepare service argument
    sprintf(serv, "%7d", port);

    // construct params for addr resolving
    struct addrinfo hints;
    bzero(&hints, sizeof(hints));
#if ACCEPT_IPV4_ONLY
    hints.ai_family = AF_INET;
#else
    hints.ai_family = AF_UNSPEC;
#endif
    hints.ai_socktype = SOCK_STREAM; // Datagram socket
    hints.ai_flags = AI_PASSIVE; // Allow wildcard IP address
    hints.ai_protocol = 0; // Any protocol

    if (!host) host = ROP_BIND_HOST_DEFAULT;

    // get addr
    if (getaddrinfo(host, serv, &hints, &sai)) {
        l.error("getaddrinfo failed! host:%s, err:%s\n", host, strerror(errno));
        goto exit;
    }
    if (sai->ai_family != AF_INET6 && sai->ai_family != AF_INET) {
        l.error("Unknown ai family: %d\n", sai->ai_family);
        goto exit;
    }

    // create socket
    fd = socket(sai->ai_family, SOCK_STREAM | SOCK_CLOEXEC, 0);
    if (fd < 0) {
        l.error("socket() failed!\n");
        goto exit;
    } else {
        int opt_val = 1;
        setsockopt(fd, SOL_SOCKET, SO_REUSEADDR, &opt_val, sizeof(opt_val));
    }

    // bind & listen
    if (bind(fd, sai->ai_addr, sai->ai_addrlen)) {
        l.error("bind() failed! %s\n", strerror(errno));
        goto exit;
    }
    listen(fd, 1);

    // success;
    ret = fd;

exit:
    if (ret < 0 && fd != -1) close(fd);
    if (sai) freeaddrinfo(sai);
    return ret;
}

int create_unixdomain_socket (const char *path)
{
    int ret = -1;
    int sfd = -1;

    if (!path || strlen(path) == 0) goto exit;

    // create socket
    sfd = socket(AF_UNIX, SOCK_STREAM | SOCK_CLOEXEC, 0);
    if (sfd == -1) {
        l.error("socket() failed. %s", strerror(errno));
        goto exit;
    }

    // connect
    {
        struct sockaddr_un addr;
        memset(&addr, 0, sizeof(addr));
        addr.sun_family = AF_UNIX;
        strncpy(addr.sun_path, path, sizeof(addr.sun_path) - 1);
        if (connect(sfd, (struct sockaddr *)&addr, sizeof(addr))) {
            l.error("connect() failed. %s", strerror(errno));
            goto exit;
        }
    }

    // success
    ret = sfd;

exit:
    if (ret == -1 && sfd != -1) close(sfd);
    return ret;
}

int create_unixdomain_server_socket (const char *path)
{
    int ret = -1;
    int fd = -1;

    // create socket
    fd = socket(AF_UNIX, SOCK_STREAM | SOCK_CLOEXEC, 0);
    if (fd < 0) {
        l.error("socket() failed!\n");
        return -1;
    }
    int opt_val = 1;
    setsockopt(fd, SOL_SOCKET, SO_REUSEADDR, &opt_val, sizeof(opt_val));

    // bind & listen
    struct sockaddr_un addr;
    memset(&addr, 0, sizeof(addr));
    addr.sun_family     = AF_UNIX;
    strncpy(addr.sun_path, path, sizeof(addr.sun_path) - 1);
    unlink(addr.sun_path);

    if (bind(fd, (struct sockaddr *)&addr, sizeof(addr))) {
        l.error("bind() failed! %s\n", strerror(errno));
        goto exit;
    }

    listen(fd, 1);

    // success;
    ret = fd;

exit:
    if (ret < 0 && fd != -1) close(fd);
    return ret;
}

int create_multicast_socket (uint32_t srcIp, uint32_t dstIp, uint16_t port)
{
    struct sockaddr_in addr;

    int fd = socket(AF_INET, SOCK_DGRAM, 0);
    if (fd < 0) return -1;

    u_int yes=1;
    if (setsockopt(fd,SOL_SOCKET,SO_REUSEADDR,&yes,sizeof(yes)) < 0) {
        l.error("SO_REUSEADDR failed");
        close(fd);
        return -1;
    }

    /* set up destination address */
    memset(&addr,0,sizeof(addr));
    addr.sin_family=AF_INET;
    addr.sin_addr.s_addr=htonl(dstIp);
    addr.sin_port=htons(port);

    /* bind to receive address */
    if (::bind(fd, (struct sockaddr *)&addr, sizeof(addr)) < 0) {
        l.error("bind failed");
        close(fd);
        return -1;
    }

    if (srcIp) {
        struct ip_mreq_source imr;
        imr.imr_multiaddr.s_addr = htonl(dstIp);
        imr.imr_interface.s_addr = htonl(INADDR_ANY);
        imr.imr_sourceaddr.s_addr = htonl(srcIp);
        if (setsockopt(fd, IPPROTO_IP, IP_ADD_SOURCE_MEMBERSHIP,
                (const char*)&imr, sizeof (struct ip_mreq_source)) < 0) {
            l.error("IP_ADD_SPOURCE_MEMBERSHIP failed");
            close(fd);
            return -1;
        }
    } else {
        /* use setsockopt() to request that the kernel join a multicast group */
        struct ip_mreq mreq;
        mreq.imr_multiaddr.s_addr=htonl(dstIp);
        mreq.imr_interface.s_addr=htonl(INADDR_ANY);
        if (setsockopt(fd, IPPROTO_IP, IP_ADD_MEMBERSHIP, &mreq,
                sizeof(mreq)) < 0) {
            l.error("IP_ADD_MEMBERSHIP failed");
            close(fd);
            return -1;
        }
    }

    return fd;
}

