#ifndef ACBA_DATETIME_H
#define ACBA_DATETIME_H

#include <string>

namespace acba {

double parseDatetime (const char *str);

/// Returns approximated seconds
double parseDuration (const char *str);

std::string toDatetime (double t);

double currentUnixTime ();

}
#endif
