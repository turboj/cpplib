#ifndef HTTP_SERVER_H
#define HTTP_SERVER_H

#include <map>
#include <string>

#include "EventDriver.h"
#include "HttpReactor.h"
#include "acba.h"

namespace acba {

struct HttpRequestHandler: RefCounter<HttpRequestHandler> {
    virtual void handleHttpRequest (HttpReactor &r) {
        r.abort("Unexpected http request");
    }
    virtual void handleWebsocketOpen (HttpReactor &r) {
        r.abort("Unexpected websocket connection");
    }
    virtual void handleWebsocketMessage (HttpReactor &r) {
        r.abort("Unexpected websocket binrary frame");
    }
    virtual void handleWebsocketTextMessage (HttpReactor &r) {
        r.abort("Unexpected websocket text frame");
    }
    virtual void handleWebsocketClose (HttpReactor &r) {
        char str[128];
        sprintf(str, "Closing websocket handler %p", this);
        r.abort(str);
    }
};

struct HttpRequestAcceptor {
    /**
     * Provide handler for given request.
     * HttpRequestHandler can handle both of http request and websocket.
     * If you need to support only one of them, check r.isWebsocket()
     * and return null for unsupported request type.
     * @arg basePath is where this acceptor is serving on.
     */
    virtual HttpRequestHandler *acceptHttpRequest (const std::string &basePath,
            HttpReactor *r) = 0;
};


struct HttpServerReactor: acba::FileReactor {

    HttpServerReactor (bool safe = true): acba::FileReactor(safe),
            log("ed", "hsrv ") {}

    int start (int port, acba::EventDriver *ed) {
        return start(port, 0, ed);
    }

    int start (int port, const char* host, acba::EventDriver *ed);
    void stop ();
    void read ();
    void bind (const std::string &path, HttpRequestAcceptor *a);

    std::map<std::string, HttpRequestAcceptor *> requestAcceptors;

    acba::Log log;
};

std::string url_encode (const std::string &s);

}
#endif

