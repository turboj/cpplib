#ifndef CONDVAR_H
#define CONDVAR_H

#include <stdint.h>
#include <pthread.h>

namespace acba {

class CondVar {
    pthread_cond_t cond;

public:
    CondVar ();
    ~CondVar () {
        pthread_cond_destroy(&cond);
    }

    int wait (pthread_mutex_t *mon) {
        return pthread_cond_wait(&cond, mon);
    }

    int wait (pthread_mutex_t *mon, int32_t delayInMilis);

    int signal () {
        return pthread_cond_signal(&cond);
    }

    int broadcast () {
        return pthread_cond_broadcast(&cond);
    }
};

}

#endif
