LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)
LOCAL_MODULE := pugixml
LOCAL_SRC_FILES += \
        $(wildcard $(LOCAL_PATH)/src/*.cpp)
LOCAL_EXPORT_C_INCLUDES := $(LOCAL_PATH)/src

include $(BUILD_STATIC_LIBRARY)
