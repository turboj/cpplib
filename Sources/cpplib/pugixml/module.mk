$(call begin-target, pugixml, staticlib)
    LOCAL_SRCS := $(wildcard $(LOCAL_PATH)/src/*.cpp)
    EXPORT_CXXFLAGS := -I$(LOCAL_PATH)/src
$(end-target)

$(call begin-target, test_pugixml_basic, executable)
    LOCAL_LABELS := TEST
    LOCAL_SRCS := $(LOCAL_PATH)/smoketest/test1.cpp
    LOCAL_IMPORTS := pugixml-staticlib
$(end-target)

$(call begin-target, test_pugixml_xpath, executable)
    LOCAL_LABELS := TEST
    LOCAL_SRCS := $(LOCAL_PATH)/smoketest/test2.cpp
    LOCAL_IMPORTS := pugixml-staticlib
$(end-target)
