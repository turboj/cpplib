#include <string>
#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <sstream>
#include <termios.h>
#include <fcntl.h>
#include <unistd.h>

#include "atsc3_model.h"
#include "atsc3_task.h"
#include "atsc3_platform.h"
#include "atsc3_web.h"
#include "acba.h"

#if USE_UNIFIEDTV_ATSC3
#include "unifiedtv_atsc3.h"
#endif

#if USE_TUNER_CLIENT
#include "tuner_client.h"
#endif

using namespace std;
using namespace acba;
using namespace atsc3;

static Log l("main");

#define TTY_DEVICE	"/dev/tty"

static int tty_fd = -1;
static struct termios save_tty;

static int setupTty() {
    struct termios tty;

    /* open controlling terminal */

    tty_fd = open(TTY_DEVICE, O_RDONLY);
    if (tty_fd == -1) {
        return -1;
    }

    int flags = fcntl(tty_fd, F_GETFL, 0);
    fcntl(tty_fd, F_SETFL, flags | O_NONBLOCK);

    /* save current terminal and signal settings */
    if (tcgetattr(tty_fd, &save_tty) == -1) {
        return -1;
    }

    /* turn off echo and canonical mode */
    tty = save_tty;

    tty.c_lflag &= ~(ECHO | ICANON);

    /* set VMIN = VTIME = 0 so read() always returns immediately */
    tty.c_cc[VMIN]  = 0;
    tty.c_cc[VTIME] = 0;

    if (tcsetattr(tty_fd, TCSAFLUSH, &tty) == -1) {
        goto fail;
    }

    return 0;

fail:

    return -1;
}

static int restoreTty() {
    struct termios tty;
    int result = 0;

    if (tcgetattr(tty_fd, &tty) == 0 &&
            tcsetattr(tty_fd, TCSADRAIN, &save_tty) == -1) {
        l.info("restoreTty:tcsetattr");
        result = -1;
    }

    save_tty = tty;

    if (close(tty_fd) == -1) {
        l.info("restoreTty:close");
        result = -1;
    }

    tty_fd = -1;

    return result;
}

struct ChannelZapper: FileReactor {
    ChannelZapper (): index(0) {
    }

#define READ_SIZE 32
    void read () override {
        char k[READ_SIZE];
        ssize_t sz = ::read(getFd(), k, READ_SIZE);
        if (sz <= 0) exit(1);

        if (sz == 3 && k[0] == 27 && k[1] == 91) {
            switch (k[2]) {
            case 65: // UP
                changeChannel(-1);
                break;
            case 66: // DOWN
                changeChannel(1);
                break;
            }
        } else if (sz == 1) {
            switch (k[0]) {
            case 's': // save
                task.serviceManager.save();
                break;

            case 'l': // load
                task.serviceManager.load();
                break;

            case 'c': // load
                task.serviceManager.clear();
                break;

            case 'd': // dump
                model.dump();
                break;

            case ' ': // background toggle
                if (model.currentService) {
                    auto i = model.backgroundServices.find(model.currentService);
                    if (i == model.backgroundServices.end()) {
                        l.info("Add background service %s",
                                model.currentService->globalId.c_str());
                        model.backgroundServices.insert(model.currentService);
                    } else {
                        l.info("Remove background service %s",
                                model.currentService->globalId.c_str());
                        model.backgroundServices.erase(model.currentService);
                    }
                    //model.backgroundServicesUpdate.notify();
                }
                break;
#if USE_TUNER_CLIENT
            case 'q': // query gateway
                l.info("Searching Tuner Servers...");
                {
                    using namespace tuner;
                    vector<ServerInfo> svrs;
                    client().searchServers(svrs, 1.0, 2.0);

                    if (svrs.size() == 0) {
                        l.info("No gateway servers found");
                        break;
                    }

                    for (auto &s: svrs) {
                        l.info("Detected Tuner Server %s uuid:%s loc:%s:%d",
                                s.name.c_str(), s.uuid.c_str(),
                                s.host.c_str(), s.port);
                    }

                    l.info("Connecting to the first tuner server...");
                    auto &s = svrs[0];
                    client().connect(s.host.c_str(), s.port);
                }
                break;
#endif
            }
        }

        for (int i = 0; i < sz; i++) {
            l.debug("KEY CODE: %d", k[i]);
        }
    }

    void changeChannel (int inc) {
        vector<Service*> services;
        for (auto bs: model.broadcastStreams) {
            for (auto sg: bs.second->serviceGroups) {
                for (auto s: sg.second->services) {
                    switch (s.second->category) {
                    case 1: // Linear A/V
                    case 2: // Linear A
                    case 3: // App-based
                        services.push_back(s.second);
                        break;
                    }
                }
            }
        }

        Service *s = nullptr;
        if (services.size() == 0) {
            index = 0;
        } else {
            index += inc;
            while (index < 0) index += services.size() + 1;
            while (index >= services.size() + 1) index -= services.size() + 1;
        }

        if (index) s = services[index - 1];

        if (model.currentService.get() != s) {
            if (index == 0) {
                l.info("Select NO Service");
            } else {
                for (int i = 0; i < services.size(); i++) {
                    l.info(" %s %s (%s)",
                        (i == index -1) ? "->" : "  ",
                        services[i]->xml.attribute("shortServiceName").value(),
                        services[i]->globalId.c_str());
                }
                usleep(300000); // give a time to see the service list.
            }
            model.selectService(s);
        }
    }

    int index;
};

#include <sys/socket.h>
#include <netinet/in.h>

struct FileRequestHandler: HttpRequestHandler {
    void handleHttpRequest (HttpReactor &r) override {
        const char *relpath = r.requestHandlerPath.c_str();
        while (*relpath && *relpath == '/') relpath++;
        if (strstr(relpath, "../")) {
            r.sendResponse(404, {"Content-Type: text/pain"}, "Invalid Path");
            return;
        }

        r.sendFile(relpath);
    }
};

struct HelloRequestHandler: HttpRequestHandler {
    void handleHttpRequest (HttpReactor &r) override {

        sockaddr_in addr;
        socklen_t len = sizeof(addr);
        if (getsockname(r.getFd(), (sockaddr*)&addr, &len)) {
            l.error("getsockname failed! %s", strerror(errno));
            r.sendResponse(404, {"Content-Type: text/plain"}, "Intalnal Error");
            return;
        }

        stringstream ss;
        {
            byte_t *ip = (byte_t*)&addr.sin_addr.s_addr;
            ss << "Socket bound for " << (int)ip[0] << '.' << (int)ip[1]
                    << '.' << (int)ip[2] << '.' << (int)ip[3]
                    << ':' << addr.sin_port << '\n';
            ss << "Handler Path: " << r.requestHandlerPath << '\n';
            ss << "Query: " << r.requestQuery << '\n';
        }
        r.sendResponse(200, {"Content-Type: text/plain"}, ss.str());
    }
};

struct SimpleWebService: HttpRequestAcceptor {
    HttpRequestHandler *acceptHttpRequest (const string &basePath, HttpReactor *r) {
        l.error("accept! %s", basePath.c_str());

        if (basePath == "/cwd") return new FileRequestHandler();
        return new HelloRequestHandler();
    }
};

struct atsc3_WebWindow {
    char url[1024];
    int zIndex;
};

static atsc3_WebWindow *_createWebWindow (const char *url, int zIndex)
{
    atsc3_WebWindow *ww = (atsc3_WebWindow*)malloc(sizeof(*ww));
    memset(ww, 0, sizeof(*ww));
    strncpy(ww->url, url, sizeof(ww->url) - 1);
    ww->zIndex = zIndex;
    l.warn("NOT IMPLEMENTED: createWebWindow(%s) returns %p", url, ww);
    return ww;
}

static void _destroyWebWindow (atsc3_WebWindow *ww)
{
    l.warn("NOT IMPLEMENTED: destroyWebWindow(%p)");
    free(ww);
    return;
}

static void _rmpStart (const char *url)
{
    l.warn("NOT IMPLEMENTED: rmpStart(%s)", url);
}

static void _rmpStop ()
{
    l.warn("NOT IMPLEMENTED: rmpStop()");
}

static const atsc3_KeyInfo _supportedKeys[] = {
    {"SystemOnlyKey0", 10, atsc3_KEYFLAG_SYSTEM_ONLY},
    {"SystemOnlyKey1", 11, atsc3_KEYFLAG_SYSTEM_ONLY},
    {"SystemOnlyKey2", 12, atsc3_KEYFLAG_SYSTEM_ONLY},
    {"SystemOnlyKey3", 13, atsc3_KEYFLAG_SYSTEM_ONLY},
    {"SystemDefaultKey0", 20},
    {"SystemDefaultKey1", 21},
    {"SystemDefaultKey2", 22},
    {"SystemDefaultKey3", 23},
    {"ApplicationDefaultKey0", 30, atsc3_KEYFLAG_APPLICATION_DEFAULT},
    {"ApplicationDefaultKey1", 31, atsc3_KEYFLAG_APPLICATION_DEFAULT},
    {"ApplicationDefaultKey2", 32, atsc3_KEYFLAG_APPLICATION_DEFAULT},
    {"ApplicationDefaultKey3", 33, atsc3_KEYFLAG_APPLICATION_DEFAULT},
};

static const atsc3_KeyInfo *_getSupportedKeys (int *num)
{
    l.warn("NOT IMPLEMENTED: _getSupportedKeys()");
    *num = sizeof(_supportedKeys) / sizeof(_supportedKeys[0]);
    return _supportedKeys;
}

static void _routeKeys (int *keys, int cnt, atsc3_WebWindow *ww)
{
    l.warn("NOT IMPLEMENTED: routeKeys()");
    for (int i = 0; i < cnt; i++) {
        auto &ki = _supportedKeys[keys[i]];
        l.warn("NOT IMPLEMENTED:     %s -> %s", ki.name,
                ww ? ww->url : "<released>");
    }
}

static void _getTuners (atsc3_Tuner **tuners, int *nTuners)
{
    l.warn("NOT IMPLEMENTED: _getTuners()");
    *nTuners = 0;
}

static void _getTunerStatus (atsc3_Tuner *tuner, atsc3_TunerStatus *st)
{
    l.warn("NOT IMPLEMENTED: _getTunerStatus()");
    return;
}

static bool _startTune (atsc3_Tuner *tuner, atsc3_TuneParams *params)
{
    l.warn("NOT IMPLEMENTED: _startTune()");
    return false;
}

static void _stopTune (atsc3_Tuner *tuner)
{
    l.warn("NOT IMPLEMENTED: _stopTune()");
}

static void _setMulticasts (atsc3_Tuner *tuner, const atsc3_MulticastAddress *addrs, int numAddrs)
{
    l.warn("NOT IMPLEMENTED: _setMulticasts()");
}

static void _rmpSetScalePosition (double scale, double xPos, double yPos)
{
    l.warn("NOT IMPLEMENTED: _rmpSetScalePosition()");
}

static void _rmpGetRMPMediaTime (atsc3_RMPMediaTime *rmpMediaTime)
{
    l.warn("NOT IMPLEMENTED: _rmpGetRMPMediaTime()");
}

static double _rmpGetPlaybackRate ()
{
    l.warn("NOT IMPLEMENTED: _rmpGetPlaybackRate()");
    return 0;
}

static void _rmpReceivedLicenseResponse (const char *systemId, const char *message)
{
    l.warn("NOT IMPLEMENTED: _rmpReceivedLicenseResponse()");
}

static void _rmpCacheLicense (const char *systemId, unsigned char* buf, int size)
{
    l.warn("NOT IMPLEMENTED: _rmpCacheLicense");
}

static void _rmpSetPreference (bool ccEnabled, const char *ccLeng, const char *ccDisplayPref,
        const char *audioLang, bool videoDescEnabled, const char *videoDescLang)
{
    l.warn("NOT IMPLEMENTED: _rmpSetPreference");
}

static const char* _rmpGetTracks ()
{
    l.warn("NOT IMPLEMENTED: _rmpGetTracks");
    return "";
}

static bool _rmpSelectTrack (int selectionId)
{
    l.warn("NOT IMPLEMENTED: _rmpSelectTrack");
    return false;
}

static bool _rmpSelectTrackExt (bool select, const char* ext)
{
    l.warn("NOT IMPLEMENTED: _rmpSelectTrackExt");
    return false;
}
static double _rmpAudioVolume (double volume)
{
    l.warn("NOT IMPLEMENTED: _rmpAudioVolume");
    return -1;
}

struct atsc3_Platform _platform = {
    {
        "Alticast",
        "Generic Android",
        "00000000-0000-0000-0000-000000000000",
        "00000000-0000-0000-0000-000000000001"
    },
    _createWebWindow,
    _destroyWebWindow,
    _rmpStart,
    _rmpStop,
    _getSupportedKeys,
    _routeKeys,
    _getTuners,
    _getTunerStatus,
    _startTune,
    _stopTune,
    _setMulticasts,
    _rmpSetScalePosition,
    _rmpGetRMPMediaTime,
    _rmpGetPlaybackRate,
    _rmpReceivedLicenseResponse,
    _rmpCacheLicense,
    _rmpSetPreference,
    _rmpGetTracks,
    _rmpSelectTrack,
    _rmpSelectTrackExt,
    _rmpAudioVolume
};

int main (int argc, char **argv)
{
    ChannelZapper zapper;

    if (argv[1] && strcmp(argv[1], "-h") == 0) {
        printf("Usage: %s [-n] [<pcap_stream_files>]\n", argv[0]);
        return -1;
    }

    init(argc, argv, &_platform);

#if USE_UNIFIEDTV_ATSC3
    unifiedtv_atsc3::start();
#endif

#if USE_TUNER_CLIENT
    struct _: Listener<> {
        void onUpdate () override {
            static const char *strstate[] = {
                "STATE_DISCONNECTED",
                "STATE_CONNECTING",
                "STATE_CONNECTED",
            };
            l.info("Tuner Client - %s", strstate[tuner::client().state]);
        }
    } cliListener;
    tuner::client().stateUpdate.addListener(&cliListener);
#endif

    for (int i = 1; argv[i]; i++) {
        if (strcmp(argv[i], "-n") == 0) {
            model.addKnownStreamLocation("net:*");
            continue;
        }
        model.addKnownStreamLocation(string("pcap:") + argv[i]);
    }

    auto ws = new SimpleWebService();
    web.httpServer.bind("/test", ws);
    web.httpServer.bind("/cwd", ws);

    setupTty();
    if (tty_fd >= 0) task.eventDriver.watchFile(tty_fd, EVENT_READ, &zapper);

    task.eventDriver.run();
    return 0;
}
