$(call begin-target, aa, executable)
    LOCAL_LABELS += MAIN
    LOCAL_SRCS := $(wildcard $(LOCAL_PATH)/*.cpp)

    LOCAL_IMPORTS := atsc3-staticlib acba-staticlib

    ifeq ($(USE_UNIFIEDTV_ATSC3),true)
        LOCAL_IMPORTS += unifiedtv_atsc3-staticlib
    endif

    ifeq ($(USE_TUNER_CLIENT),true)
        LOCAL_IMPORTS += tuner_client-staticlib
    endif
$(end-target)
