$(call begin-target, nuvyyo_adaptor, staticlib)
    LOCAL_SRCS := $(wildcard $(LOCAL_PATH)/*.cpp)
    LOCAL_IMPORTS := unifiedtv_native-staticlib
    LOCAL_IMPORTS += acba-staticlib readstream-staticlib
    EXPORT_CFLAGS := -I$(LOCAL_PATH)
    EXPORT_CXXFLAGS := $(EXPORT_CFLAGS)
$(end-target)
