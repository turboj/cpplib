#include <time.h>
#include <unistd.h>

#include "falp_reader.h"
#include "acba.h"

using namespace acba;

static Log l("falp");

int FalpReader::read (const byte_t *d, int sz, Chain *pctx)
{
    //l.info("sz:%d", sz);
    if (sz < 188) return 0;
    auto e = d + 188;

    if (d[0] != 0x47) {
        l.warn("Expected 0x47 for FTLV packet");
        return -1;
    }
    bool has_error = d[1] & 0x80;
    if ((((d[1] & 0x1F) << 8) | d[2]) != 45) {
        l.warn("Expected pid 45");
        return 188;
    }

    const byte_t *tlvstart;
    if (d[1] & 0x40) {
        d += 4;
        tlvstart = d + d[-1];
        if (tlvstart > e) {
            l.warn("Invalid payload start offset:%l", d[-1]);
            return -1;
        }
    } else {
        d += 3;
        tlvstart = nullptr;
    }

    // Silently consume the packet if error flag is set.
    if (has_error) return 188;

    int alignment = 0;
    if (buffer.size() > 0) {
        //l.info("cont");
        if (tlvstart) alignment = buffer.size() + (tlvstart - d);
        buffer.writeBytes(d, e - d);
    } else if (tlvstart && tlvstart < e) {
        //l.info("restart");
        buffer.writeBytes(tlvstart, e - tlvstart);
    } else {
        l.info("drop");
        return 188;
    }

    if (tlvstart && upper) {
        int rsz;
        do {
            rsz = upper->read(buffer.begin(), buffer.size(), pctx);
            if (rsz < 0) {
                if (alignment > 0) {
                    l.warn("invalid alp detected. try to re-align.");
                    rsz = alignment;
                    alignment = 0;
                    buffer.drop(rsz);
                    continue;
                }

                l.warn("invalid alp detected. reset ftlv buffer.");
                buffer.clear();
                return 188; // consume given ts anyway.
            }

            if (alignment > 0) {
                alignment -= rsz;
                if (alignment < 0) {
                    l.warn("alp not aligned with start pointer. "
                            "try to re-align");
                    rsz += alignment;
                    alignment = 0;
                }
            }

            buffer.drop(rsz);
        } while (rsz > 0 && buffer.size() > 0);
    }

    // ad-hoc check in case corrupted buffer grew indefinitely.
    if (buffer.size() > 64*1024) {
        l.warn("buffer grew more than 64k. reset.");
        buffer.clear();
    }

    return 188;
}
