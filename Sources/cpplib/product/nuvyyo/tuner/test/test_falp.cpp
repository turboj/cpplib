#include <unistd.h>
#include <sys/types.h>
#include <fcntl.h>
#include <stdio.h>

#include <map>
#include <string>

#include "acba.h"
#include "stream_reader.h"
#include "pcap_reader.h"
#include "falp_reader.h"
#include "alp_reader.h"
#include "ip_reader.h"
#include "udp_reader.h"

using namespace std;
using namespace acba;

static Log l("test");

struct MyReader: RefCountedStreamReader {
    int read (const byte_t *d, int sz, Chain *ctx) {
        auto ip = ctx->as<IpContext>();
        auto udp = ctx->as<UdpContext>();
        l.info("%08x:%d sz:%d", ip->destinationIp, udp->dstPort, sz);
        return sz;
    }
};

int main (int argc, char **argv)
{
    MyReader mr;
    UdpReader ur(&mr);
    IpReader ir(&ur);
    AlpReader ar;
    ar.setStreamReader(/*ip*/0, &ir);
    FalpReader fr(&ar);

    int fd = open(argv[1], O_RDONLY);
    if (fd < 0) {
        l.error("Expected alp stream filename");
        return -1;
    }

    Buffer buf;
    static const int READ_SIZE = 64 * 1024;
    ssize_t sz;
    while (true) {
        // consume stream as much as possible.
        sz = fr.read(buf.begin(), buf.size(), nullptr);
        if (sz > 0) {
            buf.drop(sz);
            continue;
        } else if (buf.size() >= READ_SIZE / 2) {
            continue; // for time sync
        }

        buf.ensureMargin(READ_SIZE);
        sz = ::read(fd, buf.end(), READ_SIZE);
        if (sz <= 0) break;
        buf.grow(sz);
    }

    close(fd);
    return 0;
}


