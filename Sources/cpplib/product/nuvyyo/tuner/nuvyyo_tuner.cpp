#include <sys/ioctl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>

#include <thread>
#include <memory>

#include "acba.h"
#include "datetime.h"
#include "tuner.h"
#include "stream_reader.h"
#include "pcap_writer.h"
#include "alp_reader.h"
#include "falp_reader.h"

#include "ite_drv.h"
#include "nuvyyo_tuner.h"

// only for accessing storageRoot
#include "atsc3_model.h"

#define ITE_READ_CHUNKSIZE (816 * 188)

#define ENABLE_PCAPDUMP 0

using namespace std;
using namespace acba;
using namespace tuner;

static Log l("ite");

struct IteTuner: Tuner {

    int id; // tuner index
    int handle; // file desc

    bool running;
    thread runner;

    byte_t readBuffer[ITE_READ_CHUNKSIZE];
    FalpReader falpReader;
    AlpReader alpReader;

    Ref<PcapWriter> pcapWriter;
    Log l;

    IteTuner (int _id, int fd): id(_id), handle(fd), running(false),
            falpReader(&alpReader), l("ite", std::to_string(id)+": ") {
        alpReader.setStreamReader(0, &ipConsumer);
        caps = CAP_ATSC3 | CAP_ATSC1;
    }

    ~IteTuner () {
        close(handle);
    }

    bool tune (const TuneParams &tp) override {

        if (running) {
            l.error("Already tuned");
            return false;
        }

        l.info("Tuning to %dHz dmod:%s", tp.frequency,
            tp.demod == DEMOD_ATSC3 ? "atsc3" : "8vsb");

        params = tp;

        running = true;
        runner = thread(bind(&IteTuner::run, this));
        return true;
    }

    void stop () override {

        if (!running) {
            l.error("Cannot stop already stopped tuner");
            return;
        }

        double tm = currentUnixTime();

        running = false;
        runner.join();
        runner = thread();

        l.info("stop took %.2f sec", currentUnixTime() - tm);

        if (pcapWriter) {
            pcapWriter->close();
            pcapWriter = nullptr;
        }
    }

    void run () {
        l.error("Tune thread begin.");

        bool locked = false;

        double tm = currentUnixTime();

        while (running) {
            ite_tune req;
            memset(&req, 0, sizeof(req));
            req.chip = id;
            req.bw_khz = 6000;
            req.freq_khz = params.frequency / 1000;
            req.mode = (params.demod == DEMOD_ATSC3) ? ITE_TUNE_MODE_ATSC3 :
                    ITE_TUNE_MODE_ATSC;

            if (ioctl(handle, IOCTL_ITE_DEMOD_ACQUIRECHANNEL, &req) == 0) break;

            l.warn("IOCTL_ITE_DEMOD_ACQUIRECHANNEL failed.");
            usleep(500000);
        }
        if (!running) goto exit;

        l.info("tuning took %.2f sec", currentUnixTime() - tm);

#if ENABLE_PCAPDUMP
        if (params.demod == DEMOD_ATSC3) {
            char str[256];
            sprintf(str, "%s/pcap/alp-%d", atsc3::model.storageRoot.c_str(),
                    params.frequency);
            pcapWriter = new PcapWriter(/*ALP*/999);
            if (!pcapWriter->open(str)) {
                l.error("Failed to create pcap file");
                pcapWriter = nullptr;
            }
        }
#endif

        ioctl(handle, IOCTL_ITE_DEMOD_STARTCAPTURE);

        falpReader.reset();
        while (running) {
            int rsz = read(handle, readBuffer, ITE_READ_CHUNKSIZE);
            if (rsz < 0) {
                l.warn("Couldn't read stream. %s", strerror(errno));
                break;
            }

            // Regard unlocked if no data received.
            if (locked != !!rsz) {
                locked = !!rsz;
                l.info("tuner %s", locked? "locked": "unlocked");
                if (params.callback) {
                    params.callback(locked ? CALLBACK_TUNER_LOCKED
                            : CALLBACK_TUNER_UNLOCKED, 0, 0);
                }
            }

            if (params.demod == DEMOD_ATSC3) {
                auto e = readBuffer + rsz - 188 + 1;
                for (auto d = readBuffer; d < e; d += 188) {
                    if (falpReader.read(d, 188, nullptr) < 0) {
                        l.info("Invalid TS packet detected while reading %d "
                                "bytes chunk.", rsz);
                        break;
                    }
                }
            } else {
                if (params.callback) {
                    params.callback(CALLBACK_TS, readBuffer, rsz);
                }
            }
        }

        ioctl(handle, IOCTL_ITE_DEMOD_STOPCAPTURE);

exit:
        if (params.callback) {
            params.callback(CALLBACK_END, 0, 0);
            params.callback = nullptr;
        }

        l.error("Tune thread end");
    }

    struct IpConsumer: StreamReader {
        int read (const byte_t *data, int sz, Chain *ctx) override {
            auto *self = container_of(this, &IteTuner::ipConsumer);
            if (self->params.callback) {
                self->params.callback(CALLBACK_IP, data, sz);
            }
            return sz;
        }
    } ipConsumer;
};

int nuvyyo::initTuners ()
{
    for (int i = 0; i < 16; i++) {
        char s[256];
        sprintf(s, "/dev/usb-it930x%d", i);
        auto fd = open(s, O_RDWR);
        if (fd < 0) break;

        l.info("Detected tuner %d at %s", i, s);
        pool().tuners.push_back(new IteTuner(i, fd));
    }

    return 0;
}
