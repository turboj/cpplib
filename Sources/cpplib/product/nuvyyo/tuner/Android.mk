LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)

LOCAL_MODULE := nuvyyo_tuner

LOCAL_SRC_FILES += $(wildcard $(LOCAL_PATH)/*.cpp)
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../tuner
LOCAL_STATIC_LIBRARIES := acba readstream atsc3

LOCAL_EXPORT_C_INCLUDES := $(LOCAL_PATH)

include $(BUILD_STATIC_LIBRARY)
