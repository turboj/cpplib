#ifndef FALP_READER
#define FALP_READER

#include <set>

#include "acba.h"
#include "stream_reader.h"

struct FalpReader: StreamReader {
    acba::Buffer buffer;
    StreamReader *upper;

    FalpReader (StreamReader *u = nullptr): upper(u) {}

    int read (const byte_t *data, int sz, Chain *ctx) override;

    void reset () {
        buffer.clear();
    }
};

#endif
