$(call begin-target, nuvyyo_tuner, staticlib)
    LOCAL_SRCS := $(wildcard $(LOCAL_PATH)/*.cpp $(LOCAL_PATH)/*.c)
    LOCAL_IMPORTS := tuner atsc3_model-staticlib
    LOCAL_IMPORTS += acba-staticlib readstream-staticlib tuner-staticlib
    EXPORT_CFLAGS := -I$(LOCAL_PATH)
    EXPORT_CXXFLAGS := -I$(LOCAL_PATH)
    EXPORT_IMPORTS := $(LOCAL_IMPORTS)
$(end-target)

$(call begin-target, test_falp, executable)
    LOCAL_LABELS := TEST
    LOCAL_SRCS := $(LOCAL_PATH)/test/test_falp.cpp
    LOCAL_IMPORTS := nuvyyo_tuner-staticlib
    LOCAL_IMPORTS += acba-staticlib readstream-staticlib
$(end-target)
