#include <sys/types.h>
#include <sys/ioctl.h>

#define	ITE_TUNE_MODE_ATSC	0
#define	ITE_TUNE_MODE_QAMB	1
#define	ITE_TUNE_MODE_DVBC	2
#define	ITE_TUNE_MODE_DVBT_T2	3
#define	ITE_TUNE_MODE_ISDBT	4
#define	ITE_TUNE_MODE_ATSC3	5
#define	ITE_TUNE_MODE_DVBS	6
#define	ITE_TUNE_MODE_DVBS2	7

#define	ITE_PWR_MIN		-8300
#define	ITE_PWR_MAX		-2700
#define	ITE_SNR_MIN		15.0
#define	ITE_SNR_MAX		30.0


typedef enum ite_bool_e
	{
	False = 0,
	True = 1
	} ite_bool;

typedef struct ite_tune_t
	{
	unsigned char		chip;
	unsigned short		bw_khz;
	unsigned int		freq_khz;
	unsigned int		error;
	unsigned char		mode;
	unsigned int		symbolrate;
	char			reserved[11];
	} ite_tune;

typedef struct ite_islock_t
	{
	unsigned char		chip;
	unsigned int		locked;
	unsigned int		error;
	char			reserved[16];
	} ite_islock;

typedef struct ite_query_t
	{
	unsigned char		chip;
	ite_bool		dlock;
	ite_bool		unlock;
	unsigned long		snr_num;
	unsigned long		snr_den;
	unsigned long		ber_num;
	unsigned long		ber_den;
	long			pwr;
	unsigned long		error;
	char			reserved[16];
	} ite_query;

typedef struct ite_sig_stat_t
	{
	ite_bool		present;
	ite_bool		dlock;
	unsigned char		qual;
	unsigned char		pwr;
	} ite_sig_stat;

typedef struct ite_sig_query_t
	{
	unsigned char		chip;
	ite_sig_stat		stat;
	unsigned int		error;
	char			reserved[16];
	} ite_sig_query;

typedef struct ite_chan_stat_t
	{
	unsigned short		abort_n;
	unsigned long		post_vbit;
	unsigned long		post_verror;
	unsigned long		soft_bit;
	unsigned long		soft_error;
	unsigned long		pre_vbit;
	unsigned long		pre_verror;
	} ite_chan_stat;

typedef struct ite_chan_query_t
	{
	unsigned char		chip;
	ite_chan_stat		stat;
	unsigned int		error;
	char			reserved[16];
	} ite_chan_query;

typedef struct ite_capture_t
	{
	unsigned char		chip;
	unsigned int		error;
	char			reserved[16];
	} ite_capture;


#define	AFA_IOC_MAGIC				'k'
#define	IOCTL_ITE_DEMOD_ACQUIRECHANNEL		_IOR( AFA_IOC_MAGIC,  0x14, ite_tune )
#define	IOCTL_ITE_DEMOD_ISLOCKED		_IOR( AFA_IOC_MAGIC,  0x15, ite_islock )
#define IOCTL_ITE_DEMOD_CXD6801_GETSTATISTIC	_IOR( AFA_IOC_MAGIC,  0x17, ite_query )
#define IOCTL_ITE_DEMOD_GETSTATISTIC		_IOR( AFA_IOC_MAGIC,  0x18, ite_sig_query )
#define IOCTL_ITE_DEMOD_GETCHANNELSTATISTIC	_IOR( AFA_IOC_MAGIC,  0x19, ite_chan_query )
#define IOCTL_ITE_DEMOD_STARTCAPTURE		_IOR( AFA_IOC_MAGIC, 0x501, ite_capture )
#define IOCTL_ITE_DEMOD_STOPCAPTURE		_IOR( AFA_IOC_MAGIC, 0x502, ite_capture )
