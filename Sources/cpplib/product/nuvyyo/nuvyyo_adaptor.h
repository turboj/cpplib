#ifndef NUVYYO_ADAPTOR_H
#define NUVYYO_ADAPTOR_H

#include "unifiedtv_native.h"

#ifdef __cplusplus
extern "C" {
#endif

/**
 * UnifiedTvCallbacks.updateComponents would provides list of ComponentNuvyyo
 * instead of type Component.
 */
typedef struct {
    unifiedtv_Component base;
    int pid;
    /**
     * In bit per second(bps)
     */
    int bitrate;
    /**
     * Frame per second
     */
    int framerate;
    /**
     * Horizontal presentaton size
     */
    int width;
    /**
     * Vertical presentation size
     */
    int height;
} unifiedtv_ComponentNuvyyo;

/**
 * Tuner Staticstics
 */
typedef struct {
    bool locked;
    /**
     * When "locked" is true, tuned location in domain specific format.
     */
    char *location;
    /**
     * SNR(Signal-to-Noise Ratio) in dB.
     * -1 if not available.
     */
    int snr;
    /**
     * Error rate between 0 and 65535. 0 if no error and 65535 if 100% error.
     * -1 if not available.
     */
    int bitErrorRate;
} unifiedtv_TunerStatus;

/**
 * A structure representing stream consumer.
 * Callback functions are called in a thread per streaming.
 * Note that however the same thread may handle multiple streamings.
 */
typedef struct {
    void *context;
    void (*read) (const void *buf, size_t sz, void *ctx); 
    void (*stop) (void *ctx); 
} unifiedtv_StreamCallbacks;

/**
 * Extended UnifiedTv Interface. 
 * Additional functions were added for:
 * - Getting status of tuner device
 * - Getting ts streams from atsc3.0 service or atsc1.0 tuner
 */
typedef struct {
    unifiedtv_UnifiedTv base;

    /**
     * Queries current status of all available tuners.
     * Returns immediately.
     * On success, (*ts) refers null-terminated array of type *TunerStatus.
     * If given buffer size is not sufficient, this returns
     * ERROR_INSUFFICIENT_RESOURCE. Then app can call this again with larger
     * buffer.
     */
    unifiedtv_ERROR (*getTunerStatus) (unifiedtv_TunerStatus ***ts, void *buf,
            size_t bufsz);

    /**
     * Starts mpeg2ts streaming.
     * sourceSpec could be either atsc1.0 tune location or atsc3.0 service id.
     */
    unifiedtv_ERROR (*startStreaming) (const char *sourceSpec,
            const unifiedtv_StreamCallbacks *consumer);

    /**
     * Stops mpeg2ts streaming previously started via startStreaming.
     * You should wait until StreamConsumer.stop() is called to acknowledge
     * the completion of stop operation.
     */
    unifiedtv_ERROR (*stopStreaming) (const char *sourceSpec);
} unifiedtv_UnifiedTvNuvyyo;

/**
 * Starts atsc3.0 stack and returns an interface to interact.
 * UnifiedTv.apiVersion is set 0.
 */
const unifiedtv_UnifiedTvNuvyyo *unifiedtv_startForNuvyyo ();

#ifdef __cplusplus
}
#endif
#endif
