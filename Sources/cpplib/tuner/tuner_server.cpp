/*
    server --1:n--> control connection --1:n--> tune --1:1--> stream connection
*/

#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>

#include <string>
#include <thread>
#include <set>
#include <functional>
#include <sstream>

#include "acba.h"
#include "EventDriver.h"
#include "MessageReactor.h"
#include "tuner.h"
#include "tuner_server.h"
#include "tuner_msg.h"
#include "socket_util.h"
#include "ScopedLock.h"

// TODO: Change to ANY.
#define TUNER_SERVER_PORT 8082

#define NUM_CONCURRENT_TUNER_CLIENTS_MAX 2

using namespace std;
using namespace acba;
using namespace tuner;

namespace {

Log l("tuner", "server: ");

EventDriver &eventDriver ()
{
    static EventDriver *obj = nullptr;
    if (!obj) {
        obj = new EventDriver();

        thread runner([]() { obj->run(); });
        runner.detach();
    }
    return *obj;
}

// TCP connection used for either control or stream.
// The stream connection is determined on receiving MSG_BIND.
struct Connection: MessageReactor, RefCounter<Connection> {

    typedef map<MSG_TYPE, function<void(const byte_t*, int, Connection&)> >
            MessageHandlerMap;
    static MessageHandlerMap messageHandlers;

    struct TuneHolder: RefCounter<TuneHolder> {
        const int id;
        Tuner *tuner;
        Ref<Connection> connection;

        TuneHolder (int _id): id(_id), tuner(nullptr) {}
    };
    map<int32_t, Ref<TuneHolder> > tunes; // key: id, used only in control conn.

    int numTunesMax; // number of concurrent tunes allowed in this connection

    uint16_t tuneIdBase; // 16MSB of tune ids assigned for this connection

    Connection (): numTunesMax(0), tuneIdBase(0) {
        ref(); // paired deref() is called on close()
    }

    void readMessage (const uint8_t *buf, int32_t sz) override {
        if (sz < 4) {
            l.error("Invalid message sz:%d", sz);
            return;
        }
        uint32_t mt = (buf[0] << 24) | (buf[1] << 16) | (buf[2] << 8) | buf[3];
        auto mhi = messageHandlers.find(MSG_TYPE(mt));
        if (mhi == messageHandlers.end()) return;
        mhi->second(buf + 4, sz - 4, *this);
    }

    void close () override {
        l.info("close %p", this);
        messageHandlers[MSG_CLOSE](nullptr, 0, *this);
        MessageReactor::close();
        deref();
    }

    void untune (uint32_t i) {
        l.info("untune %08x", i);

        auto ti = tunes.find(i);
        if (ti == tunes.end()) {
            l.error("No tune tasks %lu", i);
            return;
        }
        auto &t = ti->second;

        t->tuner->stop();
        {
            ScopedLock sl(pool().mutex);
            assert(t->tuner->inUse);
            t->tuner->inUse = false;
        }

        if (t->connection) t->connection->abort("untune");

        tunes.erase(ti);
    }

    // called from tuner thread.
    void processTunerCallback (TuneHolder *t, CALLBACK_TYPE type,
            const byte_t *data, int sz) {

        switch (type) {
        case CALLBACK_END:
            // after this returns, The ref to this connection will be released
            // by tuner instance. So, All set, nothing to clean up manually. 
            break;

        case CALLBACK_TUNER_LOCKED:
            sendMessage([=] (Buffer &buf) {
                buf.writeI32(MSG_TUNER_LOCKED);
                buf.writeI32(t->id);
            });
            break;

        case CALLBACK_TUNER_UNLOCKED:
            sendMessage([=] (Buffer &buf) {
                buf.writeI32(MSG_TUNER_UNLOCKED);
                buf.writeI32(t->id);
            });
            break;

        case CALLBACK_IP:
        case CALLBACK_LMT:
        case CALLBACK_ALP:
        case CALLBACK_TS:
            if (!t->connection) break;
            if (t->connection->flushSendBufferIfFull(1024 * 1024)) {
                l.warn("Check network condiiton. dropped 1MB of send buffer");
            }
            t->connection->sendMessage([=] (Buffer &buf) {
                // convert type from CALLBACK_TYPE to MSG_TYPE
                buf.writeI32(type - CALLBACK_IP + MSG_IP);
                buf.writeBytes(data, sz);
            });
            break;
        }
    }
};

struct SsdpResponder: FileReactor {
    string uuid;
    byte_t buffer[2048];

    SsdpResponder (const string &u): uuid(u) {}

    void read () override {
        struct sockaddr_in addr;
        socklen_t addrlen = sizeof(addr);

        memset(buffer, 0, sizeof(buffer));
        int rc = recvfrom(getFd(), buffer, sizeof(buffer), MSG_DONTWAIT,
                (sockaddr*)&addr, &addrlen);
        if (rc == -1) {
            l.error("recv failed. %s", strerror(errno));
            return;
        }
        if (addr.sin_family != AF_INET) {
            l.warn("recv from non AF_INET");
            return;
        }
        if (rc >= sizeof(buffer)) {
            l.warn("received too long message");
            return;
        }

        // TODO: Full-Parse ssdp message.
        if (strstr((char*)buffer,
                "\r\nST: urn:schemas-alticast-com:device:atscGateway:1\r\n")
                == nullptr) {
            return;
        }

        stringstream ss;
        ss << "HTTP/1.1 200 OK\r\n";
        ss << "CACHE-CONTROL: max-age = 1\r\n";
        ss << "EXT:\r\n";
        ss << "ST: urn:schemas-alticast-com:device:atscGateway:1\r\n";
        ss << "USN: uuid:" << uuid <<
                ":urn:schemas-alticast-com:device:atscGateway:1\r\n";
        ss << "AL: <port:" << TUNER_SERVER_PORT << ">\r\n";
        ss << "\r\n";
        string s = ss.str();
        
        rc = sendto(getFd(), s.data(), s.size(), MSG_DONTWAIT,
                (sockaddr*)&addr, addrlen);
        if (rc < 0) {
            l.error("sendto failed");
        }
    }
};

struct ServerImpl: Server, FileReactor {

    // tune-granted control connections. (i.e. numTunesMax > 0)
    set<Ref<Connection> > connections;

    void start (const string &uuid) override {

        // start connection listener.
        int fd = create_server_socket(TUNER_SERVER_PORT, nullptr);
        if (fd < 0) {
            l.error("Failed to create server socket with port:%d",
                    TUNER_SERVER_PORT);
            return;
        }
        ::eventDriver().watchFile(fd, EVENT_READ, this);
        log.info("listening on port %d (fd:%d)\n", TUNER_SERVER_PORT, fd);

        // start ssdp server
        fd = create_multicast_socket(0, 0xeffffffa, 1900);
        if (fd < 0) {
            l.error("Failed to create multicast socket for SSDP service");
            return;
        }
        ::eventDriver().watchFile(fd, EVENT_READ, new SsdpResponder(uuid));
    }

    void read () override {
        int fd;
        {
            sockaddr_in clientAddr;
            socklen_t len = sizeof(clientAddr);
            fd = accept(getFd(), (struct sockaddr*)&clientAddr, &len);
        }
        if (fd < 0) {
            log.warn("Accept failed.\n");
            return;
        }
        log.info("new connection fd:%d\n", fd);

        ::eventDriver().watchFile(fd, EVENT_READ, new Connection());
    }
};

ServerImpl &serverImpl ()
{
    static ServerImpl obj;
    return obj;
}

Connection::MessageHandlerMap Connection::messageHandlers = {
    {MSG_NOP, [] (const byte_t *d, int sz, Connection &conn) {
    }},
    {MSG_ALLOCATE, [] (const byte_t *d, int sz, Connection &conn) {
        auto &srv = serverImpl();
        Buffer buf((uint8_t*)d, sz);

        int n = buf.readI32();

        int cur = 0;
        for (auto &c: srv.connections) cur += c->numTunesMax;

        if (cur + n > NUM_CONCURRENT_TUNER_CLIENTS_MAX) {
            // Reached total number of client.
            l.warn("Client is full");

            conn.sendMessage(MSG_ALLOCATE_FAIL);
            return;
        }

        static uint16_t nextIdBase = 1;
        if (conn.tuneIdBase == 0) conn.tuneIdBase = nextIdBase++;
        conn.numTunesMax += n;
        srv.connections.insert(&conn);
        l.info("Granted total %d tuners to Client %p", conn.numTunesMax, &conn);
        conn.sendMessage([&] (Buffer &buf) {
            buf.writeI32(MSG_ALLOCATE_SUCCESS);
            buf.writeI16(conn.tuneIdBase);
        });
    }},
    {MSG_TUNE, [] (const byte_t *d, int sz, Connection &conn) {
        Buffer buf((uint8_t*)d, sz);
        uint32_t tid = buf.readI32();
        string loc = {(char*)buf.begin(), (size_t)buf.size()};

        auto sendFail = [&] () {
            conn.sendMessage([&] (Buffer &buf) {
                buf.writeI32(MSG_TUNE_FAIL);
                buf.writeI32(tid);
            });
        };

        if ((tid >> 16) != conn.tuneIdBase) {
            l.error("Invalid tuneId:%08x. expected base:%04x", tid,
                    conn.tuneIdBase);
            sendFail();
            return;
        }

        if (conn.tunes.find(tid) != conn.tunes.find(tid)) {
            l.error("Duplicated tuneId:%08x", tid);
            sendFail();
            return;
        }

        if (conn.tunes.size() >= conn.numTunesMax) {
            l.error("No more tuner for Client %p", &conn);
            sendFail();
            return;
        }

        // Prepare tuning
        TuneParams params;
        int capsreq = 0;
        if (loc.rfind("atsc1:", 0) == 0) {
            params.frequency = atoi(loc.c_str() + 6);
            params.demod = DEMOD_8VSB;
            capsreq |= CAP_ATSC1;
        } else if (loc.rfind("atsc3:", 0) == 0) {
            params.frequency = atoi(loc.c_str() + 6);
            params.demod = DEMOD_ATSC3;
            capsreq |= CAP_ATSC3;
        } else {
            l.error("Invalid location: %s", loc.c_str());
            sendFail();
            return;
        }
        Ref<Connection::TuneHolder> th = new Connection::TuneHolder(tid);

        // Find free tuner and try to tune with it.
        ScopedLock sl(pool().mutex);
        for (auto t: pool().tuners) {
            if (t->inUse || (t->caps & capsreq) != capsreq) continue;

            // try tune
            th->tuner = t;
            using namespace placeholders;
            params.callback = bind(&Connection::processTunerCallback,
                    ::ref(&conn), th, _1, _2, _3);
            if (!t->tune(params)) continue;

            // on success
            t->inUse = true;
            conn.tunes[th->id] = th;
            conn.sendMessage([&] (Buffer &buf) {
                buf.writeI32(MSG_TUNE_SUCCESS);
                buf.writeI32(th->id);
            });
            return;
        }

        // No proper tuner found.
        l.error("No free tuner.");
        sendFail();
    }},
    {MSG_UNTUNE, [] (const byte_t *d, int sz, Connection &conn) {
        Buffer buf((uint8_t*)d, sz);
        uint32_t tid = buf.readI32();
        conn.untune(tid);
    }},
    {MSG_MULTICASTS, [] (const byte_t *d, int sz, Connection &conn) {
        Buffer buf((uint8_t*)d, sz);

        auto id = buf.readI32();
        auto ti = conn.tunes.find(id);
        if (ti == conn.tunes.end()) {
            l.error("Invalid tune id %d", id);
            return;
        }

        vector<MulticastAddress> addrs;
        int n = buf.readI32();
        for (int i = 0; i < n; i++) {
            MulticastAddress m;
            m.sourceIp = buf.readI32();
            m.sourcePort = buf.readI16();
            m.destinationIp = buf.readI32();
            m.destinationPort = buf.readI16();
            addrs.push_back(m);
        }

        ti->second->tuner->setMulticasts(addrs.data(), n);
    }},
    {MSG_BIND, [] (const byte_t *d, int sz, Connection &conn) {
        Buffer buf((uint8_t*)d, sz);

        auto id = buf.readI32();

        for (auto &c: serverImpl().connections) {
            auto ti = c->tunes.find(id);
            if (ti == c->tunes.end()) continue;

            ti->second->connection = &conn;
            l.info("Bind stream connection to tune %d", id);
            return;
        }

        l.error("Binding failed. unknown tune id %d", id);
    }},
    {MSG_CLOSE, [] (const byte_t *d, int sz, Connection &conn) {
        if (serverImpl().connections.count(&conn) == 0) return;

        l.info("Closing all connections for Client %p", &conn);

        while (conn.tunes.size() > 0) conn.untune(conn.tunes.begin()->first);
        conn.numTunesMax = 0;
        serverImpl().connections.erase(&conn);
    }},
};

} // anonymous namespace

Server &tuner::server ()
{
    return serverImpl();
}
