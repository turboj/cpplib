#ifndef TUNER_SERVER_H
#define TUNER_SERVER_H

#include "acba.h"

namespace tuner {

struct Server {
    virtual void start (const std::string &uuid) = 0;
};

Server &server ();

}
#endif
