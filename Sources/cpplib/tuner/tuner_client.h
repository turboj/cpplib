#ifndef TUNER_CLIENT_H
#define TUNER_CLIENT_H

#include <stdint.h>

#include "tuner.h"
#include "acba.h"

#include <vector>

namespace tuner {

struct TuneProxy: acba::RefCounter<TuneProxy> {
    virtual void setMulticasts (const MulticastAddress *addrs, int numAddrs) =
            0;
    virtual void dispose () = 0;
};

struct ServerInfo {
    std::string uuid;
    std::string name;
    std::string host;
    uint16_t port;
};

struct Client {
    
    enum STATE {
        STATE_DISCONNECTED,
        STATE_CONNECTING,
        STATE_CONNECTED
    } state;
    acba::Update<> stateUpdate;

    Client (): state(STATE_DISCONNECTED) {}

    // returns false if failed to perform the request properly.
    // returns true in case of success or timeout.
    virtual bool searchServers (std::vector<ServerInfo> &outInfos,
            double minWaitSec = 1.0, double maxWaitSec = 1.0) = 0;

    virtual void connect (const char *host, int port, void *params = nullptr) =
            0; 

    virtual TuneProxy *tune (const std::string &loc, const TuneCallback &cb) =
            0;
};

Client &client ();

}


#endif
