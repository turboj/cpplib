#ifndef TUNER_MSG_H 
#define TUNER_MSG_H 

namespace tuner {

enum MSG_TYPE {
    MSG_NOP,
    MSG_ALLOCATE, // num(i32)
    MSG_ALLOCATE_SUCCESS, // tuneIdBase(i16): 16MSB of tunerId
    MSG_ALLOCATE_FAIL,
    MSG_TUNE, // tuneId(i32), location_string
    MSG_TUNE_SUCCESS, // tuneId(i32)
    MSG_TUNE_FAIL, // tuneId(i32)
    MSG_UNTUNE, // tuneId(i32)
    MSG_MULTICASTS, // tunerId(i32), len(i32),
            // [srcIp(i32), srcPort(i16), dstIp(i32), dstPort(i16)]
    MSG_BIND, // tuneId(i32)
    MSG_CLOSE, // used internally.
    MSG_END, // not used currently.
    MSG_TUNER_LOCKED, // tuneId(i32)
    MSG_TUNER_UNLOCKED, // tuneId(i32)

    // NOTE: Keep ordering with corresponding values of CALLBACK_TYPE.
    MSG_IP,
    MSG_LMT,
    MSG_ALP,
    MSG_TS,
};

}
#endif
