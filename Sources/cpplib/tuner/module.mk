$(call begin-target, tuner)
    EXPORT_CXXFLAGS := -I$(LOCAL_PATH)
$(end-target)

$(call begin-target, tuner_server, staticlib)
    LOCAL_SRCS := $(LOCAL_PATH)/tuner_server.cpp
    LOCAL_IMPORTS := acba-staticlib boost-staticlib
    EXPORT_CXXFLAGS := -I$(LOCAL_PATH) -DUSE_TUNER_SERVER=1
$(end-target)

$(call begin-target, tuner_client, staticlib)
    LOCAL_SRCS := $(LOCAL_PATH)/tuner_client.cpp
    LOCAL_IMPORTS := acba-staticlib boost-staticlib
    EXPORT_CXXFLAGS := -I$(LOCAL_PATH) -DUSE_TUNER_CLIENT=1
$(end-target)
