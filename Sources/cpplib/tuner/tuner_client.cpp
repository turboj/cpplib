/*
    client -- 1:1 --> connection (control)
       |                7
      1:n   --- n:1 ----
       |   /
       v  /
     tune -- 1:1 --> connection (stream)
       ^                 |
       +-----------------+
*/

#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#include <thread>
#include <map>
#include <functional>
#include <vector>
#include <sstream>
#include <regex>

#include "tuner_client.h"
#include "tuner_msg.h"
#include "acba.h"
#include "ScopedLock.h"
#include "EventDriver.h"
#include "MessageReactor.h"
#include "socket_util.h"

using namespace std;
using namespace placeholders;
using namespace acba;
using namespace tuner;

namespace {

Log l("tuner", "client: ");

EventDriver &eventDriver ()
{
    static EventDriver *obj = nullptr;
    if (!obj) {
        obj = new EventDriver();

        thread runner([]() { obj->run(); });
        runner.detach();
    }
    return *obj;
}

void async (const function<void()> &fn)
{
    static struct _: Reactor {

        pthread_mutex_t mutex;
        vector<function<void()>> asyncTasks;

        _ (): mutex(PTHREAD_MUTEX_INITIALIZER) {}

        void processEvent (int events) override {
            vector<function<void()>> jobs;
            {
                ScopedLock sl(mutex);
                jobs.swap(asyncTasks);
            }

            for (auto &f: jobs) f();
        }

        void add (const function<void()> &fn) {
            ScopedLock sl(mutex);
            asyncTasks.push_back(fn);
            ::eventDriver().setTimeout(0, this);
        }
    } asyncRunner;

    asyncRunner.add(fn);
}

struct Connection;

struct TuneProxyImpl: TuneProxy {

    const uint32_t id;
    const string location;
    const TuneCallback callback;

    // see diagram at the beginning of the file
    const Ref<Connection> control;
    Ref<Connection> stream;

    TuneProxyImpl (uint32_t _id, const Ref<Connection> &ctl, const string &loc,
            const TuneCallback &cb): id(_id), control(ctl), location(loc),
            callback(cb) {}

    ~TuneProxyImpl () {
        async(bind([] (const TuneCallback &cb) {
            cb(CALLBACK_END, nullptr, 0);
        }, callback));
    }

    void setMulticasts (const MulticastAddress *addrs, int numAddrs) override;
    void dispose () override;

    void start ();
};

struct Connection: MessageReactor, RefCounter<Connection> {

    typedef map<MSG_TYPE, function<void(const byte_t*, int, Connection&)> >
            MessageHandlerMap;
    static MessageHandlerMap messageHandlers;

    // wait until writable and then check SO_ERROR to confirm that
    // the connection was established.
    bool waitingConnect;

    // 16MSB of tune ids assigned for this connection
    // used only for control connection
    uint16_t tuneIdBase;

    Ref<TuneProxyImpl> tune; // used only for stream connection

    Connection (): waitingConnect(true), tuneIdBase(0) {
        ref(); // paired deref() is called on close()
    }

    void readMessage (const uint8_t *buf, int32_t sz) override {
        if (sz < 4) {
            l.error("Invalid message sz:%d", sz);
            return;
        }
        uint32_t mt = (buf[0] << 24) | (buf[1] << 16) | (buf[2] << 8) | buf[3];
        auto mhi = messageHandlers.find(MSG_TYPE(mt));
        if (mhi == messageHandlers.end()) return;
        mhi->second(buf + 4, sz - 4, *this);
    }

    void close () override {
        l.info("close %p", this);
        messageHandlers[MSG_CLOSE](nullptr, 0, *this);
        MessageReactor::close();
        deref();
    }

    void write () override {
        // Check if connection was made properly.
        if (waitingConnect) {
            waitingConnect = false;

            int err = -1;
            socklen_t errlen = sizeof(err);
            getsockopt(getFd(), SOL_SOCKET, SO_ERROR, &err, &errlen);
            if (err) {
                abort("Connect failed");
                return;
            }

            // Start reading data from socket
            setReadable(true);

            // fall through
        }

        MessageReactor::write();
    }
};

struct SsdpRequester: FileReactor {
    vector<ServerInfo> &serviceInfos;

    pthread_mutex_t mutex;
    CondVar done;
    uint64_t waitTime;
    byte_t buffer[2048];

    SsdpRequester (vector<ServerInfo> &si, uint64_t wt): serviceInfos(si),
            waitTime(wt), mutex(PTHREAD_MUTEX_INITIALIZER) {}

    void read () override {
        struct sockaddr_in addr;
        socklen_t addrlen = sizeof(addr);

        memset(buffer, 0, sizeof(buffer));
        int rc = recvfrom(getFd(), buffer, sizeof(buffer), MSG_DONTWAIT,
                (sockaddr*)&addr, &addrlen);
        if (rc == -1) {
            l.error("recv failed. %s", strerror(errno));
            return;
        }
        if (addr.sin_family != AF_INET) {
            l.warn("recv from non AF_INET");
            return;
        }
        if (rc >= sizeof(buffer)) {
            l.warn("received too long message");
            return;
        }

        // TODO: Full-Parse ssdp message
        static regex re("\r\nUSN: uuid:(.*):urn:schemas-alticast-com:"
                "device:atscGateway:1\r\n.*AL: <port:(.*)>\r\n");
        cmatch cm;
        if (!regex_search((char*)buffer, cm, re)) return;
        auto uuid = cm[1].str();
        auto port = atoi(cm[2].str().c_str());
        if (uuid.empty() || port <= 0) return;

        ServerInfo si;
        si.uuid = uuid;
        si.name = uuid;
        {
            char buf[80];
            auto h = reinterpret_cast<byte_t*>(&addr.sin_addr.s_addr);
            sprintf(buf, "%u.%u.%u.%u", h[0], h[1], h[2], h[3]);
            si.host = buf;
        }
        si.port = port;
        serviceInfos.push_back(si);
    }

    void close () override {
        // wait more if on serviceInfos are available.
        if (waitTime > 0) {
            if (serviceInfos.size() == 0) {
                getEventDriver()->setTimeout(waitTime, this);
                waitTime = 0;
                return;
            }
        }

        ScopedLock sl(mutex);
        waitTime = -1;
        done.signal();

        FileReactor::close();
    }

    void wait () {
        ScopedLock sl(mutex);
        while (waitTime != -1) done.wait(&mutex);
    };
};

struct ClientImpl: Client {

    pthread_mutex_t mutex; // monitor for all fields in this struct.
    Ref<Connection> connection;
    map<uint32_t, Ref<TuneProxyImpl> > tunes;
    int numTunesMax; // number of concurrent tunes allowed in the connection
    string host; // valid except when STATE_DISCONNECTED
    int port; // valid except when STATE_DISCONNECTED

    ClientImpl (): mutex(PTHREAD_MUTEX_INITIALIZER), numTunesMax(0) {}

    bool searchServers (vector<ServerInfo> &outInfos,
            double minWaitSec = 1.0, double maxWaitSec = 1.0) override {

        // create socket
        int fd = socket(AF_INET, SOCK_DGRAM, 0);
        if (fd < 0) {
            l.error("Failed to open");
            return false;
        }
        int opt_val = 1;
        setsockopt(fd, SOL_SOCKET, SO_REUSEADDR, &opt_val, sizeof(opt_val));

        // bind
        struct sockaddr_in addr;
        memset(&addr, 0, sizeof(addr));
        addr.sin_family = AF_INET;
        addr.sin_addr.s_addr = INADDR_ANY;
        addr.sin_port = 0; // any port availbale.
        if (::bind(fd, (sockaddr*)&addr, sizeof(addr))) {
            l.error("Failed to bind");
            close(fd);
            return false;
        }

        // send request
        static string queryMessage =
                "M-SEARCH * HTTP/1.1\r\n"
                "HOST: 239.255.255.250:1900\r\n"
                "MAN: \"ssdp:discover\"\r\n"
                "MX: 1\r\n"
                "ST: urn:schemas-alticast-com:device:atscGateway:1\r\n"
                "\r\n";
        memset(&addr, 0, sizeof(addr));
        addr.sin_family = AF_INET;
        addr.sin_addr.s_addr = htonl(0xeffffffa); // 239.255.255.250
        addr.sin_port = htons(1900);
        int w = sendto(fd, queryMessage.data(), queryMessage.size(),
                MSG_DONTWAIT, (sockaddr*)&addr, sizeof(addr));
        if (w == -1) {
            l.error("SSDP request failed");
            close(fd);
            return false;
        }

        // make maxWaitSec relative to minWaitSec
        maxWaitSec -= minWaitSec;
        if (maxWaitSec < 0.0) maxWaitSec = 0.0;

        // wait to gather response
        SsdpRequester req(outInfos, maxWaitSec * 1000000);
        ::eventDriver().watchFile(fd, EVENT_READ, &req);
        ::eventDriver().setTimeout(minWaitSec * 1000000, &req);
        req.wait();

        l.info("Found %d tuner servers", outInfos.size());
        return true;
    }

    void connect (const char *host0, int p, void *params) override {

        async(bind([=] (string &h) {
            ScopedLock sl(mutex);

            if (state != STATE_DISCONNECTED) {
                l.warn("Already connecting or connected");
                return;
            }

            state = STATE_CONNECTING;
            host = h;
            port = p;
            stateUpdate.notify();

            int fd = create_socket(host.c_str(), port);
            if (fd < 0) {
                l.warn("Failed to create socket");
                state = STATE_DISCONNECTED;
                stateUpdate.notify();
                return;
            }

            connection = new Connection();
            ::eventDriver().watchFile(fd, EVENT_WRITE, connection.get());

            // try to allocate a tuner
            // NOTE: message transfer is delayed until the connection is made.
            connection->sendMessage([] (Buffer &buf) {
                buf.writeI32(MSG_ALLOCATE);
                buf.writeI32(1);
            });
        }, string(host0)));
    }

    TuneProxy *tune (const std::string &loc, const TuneCallback &cb) override {

        ScopedLock sl(mutex);

        if (state != STATE_CONNECTED) {
            l.warn("tune failed. not yet connected");
            return nullptr;
        }

        if (tunes.size() >= numTunesMax) {
            l.warn("tune failed. no more resource", loc.c_str());
            return nullptr;
        }

        // Define new tunerId
        static uint16_t nextId = 0;
        uint32_t tid;
        do {
            tid = (connection->tuneIdBase << 16) + (nextId++ & 0xffff);
        } while (tunes.find(tid) != tunes.end());

        auto &t = tunes[tid];
        t = new TuneProxyImpl(tid, connection, loc, cb);
        t->start();

        return t.get();
    }
};

ClientImpl &clientImpl ()
{
    static ClientImpl obj;
    return obj;
}

void _redirect (CALLBACK_TYPE ctype, const byte_t *d, int sz, Connection &conn) {
    if (conn.tune) conn.tune->callback(ctype, d, sz);
}

Connection::MessageHandlerMap Connection::messageHandlers = {
    {MSG_NOP, [] (const byte_t *d, int sz, Connection &conn) {
    }},
    {MSG_ALLOCATE_FAIL, [] (const byte_t *d, int sz, Connection &conn) {
        auto &cli = clientImpl();

        ScopedLock sl(cli.mutex);

        // Once allocated before, no need to disconnect.
        if (cli.numTunesMax > 0) return;

        conn.abort("Allocation failed");
        cli.connection = nullptr;
        cli.state = Client::STATE_DISCONNECTED;
        cli.stateUpdate.notify();
    }},
    {MSG_ALLOCATE_SUCCESS, [] (const byte_t *d, int sz, Connection &conn) {
        auto &cli = clientImpl();
        Buffer buf((uint8_t*)d, sz);

        uint16_t tidBase = buf.readI16();

        ScopedLock sl(cli.mutex);

        if (conn.tuneIdBase != 0 && conn.tuneIdBase != tidBase) {
            l.error("tunerIdBase mismatch! previous:%08x got:%08x",
                    conn.tuneIdBase, tidBase);
            // TODO
        }
        conn.tuneIdBase = tidBase;

        cli.numTunesMax += 1;
        cli.state = Client::STATE_CONNECTED;
        cli.stateUpdate.notify();
    }},
    {MSG_TUNE_SUCCESS, [] (const byte_t *d, int sz, Connection &conn) {
        auto &cli = clientImpl();
        Buffer buf((uint8_t*)d, sz);

        uint32_t tid = buf.readI32(); // tuneId

        ScopedLock sl(cli.mutex);

        auto ti = cli.tunes.find(tid);
        if (ti == cli.tunes.end()) {
            l.warn("No such tune proxy with id:%08x", tid);
            return;
        }

        auto t = ti->second;

        l.info("Tune successful. Biding stream connection with tuneId:%d", tid);
        t->stream->sendMessage([=] (Buffer &buf) {
            buf.writeI32(MSG_BIND);
            buf.writeI32(tid);
        });
    }},
    {MSG_TUNE_FAIL, [] (const byte_t *d, int sz, Connection &conn) {
        auto &cli = clientImpl();
        Buffer buf((uint8_t*)d, sz);

        auto tid = buf.readI32(); // appData

        ScopedLock sl(cli.mutex);

        auto ti = cli.tunes.find(tid);
        if (ti == cli.tunes.end()) {
            l.warn("No tune proxy to bind with MSG_TUNE_FAIL. id:%u", tid);
            return;
        }

        // TODO: send error

        auto &t = ti->second;
        if (t->stream) {
            t->stream->abort("tune failed"); // TODO: confirm ref clean-up
            t->stream = nullptr;
        }
        cli.tunes.erase(ti);
    }},
    {MSG_TUNER_LOCKED, [] (const byte_t *d, int sz, Connection &conn) {
        l.info("MSG_TUNER_LOCKED tune:%p", conn.tune.get());
        auto &cli = clientImpl();
        Buffer buf((uint8_t*)d, sz);

        auto tid = buf.readI32();

        ScopedLock sl(cli.mutex);

        auto ti = cli.tunes.find(tid);
        if (ti == cli.tunes.end()) {
            l.warn("No tune proxy for MSG_TUNER_LOCKED. id:%u", tid);
            return;
        }
        ti->second->callback(CALLBACK_TUNER_LOCKED, d, sz);
    }},
    {MSG_TUNER_UNLOCKED, [] (const byte_t *d, int sz, Connection &conn) {
        l.info("MSG_TUNER_UNLOCKED tune:%p", conn.tune.get());
        auto &cli = clientImpl();
        Buffer buf((uint8_t*)d, sz);

        auto tid = buf.readI32();

        ScopedLock sl(cli.mutex);

        auto ti = cli.tunes.find(tid);
        if (ti == cli.tunes.end()) {
            l.warn("No tune proxy for MSG_TUNER_UNLOCKED. id:%u", tid);
            return;
        }
        ti->second->callback(CALLBACK_TUNER_UNLOCKED, d, sz);
    }},
    {MSG_END, [] (const byte_t *d, int sz, Connection &conn) {
        l.error("Unexpected MSG_END");
    }},
    {MSG_IP, bind(_redirect, CALLBACK_IP, _1, _2, _3)},
    {MSG_LMT, bind(_redirect, CALLBACK_LMT, _1, _2, _3)},
    {MSG_ALP, bind(_redirect, CALLBACK_ALP, _1, _2, _3)},
    {MSG_TS, bind(_redirect, CALLBACK_TS, _1, _2, _3)},
    {MSG_CLOSE, [] (const byte_t *d, int sz, Connection &conn) {
        auto &cli = clientImpl();

        // in case of stream connection
        if (conn.tune) {
            conn.tune->stream = nullptr;
            conn.tune = nullptr;
            return;
        }

        // abort if current connection is not control connection
        if (&conn != cli.connection.get()) {
            l.warn("Closing unknown connection");
            return;
        }

        // clean-up all connections
        ScopedLock sl(cli.mutex);

        for (auto ti = cli.tunes.begin(); ti != cli.tunes.end();) {
            auto &t = ti->second;
            if (t->stream) {
                t->stream->abort("Disconnect with server");
                t->stream = nullptr;
            }

            ti = cli.tunes.erase(ti);
        }

        cli.connection = nullptr;
        cli.state = Client::STATE_DISCONNECTED;
        cli.stateUpdate.notify();
    }},
};

void TuneProxyImpl::setMulticasts (const MulticastAddress *addrs0, int numAddrs)
{
    async(bind([=] (void *, vector<MulticastAddress> &addrs) {
        auto &cli = clientImpl();
        cli.connection->sendMessage([&] (Buffer &buf) {
            buf.writeI32(MSG_MULTICASTS);
            buf.writeI32(id);
            buf.writeI32(addrs.size());
            for (auto &m: addrs) {
                buf.writeI32(m.sourceIp);
                buf.writeI16(m.sourcePort);
                buf.writeI32(m.destinationIp);
                buf.writeI16(m.destinationPort);
            }
        });
    }, ::ref(this), vector<MulticastAddress>(addrs0, addrs0 + numAddrs)));
}

void TuneProxyImpl::start ()
{
    async(bind([=] (void *) {
        auto &cli = clientImpl();

        ScopedLock sl(cli.mutex);

        if (cli.state != Client::STATE_CONNECTED) {
            l.error("No connection to gateway yet");
            return;
        }

        // create tune connection first.
        int fd = create_socket(cli.host.c_str(), cli.port);
        if (fd < 0) {
            l.error("Failed to create socket for tune connection");
            return;
        }
        stream = new Connection();
        stream->tune = this;
        ::eventDriver().watchFile(fd, EVENT_WRITE, stream.get());

        // request tune.
        control->sendMessage([&] (Buffer &buf) {
            buf.writeI32(MSG_TUNE);
            buf.writeI32(id);
            buf.writeBytes((const uint8_t*)location.c_str(), location.size());
        });
    }, ::ref(this)));
}


void TuneProxyImpl::dispose ()
{
    auto &cli = clientImpl();

    ScopedLock sl(cli.mutex);

    if (cli.tunes.erase(id) == 0) return;

    async(bind([&] (void*) {

        if (stream) {
            stream->abort("dispose");
            stream = nullptr;
        }

        // request untune.
        control->sendMessage([=] (Buffer &buf) {
            buf.writeI32(MSG_UNTUNE);
            buf.writeI32(id);
        });
    }, ::ref(this)));
}

}

Client &tuner::client ()
{
    return clientImpl();
}
