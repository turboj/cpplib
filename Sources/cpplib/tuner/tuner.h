#ifndef TUNER_H
#define TUNER_H

#include <pthread.h>

#include <functional>

#include "acba.h"

namespace tuner {

const int CAP_ATSC3 = 1<<0;
const int CAP_ATSC1 = 1<<1;

struct MulticastAddress {
    uint32_t sourceIp, destinationIp;
    uint16_t /*unused*/sourcePort, destinationPort;
};

enum DEMOD_TYPE {
    DEMOD_ATSC3,
    DEMOD_8VSB // for atsc1
};

enum CALLBACK_TYPE {
    CALLBACK_END = 0,
    CALLBACK_TUNER_LOCKED,
    CALLBACK_TUNER_UNLOCKED,

    // NOTE: Keep ordering with corresponding values of MSG_TYPE.
    CALLBACK_IP,
    CALLBACK_LMT,
    CALLBACK_ALP,
    CALLBACK_TS,
};

typedef std::function<void(CALLBACK_TYPE type, const byte_t *data, int sz)>
        TuneCallback;

struct TuneParams {
    int frequency;
    DEMOD_TYPE demod;
    TuneCallback callback;
};

struct Tuner {
    bool inUse; // access with TunerPool.mutex

    int caps;
    TuneParams params;
    pthread_mutex_t mutex;

    Tuner (): mutex(PTHREAD_MUTEX_INITIALIZER), inUse(false), caps(0) {}

    virtual bool tune (const TuneParams &params) = 0;
    virtual void stop () = 0;

    virtual void setMulticasts (const MulticastAddress *addrs, int numAddrs) {
        // does nothing
    }

};

struct TunerPool {
    std::vector<Tuner*> tuners;
    pthread_mutex_t mutex;

    TunerPool (): mutex(PTHREAD_MUTEX_INITIALIZER) {}
};

inline TunerPool &pool ()
{
    static TunerPool thePool;
    return thePool;
}

}
#endif
